<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<div class="container text">
	<h2>Контактная информация</h2>
</div>
		<div class="contact contact-frame">
			<div class="container">
				<div class="top">
					<div class="col_left" style="width:55%">
						<h2><div itemscope itemtype="http://schema.org/LocalBusiness">Интернет-магазин "<span itemprop="name">Знатная Дама</span>"</h2>
						
						<br />
						<div class="block">
							<p><strong>Телефоны:</strong></p>
							<p><i class="sprites-phone"></i><span itemprop="telephone roistat-phone">8 (800) 333 34 24</span><br /><span style="color:#ab0534">(звонок бесплатный)</span></p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-пт</span>09:00 — 18:00</p>
							<p><span>сб-вс</span>10:00 — 15:00</p>
						</div>
						<div class="block">
						<a href="<?=SITE_TEMPLATE_PATH?>/images/feedback/ivahnenko.jpg" rel="lightbox"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mini/ivahnenko.jpg" style="width:60px; float:left; margin: 0 10px 0 0; border-radius:90% 90% "></a>
							<p><strong>Менеджер</strong></p>
							<p>Ивахненко Анна</p>
							<div style="clear:both"><br></div>
							<p class="mailto"><i class="sprites-mailto"></i><a href="mailto:anna@z-dama.ru">anna@z-dama.ru</a></p>
							<p><i class="sprites-phone"></i>+7 (958) 100 35 68, доб. 102</p>
						</div>
						<div class="block">
							<a href="<?=SITE_TEMPLATE_PATH?>/images/feedback/nagornih.jpg" rel="lightbox"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mini/nagornih.jpg" style="width:60px; float:left; margin: 0 10px 0 0; border-radius:90% 90% "></a>
							<p><strong>Менеджер</strong></p>
							<p>Елена Нагорных</p>
							<div style="clear:both"><br></div>
							<p class="mailto"><i class="sprites-mailto"></i><a href="mailto:nagornih@z-dama.ru">nagornih@z-dama.ru</a></p>
							<p><i class="sprites-phone"></i>+7 (958) 100 35 68, доб. 103</p>
						</div>
						<div class="block">
							<a href="<?=SITE_TEMPLATE_PATH?>/images/feedback/morozova.jpg" rel="lightbox"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mini/morozova.jpg" style="width:60px; float:left; margin: 0 10px 0 0; border-radius:90% 90% "></a>
							<p><strong>Менеджер</strong></p>
							<p>Елена Морозова</p>
							<div style="clear:both"><br></div>
							<p class="mailto"><i class="sprites-mailto"></i><a href="mailto:morozova_e@z-dama.ru">morozova_e@z-dama.ru</a></p>
							<p><i class="sprites-phone"></i>+7 (958) 100 35 68, доб. 104</p>
						</div>
					</div>
					<div class="col_right" style="width:45%">						
						<div class="map"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=FzSlpm1axEiDoinW3WZdTNgAS7BTIOtL&width=440&height=420&lang=ru_RU&sourceType=constructor&scroll=true"></script></div>
						<!--<div class="map"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ySrTWj0y6g1so-nZO4wKcrJyVZQAsDiP&amp;width=600&amp;height=420&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true "></script>-->
						<div id="mapmain"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="line"></div>
			<div class="container">
				<div class="bottom">
					<div class="col_left rekv">
						<h3>Наши адреса</h3>
						<!--<div class="block">-->
						<p><strong>Фактический адрес:</strong></p>
						<p itemscope itemtype="http://schema.org/PostalAddress">г. <span itemprop="addressLocality">Курск</span></span>, <span itemprop="streetAddress">50 лет Октября 173Б</span>, офис-центр "Примадонна", 2 этаж</p>
						<p><strong>Юридичекий адрес:</strong></p>
						<p itemscope itemtype="http://schema.org/PostalAddress">г. <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Волжский Бульвар, д.8</span></p>
						<p><br/><strong>Почтовый адрес для отправки корреспонденции и возвратов:</strong> <br/>305048, г.Курск, а/я 12, ООО "СержиоМелихофф" </p>
						<p><br/><strong>Ваши жалобы и предложения вы можете отправить на почту:</strong> <br/><a href="mailto:director@z-dama.ru">director@z-dama.ru</a></p>
							<!--<p><strong>ООО «Беларди»</strong></p>
							<p>ИНН/КПП 4632170942/463201001<br />
							ОГРН 1124632015001<br />
							р/сч 40702810519200025267<br />
							в ОАО АКБ «Авангард»<br />
							к/с № 30101810000000000201<br />
							БИК 044525201</p>
						</div>
						<div class="block">

						</div>-->

					</div>
<div class="col_right">
	<!--<h3>Наши договоры</h3>					
<p>Договор ООО "Примадонна": <a href="http://www.z-dama.ru/engine/images/primadonna.doc">primadonna.doc</a></p>
<p>Договор ИП Мелихов С.А.: <a href="http://www.z-dama.ru/engine/images/ip-melihov.doc">ip-melihov.doc</a></p>
<p>Договор ООО "Беларди": <a href="http://www.z-dama.ru/engine/images/belardi.doc">belardi.doc</a></p>
<p>Договор ООО "СержиоМелихофф": <a href="http://www.z-dama.ru/engine/images/znatnaydama.doc">znatnaydama.doc</a></p>	
<br /><br />-->
<h3>Наши реквизиты</h3>					
<p>Реквизиты ООО "СержиоМелихофф": <a href="/rekvisiti/">перейти на страницу</a></p>						
					
</div>					
				</div>
			</div>
		</div>

		<div class="contact contact-bottom container" id="shops">
			<div class="top">
				<div class="text">
					<h3>Магазины (Вы можете получить товар здесь)</h3>
				</div>
<div class="card">
	<a name="bottomtabs"></a>
	<div class="tabs" id="bottom_tabs">
		<label for="tab0" class="tab active">г. Белгород</label>
		<!--<label for="tab1" class="tab">г. Брянск</label>			-->
		<label for="tab2" class="tab">г. Воронеж</label>
		<label for="tab3" class="tab">г. Орел</label>
		<label for="tab4" class="tab">г. Тула</label>
	</div>
	<div class="tabs-content">
				
		<input type="radio" name="tab" class="hider" id="tab0" checked/>
		<div class="tab-content">
			<div class="feedback">
				<div class="center" itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">ул. 50-летия Белгородской области, д. 11, универмаг "МАЯК", 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<p itemprop="telephone"><i class="sprites-phone"></i>+7 (4722) 205-518</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>09:00 — 20:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="mayak" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_full_IMG_5569.JPG" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_thumb_IMG_5569.JPG" alt=""></a><a rel="mayak" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_full_IMG_5572.JPG" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_thumb_IMG_5572.JPG" alt=""></a><a rel="mayak" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_full_IMG_5866.JPG" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/mayak_thumb_IMG_5866.JPG" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=rcLEanxK7VY36BiecbS03XvM1ZAkjfw4&width=460&height=300&id=map1"></script>
							<div id="map1"></div>
						</div>
					</div>
				</div>
			</div>
		</div>		

		<input type="radio" name="tab" class="hider" id="tab1"/>
		<div class="tab-content">
			<div class="feedback">
				<div class="center" itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">

						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">ул. 3-го Интернационала, д.17А, торговый дом "Весна", 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name"></p></span>-->
							<p itemprop="telephone"><i class="sprites-phone"></i>+7 (4832) 320-401</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>10:00 — 20:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="bryansk" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_full_br1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_thumb_br1.jpg" alt=""></a><a rel="bryansk" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_full_br2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_thumb_br2.jpg" alt=""></a><a rel="bryansk" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_full_br3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/bryansk_thumb_br3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=wDJEPLawSYWQIoTnIhVjHZCLO1o48pf4&width=460&height=300&id=map2"></script>
						<div id="map2"></div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<input type="radio" name="tab" class="hider" id="tab2"/>
		<div class="tab-content">
			<div class="feedback">
				<div class="center" itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">пр-т Патриотов, д. 3 А, ТЦ "Юго-Запад", 1 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name">Павлова Сона Александровна</p></span>-->
							<p itemprop="telephone"><i class="sprites-phone"></i>+7 (4732) 617-088</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>09:00 — 21:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="uz" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_full_DSCF6894-(3).jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_thumb_DSCF6894-(3).jpg" alt=""></a><a rel="uz" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_full_DSCF6907 (3).JPG" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_thumb_DSCF6907 (3).JPG" alt=""></a><a rel="uz" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_full_DSCF7207.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/uz_thumb_DSCF7207.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=sT007u7V059xgbj4fuNWpSTlliImZ87x&width=460&height=300&id=map3"></script>
						<div id="map3"></div>
						</div>
						
					</div>
				</div>		
			</div>
			<div class="feedback">				
				<div class="center"  style="background: #e9f0ed;"  itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">Ленинский пр-т, д. 174 п, ТЦ "Максимир", 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name">Филозопова Елена Александровна</p></span>-->
							<p itemprop="telephone"><i class="sprites-phone"></i>+7 (4732) 333-061</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>10:00 — 22:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="maksimir" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_full_1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_thumb_1.jpg" alt=""></a><a rel="maksimir" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_full_2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_thumb_2.jpg" alt=""></a><a rel="maksimir" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_full_3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/maksimir_thumb_3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=xB4dJ9G8eCbt_IkGzf_6IkbKrKKdoKdW&width=460&height=300&id=map4"></script>
						<div id="map4"></div>
						</div>
						
					</div>
				</div>	
			</div>
		</div>

		<input type="radio" name="tab" class="hider" id="tab3"/>
		<div class="tab-content">
			<div class="feedback">
				<div class="center"  itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">ул. Тургенева, 40, ТЦ "Водолей", 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name">Лосикова Олеся Александровна</p></span>-->
							<p><i class="sprites-phone"></i>+7 (4862) 443-156</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p itemprop="telephone"><span>пн-вс</span>10:00 — 21:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="vodoley" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_full_1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_thumb_1.jpg" alt=""></a><a rel="vodoley" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_full_2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_thumb_2.jpg" alt=""></a><a rel="vodoley" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_full_3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/vodoley_thumb_3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=Ik8ShTM9215W7hg6BTV1sHWuPkmigk1L&width=460&height=300&id=map5"></script>
						<div id="map5"></div>
						</div>
						
					</div>
				</div>		
			</div>
			<div class="feedback">				
				<div class="center"  itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">Кромское шоссе, д. 4, МегаГринн, 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name">Чикина Елена Михайловна</p></span>-->
							<p itemprop="telephone"><i class="sprites-phone"></i>+7 (4862) 443-272</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>10:00 — 22:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="kromskoe" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_full_k1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_thumb_k1.jpg" alt=""></a><a rel="kromskoe" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_full_k2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_thumb_k2.jpg" alt=""></a><a rel="kromskoe" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_full_k3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/kromskoe_thumb_k3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=G4ZPJACntHGLnmHXVFWWrdyXLVPjjMB7&width=460&height=300&id=map6"></script>
						<div id="map6"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type="radio" name="tab" class="hider" id="tab4"/>
		<div class="tab-content">
			<div class="feedback">

				<div class="center"  itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">ул. Путейская 5, ТЦ "Сарафан", 1 корпус, 2 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Телефон</strong></p>
							<!--<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name">Гришина Наталья Викторовна</p></span>-->
							<p  itemprop="telephone"><i class="sprites-phone"></i>+7 (4872)524-161</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>10:00 — 21:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="sarafan" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_full_1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_thumb_1.jpg" alt=""></a><a rel="sarafan" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_full_2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_thumb_2.jpg" alt=""></a><a rel="sarafan" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_full_3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/sarafan_thumb_3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=8lqdZPzOQX6xX9nMgD9ldv9uZrF-C8lR&width=460&height=300&id=map7"></script>
						<div id="map7"></div>
						</div>
					</div>
				</div>		
			</div>
			<div class="feedback">
				<div class="center"  itemscope itemtype="http://schema.org/Organization">
					<div class="col_left">
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>Адрес:</strong> <span itemprop="streetAddress">ул. Металлургов, д.62-а, ТЦ "Демидовский", 1 этаж</span></p>
						<br />
						<div class="block">
							<p><strong>Управляющий</strong></p>
							<span itemprop="alumni" itemscope itemtype="http://schema.org/Person"><p itemprop="name"></p></span>
							<p><i class="sprites-phone"></i>+7 (4872)520-350</p>
						</div>
						<div class="block workday">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span>10:00 — 21:00</p>
						</div>
						<div class="pic_list"><div class="pic_list_inner">
							<a rel="demid" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_full_1.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_thumb_1.jpg" alt=""></a><a rel="demid" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_full_2.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_thumb_2.jpg" alt=""></a><a rel="demid" href="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_full_3.jpg" class="fancy"><img src="<?=SITE_TEMPLATE_PATH?>/images/feedback/demid_thumb_3.jpg" alt=""></a>
						</div></div>
					</div>
					<div class="col_right">
						<div class="map"><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=-Fga0Bmvp6QPYi90tB8yZHlPk-K2P2pc&width=460&height=300&id=map8"></script>
						<div id="map8"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</div>	

</div>				



	
						
	
					
			</div>
			
			<div class="top">
				<div class="text">
					<h3>Точки выдачи (доставим в любой регион РФ)</h3>
				</div>			
				<ul>
					<li><a href="/tsentry-vydachi-tovarov/belgorod/">Белгород</a></li>
					<li><a href="/tsentry-vydachi-tovarov/volgograd/">Волгоград</a></li>
					<li><a href="/tsentry-vydachi-tovarov/voronezh/">Воронеж</a></li>
					<li><a href="/tsentry-vydachi-tovarov/ekaterinburg/">Екатеринбург</a></li>
					<li><a href="/tsentry-vydachi-tovarov/irkutsk/">Иркутск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/kazan/">Казань</a></li>
					<li><a href="/tsentry-vydachi-tovarov/kaliningrad/">Калининград</a></li>
					<li><a href="/tsentry-vydachi-tovarov/krasnodar/">Краснодар</a></li>
					<li><a href="/tsentry-vydachi-tovarov/krasnoyarsk/">Красноярск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/murmansk/">Мурманск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/nizhniy-novgorod/">Нижний Новгород</a></li>
					<li><a href="/tsentry-vydachi-tovarov/novosibirsk/">Новосибирск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/omsk/">Омск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/orel/">Орел</a></li>
					<li><a href="/tsentry-vydachi-tovarov/orenburg/">Оренбург</a></li>
					<li><a href="/tsentry-vydachi-tovarov/perm/">Пермь</a></li>
					<li><a href="/tsentry-vydachi-tovarov/samara/">Самара</a></li>
					<li><a href="/tsentry-vydachi-tovarov/saint-petersburg/">Санкт-Петербург</a></li>
					<li><a href="/tsentry-vydachi-tovarov/tomsk/">Томск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/tula/">Тула</a></li>
					<li><a href="/tsentry-vydachi-tovarov/tumen/">Тюмень</a></li>
					<li><a href="/tsentry-vydachi-tovarov/ufa/">Уфа</a></li>
					<li><a href="/tsentry-vydachi-tovarov/khabarovsk/">Хабаровск</a></li>
					<li><a href="/tsentry-vydachi-tovarov/chelyabinsk/">Челябинск</a></li>
					</ul>
			</div>				
		</div>	

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>