<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Центры выдачи товаров");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"info_right",
					Array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "info2level",
						"DELAY" => "N",
						"MAX_LEVEL" => "2",
						"MENU_CACHE_GET_VARS" => array(""),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_THEME" => "site",
						"ROOT_MENU_TYPE" => "oprosright",
						"USE_EXT" => "N"
					)
				);?>
				
				 
				 	
				 				
				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h1>Центры выдачи заказов.</h1>
<h3>Заказ в регионах - это просто!</h3>
<p>Мы считаем, что любой наш покупатель, из любого города России, должен получить максимально комфортные условия для покупок через наш интернет-магазин. &nbsp;</p>
<p>Мы постоянно открываем новые центры выдачи в других регионах РФ.</p>
<p>Во многих городах есть наши фирменные магазины.</p>
<p>Также, покупатели могут воспользоваться доставкой почтой, экпресс-доставкой или услугами транспортных компаний.</p>
<p>Уже сейчас во многих городах России наши покупатели могут:</p>
<ul>
<li>самостоятельно забрать товар в центре выдачи или розничном магазине</li>
<li>позвонить по местному номеру телефона и задать все необходимые вопросы</li>
<li>заказать доставку на дом</li>
</ul>
<h3>Узнайте какие возможности для покупок есть в Вашем городе</h3>
<p>Телефоны, адреса пунктов выдачи (магазинов), отзывы покупателей из Вашего города и многое другое:</p>
<div class="contact contact-bottom">
<div class="top">
<ul>
<li><a href="/tsentry-vydachi-tovarov/arkhangelsk/">Архангельск</a></li>
<li><a href="/tsentry-vydachi-tovarov/belgorod/">Белгород</a></li>
<li><a href="/tsentry-vydachi-tovarov/barnaul/">Барнаул</a></li>
<li><a href="/tsentry-vydachi-tovarov/volgograd/">Волгоград</a></li>
<li><a href="/tsentry-vydachi-tovarov/voronezh/">Воронеж</a></li>
<li><a href="/tsentry-vydachi-tovarov/vladivostok/">Владивосток</a></li>
<li><a href="/tsentry-vydachi-tovarov/vologda/">Вологда</a></li>
<li><a href="/tsentry-vydachi-tovarov/ekaterinburg/">Екатеринбург</a></li>
<li><a href="/tsentry-vydachi-tovarov/irkutsk/">Иркутск</a></li>
<li><a href="/tsentry-vydachi-tovarov/izevsk/">Ижевск</a></li>
<li><a href="/tsentry-vydachi-tovarov/kazan/">Казань</a></li>
<li><a href="/tsentry-vydachi-tovarov/kaliningrad/">Калининград</a></li>
<li><a href="/tsentry-vydachi-tovarov/krasnodar/">Краснодар</a></li>
<li><a href="/tsentry-vydachi-tovarov/krasnoyarsk/">Красноярск</a></li>
<li><a href="/tsentry-vydachi-tovarov/kemerovo/">Кемерово</a></li>
<li><a href="/tsentry-vydachi-tovarov/lipetsk/">Липецк</a></li>
<li><a href="/tsentry-vydachi-tovarov/murmansk/">Мурманск</a></li>
<li><a href="/tsentry-vydachi-tovarov/nizhny-novgorod/">Нижний Новгород</a></li>
<li><a href="/tsentry-vydachi-tovarov/novosibirsk/">Новосибирск</a></li>
<li><a href="/tsentry-vydachi-tovarov/nizhnevartovsk/">Нижневартовск</a></li>
<li><a href="/tsentry-vydachi-tovarov/omsk/">Омск</a></li>
<li><a href="/tsentry-vydachi-tovarov/orel/">Орел</a></li>
<li><a href="/tsentry-vydachi-tovarov/orenburg/">Оренбург</a></li>
<li><a href="/tsentry-vydachi-tovarov/perm/">Пермь</a></li>
<li><a href="/tsentry-vydachi-tovarov/rostov/">Ростов</a></li>
<li><a href="/tsentry-vydachi-tovarov/ryazan/">Рязань</a></li>
<li><a href="/tsentry-vydachi-tovarov/samara/">Самара</a></li>
<li><a href="/tsentry-vydachi-tovarov/saratov/">Саратов</a></li>
<li><a href="/tsentry-vydachi-tovarov/sochi/">Сочи</a></li>
<li><a href="/tsentry-vydachi-tovarov/smolensk/">Смоленск</a></li>
<li><a href="/tsentry-vydachi-tovarov/saint-petersburg/">Санкт-Петербург</a></li>
<li><a href="/tsentry-vydachi-tovarov/tomsk/">Томск</a></li>
<li><a href="/tsentry-vydachi-tovarov/tula/">Тула</a></li>
<li><a href="/tsentry-vydachi-tovarov/tumen/">Тюмень</a></li>
<li><a href="/tsentry-vydachi-tovarov/tver/">Тверь</a></li>
<li><a href="/tsentry-vydachi-tovarov/tollyati/">Тольяти</a></li>
<li><a href="/tsentry-vydachi-tovarov/ufa/">Уфа</a></li>
<li><a href="/tsentry-vydachi-tovarov/uliyanovsk/">Ульяновск</a></li>
<li><a href="/tsentry-vydachi-tovarov/ulan-ude/">Улан-Удэ</a></li>
<li><a href="/tsentry-vydachi-tovarov/khabarovsk/">Хабаровск</a></li>
<li><a href="/tsentry-vydachi-tovarov/chelyabinsk/">Челябинск</a></li>
<li><a href="/tsentry-vydachi-tovarov/cherepovets/">Череповец</a></li>
<li><a href="/tsentry-vydachi-tovarov/cheboksary/">Чебоксары</a></li>
<li><a href="/tsentry-vydachi-tovarov/yaroslavl/">Ярославль</a></li>
</ul>
</div>
</div>
<h3>Не нашли свой город? Не проблема!</h3>
<p>Доставим Вашу покупку в любой регион РФ, Белоруссию и Казахстан.</p>
<p>Даже если в Вашем городе нет нашего пункта выдачи или магазина!&nbsp;<a style="line-height: 1.5;" href="http://www.z-dama.ru/informaciya-dlya-pokupateley/">Подробнее о доставке</a></p>
<p><br>Подробную информацию о доставке вы можете получить по телефону: &nbsp;<strong style="line-height: 1.5;">8 (800) 333-34-24.</strong>&nbsp;</p>
<p><strong>Звонок бесплатный по всей территории РФ!</strong></p>
</div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>