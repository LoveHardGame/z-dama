<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Женская одежда больших размеров в Екатеринбурге");
$APPLICATION->SetTitle("Екатеринбург");
?>

<style>
.catalog .rubric li.main_category > a {
    background: #CFD9D5;
    color: #901d16;
    font-weight: bold;
}
</style>

<div class="container catalog">
			
    <div class="col_left" id="upperborder">

		
		<aside id="leftaside" style="position: static; margin-left: 0px; top: 0px;">
					
            <ul class="rubric a-big-block a-odd">
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list",
					"left-1lvl",
					Array(
						"ADD_SECTIONS_CHAIN" => "Y",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"COMPONENT_TEMPLATE" => "left-1lvl",
						"COUNT_ELEMENTS" => "Y",
						"HIDE_SECTION_NAME" => "N",
						"IBLOCK_ID" => "2",
						"IBLOCK_TYPE" => "catalog",
						"SECTION_CODE" => "",
						"SECTION_FIELDS" => array(0=>"",1=>"",),
						"SECTION_ID" => $_REQUEST["SECTION_ID"],
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
						"SHOW_PARENT_NAME" => "Y",
						"TOP_DEPTH" => "1",
						"VIEW_MODE" => "LIST"
					)
				);?>
            </ul>
											
        </aside>
		<div class="no_mobile">
			<div id="ok_group_widget"></div>
			<script>
			!function (d, id, did, st) {
			  var js = d.createElement("script");
			  js.src = "https://connect.ok.ru/connect.js";
			  js.onload = js.onreadystatechange = function () {
			  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
				if (!this.executed) {
				  this.executed = true;
				  setTimeout(function () {
					OK.CONNECT.insertGroupWidget(id,did,st);
				  }, 0);
				}
			  }}
			  d.documentElement.appendChild(js);
			}(document,"ok_group_widget","51925631893692","{width:290,height:135}");
			</script>
			<script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
			<!-- VK Widget -->
			<div id="vk_groups" style="margin-top:12px;"></div>
			<script type="text/javascript">
			VK.Widgets.Group("vk_groups", {mode: 1, width: "290", height: "135", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 49569254);
			</script>				
 		</div>		
    </div>
    <div class="col_right">
		<div class="no_mobile" style="height:110px; overflow:hidden; margin-bottom:10px;">

			<div id="owlbanner" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
						
				<div class="owl-item" style='text-align: center;'>
					<a href="/company/news/151-news.html" style="text-decoration:none;" rel="nofollow" title="Перейти к просмотру" target="_blank">
						<img style='width: 100%; height: 113px;' src="<?=SITE_TEMPLATE_PATH?>/images/banners/banner_1485261802.png" alt="" />
					</a>
				</div>		
				<div class="owl-item" style='text-align: center;'>
					<a href="/informaciya-dlya-pokupateley/sovmestnye-pokupki/" style="text-decoration:none;" rel="nofollow" title="Перейти к просмотру" target="_blank">
						<img style='width: 100%; height: 113px;' src="<?=SITE_TEMPLATE_PATH?>/images/banners/banner_1485270732.png" alt="" />
					</a>
				</div>		
				<div class="owl-item" style='text-align: center;'>
					<a href="/company/news/157-news.html" style="text-decoration:none;" rel="nofollow" title="Перейти к просмотру" target="_blank">
						<img style='width: 100%; height: 113px;' src="<?=SITE_TEMPLATE_PATH?>/images/banners/banner_1486464261.png" alt="" />
					</a>
				</div>						
										
			</div>		
					
		</div>	
				
		<article class="text" id="content">
					<div class="contact contact-bottom">
			<div class="bottom">
				<div class="text">
					<h2>Одежда для полных женщин в Екатеринбург</h2>
					<p>Здесь вы найдете всю необходимую информацию, которая облегчит процесс покупки женской одежды в Вашем городе.</p>
				</div>
				<div class="center">
					<div class="col_left">
						<h3>Пункт выдачи товаров</h3>
						<p><strong>г. Екатеринбург</strong>, ул. Щорса, д. 32</p>
						<div class="block">								
							<p><i class="sprites-phone"></i> 8 (800) 333-34-24</p>							
						</div>
						<div class="block">
							<p><strong>Режим работы</strong></p>
							<p><span>пн-вс</span> 10:00 — 18:00</p>
						</div>						
					</div>
					<div class="col_right"  style="height:300px;">
						<div class="map"><script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=_DrOfm4YDb6tytyRz_0FLhH-ZiOOs2nI&width=460&height=300&id=map3"></script></script></script>
							<div id="map3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>		
			<div id="site-content">
				<p><strong style="line-height: 1.5;">Варианты доставки в Екатеринбург:</strong></p>
				<ul>
					<li>Самовывоз через розничный магазин;&nbsp;</li>
					<li>Почта России;</li>
					<li>Экспресс-доставка;</li>
					<li>Транспортные компании.</li>
				</ul>
				<p><strong style="line-height: 1.5;">Варианты оплаты</strong></p>
				<ul>
					<li>Visa / MasterСard (моментально и без комиссии);</li>
					<li>Безналичные расчет (для юридических и физических лиц).</li>
				</ul>
			</div>
		</article>
				
    </div>
    <div class="clear"></div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>