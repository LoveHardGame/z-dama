<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>
<div class="container register">
			<div class="text">
				<h1>Регистрация</h1>
			</div>
			<div class="relative">
				<aside id="aside" style="position: static; margin-left: 0px;">
					<div class="form-info form-info-error register_error_messages_block"></div>
				</aside>
				<div id="content" style="margin-left: 0px;">
					
					<div id="site-content">
						<?$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	"site_register", 
	array(
		"AUTH" => "Y",
		"USE_CAPTCHA" => "N",
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "SECOND_NAME",
			3 => "LAST_NAME",
			4 => "PERSONAL_PHONE",
		),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "SECOND_NAME",
			3 => "LAST_NAME",
			4 => "PERSONAL_PHONE",
		),
		"SUCCESS_PAGE" => "/",
		"USER_PROPERTY" => array(
		),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
		"COMPONENT_TEMPLATE" => "site_register"
	),
	false
);?>
						<script language="javascript" type="text/javascript">
<!--
	function ClearRegistrationErrors() {
		$('#registration .has_errors').removeClass("has_errors");
	};
	
	function HideErrors() {
		$('.register_error_messages_block').css('display', 'none');
	}
	
	function ShowRegistrationErrors(errors_data) {
		ClearRegistrationErrors();
		
		for (var i = 0; i < errors_data["wrong_fields"].length; i++) {
			$('[name="'+errors_data["wrong_fields"][i]+'"]').addClass('has_errors');
		};
		
		$('.register_error_messages_block').css('display', 'block');
		$('.register_error_messages_block').html(errors_data["text"]);
	};

/*	$('#registration').submit(function() {
		var params = {};
		$.each($('#registration').serializeArray(), function(index,value) {
			params[value.name] = value.value;
		});

		params['skin'] = site_skin;

		ShowLoading('');

	  return false;
	});*/


//-->
</script></div>
				</div>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>