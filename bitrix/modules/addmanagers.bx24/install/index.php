<?php

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class addmanagers_bx24 extends CModule 
{
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    function addmanagers_bx24()
    {
        $arModuleVersion = [];
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));

        // подключаем версию модуля
        include($path."/version.php");

        $this->MODULE_ID = 'addmanagers.bx24';
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->PARTNER_NAME = 'Miha';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        // $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        // название модуля
        $this->MODULE_NAME = "addmanagers.bx24";
        // описание модуля
        $this->MODULE_DESCRIPTION = "Менеджеры";
    }

    // метод установки модуля
    function DoInstall()
    {
        global $APPLICATION;

        RegisterModule($this->MODULE_ID);
    }

    // метод удаления модуля
    function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);
    }

}