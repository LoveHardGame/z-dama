<?php

IncludeModuleLangFile(_FILE_);

// добавим пункт меню в админ bitrix 
$aMenu = [
    "parent_menu" => "global_menu_settings",
    "sort" => 100,
    "url" => "addmanagers.php?lang=".LANGUAGE_ID,
    "text" => "Синхронизация пользователей битрикс24",
    "items_id" => "menu_addmanagersbx24",
    "module_id" => "addmanagers.bx24",
    "icon" => "user_menu_icon"
];

return $aMenu;