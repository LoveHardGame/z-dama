<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("VR_CALLBACK_NAME"),
	"DESCRIPTION" => GetMessage("VR_CALLBACK_DESCRIPTION"),
	"ICON" => "/images/eaddform.gif",
	"PATH" => array(
		"ID" => "vr",
		"NAME" => GetMessage("VR_DIRECTORY_NAME"),
	),
);
?>