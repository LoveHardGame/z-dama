<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$isWindows = (PHP_OS == "WINNT")?true:false;

$searchBitrixStr = ($isWindows)?"\\bitrix\\components\\":"/bitrix/components/";

$arResult["COMPONENT_DIR"] = dirname(__FILE__);
$arResult["COMPONENT_AJAX_DIR"] = $arResult["COMPONENT_DIR"]."/ajax/";
$arResult["COMPONENT_AJAX_DIR"] = substr($arResult["COMPONENT_AJAX_DIR"], strpos($arResult["COMPONENT_AJAX_DIR"],$searchBitrixStr));
$arResult["CALLBACK_AJAX_PROCESSOR"] = $arResult["COMPONENT_AJAX_DIR"]."process.php";

if ($isWindows)
{
	$arResult["COMPONENT_DIR"] = str_replace("\\","\\\\",$arResult["COMPONENT_DIR"]);	
	$arResult["COMPONENT_DIR"] = str_replace("/","\\\\",$arResult["COMPONENT_DIR"]);
	
	$arResult["COMPONENT_AJAX_DIR"] = str_replace("\\","\\\\",$arResult["COMPONENT_AJAX_DIR"]);
	$arResult["COMPONENT_AJAX_DIR"] = str_replace("/","\\\\",$arResult["COMPONENT_AJAX_DIR"]);
	
	$arResult["CALLBACK_AJAX_PROCESSOR"] = str_replace("\\","\\\\",$arResult["CALLBACK_AJAX_PROCESSOR"]);
	$arResult["CALLBACK_AJAX_PROCESSOR"] = str_replace("/","\\\\",$arResult["CALLBACK_AJAX_PROCESSOR"]);
}


$arParams["PROPERTY_FIO"] = (isset($arParams["PROPERTY_FIO"]))?$arParams["PROPERTY_FIO"]:"fio";
$arParams["PROPERTY_FORM_NAME"] = (isset($arParams["PROPERTY_FORM_NAME"]))?$arParams["PROPERTY_FORM_NAME"]:"form";
$arParams["PROPERTY_SITE"] = (isset($arParams["PROPERTY_SITE"]))?$arParams["PROPERTY_SITE"]:"site";
$arParams["INCLUDE_JQUERY"] = (isset($arParams["INCLUDE_JQUERY"]))?$arParams["INCLUDE_JQUERY"]:"Y";

$this->IncludeComponentTemplate();
?>