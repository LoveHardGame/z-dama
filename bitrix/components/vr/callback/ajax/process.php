<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->RestartBuffer();

$IBLOCK_ID = (isset($_REQUEST["IBLOCK_ID"]))?intval($_REQUEST["IBLOCK_ID"]):-1;
if ($IBLOCK_ID < 0) {echo "-1";	exit();}

$phone = (isset($_REQUEST["phone"]))?strval($_REQUEST["phone"]):"";
$fio = (isset($_REQUEST["fio"]))?strval($_REQUEST["fio"]):"";
$form = (isset($_REQUEST["form"]))?strval($_REQUEST["form"]):"";
$email_to = (isset($_REQUEST["email_to"]))?strval($_REQUEST["email_to"]):"";
$page = (isset($_REQUEST["page"]))?strval($_REQUEST["page"]):"";
$MAIL_TEMPLATE = (isset($_REQUEST["MAIL_TEMPLATE"]))?strval($_REQUEST["MAIL_TEMPLATE"]):"";
$PROPERTY_FIO = (isset($_REQUEST["PROPERTY_FIO"]))?strval($_REQUEST["PROPERTY_FIO"]):"";
$PROPERTY_FORM_NAME = (isset($_REQUEST["PROPERTY_FORM_NAME"]))?strval($_REQUEST["PROPERTY_FORM_NAME"]):"";
$PROPERTY_PAGE = (isset($_REQUEST["PROPERTY_PAGE"]))?strval($_REQUEST["PROPERTY_PAGE"]):"";
$email = (isset($_REQUEST["email"]))?strval($_REQUEST["email"]):"";
$kvadrat = (isset($_REQUEST["kvadrat"]))?strval($_REQUEST["kvadrat"]):"";


$PROP = array();
$PROP["phone"] = $phone;
$PROP[$PROPERTY_FIO] = $fio;
$PROP[$PROPERTY_FORM_NAME] = $form;
$PROP[$PROPERTY_PAGE] = $page ;
$PROP["email"] = $email ;
$PROP["kvadrat"] = $kvadrat ;
$error = false;
$emailError = false;
$iblockError = false;

try 
{
	$arFields = array(
	"EMAIL_TO" => $email_to,
	"AUTHOR" => $fio,
	"TEXT" => GetMessage("VR_CALLBACK_VAM_PRISLALI_NOMER_T").$PROP["phone"].' c '.GetMessage("VR_CALLBACK_FORMY").$PROP["form"].' c '.GetMessage("VR_CALLBACK_SAYTA").$PROP["site"],
	);

	//CEvent::Send("FEEDBACK_FORM", SITE_ID, $arFields);
	//if ($email_to!='')
	//{
		$mailNumber=CEvent::Send($MAIL_TEMPLATE, SITE_ID, $arFields);
		if ($mailNumber < 0) {$emailError = true;}
		//echo $mailNumber.'<br />';
		//echo $email;
	//}
} 
catch (Exception $e){}

if (CModule::IncludeModule("iblock"))
{
	$el = new CIBlockElement;
	
	$arValuesArray = Array(
	  "IBLOCK_SECTION_ID" => false,
	  "IBLOCK_ID"      => $IBLOCK_ID,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => $PROP["phone"],
	  "ACTIVE"         => "Y",           
	);

	$arValuesArray["DATE_ACTIVE_FROM"] = ConvertTimeStamp(time()+CTimeZone::GetOffset(), "FULL");
	
	$ELEMENT_ID = $el->Add($arValuesArray);	
	if (!$ELEMENT_ID) {$iblockError = true;}
}

$error = $emailError && $iblockError;
if ($error)
{
	$resultErrorMsg = ($emailError)?'10':'20';
	echo $resultErrorMsg;
}
echo "1";
die();?>