<?php
/*
=====================================================
 ����: index.php
-----------------------------------------------------
 ����������: ������� ��������
=====================================================
*/



@session_start ();
@ob_start ();
@ob_implicit_flush ( 0 );

//@error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
@ini_set ( 'display_errors', false );
@ini_set ( 'html_errors', false );
//@ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );

header('Content-Type: text/html; charset=cp-1251');

define ( 'DELSITO', true );

$member_id = FALSE;
$is_logged = FALSE;

define ( 'ROOT_DIR', dirname ( __FILE__ ) );
define ( 'ENGINE_DIR', ROOT_DIR . '/engine' );

require_once ROOT_DIR . '/engine/init.php';

if($_SERVER['REQUEST_URI']=="/")
{
header('HTTP/1.1 200 OK');
$time = time() - 60;
header('Last-Modified: '.gmdate('D, d M Y H:i:s', $time).' GMT');

ClearFilter();
$OutFilter['filter_size'] = 'small';

}

if (isset($_REQUEST['utm_source'])) {
		if(@$db->safesql(strip_tags($_REQUEST['utm_source'])) == "tradetracker") $_SESSION['CPA'] = 1;
		if(@$db->safesql(strip_tags($_REQUEST['utm_source'])) == "tt-retargeting") $_SESSION['CPA'] = 1;
		if(@$db->safesql(strip_tags($_REQUEST['utm_source'])) == "mixmarket") $_SESSION['CPA'] = 2;
}


if (clean_url ( $_SERVER['HTTP_HOST'] ) != clean_url ( $config['http_home_url'] )) {
	
	$replace_url = array ();
	$replace_url[0] = clean_url ( $config['http_home_url'] );
	$replace_url[1] = clean_url ( $_SERVER['HTTP_HOST'] );

} else
	$replace_url = false;

$tpl->load_template ( 'main.tpl' );

$tpl->set ( '{calendar}', $tpl->result['calendar'] );
$tpl->set ( '{archives}', $tpl->result['archive'] );
$tpl->set ( '{tags}', $tpl->result['tags_cloud'] );
$tpl->set ( '{vote}', $tpl->result['vote'] );
$tpl->set ( '{topnews}', $tpl->result['topnews'] );
$tpl->set ( '{login}', $tpl->result['login_panel'] );
$tpl->set ( '{info}',  $tpl->result['info'] );
$tpl->set ( '{speedbar}', $tpl->result['speedbar'] );

$_SESSION['MetricaName'] = get_MertricaName($catalog_id);

//$tpl->set ( '{SSO_TOKEN}', $_SESSION['sso_token'] );

$DiscountTypeChosen = $_SESSION['discount_type'] ? ($_SESSION['discount_type'] == 1000 ? true : true) : false;
$tpl->set('{user_discount_type}', $DiscountTypeChosen ? 'off' : 'on');
$tpl->set('{discount_type_choose_class}', $DiscountTypeChosen ? 'on' : 'off');
$tpl->set('{discount_type_description}', GetDiscountTypeName($_SESSION['discount_type']));


if (count ( $banners ) and $config['allow_banner']) {
	
	foreach ( $banners as $name => $value ) {
		$tpl->copy_template = str_replace ( "{banner_" . $name . "}", $value, $tpl->copy_template );
	}

}

$tpl->set_block ( "'{banner_(.*?)}'si", "" );

if (count ( $informers ) and $config['rss_informer']) {
	foreach ( $informers as $name => $value ) {
		$tpl->copy_template = str_replace ( "{inform_" . $name . "}", $value, $tpl->copy_template );
	}
}

if ($allow_active_news AND $config['allow_change_sort'] AND $do != "userinfo") {
	
	$tpl->set ( '[sort]', "" );
	$tpl->set ( '{sort}', news_sort ( $do ) );
	$tpl->set ( '[/sort]', "" );

} else {
	
	$tpl->set_block ( "'\\[sort\\](.*?)\\[/sort\\]'si", "" );

}

if ($site_module == "showfull" ) {

	if (is_array($cat_list) AND count($cat_list) > 1 ) $category_id = implode(",", $cat_list);

}

if (stripos ( $tpl->copy_template, "[category=" ) !== false) {
	$tpl->copy_template = preg_replace ( "#\\[category=(.+?)\\](.*?)\\[/category\\]#ies", "check_category('\\1', '\\2', '{$category_id}')", $tpl->copy_template );
}

if (stripos ( $tpl->copy_template, "[not-category=" ) !== false) {
	$tpl->copy_template = preg_replace ( "#\\[not-category=(.+?)\\](.*?)\\[/not-category\\]#ies", "check_category('\\1', '\\2', '{$category_id}', false)", $tpl->copy_template );
}

if (stripos ( $tpl->copy_template, "{custom" ) !== false) {
	$tpl->copy_template = preg_replace ( "#\\{custom category=['\"](.+?)['\"] template=['\"](.+?)['\"] aviable=['\"](.+?)['\"] from=['\"](.+?)['\"] limit=['\"](.+?)['\"] cache=['\"](.+?)['\"]\\ type=['\"](.+?)['\"]\\}#ies", "custom_print('\\1', '\\2', '\\3', '\\4', '\\5', '\\6', '\\7', '{$site_module}')", $tpl->copy_template );
}

if ( strpos( $tpl->copy_template, "{navi" ) !== false ) {
    $tpl->copy_template = preg_replace( "#\\{navi id=['\"](.+?)['\"] template=['\"](.+?)['\"] cache=['\"](.+?)['\"]\\}#ies", "getnavi('\\1', '\\2', '\\3')", $tpl->copy_template );
}

$config['http_home_url'] = explode ( "index.php", strtolower ( $_SERVER['PHP_SELF'] ) );
$config['http_home_url'] = reset ( $config['http_home_url'] );

if (! $user_group[$member_id['user_group']]['allow_admin']) $config['admin_path'] = "";



if($do == 'showcart'){
$ajax .= <<<HTML
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5KH8HZW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--noindex-->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = _tmr || [];
_tmr.push({id: "2030090", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=2030090;js=na" style="border:0;" height="1" width="1" alt="�������@Mail.ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->
<div id="loading-layer" style="display:none"><div id="loading-layer-text">{$lang['ajax_info']}</div></div>
<script language="javascript" type="text/javascript">
<!--
var site_root       = '{$config['http_home_url']}';
var site_admin      = '{$config['admin_path']}';
var user_login_hash = '{$user_login_hash}';
var user_group      = {$member_id['user_group']};
var site_skin       = '{$config['skin']}';
var site_wysiwyg    = '{$config['allow_comments_wysiwyg']}';
var quick_wysiwyg  = '{$config['allow_quick_wysiwyg']}';
var site_act_lang   = ["�����������", "���", "{$lang['p_enter']}", "{$lang['p_cancel']}", "{$lang['p_save']}"];
var menu_short     = '{$lang['menu_short']}';
var menu_full      = '{$lang['menu_full']}';
var menu_profile   = '{$lang['menu_profile']}';
var menu_send      = '{$lang['menu_send']}';
var menu_uedit     = '{$lang['menu_uedit']}';
var site_info       = '{$lang['p_info']}';
var site_confirm    = '{$lang['p_confirm']}';
var site_prompt     = '{$lang['p_prompt']}';
var site_req_field  = '{$lang['comm_req_f']}';
var site_del_agree  = '{$lang['news_delcom']}';
var site_complaint  = '{$lang['add_to_complaint']}';
var site_big_text   = '{$lang['big_text']}';
var site_orfo_title = '{$lang['orfo_title']}';
var site_p_send     = '{$lang['p_send']}';
var site_p_send_ok  = '{$lang['p_send_ok']}';
var site_save_ok    = '{$lang['n_save_ok']}';
var site_del_news   = '{$lang['news_delnews']}';
var site_anti_cash  = '{$config["anti_cash"]}';\n
HTML;
}else{
$ajax .= <<<HTML
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5KH8HZW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--noindex-->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = _tmr || [];
_tmr.push({id: "2030090", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=2030090;js=na" style="border:0;" height="1" width="1" alt="�������@Mail.ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->
<div id="loading-layer" style="display:none"><div id="loading-layer-text">{$lang['ajax_info']}</div></div>
<script language="javascript" type="text/javascript">
<!--
var site_root       = '{$config['http_home_url']}';
var site_admin      = '{$config['admin_path']}';
var user_login_hash = '{$user_login_hash}';
var user_group      = {$member_id['user_group']};
var site_skin       = '{$config['skin']}';
var site_wysiwyg    = '{$config['allow_comments_wysiwyg']}';
var quick_wysiwyg  = '{$config['allow_quick_wysiwyg']}';
var site_act_lang   = ["{$lang['p_yes']}", "{$lang['p_no']}", "{$lang['p_enter']}", "{$lang['p_cancel']}", "{$lang['p_save']}"];
var menu_short     = '{$lang['menu_short']}';
var menu_full      = '{$lang['menu_full']}';
var menu_profile   = '{$lang['menu_profile']}';
var menu_send      = '{$lang['menu_send']}';
var menu_uedit     = '{$lang['menu_uedit']}';
var site_info       = '{$lang['p_info']}';
var site_confirm    = '{$lang['p_confirm']}';
var site_prompt     = '{$lang['p_prompt']}';
var site_req_field  = '{$lang['comm_req_f']}';
var site_del_agree  = '{$lang['news_delcom']}';
var site_complaint  = '{$lang['add_to_complaint']}';
var site_big_text   = '{$lang['big_text']}';
var site_orfo_title = '{$lang['orfo_title']}';
var site_p_send     = '{$lang['p_send']}';
var site_p_send_ok  = '{$lang['p_send_ok']}';
var site_save_ok    = '{$lang['n_save_ok']}';
var site_del_news   = '{$lang['news_delnews']}';
var site_anti_cash  = '{$config["anti_cash"]}';\n
HTML;
}

if ($user_group[$member_id['user_group']]['allow_all_edit']) {
	
	$ajax .= <<<HTML
var site_notice     = '{$lang['btn_notice']}';
var site_p_text     = '{$lang['p_text']}';
var site_del_msg    = '{$lang['p_message']}';
var allow_site_delete_news   = true;\n
HTML;

} else {
	
	$ajax .= <<<HTML
var allow_site_delete_news   = false;\n
HTML;

}



$ajax .= <<<HTML
//-->
</script>
<!--/noindex-->
HTML;

//if ($allow_comments_ajax AND ($config['allow_comments_wysiwyg'] == "yes" OR $config['allow_quick_wysiwyg'])) $js_array[] ="engine/editor/jscripts/tiny_mce/jquery.tinymce.js";

$tpl->set ( '{AJAX}', $ajax."\n".build_js($js_array, $config)."<noscript><span class='js_error'>� ����� �������� ��������� ��������� <strong>javascript</strong>. ��� ���������� ������ � ����������� ����� ���������� �������� ���, � ���������� ������ �����!</span></noscript>" );


$tpl->set ( '{headers}', $metatags );

$tpl->set ( '{content}', "<div id='site-content'>" . $tpl->result['content'] . "</div>" );

//������������� �������� �� ������� ��������
$ct_array=GetCategoryAndChildren(15);//�������� ���������� ��� ���� ���������� (� �������� � ��������)
foreach($ct_array as $c){
    $cat_id[]=$c['id'];//������ ������ � ����������������
}
//������� ������� 1 ���������, 2 ������ �������, 3 �������
$tpl->set('{super_slide}', "<div class=\"container hdr a-big-block\"><h2>�������</h2><div  id='slide_new' style='width: 935px;margin-left: 40px;'>".GetSlider($cat_id, 1, 20)."</div></div>

");//<div class=\"container hdr a-big-block\"><h2>������ ������</h2><div  id='slide_new' style='width: 935px;margin-left: 40px;'>".GetSlider($cat_id, 10, 20)."</div></div>
$bnrs=GetBanners_new("main");
$banners_content="";
$counter=count($bnrs)+1;
foreach ($bnrs as $b){
	$counter=$counter-1;
	$banners_content=$banners_content."<a href=\"".$b['link']."\" id=\"id".$counter."\" class=\"main_banner_sliding\"><img src=\"templates/main/images/banners/".$b['image']."\" alt=\"\" title=\"������� � ���������\" ></a>";
}
$tpl->set('{banners}', $banners_content);
$tpl->compile ( 'main' );
$tpl->result['main'] = str_ireplace( '{THEME}', $config['http_home_url'] . 'templates/' . $config['skin'], $tpl->result['main'] );
if ($replace_url) $tpl->result['main'] = str_replace ( $replace_url[0]."/", $replace_url[1]."/", $tpl->result['main'] );
$tpl->result['main'] = str_replace ( '<img src="http://'.$_SERVER['HTTP_HOST'].'/', '<img src="/', $tpl->result['main'] );

echo $tpl->result['main'];

$tpl->global_clear ();
$db->close ();

GzipOut ();

?>