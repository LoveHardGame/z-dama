<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?>
    <!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
        <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
        <script language="javascript" type="text/javascript">
        <!--
        var site_root       = '/';
        var site_admin      = '';
        var user_login_hash = '';
        var user_group      = 5;
        var site_skin       = 'productfull';
        var site_wysiwyg    = 'no';
        var quick_wysiwyg  = '0';
        var site_act_lang   = ["Äà", "Íåò", "Ââîä", "Îòìåíà", "Ñîõðàíèòü"];
        var menu_short     = 'Áûñòðîå ðåäàêòèðîâàíèå';
        var menu_full      = 'Ïîëíîå ðåäàêòèðîâàíèå';
        var menu_profile   = 'Ïðîñìîòð ïðîôèëÿ';
        var menu_send      = 'Îòïðàâèòü ñîîáùåíèå';
        var menu_uedit     = 'Àäìèíöåíòð';
        var site_info       = 'Информация';
        var site_confirm    = 'Ïîäòâåðæäåíèå';
        var site_prompt     = 'Ââîä èíôîðìàöèè';
        var site_req_field  = 'Çàïîëíèòå âñå íåîáõîäèìûå ïîëÿ';
        var site_del_agree  = 'Âû äåéñòâèòåëüíî õîòèòå óäàëèòü? Äàííîå äåéñòâèå íåâîçìîæíî áóäåò îòìåíèòü';
        var site_complaint  = 'Óêàæèòå òåêñò âàøåé æàëîáû äëÿ àäìèíèñòðàöèè:';
        var site_big_text   = 'Âûäåëåí ñëèøêîì áîëüøîé ó÷àñòîê òåêñòà.';
        var site_orfo_title = 'Óêàæèòå êîììåíòàðèé äëÿ àäìèíèñòðàöèè ê íàéäåííîé ãðàììàòè÷åñêîé îøèáêå';
        var site_p_send     = 'Îòïðàâèòü';
        var site_p_send_ok  = 'Óâåäîìëåíèå óñïåøíî îòïðàâëåíî';
        var site_save_ok    = 'Èçìåíåíèÿ óñïåøíî ñîõðàíåíû.';
        var site_del_news   = 'Óäàëèòü ñòàòüþ';
        var site_anti_cash  = '';
        var allow_site_delete_news   = false;
        //-->
        </script>
        <?$APPLICATION->ShowHead();?>
        <?
        // $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
        // $APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
        // $APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/main.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/menu_style.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/engine.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jetzoom.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.jsscrollpane.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.nouislider.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/owl.carousel.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/owl.theme.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/filter.css");

        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-ui.min.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/maskedinput.js");
        
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.sticky.js");
        
        

        
        
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/lb_main.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/tabs.js")
      
        ?>
      
        <title><?$APPLICATION->ShowTitle()?></title>
    </head>
<? $class = ($curPage != SITE_DIR."index.php") ? 'inner' : 'home' ?>
<body class="<?=$class?>">
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?$APPLICATION->IncludeComponent("bitrix:eshop.banner", "", array());?>
    <header>
        <div class="logobox">
            <div class="container">
                <a href="/" class="lb-logo"></a>
                <div class="lb-caption">Производитель женской одежды до 70 размера<br><a href="/feedback/">85 центров выдачи в РФ</a></div>
                <div class="lb-search">
                    <form action="/catalog/search/" method="post">
                        <input name="searchword" type="text" class="lb-search__input" placeholder="Поиск...">
                        <button class="lb-search__btn"><i class="sprites-header-icon-1"></i></button>
                    </form>
                </div>
                <div class="lb-phone">
                    <div class="lb-phone__free"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?></div>
                    <div class="lb-phone__num roistat-phone"  title="Звонок бесплатный"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?></div>
                    <?$APPLICATION->IncludeComponent(
                        "vr:callback",
                        "zdama-callback",
                        array(
                            "EMAIL_TO" => "am@z-dama.ru",
                            "IBLOCK_ID" => "41",
                            "IBLOCK_TYPE" => "callback",
                            "INCLUDE_JQUERY" => "Y",
                            "MAIL_TEMPLATE" => "FEEDBACK_FORM",
                            "PROPERTY_FIO" => "FIO",
                            "PROPERTY_FORM_NAME" => "FORM",
                            "PROPERTY_PAGE" => "PAGE",
                            "COMPONENT_TEMPLATE" => "zdama-callback"
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
        <nav class="container a-big-n-block menu-top-mobile">
            <a class="logo-mobile" href="/"></a>
            <ul>
                <!--<li><a href="/compare/" rel="nofollow" title="Товары к сравнению"><i class="icon-3"></i></a></li>-->
                <li><a href="/personal/cart/" rel="nofollow" title="Корзина"><i class="icon-4"></i></a></li>
                <li><a href="/personal/" rel="nofollow" title="Войти в личный кабинет"><i class="icon-6"></i></a></li>
            </ul>
        </nav>
        
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "main_menu",
                array(
                    "ROOT_MENU_TYPE" => "main",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "COMPONENT_TEMPLATE" => "main_menu"
                ),
                false
            );?>
        
        <form action="/catalog/search/" method="post" class="container a-big-n-block search-mobile">
            <div>
                <input type="text" name="searchword" placeholder="НАЙТИ" /><button><i class="sprites-header-icon-1"></i></button>
            </div>
        </form>
    </header>

<?if ($curPage != SITE_DIR."index.php"):?>
    <? /*
<div class="row">
	<div class="col-lg-12">
		<?$APPLICATION->IncludeComponent("bitrix:search.title", "visual", array(
				"NUM_CATEGORIES" => "1",
				"TOP_COUNT" => "5",
				"CHECK_DATES" => "N",
				"SHOW_OTHERS" => "N",
				"PAGE" => SITE_DIR."catalog/",
				"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS") ,
				"CATEGORY_0" => array(
					0 => "iblock_catalog",
				),
				"CATEGORY_0_iblock_catalog" => array(
					0 => "all",
				),
				"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
				"SHOW_INPUT" => "Y",
				"INPUT_ID" => "title-search-input",
				"CONTAINER_ID" => "search",
				"PRICE_CODE" => array(
					0 => "BASE",
				),
				"SHOW_PREVIEW" => "Y",
				"PREVIEW_WIDTH" => "75",
				"PREVIEW_HEIGHT" => "75",
				"CONVERT_CURRENCY" => "Y"
			),
			false
		);?>
	</div>
</div>
*/ ?>
<?endif?>

<?if ($curPage != SITE_DIR."index.php"):?>
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", array(
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "-"
    ),
        false,
        Array('HIDE_ICONS' => 'Y')
    );?>
    <? /* <h1 class="bx-title dbg_title" id="pagetitle"><?=$APPLICATION->ShowTitle(false);?></h1> */ ?>
<?endif?>

<?$needSidebar = preg_match("~^".SITE_DIR."(catalog|personal\/cart|personal\/order\/make)/~", $curPage);?>