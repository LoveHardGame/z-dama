$(function () {
    $(".fw-select").on("click", function () {
        $popup = $(this).next(".fw-popup");
        $(".fw-popup").not($popup).removeClass("fw-popup--open");
        $popup.toggleClass("fw-popup--open")
    });
    $('html').on("click", function () {
        $(".fw-popup").removeClass("fw-popup--open");
    });

    $('.fw-popup, .fw-select').on("click", function (event) {
        event.stopPropagation();
    });
    var slider = document.getElementById('slider');

    noUiSlider.create(slider, {
        start: [$(slider).data("start"), $(slider).data("end")],
        connect: true,
        range: {
            'min': $(slider).data("min"),
            'max': $(slider).data("max")
        }
    });

    var inputFrom = document.getElementById('input-from');
    var inputTo = document.getElementById('input-to');

    slider.noUiSlider.on('update', function( values, handle ) {

        var value = values[handle];

        if ( handle ) {
            inputTo.value = Math.round(value);
        } else {
            inputFrom.value = Math.round(value);
        }
    });

    inputFrom.addEventListener('change', function(){
        slider.noUiSlider.set([this.value, null]);
    });

    inputTo.addEventListener('change', function(){
        slider.noUiSlider.set([null, this.value]);
    });
});
