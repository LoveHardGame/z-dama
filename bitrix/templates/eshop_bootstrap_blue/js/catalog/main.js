	function ShowOnOverData(idlayer){
		
		$( '#showomoredata'+idlayer ).show();
		$( '#pid'+idlayer ).addClass( "hoverproduct" );
		
	} 	
	
	function HideOnOverData(idlayer){
		
		$( '#showomoredata'+idlayer ).hide();
		$( '#pid'+idlayer ).removeClass( "hoverproduct" );
		
	}	

$(function() {
	

	var owlbanner = $("#owlbanner");
	if (owlbanner.length) {
		$(window).resize(function() {
			owlbanner.owlCarousel({
				items: 1,
				autoPlay: true,
				pagination: true,
				navigation:false,
				responsive: false,
				slideSpeed : 400,
				paginationSpeed : 300,
				navRewind:false,
				autoplaySpeed:400
			});
			
			$(".owlbanner .next").unbind('click').click(function(){
				owlbanner.trigger('owl.next');
			});
			$(".owlbanner .prev").unbind('click').click(function(){
				owlbanner.trigger('owl.prev');
			});			

		});	
		
		$(window).resize();
	}	
	
	var owlbanner2 = $("#owlbanner2");
	if (owlbanner2.length) {
		$(window).resize(function() {
			owlbanner2.owlCarousel({
				items: 1,
				autoPlay: true,
				pagination: false,
				navRewind:false,
				navigation:false,
				responsive: false,
				slideSpeed : 400,
				paginationSpeed : 400,
				autoplaySpeed:600
			});
		});	
		
		$(window).resize();
	}	

	var owl = $("#owl");
	if (owl.length) {
		$(window).resize(function() {
			owl.owlCarousel({
				items: 3,
				autoPlay: false,
				pagination: false,
				responsive: false,
				afterMove: function(owl) {
					var vis = owl.data('owlCarousel').visibleItems;
					owl.find('.owl-item').removeClass('last');
					owl.find('.owl-item').eq(vis[vis.length - 1]).addClass('last');
				},
				beforeMove: function() {
					owl.find('.popup-trigger').attr('checked', false);
				}

			});
			owl.find('.owl-item').eq(3).addClass('last');
			$(".next").unbind('click').click(function(){
				owl.trigger('owl.next');
			});
			$(".prev").unbind('click').click(function(){
				owl.trigger('owl.prev');
			});
			owl.find('.popup-trigger').change(function() {
				var t = $(this);
				owl.find('.popup-trigger').not(t).attr('checked', false);
				owl.find('.owl-item').removeClass('active');
				t.parents('.owl-item').addClass('active');
			});
		});
		$(window).resize();
	}

	var aside = $('#aside');
	if (aside.length) {
		aside.stickyMojo({footerID: '#footer', contentID: '#content'});
	}


	var compare = $(".compare .list");
	if (compare.length) {
		compare.jScrollPane({
			autoReinitialise: true,
			horizontalDragMaxWidth: 86,
			horizontalDragMinWidth: 86
		});

		$(window).resize(function() {
			if ($(window).width() >= 1008) {
				compare.prepend($('.delete'));
			}
			else {
				compare.find('.jspPane').prepend($('.delete'));
			}
		});
		$(window).resize();
	}

	var tab = $(".tab");
	if (tab.length) {
		tab.click(function() {
			tab.removeClass('active');
			$(this).addClass('active');
		});
	}

	var refer = $(".card .refer .list");
	if (refer.length) {
		$(window).resize(function() {
			refer.owlCarousel({
				items: 5,
				autoPlay: false,
				pagination: false,
				responsive: false,
				navigation: true
			});
		});
		$(window).resize();
	}


	
	$(".video").fancybox({
		maxWidth	: 640,
		maxHeight	: 480,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		arrows:false,
		modal:false				
	});
	
	
	var search = $(".search a");
	if (search.length) {
		var nav = search.parents('nav');
		search.click(function() {
			var t = $(this).parents('li:first');
			var o = t.find('.search-form');
			if (o.length) {
				var SearchInputs = o.find('input[name="searchword"]');
				var SearchWord = SearchInputs.val().trim();
				if (SearchWord) {
					o.find('form').submit();
				}
				else {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				}
			}
			else {
				t.prepend('<div class="search-form"><form action="/catalog/search/" method="post"><input type="text" name="searchword" id="searchform" autocomplete="off"/></form></div>');
				o = t.find('.search-form');
				$( "#searchform" ).focus();
				
				$( "#searchform" ).focusout(function() {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				});
			}
			var width = t.offset().left - nav.offset().left - 5;
			o.animate({
				left: -width,
				width: width
			}, 300);
			return false;
		});
	}
	
	//===========================================================================================
	

	// Chosen
	$(".chosen_select").chosen({
		width: "216px",
		allow_single_deselect: true,
		no_results_text: "Не найдено",
		placeholder_text_multiple: "Выберите нужные значения",
		search_contains: true
	});
	
	// Изменение в input'е числового значения фильтра.
	// Для передвижения ползунка.
	$("input.BigFilterRangeValue").change(function() {
		if ($(this).hasClass("min")) {
			var CurValue = parseInt($(this).val());
			var MinValue = $(this).siblings('input.min_value[type="hidden"]').val();
			if (isNaN(CurValue) || (CurValue < MinValue)) {
				$(this).val(MinValue);
				$(this).siblings(".ui-slider").slider("values", 0, MinValue);
			}
			else {
				$(this).siblings(".ui-slider").slider("values", 0, CurValue);
			}
		}
		else if ($(this).hasClass("max")) {
			var CurValue = parseInt($(this).val());
			var MaxValue = $(this).siblings('input.max_value[type="hidden"]').val();
			if (isNaN(CurValue) || (CurValue > MaxValue)) {
				$(this).val(MaxValue);
				$(this).siblings(".ui-slider").slider("values", 1, MaxValue);
			}
			else {
				$(this).siblings(".ui-slider").slider("values", 1, CurValue);
			}
		}
	});
	


	// Сортировка
	// Установить сортировку
	function SetSorting(Type, Direction) {
		jQuery.ajax({
			url: '/engine/ajax/storesession.php',
			type: 'POST',
			dataType : 'json',
			data: {
				action: 'set_catalog_sorting',
				sorting_type: Type,
				sorting_direction: Direction
			},
			success: function(data, textStatus, xhr) {
				window.location.replace(window.location.pathname);
			},
			error: function(xhr, textStatus, errorThrown) {
				SITEErrorMessage(120);
			},
			complete: function(xhr, textStatus) {
			}
		});
	}
    $("select[name=sort_c]").on("change", function(){//Сортировка товаров каталога
        switch($(this).val()){
            case "sorting_price_asc": SetSorting('price', 'asc'); break;// "По возростанию"
            case "sorting_price_desc": SetSorting('price', 'desc'); break;// "По убыванию"
            case "sorting_price_no": SetSorting('none', 'none'); break;// "Без сортировки"
            case "sorting_position": SetSorting('position', 'asc'); break;// "По позиции"
        }
    });
	$('#sorting_panel').show();
	
		$(window).scroll(function(){
		if ($(this).scrollTop() > 650) {
		$('.scrollup').fadeIn();
		} else {
		$('.scrollup').fadeOut();
		}
		});
		 
		$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
		});		
	
	// Вкл./выкл. фильтр
	$('#filter-hider').change(function() {
		if ($(this).prop('checked')) {
			$('#filter_block').slideDown(500);
		}
		else {
			$('#filter_block').slideUp(500);
		}
	});
	
	
});
	
// Нажатие на кнопку "В корзину"
function AddToBasketBtnClick(product_id) {
	if (product_id) {
		// alert("Color: " + $('input[name="color_' + product_id + '"]:checked').val());
		// alert("Size: " + $('input[name="size_' + product_id + '"]:checked').val());
		var ColorID = $('input[name="color_' + product_id + '"]:checked').val();
		var Size = $('input[name="size_' + product_id + '"]:checked').val();
		
		if (ColorID && Size) {
			// AddToCard(product_id,'1');
			AddToCardOneClick(product_id, ColorID, Size);
			// SITEalert("<p>Товар: " + product_id + ". Цвет: " + ColorID + ". Размер: " + Size + "</p><br/>", "");
		}
		else {
			SITEalert("<p>Для того, чтобы добавить товар в корзину, выберите цвет и размер.</p><br/>", "");
		}
	}
}
