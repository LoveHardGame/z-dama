var c_cache        = new Array();

function RunAjaxJS(insertelement, data){
	var milisec = new Date;
    var jsfound = false;
    milisec = milisec.getTime();

    var js_reg = /<script.*?>(.|[\r\n])*?<\/script>/ig;

    var js_str = js_reg.exec(data);
    if (js_str != null) {

		var js_arr = new Array(js_str.shift());
        var jsfound = true;
        
        while(js_str) {
           js_str = js_reg.exec(data);
           if (js_str != null) js_arr.push(js_str.shift());
        }

        for(var i=0; i<js_arr.length;i++) {
           data = data.replace(js_arr[i],'<span id="'+milisec+i+'" style="display:none;"></span>');
        }
	}
    
	$("#" + insertelement).html(data);

    if (jsfound) {

       var js_content_reg = /<script.*?>((.|[\r\n])*?)<\/script>/ig;

       for (i = 0; i < js_arr.length; i++) {
           var mark_node = document.getElementById(milisec+''+i);
           var mark_parent_node = mark_node.parentNode;
           mark_parent_node.removeChild(mark_node);
                                
           js_content_reg.lastIndex = 0;
           var js_content = js_content_reg.exec(js_arr[i]);
           var script_node = mark_parent_node.appendChild(document.createElement('script'));
		   script_node.text = js_content[1];  

           var script_params_str = js_arr[i].substring(js_arr[i].indexOf(' ',0),js_arr[i].indexOf('>',0));
           var params_arr = script_params_str.split(' ');

           if (params_arr.length > 1) {
              for (var j=0;j< params_arr.length; j++ ) {
                                        
                  if(params_arr[j].length > 0){
                       var param_arr = params_arr[j].split('=');
                       param_arr[1] = param_arr[1].substr(1,(param_arr[1].length-2));
                       script_node.setAttribute(param_arr[0],param_arr[1]);
                  }

               }
           }

       }
    }
};

function SurchargeIbAvangard(id,summ,acquiring,manager){

		ShowLoading('');
	
		$.get(site_root + "engine/ajax/surcharge.ib.avangard.php", {id:id, summ:summ, acquiring:acquiring, manager:manager, action: "ibregister"}, function(data){
				
			HideLoading('');
				
			if (data) {
		
				if (data.status == "ok") {		
						document.location.href=data.text;					
				} else {
						SITEalert(data.text, site_info);
				}
		
			}



		}, "json");		
			
		return false;

}

function PayIbAvangard(id){

		ShowLoading('');
	
		$.get(site_root + "engine/ajax/ib.avangard.php", {id:id, action: "ibregister"}, function(data){
				
			HideLoading('');
				
			if (data) {
		
				if (data.status == "ok") {		
						document.location.href=data.text;					
				} else {
						SITEalert(data.text, site_info);
				}
		
			}



		}, "json");		
			
		return false;

}

function ClearFilterType(type){

	if(type == 'size') $('input[name="size[]"]').prop('checked', false);
	if(type == 'season') $('input[name="season[]"]').prop('checked', false);
	if(type == 'material') $('input[name="material[]"]').prop('checked', false);
	if(type == 'color') $('input[name="color[]"]').prop('checked', false);
	if(type == 'availability'){
		$('input[name="availability[yes][]"]').prop('checked', false);
		$('input[name="availability[no][]"]').prop('checked', false);
	}
	if(type == 'range'){
		$('#input-from').val("");
		$('#input-to').val("");
	}
	
	$( "#bigfilter" ).submit();
}

function ClearFilter(category){
        ShowLoading('');
		$.get(site_root + "engine/ajax/filter.php", { action: "clear",  category: category }, function(data){		
			document.location.href=data;
		});	
        HideLoading('');
               
}

function RepeatOrder(id, userid){

		ShowLoading('');

		$.get(site_root + "engine/ajax/repeat.order.php", { id:id, userid:userid, action: "repeat" }, function(data){
			HideLoading('');	
				
			document.location.href=data;
			
		});	

}

function RestoreOrder(type){

		ShowLoading('');

		$.get(site_root + "engine/ajax/restore.order.php", {type:type}, function(data){
			HideLoading('');	
				
			document.location.href=data;
			
		});	

}

function ClearCart(){


    SITEconfirm( 'Вы действительно хотите удалить все товары из корзины? Данное действие невозможно будет отменить.', site_confirm, function () {

	ShowLoading('');

		$.get(site_root + "engine/ajax/add.basket.php", { action: "clear" }, function(data){
			HideLoading('');	
				
			document.location.href=data;
			
		});	

	} );



}


function DeleteFromCard(code, color){

    SITEconfirm( 'Вы действительно хотите удалить выбранный товар из корзины? Данное действие невозможно будет отменить.', site_confirm, function () {

	ShowLoading('');

			$.get(site_root + "engine/ajax/add.basket.php", { code: code, color: color, action: "delete" }, function(data){
				HideLoading('');	
				
				//SendActionpayObject(9, code);
					
				document.location.href=data;
			});	

	} );



}

function ChangeUserType(type){

		$.get(site_root + "engine/ajax/change.user.php", { type: type }, function(data){				
			document.location.href=data;
		});	   
        
}

function CheckDiscount(DiscountType, CartSum) {
	var RealDiscountType = -1;

	if (CartSum < 15000) {
		RealDiscountType = 6;
	}
	else if (CartSum < 17500) {
		RealDiscountType = 1;
	}
	else if (CartSum < 20000) {
		RealDiscountType = 2;
	}
	else if (CartSum < 22500) {
		RealDiscountType = 3;
	}
	else if (CartSum < 25000) {
		RealDiscountType = 4;
	}
	else if (CartSum >= 25000) {
		RealDiscountType = 5;
	}

	if (DiscountType == RealDiscountType) {
		return 0;
	}
	else if (RealDiscountType > DiscountType) {
		return 1;
	}
	else if (RealDiscountType < DiscountType) {
		return -1;
	}
}

function TestAjax(Param) {
	//console.log('sdfasdf');
	jQuery.ajax({
		url: '/engine/ajax/getdiscounttype.php',
		type: 'GET',
		data: {
			'parameter': Param
		},
		// dataType : 'json',
		success: function(data, textStatus, xhr) {
			console.log(data); // do with data e.g success message
			alert(data);
			return data;
		},
		error: function(xhr, textStatus, errorThrown) {
			console.log(textStatus.reponseText);
			// alert("Error: " . textStatus);
			return -1;
		}
	});
}

// Получить тип доставки
function GetDeliveryType() {
	delivery = $('select[name="delivery"]').val();
	return delivery;
}

function GetPayDeliveryType() {
	paydelivery = $('select[name="paydelivery"]').val();
	return paydelivery;
}

// Пользователь согласился с ценой
function SetUserAgreedPrice() {
	var AjaxData = {};
	AjaxData['action_name'] = 'SetUserAgreedPrice';

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType: 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
			else if (data['operation_result'] == 'ok'){
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(123);
		},
		complete: function(xhr, textStatus) {
			window.location.replace(window.location);
		}
	});
}

function NeedMoreGoldDialog(message, title, DeliveryType) {
	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		closeOnEscape: false,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
		buttons: {
			"Вернуться в каталог": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();

				var AjaxData = {};
				AjaxData['action_name'] = 'SetShowcartMode';
				AjaxData['mode'] = 'show_my_price';
				AjaxData['delivery_type'] = GetDeliveryType();

				jQuery.ajax({
					url: '/engine/ajax/runpostaction.php',
					type: 'GET',
					data: AjaxData,
					dataType: 'json',
					async: false,
					success: function(data, textStatus, xhr) {
						if (data['operation_result'] == 'error') {
							SITEalert(data["errors"], 'Ошибка!');
						}
						else if (data['operation_result'] == 'ok'){
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						SITEErrorMessage(121);
					},
					complete: function(xhr, textStatus) {
						if (DeliveryType) {
							ChangeDeliveryType(DeliveryType);
						}
						else {
							window.location.replace("/catalog/15/");
						}
					}
				});
			},
			"Пересчитать заказ по розничным ценам": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();
				
				SetUserAgreedPrice();

				var AjaxData = {};
				AjaxData['action_name'] = 'SetShowcartMode';
				AjaxData['mode'] = 'show_my_price';
				AjaxData['delivery_type'] = GetDeliveryType();

				jQuery.ajax({
					url: '/engine/ajax/runpostaction.php',
					type: 'GET',
					data: AjaxData,
					dataType: 'json',
					async: false,
					success: function(data, textStatus, xhr) {
						if (data['operation_result'] == 'error') {
							SITEalert(data["errors"], 'Ошибка!');
						}
						else if (data['operation_result'] == 'ok'){
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						SITEErrorMessage(121);
					},
					complete: function(xhr, textStatus) {
						if (DeliveryType) {
							ChangeDeliveryType(DeliveryType);
						}
						else {
							window.location.replace(window.location);
						}
					}
				});
			}
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function NeedMoreGoldForCampaignDialog(message, title, DeliveryType) {
	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		closeOnEscape: false,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
		buttons: {
			"Вернуться в каталог": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();
				window.location.replace("/catalog/15/");
			},
			"Всё равно оформить заказ": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();
				ValidatePayerReceiverForms('no_campaign');
				// window.location.replace(window.location);
			}
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

// Сохраняет формы со страницы корзины в сессию
function SaveShowcartFormsIntoSession(onComplete) {
	var AjaxData = {};
	AjaxData['action_name'] = 'SaveShowcartForms';
	AjaxData['receiver_id'] = $('#selUserReceiver').val();
	// AjaxData['delivery_address'] = {};
	// AjaxData['receiver_info'] = {};
	AjaxData['form_data'] = {};
	
	// Читаем input[type="text"] с формы адреса
	$('.delivery_address_form input[type="text"]').each(function() {
		AjaxData['form_data'][$(this).attr('name')] = $(this).val();
	});
	// Читаем input[type="text"] с формы получателя
	$('.receiver_info_form input[type="text"]').each(function() {
		AjaxData['form_data'][$(this).attr('name')] = $(this).val();
	});
	// Читаем input[type="radio"] с формы получателя (способ оплаты)
	$('.receiver_info_form input[type="radio"]').each(function() {
		if ($(this).prop('checked')) {
			AjaxData['form_data'][$(this).prop('name')] = $(this).val();
		}
	});
	// Читаем select с формы получателя (получатель)
	$('.receiver_info_form select').each(function() {
		AjaxData['form_data'][$(this).prop('name')] = $(this).val();
	});
	
	/*jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		success: function(data, textStatus, xhr) {
			if (data.operation_result == 'ok') {
				// alert(JSON.stringify(data));
				onComplete();
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
		}
	});*/
}

function ChangeDeliveryType(delivery_type, pay_delivery_type) {
	var AjaxData = {};
	AjaxData['action_name'] = 'SetShowcartMode';
	AjaxData['mode'] = 'show_my_price';
	AjaxData['delivery_type'] = delivery_type;
	AjaxData['pay_delivery_type'] = pay_delivery_type;
	
	/*jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		// async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
			else if (data['operation_result'] == 'ok') {
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			alert(textStatus);
			SITEErrorMessage(122);
		},
		complete: function(xhr, textStatus) {
			SaveShowcartFormsIntoSession(function() {window.location.reload();});
		}
	});*/

}



function OnDeliveryTypeChange(message, title, delivery_type) {
	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		closeOnEscape: false,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
		buttons: {
			"Пересчитать": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();
				ChangeDeliveryType(delivery_type);
			}
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

// Очищает ошибки заполнения формы в корзине
function ClearCartErrors() {
	$('.has_errors').removeClass('has_errors');
	$('.cart_error_messages_block').html("");
	$('.cart_error_messages_block').css('display', 'none');
}

// Отображает ошибки заполнения формы в корзине
function ShowCartErrors(errors_data) {
	ClearCartErrors();
	
	var errors_text_html = errors_data.text;
	$('.cart_error_messages_block').css('display', 'block');
	$('.cart_error_messages_block').html(errors_text_html);

	if (errors_data["status"] == "no_delivery_type") {
		$('#selDeliveryType').addClass("has_errors");
	}
	
	if (errors_data["status"] == "no_pay_delivery_type") {
		$('#selPayDeliveryType').addClass("has_errors");
	}	
	
	for (var i = 0; i < errors_data["wrong_fields"].length; i++) {
		$('[name="'+errors_data["wrong_fields"][i]+'"]').addClass('has_errors');
	};
};

var conf_message="Обратите внимание!\n\nНекоторых товаров в вашей корзине, нет в наличии. Они автоматически удалятся. Вы хотите продолжить офромление заказа без этих товаров?";
function SubmitOrder(campaign, mode, one_click, in_stock) {
	if($("span.el.not_am").length>0 && confirm(conf_message)!=true){
		return false;
	}
	ShowLoading("Пожалуйста ожидайте, ваш заказ оформляется");
	var size_num = 0;
    var color = "";    
    var count = "";
	var size = "";
	var product_id = "";
	var reorder = "";
	var discounttype = "";
	var deliverytype = "";
	var paydeliverytype = "";
	
	if (!campaign) {
		campaign = 0;
	}

	if (!mode) {
		mode = 0;
	}

	// Сумма покупок
	var DiscountType = $('#inputDiscountType').attr('value');
	// Тип скидки
	var CartSum = $('#inputCartSum').attr('value');
	// Тип скидки
	var cupon = $('#cupon').attr('value');	

    // var inputs = document.cartlist.getElementsByTagName('input');
    var inputs = $('.tab-content:visible input.sizeselect[type="text"]');
    // var inputs = $('input.sizeselect.retail[type="text"]');
	//var inputs = document.cartlist.getElementsByClassName('sizeselect');
    
    for (var i = 0; i < inputs.length; i++){
	
	    if (true) {
	    // if (inputs[i].type == 'text' && inputs[i].className == 'sizeselect') {
	    	
			if(inputs[i].value < 0){
					SITEalert ( "Числовое значение количества заказываемой позиции должно быть больше нуля!", site_info );
					HideLoading('');
					return false;					
			}
			
			if(isNaN( inputs[i].value )) {
					SITEalert ( "<b>Введите корректное числовое значение количества заказываемой позиции! Количество указывается только цифрами!</b>", site_info );
					HideLoading('');
					return false;						
			}

            color = color+inputs[i].name+",";    
			size = size+inputs[i].id+","; 
            count = count+inputs[i].value+","; 
			product_id = product_id+inputs[i].title+","; 					
            size_num ++;  		

		}
		
    }
    
	if ($('#oid').is(':checked')) {
		reorder = 1;
	}	
	
	if ($('#funded').is(':checked')) {
			discounttype = 2;
	}	
	
	if ($('#single').is(':checked')) {
			discounttype = 1;
	}	
	
	deliverytype = 0;
	paydeliverytype = 0;

	deliverytype = GetDeliveryType();
	paydeliverytype = GetPayDeliveryType();

	var IsWholesale = "1";
	if ($("#retail_order_pane").is(':visible')) {
		IsWholesale = "0";
	}
	
	// Получаем даные из форм
	var PaymentWay = ''; // способ оплаты
	$('.receiver_info_form input[name="pay"]').each(function() {
		if ($(this).prop('checked')) {
			PaymentWay = $(this).val();
		}
	});
	var FormData = {
		'delivery_type': $('#selDeliveryType').val(),
		'pay_delivery_type': $('#selPayDeliveryType').val(),
		'delivery_addr_zip': $('.delivery_address_form input[name="delivery_address_zip"]').val(),
		'delivery_addr_region': $('.delivery_address_form input[name="delivery_address_region"]').val(),
		'delivery_addr_district': $('.delivery_address_form input[name="delivery_address_district"]').val(),
		'delivery_addr_city': $('.delivery_address_form input[name="delivery_address_city"]').val(),
		'delivery_addr_street': $('.delivery_address_form input[name="delivery_address_street"]').val(),
		'delivery_addr_house': $('.delivery_address_form input[name="delivery_address_house"]').val(),
		'delivery_addr_building': $('.delivery_address_form input[name="delivery_address_building"]').val(),
		'delivery_addr_flat': $('.delivery_address_form input[name="delivery_address_flat"]').val(),
		
		'receiver_id': $('#selUserReceiver').val(),
		'receiver_type_id': $('#selUserReceiverType').val(),
		'receiver_property_type': $('#selPropertyType').val(),
		'receiver_fio': $('.receiver_info_form input[name="payer_info_fio"]').val(),
		'receiver_name': $('.receiver_info_form input[name="payer_info_name"]').val(),
		'receiver_email': $('.receiver_info_form input[name="payer_info_email"]').val(),
		'receiver_phone': $('.receiver_info_form input[name="payer_info_phone"]').val(),
		'cupon': $('#cupon input[name="cupon"]').val(),
		'payment_way': PaymentWay
	};
	
	// alert(JSON.stringify(FormData));
	
	/*$.post(site_root + "engine/ajax/add.basket.php", {
			product_id: product_id,
			color: color,
			size: size,
			size_num: size_num,
			count: count,
			is_wholesale: IsWholesale,
			action: "submit",
			oid: reorder,
			discounttype: discounttype,
			deliverytype: deliverytype,
			paydeliverytype: paydeliverytype,
			campaign: campaign,
			mode: mode,
			cupon: cupon,
			form_data: FormData,
			one_click: one_click, 
			in_stock:in_stock
		}, function(data) {
		
		if (data) {

			// Всё OK
			if (data.status == "ok") {		
				yaCounter1010447.reachGoal('Successful_Order');
				if(data.pay_delivery_type == 3)window.location.replace('/showcart/thanks/'+data.order_id+'/');
				else{
					$("#basket").html(data.basket);				
					$(".ProductsWrapper").html(data.result);
				}
			}
			
			// Неверный тип скидки
			else if (data.status == "no_stock") {
				SITENoStockConfirm(data.text, site_info,
					// Кнопка "OK"
					function() {
						jQuery.ajax({
							url: '/engine/ajax/add.basket.php',
							type: 'POST',
							data: {
								'in_stock': '2',
								'action': 'submit'
							},
							// dataType : 'json',
							success: function(data, textStatus, xhr) {
							},
							error: function(xhr, textStatus, errorThrown) {
								SITEErrorMessage(108);
							},
							complete: function(xhr, textStatus) {
								//window.location.replace(window.location.pathname);
							}
						});
					},
					
					//не ок
					function() {
						jQuery.ajax({
							url: '/engine/ajax/add.basket.php',
							type: 'POST',
							data: {
								'in_stock': '1',
								'action': 'submit'
							},
							// dataType : 'json',
							success: function(data, textStatus, xhr) {
							},
							error: function(xhr, textStatus, errorThrown) {
								SITEErrorMessage(108);
							},
							complete: function(xhr, textStatus) {
								//window.location.replace(window.location.pathname);
							}
						});
					}					
				);
				yaCounter1010447.reachGoal('DoOrder');
			}			
			
			// Неверный тип скидки
			else if (data.status == "discount_type_wrong") {
				SITEconfirm(data.text, site_info,
					// Кнопка "OK"
					function() {
						jQuery.ajax({
							url: '/engine/ajax/storesession.php',
							type: 'POST',
							data: {
								'discount_type': data.real_discount_type
							},
							// dataType : 'json',
							success: function(data, textStatus, xhr) {
							},
							error: function(xhr, textStatus, errorThrown) {
								SITEErrorMessage(108);
							},
							complete: function(xhr, textStatus) {
								window.location.replace(window.location.pathname);
							}
						});
					}
				);
				yaCounter1010447.reachGoal('DoOrder');
			}
			
			// Не выбран тип оплаты
			else if (data.status == "no_pay_delivery_type") {
				yaCounter1010447.reachGoal('DoOrder');
				ShowCartErrors(data);
			}
			
			// Не выбран тип доставки
			else if (data.status == "no_delivery_type") {
				yaCounter1010447.reachGoal('DoOrder');
				ShowCartErrors(data);
			}			
			
			// Неправильно заполнены формы
			else if (data.status == "bad_form_data") {
				yaCounter1010447.reachGoal('DoOrder');
				ShowCartErrors(data);
			}
			// Сумма заказа маловата
			else if (data.status == "need_more_gold") {
				yaCounter1010447.reachGoal('DoOrder');
				NeedMoreGoldDialog(data.text, "");
			}
			// Сумма заказа маловата
			else if (data.status == "need_more_gold_for_campaign") {
				yaCounter1010447.reachGoal('DoOrder');
				NeedMoreGoldForCampaignDialog(data.text, "");
			}
			// Номерная ошибка
			else if (data.status == "numbered_error") {
				yaCounter1010447.reachGoal('DoOrder');
				SITEErrorMessage(data.error_number);
			}
			// "Прочие опасности!"
			else {
				yaCounter1010447.reachGoal('DoOrder');
				SITEalert(data.text, site_info);
			}
			HideLoading();
		}
	}, "json").fail(function(x) {
		// SITEalert(JSON.stringify(x), site_info);
		// alert(JSON.stringify(x));
		// SITEErrorMessage(111);
		// window.location.replace(window.location.pathname);
	});*/

	return false;
}

/*
function Recalculate() {
	var size_num = 0;
	var color = "";
	var count = "";
	var size = "";
	var product_id = "";
	
	 var inputs = $('.tab-content:visible input.sizeselect[type="text"]');
	//var inputs = document.cartlist.getElementsByClassName('sizeselect');
	for (var i = 0; i < inputs.length; i++){
		if (true) {

				if(inputs[i].value < 0){
						SITEalert ( "Числовое значение количества заказываемой позиции должно быть больше нуля!", site_info );
						return false;
				}

				if(isNaN( inputs[i].value )) {
						SITEalert ( "<b>Введите корректное числовое значение количества заказываемой позиции! Количество указывается только цифрами!</b>", site_info );
						return false;
				}

				color = color+inputs[i].name+",";
				size = size+inputs[i].id+",";
				count = count+inputs[i].value+",";
				product_id = product_id+inputs[i].title+",";
				size_num ++;

		}
	}

$.post(site_root + "engine/ajax/add.basket.php", { product_id: product_id, color: color, size: size, size_num: size_num, count: count, skin: site_skin, action: "recalculate" }, function(data){
		document.location.href=data;
	});
	
	return false;
}*/

// Обновление маленькой корзины в верхнем меню
function RefreshSmallBasketPopUp() {
	// Получаем HTML, который будем вставлять на место оригинального блока с размерами
	var AjaxData = {};
	AjaxData['action_name'] = 'GetTopMenuShowcartPopupHTML';
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType: 'json',
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'ok'){
				var BasketElement = $('#basket').eq(0);
				// Удаляем выпадашку для корзины, если есть
				BasketElement.siblings('.a-big-block.menu-pop.menu-cart').remove();
				// Пишем туда полученный HTML
				BasketElement.before(data['html']);
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
		}
	});
}

function AddToCard(prodid, type){
	var size_num = 0;
    var color = "";    
    var count = "";
	var size = "";
	
	
    
    var inputs = document.sizesform.getElementsByTagName('input');
    
    for (var i = 0; i < inputs.length; i++){
	
	    if (inputs[i].type == 'text' && inputs[i].value != "") {
		
			if (inputs[i].value < 0 || inputs[i].value == 0 || isNaN( inputs[i].value )) {
					SITEalert ( "<b>Значение количества заказываемой позиции должно быть больше нуля и указывается только цифрами!</b>", site_info );
					//HideLoading('');
					return false;					
			}
			else {
				color = color+inputs[i].name+",";
				size = size+inputs[i].id+","; 
				count = count+inputs[i].value+","; 			
				size_num ++;  			
			}

		}
		
			
    }

	if (size_num == 0) {
	
		SITEalert ( "Вы не указали кол-во товаров для добавления!", site_info );
		//HideLoading('');
		return false;	
	
	}else {
	
		$.post(site_root + "engine/ajax/add.basket.php", { prodid: prodid, color: color, size: size, size_num: size_num, count: count, type:type }, function(data){
			
		
			
			if (data) {
				
				if (data.status == "ok") {
					
					ShowLoading('Товар добавлен в корзину!');
		
					$("#basket").html(data.result);
					$("#yandex").html(data.yandex);
					RefreshSmallBasketPopUp();
					//$("html"+( ! $.browser.opera ? ",body" : "")).animate({scrollTop: $("#basket").position().top}, 1100);
					
					//clear inputs after job
					for (var i = 0; i < inputs.length; i++){
					
						var node = inputs[i];
						
						if (node.getAttribute('type') == 'text') {
							inputs[i].value ='';            
						}	
					}
					
					// alert()

					HideLoading('');	
				} else {

					SITEalert(data.text, site_info);

				}
	
			}else SITEalert('Ошибка добавления!', site_info);



		}, "json");		
	
	}
	
	yaCounter1010447.reachGoal('AddToBasket');
	
	return false;

}

// Склоняет существительное в зависимости от числа
function ConjugateNoun(num, vFormOne, vFormTwo, vFormZero) {
	var absNum = Math.abs(num);
	if ((absNum >= 5) && (absNum <= 20)) {
		return vFormZero;
	}
	else {
		var remainder = absNum % 10;
		if (remainder == 1) {
			return vFormOne;
		}
		else if ((remainder >= 2) && (remainder <= 4)) {
			return vFormTwo;
		}
		else {
			return vFormZero;
		}
	}
}

function AddToCardOneClick(prodid, color_id, size){
	ShowLoading('Товар добавлен в корзину');
		
	yaCounter1010447.reachGoal('AddToBasket');
    
	$.post(site_root + "engine/ajax/add.basket.php", { prodid: prodid, color: color_id+' ', size: size+' ', size_num: '1 ', count: '1 ', type: 1 }, function(data){
		HideLoading('');
		
		if (data) {
			if (data.status == "ok") {

				$("#basket").html('<i class="icon-4"></i>Корзина: '
					+ data.cart_count + ' шт. на ' + data.real_sum + ' P');

				RefreshSmallBasketPopUp();
			}
			else {
				SITEalert(data.text, site_info);
			}
		}
	}, "json").fail(function(e) {});
	
	return false;
}

function IPMenu( m_ip, l1, l2, l3 ){

	var menu=new Array();
	
	menu[0]='<a href="https://www.nic.ru/whois/?ip=' + m_ip + '" target="_blank">' + l1 + '</a>';
	menu[1]='<a href="' + site_root + site_admin + '?mod=iptools&ip=' + m_ip + '" target="_blank">' + l2 + '</a>';
	menu[2]='<a href="' + site_root + site_admin + '?mod=blockip&ip=' + m_ip + '" target="_blank">' + l3 + '</a>';
	
	return menu;
};


function ajax_save_for_edit( news_id, event )
{
	var allow_br = 0;
	var news_txt = '';

	var params = {};

	$.each($('#ajaxnews'+news_id).serializeArray(), function(index,value) {

		if (value.name.indexOf("xfield") != -1) {
			params[value.name] = value.value;
		}

	});

	if (document.getElementById('allow_br_'+news_id).checked) { params['allow_br'] = 1; }

	if (quick_wysiwyg == "1") {

		params['news_txt'] = $('#siteeditnews'+news_id).html();

	} else {

		params['news_txt'] = $('#siteeditnews'+news_id).val();

	}

	params['title'] = $('#edit-title-'+news_id).val();
	params['reason'] = $('#edit-reason-'+news_id).val();
	params['id'] = news_id;
	params['field'] = event;
	params['action'] = "save";

	ShowLoading('');

	$.post(site_root + "engine/ajax/editnews.php", params, function(data){

		HideLoading('');

		if (data != "ok") {

			SITEalert ( data, site_info );

		} else {

			$('#sitepopup-news-id-' + news_id).dialog('close');
			SITEalert ( site_save_ok, site_info );

		}

	});

	return false;
};

function ajax_prep_for_edit( news_id, event )
{

	ShowLoading('');

	$.get(site_root + "engine/ajax/editnews.php", { id: news_id, field: event, action: "edit" }, function(data){

		HideLoading('');

		$('body').append('<div id="modal-overlay" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #666666; opacity: .40;filter:Alpha(Opacity=40); z-index: 999; display:none;"></div>');
		$('#modal-overlay').css({'filter' : 'alpha(opacity=40)'}).fadeIn();

		var b = {};
	
		b[site_act_lang[3]] = function() { 
			$(this).dialog('close');	
		};
	
		b[site_act_lang[4]] = function() { 
			ajax_save_for_edit( news_id, event );			
		};
	
		$('#sitepopup-news-id-' + news_id).remove();
						
		$('body').append("<div id='sitepopup-news-id-"+news_id+"' title='"+menu_short+"' style='display:none'></div>");
						
		$('#sitepopup-news-id-' + news_id).dialog({
			autoOpen: true,
			width: "800",
			height: 500,
			buttons: b,
			dialogClass: "modalfixed",
			close: function(event, ui) {
					$('#modal-overlay').fadeOut(function() {
			        $('#modal-overlay').remove();
			    });
			 }
		});

		if ($(window).width() > 830 && $(window).height() > 530 ) {
			// $('.modalfixed.ui-dialog').css({position:"fixed"});
			$( '#sitepopup-news-id-' + news_id ).dialog( "option", "position", ['0','0'] );
		}

		RunAjaxJS('sitepopup-news-id-'+news_id, data);

	});

	return false;
};


function ajax_comm_edit( c_id, area )
{
	if ( ! c_cache[ c_id ] || c_cache[ c_id ] == '' )
	{
		c_cache[ c_id ] = $('#comm-id-'+c_id).html();
	}

	ShowLoading('');

	$.get(site_root + "engine/ajax/editcomments.php", { id: c_id, area: area, action: "edit" }, function(data){

		HideLoading('');

		RunAjaxJS('comm-id-'+c_id, data);

		setTimeout(function() {
           $("html:not(:animated)"+( ! $.browser.opera ? ",body:not(:animated)" : "")).animate({scrollTop: $("#comm-id-" + c_id).offset().top - 70}, 700);
        }, 100);

	});
	return false;
};

function ajax_cancel_comm_edit( c_id )
{
	if ( c_cache[ c_id ] != "" )
	{
		$("#comm-id-"+c_id).html(c_cache[ c_id ]);
	}
	
	return false;
};

function ajax_save_comm_edit( c_id, area )
{
	var comm_txt = '';

	if (site_wysiwyg == "yes") {

		comm_txt = $('#siteeditcomments'+c_id).html();

	} else {

		comm_txt = $('#siteeditcomments'+c_id).val();

	}

	ShowLoading('');

	$.post(site_root + "engine/ajax/editcomments.php", { id: c_id, comm_txt: comm_txt, area: area, action: "save" }, function(data){

		HideLoading('');
		c_cache[ c_id ] = '';
		$("#comm-id-"+c_id).html(data);

	});
	return false;
};

function DeleteComments(id, hash) {

    SITEconfirm( site_del_agree, site_confirm, function () {

		ShowLoading('');
	
		$.get(site_root + "engine/ajax/deletecomments.php", { id: id, site_allow_hash: hash }, function(r){
	
			HideLoading('');
	
			r = parseInt(r);
		
			if (!isNaN(r)) {
		
				$("html"+( ! $.browser.opera ? ",body" : "")).animate({scrollTop: $("#comment-id-" + r).offset().top - 70}, 700);
		
				setTimeout(function() { $("#comment-id-" + r).hide('blind',{},1400)}, 700);
				
			}
	
		});

	} );

};

function doAccumulative( payer_id )
{
	ShowLoading('');
	
	var payer_id = ""

	
	
	if ($('#payer_select').is(':checked')) {
				payer_id = $('#payer_select').value;
	}


	$.get(site_root + "engine/ajax/get.discount.php", { payer_id: payer_id}, function(data){

		HideLoading('');

		$('#discountsumm').html(data.discountsumm);
		$('#discounttype').html(data.discounttype);

	}, "json");

	return false;
	
};

function doFavorites( fav_id, event, type )
{
	ShowLoading('');

	$.get(site_root + "engine/ajax/favorites.php", { fav_id: fav_id, action: event, skin: site_skin, type : type }, function(data){

		HideLoading('');
		
		if (type == 'comparison') {
			window.location.reload();
		}
		else {
			$("#fav-id-" + fav_id).parent().html(data.text);
			$("#favcount").html(data.counter);
		}
		
		
	}, "json");

	return false;
	
};

function CheckLogin()
{
	var name = document.getElementById('name').value;

	ShowLoading('');

	$.post(site_root + "engine/ajax/registration.php", { name: name }, function(data){

		HideLoading('');

		$("#result-registration").html(text);

	});

	return false;
};

function doCalendar(month, year, effect){

	ShowLoading('');

	$.get(site_root + "engine/ajax/calendar.php", { month: month, year: year }, function(data){
		HideLoading('');

		if (effect == "left" ) {
			$("#calendar-layer").hide('slide',{ direction: "left" }, 500).html(data).show('slide',{ direction: "right" }, 500);
		} else {
			$("#calendar-layer").hide('slide',{ direction: "right" }, 500).html(data).show('slide',{ direction: "left" }, 500);
		}

	});
};

function ShowBild(sPicURL) {
window.open(site_root + 'engine/modules/imagepreview.php?image='+sPicURL, '', 'resizable=1,HEIGHT=200,WIDTH=200, top=0, left=0, scrollbars=yes');
};

function doRate( rate, id ) {
	ShowLoading('');

	$.get(site_root + "engine/ajax/rating.php", { go_rate: rate, news_id: id, skin: site_skin }, function(data){

		HideLoading('');

		$("#ratig-layer").html(data);

	});
};

function doCatalogRate( rate, id ) {
    ShowLoading('');

    $.get(site_root + "engine/ajax/catalog_rating.php", { go_rate: rate, product_id: id, skin: site_skin }, function(data){

        HideLoading('');

        $("#ratig-layer").html(data);

    });
};

function DoNewsRate( rate, id ) {
	ShowLoading('');

	$.get(site_root + "engine/ajax/rating.php", { go_rate: rate, news_id: id, skin: site_skin, mode: "short" }, function(data){

		HideLoading('');

		$("#ratig-layer-" + id).html(data);

	});

};

function DoCatalogRate( rate, id ) {
    ShowLoading('');

    $.get(site_root + "engine/ajax/catalog_rating.php", { go_rate: rate, product_id: id, skin: site_skin, mode: "short" }, function(data){

        HideLoading('');

        $("#ratig-layer-" + id).html(data);

    });

};

function doAddComments(){

	var form = document.getElementById('site-comments-form');

	if (site_wysiwyg == "yes") {
		document.getElementById('comments').value = $('#comments').html();
		var editor_mode = 'wysiwyg';
	} else { var editor_mode = ''; }

	if (form.comments.value == '' || form.name.value == '')
	{
		SITEalert ( site_req_field, site_info );
		return false;
	}

	if ( form.question_answer ) {

	   var question_answer = form.question_answer.value;

    } else { var question_answer = ''; }

	if ( form.sec_code ) {

	   var sec_code = form.sec_code.value;

    } else { var sec_code = ''; }

	if ( form.recaptcha_response_field ) {
	   var recaptcha_response_field= Recaptcha.get_response();
	   var recaptcha_challenge_field= Recaptcha.get_challenge();
    } else {
	   var recaptcha_response_field= '';
	   var recaptcha_challenge_field= '';
	}

	if ( form.allow_subscribe ) {

		if ( form.allow_subscribe.checked == true ) {
	
		   var allow_subscribe= "1";

		} else {

		   var allow_subscribe= "0";

		}

    } else { var allow_subscribe= "0"; }

	ShowLoading('');

	$.post(site_root + "engine/ajax/addcomments.php", { post_id: form.post_id.value, comments: form.comments.value, name: form.name.value, mail: form.mail.value, editor_mode: editor_mode, skin: site_skin, sec_code: sec_code, question_answer: question_answer, recaptcha_response_field: recaptcha_response_field, recaptcha_challenge_field: recaptcha_challenge_field, allow_subscribe: allow_subscribe }, function(data){

		if ( form.sec_code ) {
           form.sec_code.value = '';
           reload();
	    }

		HideLoading('');

		RunAjaxJS('site-ajax-comments', data);

		if (data != 'error' && document.getElementById('blind-animation')) {

			$("html"+( ! $.browser.opera ? ",body" : "")).animate({scrollTop: $("#site-ajax-comments").offset().top - 70}, 1100);
	
			setTimeout(function() { $('#blind-animation').show('blind',{},1500)}, 1100);
		}

	});

};

function CommentsPage( cstart, news_id ) 
{
	ShowLoading('');

	$.get(site_root + "engine/ajax/comments.php", { cstart: cstart, news_id: news_id, skin: site_skin }, function(data){

		HideLoading('');

		if (!isNaN(cstart) && !isNaN(news_id)) {

			$('#site-comm-link').unbind('click');

			$('#site-comm-link').bind('click', function() {
				CommentsPage( cstart, news_id );
				return false;
			});

		
		}

		scroll( 0, $("#site-comments-list").offset().top - 70 );
	
		$("#site-comments-list").html(data.comments); 
		$(".site-comments-navigation").html(data.navigation); 


	}, "json");

	return false;
};

function site_copy_quote(qname) 
{
	site_txt= '';

	if (window.getSelection) 
	{
		site_txt=window.getSelection();
	}
	else if (document.selection) 
	{
		site_txt=document.selection.createRange().text;
	}
	if (site_txt != "")
	{
		site_txt='[quote='+qname+']'+site_txt+'[/quote]\n';
	}
};

function site_ins(name) 
{
	if ( !document.getElementById('site-comments-form') ) return false;

	var input=document.getElementById('site-comments-form').comments;
	var finalhtml = "";

	if (site_wysiwyg == "no") {
		if (site_txt!= "") {
			input.value += site_txt;
		}
		else { 
			input.value += "[b]"+name+"[/b],"+"\n";
		}
	} else {
		if (site_txt!= "") {
			finalhtml = site_txt;
		}
		else { 
			finalhtml = "<b>"+name+"</b>,"+"<br />";
		}

	tinyMCE.execInstanceCommand('comments', 'mceInsertContent', false, finalhtml, true) 
	}

};

function ShowOrHide( id ) {

	  var item = $("#" + id);

	  if ( document.getElementById('image-'+ id) ) {

		var image = document.getElementById('image-'+ id);

	  } else {

		var image = null;
	  }

	var scrolltime = (item.height() / 200) * 1000;

	if (scrolltime > 3000 ) { scrolltime = 3000; }

	if (scrolltime < 250 ) { scrolltime = 250; }

	if (item.css("display") == "none") { 

		item.show('blind',{}, scrolltime );

		if (image) { image.src = site_root + 'templates/'+ site_skin + '/siteimages/spoiler-minus.gif';}

	} else {

		if (scrolltime > 2000 ) { scrolltime = 2000; }

		item.hide('blind',{}, scrolltime );
		if (image) { image.src = site_root + 'templates/'+ site_skin + '/siteimages/spoiler-plus.gif';}
	}

};


function ckeck_uncheck_all() {
    var frm = document.pmlist;
    for (var i=0;i<frm.elements.length;i++) {
        var elmnt = frm.elements[i];
        if (elmnt.type=='checkbox') {
            if(frm.master_box.checked == true){ elmnt.checked=false; }
            else{ elmnt.checked=true; }
        }
    }
    if(frm.master_box.checked == true){ frm.master_box.checked = false; }
    else{ frm.master_box.checked = true; }
};

function confirmDelete(url){

    SITEconfirm( site_del_agree, site_confirm, function () {
		document.location=url;
	} );
};

function setNewField(which, formname)
{
	if (which != selField)
	{
		fombj    = formname;
		selField = which;

	}
};

function site_news_delete( id ){

		var b = {};
	
		b[site_act_lang[1]] = function() { 
			$(this).dialog("close");						
		};

		if (allow_site_delete_news) {

			b[site_del_msg] = function() { 
				$(this).dialog("close");
	
				var bt = {};
						
				bt[site_act_lang[3]] = function() { 
					$(this).dialog('close');						
				};
						
				bt[site_p_send] = function() { 
					if ( $('#site-promt-text').val().length < 1) {
						$('#site-promt-text').addClass('ui-state-error');
					} else {
						var response = $('#site-promt-text').val()
						$(this).dialog('close');
						$('#sitepopup').remove();
						$.post(site_root + 'engine/ajax/message.php', { id: id,  text: response },
							function(data){
								if (data == 'ok') { document.location=site_root + 'index.php?do=deletenews&id=' + id + '&hash=' + user_login_hash; } else { SITEalert('Send Error', site_info); }
						});
		
					}				
				};
						
				$('#sitepopup').remove();
						
				$('body').append("<div id='sitepopup' title='"+site_notice+"' style='display:none'><br />"+site_p_text+"<br /><br /><textarea name='site-promt-text' id='site-promt-text' class='ui-widget-content ui-corner-all' style='width:97%;height:100px; padding: .4em;'></textarea></div>");
						
				$('#sitepopup').dialog({
					autoOpen: true,
					width: 500,
					dialogClass: "modalfixed",
					buttons: bt
				});

				// $('.modalfixed.ui-dialog').css({position:"fixed"});
				$('#sitepopup').dialog( "option", "position", ['0','0'] );
						
			};
		}
	
		b[site_act_lang[0]] = function() { 
			$(this).dialog("close");
			document.location=site_root + 'index.php?do=deletenews&id=' + id + '&hash=' + user_login_hash;					
		};
	
		$("#sitepopup").remove();
	
		$("body").append("<div id='sitepopup' title='"+site_confirm+"' style='display:none'><br /><div id='sitepopupmessage'>"+site_del_agree+"</div></div>");
	
		$('#sitepopup').dialog({
			autoOpen: true,
			width: 500,
			dialogClass: "modalfixed",
			buttons: b
		});

		// $('.modalfixed.ui-dialog').css({position:"fixed"});
		$('#sitepopup').dialog( "option", "position", ['0','0'] );


};

function MenuNewsBuild( m_id, event ){

var menu=new Array();

menu[0]='<a onclick="ajax_prep_for_edit(\'' + m_id + '\', \'' + event + '\'); return false;" href="#">' + menu_short + '</a>';

if (site_admin != '') {

	menu[1]='<a href="' + site_root + site_admin + '?mod=editnews&action=editnews&id=' + m_id + '" target="_blank">' + menu_full + '</a>';

}

if (allow_site_delete_news) {

	menu[2]='<a onclick="sendNotice (\'' + m_id + '\'); return false;" href="#">' + site_notice + '</a>';
	menu[3]='<a onclick="site_news_delete (\'' + m_id + '\'); return false;" href="#">' + site_del_news + '</a>';

}

return menu;
};

function sendNotice( id ){
	var b = {};

	b[site_act_lang[3]] = function() { 
		$(this).dialog('close');						
	};

	b[site_p_send] = function() { 
		if ( $('#site-promt-text').val().length < 1) {
			$('#site-promt-text').addClass('ui-state-error');
		} else {
			var response = $('#site-promt-text').val()
			$(this).dialog('close');
			$('#sitepopup').remove();
			$.post(site_root + 'engine/ajax/message.php', { id: id,  text: response, allowdelete: "no" },
				function(data){
					if (data == 'ok') { SITEalert(site_p_send_ok, site_info); }
				});

		}				
	};

	$('#sitepopup').remove();
					
	$('body').append("<div id='sitepopup' title='"+site_notice+"' style='display:none'><br />"+site_p_text+"<br /><br /><textarea name='site-promt-text' id='site-promt-text' class='ui-widget-content ui-corner-all' style='width:97%;height:100px; padding: .4em;'></textarea></div>");
					
	$('#sitepopup').dialog({
		autoOpen: true,
		width: 500,
		dialogClass: "modalfixed",
		buttons: b
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );

};

function AddComplaint( id, action ){
	var b = {};

	b[site_act_lang[3]] = function() { 
		$(this).dialog('close');						
	};

	b[site_p_send] = function() { 
		if ( $('#site-promt-text').val().length < 1) {
			$('#site-promt-text').addClass('ui-state-error');
		} else {
			var response = $('#site-promt-text').val()
			$(this).dialog('close');
			$('#sitepopup').remove();
			$.post(site_root + 'engine/ajax/complaint.php', { id: id,  text: response, action: action },
				function(data){
					if (data == 'ok') { SITEalert(site_p_send_ok, site_info); } else { SITEalert(data, site_info); }
				});

		}				
	};

	$('#sitepopup').remove();
					
	$('body').append("<div id='sitepopup' title='"+site_complaint+"' style='display:none'><br /><textarea name='site-promt-text' id='site-promt-text' class='ui-widget-content ui-corner-all' style='width:97%;height:100px; padding: .4em;'></textarea></div>");
					
	$('#sitepopup').dialog({
		autoOpen: true,
		width: 500,
		dialogClass: "modalfixed",
		buttons: b
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );

};

function SITEalert(message, title){

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");
	// $("body").prepend("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();							
			} 
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function SiteAlert(message, title){

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");
	 
	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();							
			} 
		}
	});
    $('.ui-dialog').css({top:"500"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function SITEalertWithFunction(message, title, func){

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");
	// $("body").prepend("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();							
				if (func) {
					func();
				}
			} 
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

// Сообщение, которое выводится при регистрации, если существует номер телефона
function SITEalertPhoneExists(message, title, phone_number){
	if (title == undefined) {
		title = '';
	}
	
	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");
	// $("body").prepend("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		buttons: {
			"К восстановлению пароля по e-mail": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();							
				document.location="/lostpassword/";
			},
			"Отправить мне пароль по SMS": function() {
				var AjaxData = {};
				AjaxData['action_name'] = 'SendNewPassSMS';
				AjaxData['phone_number'] = phone_number;
				// alert(JSON.stringify(AjaxData));

				jQuery.ajax({
					url: '/engine/ajax/runpostaction.php',
					type: 'POST',
					data: AjaxData,
					dataType : 'json',
					async: true,
					success: function(data, textStatus, xhr) {

					},
					error: function(xhr, textStatus, errorThrown) {
					}
				});

				$(this).dialog("close");
				$("#sitepopup").remove();
				SITEalertWithFunction("Ваш новый пароль будет отправлен Вам в SMS.", '', function() {
					document.location="/login/";
				});
			}
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

// Сообщение, которое выводится при добавлении товара в корзину
function SITEalertAddedToBasket(message, title){
	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");
	// $("body").prepend("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed",
		buttons: {
			"Продолжить покупку": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();							
			},
			"Перейти в корзину": function() {
				$(this).dialog("close");
				$("#sitepopup").remove();
				document.location="/showcart";
			}
		}
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function SITENoStockConfirm(message, title, callback_one, callback_two){

	var b = {};

	b[site_act_lang[1]] = function() { 
					$(this).dialog("close");
					$("#sitepopup").remove();
					if( callback_one ) callback_one();	
			    };

	b[site_act_lang[0]] = function() { 
					$(this).dialog("close");
					$("#sitepopup").remove();
					if( callback_two ) callback_two();
				};

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 500,
		dialogClass: "modalfixed",
		buttons: b
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function SITEconfirm(message, title, callback){

	var b = {};

	b[site_act_lang[1]] = function() { 
					$(this).dialog("close");
					$("#sitepopup").remove();						
			    };

	b[site_act_lang[0]] = function() { 
					$(this).dialog("close");
					$("#sitepopup").remove();
					if( callback ) callback();
				};

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"</div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 500,
		dialogClass: "modalfixed",
		buttons: b
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );
};

function SITEprompt(message, d, title, callback, allowempty){

	var b = {};

	b[site_act_lang[3]] = function() { 
					$(this).dialog("close");						
			    };

	b[site_act_lang[2]] = function() { 
					if ( !allowempty && $("#site-promt-text").val().length < 1) {
						 $("#site-promt-text").addClass('ui-state-error');
					} else {
						var response = $("#site-promt-text").val()
						$(this).dialog("close");
						$("#sitepopup").remove();
						if( callback ) callback( response );	
					}				
				};

	$("#sitepopup").remove();

	$("body").append("<div id='sitepopup' title='" + title + "' style='display:none'><br />"+ message +"<br /><br /><input type='text' name='site-promt-text' id='site-promt-text' class='ui-widget-content ui-corner-all' style='width:97%; padding: .4em;' value='" + d + "'/></div>");

	$('#sitepopup').dialog({
		autoOpen: true,
		width: 500,
		dialogClass: "modalfixed",
		buttons: b
	});

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#sitepopup').dialog( "option", "position", ['0','0'] );

	if (d.length > 0) {
		$("#site-promt-text").select().focus();
	} else {
		$("#site-promt-text").focus();
	}

};

var site_user_profile = '';
var site_user_profile_link = '';

function ShowPopupProfile( r, allowedit )
{
	var b = {};

	b[menu_profile] = function() { 
					document.location=site_user_profile_link;						
			    };

	if (user_group != 5) {

		b[menu_send] = function() { 
						document.location=site_root + 'index.php?do=pm&doaction=newpm&username=' + site_user_profile;						
				    };
	}

	if (allowedit == 1) {

		b[menu_uedit] = function() {
					$(this).dialog("close");
					window.open('' + site_root + site_admin + '?mod=editusers&action=edituser&user=' + site_user_profile + '', 'User','toolbar=0,location=0,status=0, left=0, top=0, menubar=0,scrollbars=yes,resizable=0,width=540,height=500');					
			    };

	}

	$("#siteprofilepopup").remove();

	$("body").append(r);

	$('#siteprofilepopup').dialog({
		autoOpen: true,
		show: 'fade',
		hide: 'fade',
		buttons: b,
		width: 450
	});
	
	return false;
};

function ShowProfile( name, url, allowedit )
{

	if (site_user_profile == name && document.getElementById('siteprofilepopup')) {$('#siteprofilepopup').dialog('open');return false;}

	site_user_profile = name;
	site_user_profile_link = url;

	ShowLoading('');

	$.get(site_root + "engine/ajax/profile.php", { name: name, skin: site_skin }, function(data){

		HideLoading('');

		ShowPopupProfile( data, allowedit );

	});

	
	return false;
};

function FastSearch()
{
	$('#story').attr('autocomplete', 'off');
	$('#story').blur(function(){
		 	$('#searchsuggestions').fadeOut();
	});

	$('#story').keyup(function() {
		var inputString = $(this).val();

		if(inputString.length == 0) {
			$('#searchsuggestions').fadeOut();
		} else {

			if (site_search_value != inputString && inputString.length > 3) {
				clearInterval(site_search_delay);
				site_search_delay = setInterval(function() { site_do_search(inputString); }, 600);
			}

		}
	
	});
};

function site_do_search( inputString )
{
	clearInterval(site_search_delay);

	$('#searchsuggestions').remove();

	$("body").append("<div id='searchsuggestions' style='display:none'></div>");

	$.post(site_root + "engine/ajax/search.php", {query: ""+inputString+""}, function(data) {
			$('#searchsuggestions').html(data).fadeIn().css({'position' : 'absolute', top:0, left:0}).position({
				my: "left top",
				at: "left bottom",
				of: "#story",
				collision: "fit flip"
			});
		});

	site_search_value = inputString;

};

function ShowLoading( message )
{
	if ( message )
	{
		$("#loading-layer-text").html(message);
	}
		
	var setX = ( $(window).width()  - $("#loading-layer").width()  ) / 2;
	var setY = ( $(window).height() - $("#loading-layer").height() ) / 2;
			
	$("#loading-layer").css( {
		left : setX + "px",
		top : setY + "px",
		position : 'fixed',
		zIndex : '99',
		// backgroundColor:'#3ca413'
		backgroundColor:'#5B9B6D'
	});
	
	$("#loading-layer").fadeTo(400, 1);
};

function HideLoading( message )
{
	$("#loading-layer").fadeTo(1000, 0.98, function() {
		$("#loading-layer").fadeOut(400);
	});
};

function ShowAllVotes( )
{
	if (document.getElementById('sitevotespopup')) {$('#sitevotespopup').dialog('open');return false;}

	$.ajaxSetup({
	  cache: false
	});

	ShowLoading('');

	$.get(site_root + "engine/ajax/allvotes.php?site_skin=" + site_skin, function(data){

		HideLoading('');
		$("#sitevotespopup").remove();	
	
		$("body").append( data );

		$(".sitevotebutton").button();

			$('#sitevotespopup').dialog({
				autoOpen: true,
				show: 'fade',
				hide: 'fade',
				width: 600,
				height: 150
			});

			if ($('#sitevotespopupcontent').height() > 400 ) {

				$('#sitevotespopupcontent').height(400);
				$('#sitevotespopup').dialog( "option", "height", $('#sitevotespopupcontent').height() + 40 );
				$('#sitevotespopup').dialog( "option", "position", 'center' );
			} else {

				$('#sitevotespopup').dialog( "option", "height", $('#sitevotespopupcontent').height() + 40 );
				$('#sitevotespopup').dialog( "option", "position", 'center' );

			}

	 });

	return false;
};

function fast_vote( vote_id )
{
	var vote_check = $('#vote_' + vote_id + ' input:radio[name=vote_check]:checked').val();

	ShowLoading('');

	$.get(site_root + "engine/ajax/vote.php", { vote_id: vote_id, vote_action: "vote", vote_mode: "fast_vote", vote_check: vote_check, vote_skin: site_skin }, function(data){

		HideLoading('');

		$("#site-vote_list-" + vote_id).fadeOut(500, function() {
			$(this).html(data);
			$(this).fadeIn(500);
		});

	});

	return false;
};

function AddIgnorePM( id, text ){

    SITEconfirm( text, site_confirm, function () {

		ShowLoading('');
	
		$.get(site_root + "engine/ajax/pm.php", { id: id, action: "add_ignore", skin: site_skin }, function(data){
	
			HideLoading('');
	
			SITEalert ( data, site_info );
			return false;
		
	
		});

	} );
};

function DelIgnorePM( id, text ){

    SITEconfirm( text, site_confirm, function () {

		ShowLoading('');
	
		$.get(site_root + "engine/ajax/pm.php", { id: id, action: "del_ignore", skin: site_skin }, function(data){
	
			HideLoading('');
	
			$("#site-ignore-list-" + id).html('');
			SITEalert ( data, site_info );
			return false;
		
	
		});

	} );
};
function dropdownmenu(obj, e, menucontents, menuwidth){

	if (window.event) event.cancelBubble=true;
	else if (e.stopPropagation) e.stopPropagation();

	var menudiv = $('#dropmenudiv');

	if (menudiv.is(':visible')) { clearhidemenu(); menudiv.fadeOut('fast'); return false; }

	menudiv.remove();

	$('body').append('<div id="dropmenudiv" style="display:none;position:absolute;z-index:100;width:165px;"></div>');

	menudiv = $('#dropmenudiv');

	menudiv.html(menucontents.join(""));

	if (menuwidth) menudiv.width(menuwidth);

	var windowx = $(document).width() - 30;
	var offset = $(obj).offset();

	if (windowx-offset.left < menudiv.width())
			offset.left = offset.left - (menudiv.width()-$(obj).width());

	menudiv.css( {
		left : offset.left + "px",
		top : offset.top+$(obj).height()+"px"
	});

	menudiv.fadeTo('fast', 0.9);

	menudiv.mouseenter(function(){
	      clearhidemenu();
	    }).mouseleave(function(){
	      delayhidemenu();
	});

	$(document).one("click", function() {
		hidemenu();
	});

	return false;
};

function hidemenu(e){
	$("#dropmenudiv").fadeOut("fast");
};

function delayhidemenu(){
	delayhide=setTimeout("hidemenu()",1000);
};

function clearhidemenu(){

	if (typeof delayhide!="undefined")
		clearTimeout(delayhide);
};

////////////////////////////////////////////////////////////////////////////
// Формы добавления нового плательщика и получателя

// Стандартное сообщение о нумерованной ошибке
function SITEErrorMessage(ErrorNumber) {
	// SITEalert("<p>Возникла внутренняя ошибка сервера № "
	// 	+"<span class=\"alert_error_number\" style=\"font-size: 20px; font-weight: bold; color: #d00;\">"
	// 	+ErrorNumber+"</span>.<br/>"
	// 	+"Обратитесь, пожалуйста, в техническую поддержку и сообщите номер ошибки.<br/>"
	// 	+"Приносим искренние извинения за неудобства!</p>"
	// 	+"<br/><p><a href=\"mailto:andrew@z-dama.ru?subject=Ошибка № "+ErrorNumber+"\" "
	// 	+"style=\"font-weight: bold; font-size: larger;\">"
	// 	+"Отправить e-mail в техническую поддержку</a></p>",
	// 	'Ошибка!');
	
	if (ErrorNumber != 199) {
		WriteErrorLog(ErrorNumber);
	}
}

// Записать ошибку в лог
function WriteErrorLog(error_code) {
	var FuncRes = undefined;

	var AjaxData = {};
	AjaxData['action_name'] = 'WriteErrorLog';
	AjaxData['error_code'] = error_code;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: true,
		success: function(data, textStatus, xhr) {

		},
		error: function(xhr, textStatus, errorThrown) {
			// SITEErrorMessage(199);
		}
	});
	
	return FuncRes;
}

// Установить видимость элементов по спискам id's
function SetVisibilityByIDs(prefix, suffix, VisibleIDsList, InvisibleIDsList, FastHideShow) {
	if (FastHideShow === undefined) {
		FastHideShow = false;
	}

	// Показ блоков, которые должны появиться
	for (var i = 0; i < VisibleIDsList.length; i++) {
		var Block = $('#'+prefix+VisibleIDsList[i]+suffix);

		var BlockDisplayProperty = Block.css('display');
		if ((BlockDisplayProperty == 'none')||(BlockDisplayProperty === undefined)) {
			if (FastHideShow) {
				Block.css('display', 'block');
			}
			else {
				Block.show(500);
			}
		}
	}

	// Скрытие блоков, которые должны исчезнуть
	for (var i = 0; i < InvisibleIDsList.length; i++) {
		var Block = $('#'+prefix+InvisibleIDsList[i]+suffix);

		var BlockDisplayProperty = Block.css('display');
		if ((BlockDisplayProperty != 'none')||(BlockDisplayProperty === undefined)) {
			if (FastHideShow) {
				Block.css('display', 'none');
			}
			else {
				Block.hide(500);
			}
		}
	}
}

// Устанавливает видимость блоков диалоговых окон при создании и редактировании
// плательщика или получателя
// OptionsObj: payer_type, equal_addr
function SetDialogBlocksVisibility(action, record_type, OptionsObj) {
	if (OptionsObj == undefined) {
		OptionsObj = {}
	}

	// Префикс в id's блоков
	var BlocksPrefix = undefined;
	if (action == 'new') {
		if ((record_type == 1)||(record_type == 3)) {
			BlocksPrefix = 'new_payer_';
		}
		else if (record_type == 2) {
			BlocksPrefix = 'new_receiver_';
		}
	}
	else if (action == 'edit') {
			BlocksPrefix = 'edit_payer_';
	}

	// Тип плательщика
	// если не задан
	if (OptionsObj['payer_type'] === undefined) {
		return false;
	}
	else {
		PayerType = OptionsObj['payer_type'];
	}

	// Список видимых и невидимых блоков
	var VisibleBlocks = [];
	var InvisibleBlocks = [];

	// Физическое лицо
	if (PayerType == 1) {
		VisibleBlocks.push('last_name', 'name', 'second_name', 'real_addr');
		
		InvisibleBlocks.push('off_addr', 'inn', 'kpp', 'ogrn', 'property_type', 'equal_addr');
		
		$('#'+BlocksPrefix+'name_block label').html('Имя: <span class="requiring_star">*</span>');
	}
	// Юридическое лицо
	else if (PayerType == 2) {
		VisibleBlocks.push('name', 'off_addr', 'inn', 'kpp', 'ogrn');
		VisibleBlocks.push('property_type', 'equal_addr');
		
		InvisibleBlocks.push('last_name', 'second_name');
		
		$('#'+BlocksPrefix+'name_block label').html('Название: <span class="requiring_star">*</span>');
	}
	// ИП
	else if (PayerType == 3) {
		VisibleBlocks.push('last_name', 'name', 'second_name', 'inn', 'ogrn', 'off_addr');

		InvisibleBlocks.push('kpp', 'property_type');

		$('#'+BlocksPrefix+'name_block label').html('Имя: <span class="requiring_star">*</span>');
	}
	
	// Юридический и фактический адрес
	if ((record_type == 1)||(record_type == 3)) {
		if ((PayerType == 2)||(PayerType == 3)) {
			VisibleBlocks.push('off_addr', 'equal_addr');
	
			// Совпадение адресов
			var EqualAddr = undefined;
			// если не задано
			if (OptionsObj['equal_addr'] !== undefined) {
				EqualAddr = OptionsObj['equal_addr'];
			}

			if (EqualAddr !== undefined) {
				// если адреса совпадают
				if (EqualAddr) {
					InvisibleBlocks.push('real_addr');
				}
				else {
					VisibleBlocks.push('real_addr');
				}
			}
			else {
				VisibleBlocks.push('real_addr');
			}
		}
		else if (PayerType == 1) {
			InvisibleBlocks.push('off_addr', 'equal_addr');
		}
	}
	else if (record_type == 2) {
		InvisibleBlocks.push('off_addr', 'equal_addr');
		
		VisibleBlocks.push('real_addr');
	}

	// Номер телефона
	if ((record_type == 2)||(record_type == 3)) {
		VisibleBlocks.push('phone_number');
	}
	else if (record_type == 1) {
		InvisibleBlocks.push('phone_number');
	}

	SetVisibilityByIDs(BlocksPrefix, '_block', VisibleBlocks, InvisibleBlocks);
}

// Диалог правки плательщика
function EditPayerDialog(title, callback_ok, callback_cancel, callback_close){
	// ID элемента
	var DialogElementID = 'edit_payer_dialog';

	$('#'+DialogElementID).dialog({
		autoOpen: true,
		width: 470,
		dialogClass: "modalfixed edit_payer",
		buttons: {
			"Отмена": function() { 
				if (callback_cancel) {
					callback_cancel();
				}
				$(this).dialog("close");
			},
			"Сохранить": function() { 
				var SaveFuncRes = SaveEditedPayer();

				if (SaveFuncRes == 'ok') {
					if (callback_ok) {
						callback_ok();
					}
					$(this).dialog("close");
				}
				else if (SaveFuncRes == 'error') {
				}
			}
		},
		close: function(event, ui) {
			if (callback_close) {
				callback_close();
			};
		}
	});

	// Снимаем галочку "Совпадает с юридическим адресом"
	$('#edit_payer_equal_addr').prop('checked', false);

	// Заголовок диалогового окна
	$('#'+DialogElementID).dialog('option', 'title', title);

	// $('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#'+DialogElementID).dialog( "option", "position", ['0','0'] );
};

// Выбор типа плательщика в диалоге правки плательщика
function OnEditPayerTypeChange(FastHideShow) {
	// Выбранный тип плательщика
	var NewPayerType = $('input[name="edit_payer_type"]:checked').val();
	// Тип записи
	var RecordType = $('#edit_payer_record_type').val();

	// Физическое лицо
	if (NewPayerType == 1) {

		if (FastHideShow) {
			$('#edit_payer_real_addr_block, #edit_payer_last_name_block, #edit_payer_second_name_block').css('display', 'block');
			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_kpp_block, #edit_payer_ogrn_block, #edit_payer_property_type_block, #edit_payer_off_addr_block').css('display', 'none');
		}
		else {
			$('#edit_payer_real_addr_block, #edit_payer_last_name_block, #edit_payer_second_name_block').show(500);
			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_kpp_block, #edit_payer_ogrn_block, #edit_payer_property_type_block, #edit_payer_off_addr_block').hide(500);
		}

		$('#edit_payer_last_name_block label').html('Фамилия:<span class="requiring_star">*</span>');
		$('#edit_payer_name_block label').html('Имя:<span class="requiring_star">*</span>');
		$('#edit_payer_second_name_block label').html('Отчество:<span class="requiring_star">*</span>');
	}
	// Юридические лицо
	else if (NewPayerType == 2) {
		// var VisibleBlocksSelector = '#edit_payer_inn_block, #edit_payer_kpp_block, #edit_payer_ogrn_block, #edit_payer_property_type_block, #edit_payer_real_addr_block, #edit_payer_off_addr_block';
		// // если плательщик
		// if (RecordType == 1) {
		// 	VisibleBlocksSelector = VisibleBlocksSelector + ', #edit_payer_off_addr_block';
		// }
		// // если получатель
		// if (RecordType == 2) {
		// 	$('#edit_payer_off_addr_block').css('display', 'none');
		// }

		if (FastHideShow) {
			if (RecordType == 1) {
				// Галочка "Совпадает с юридическим адресом"
				if ($('#edit_payer_equal_addr').is(':checked')) {
					$('#edit_payer_real_addr_block').css('display', 'none');
				}
				else {
					$('#edit_payer_real_addr_block').css('display', 'block');				
				}
			}
			else if (RecordType == 2) {
				$('#edit_payer_real_addr_block').css('display', 'block');
			}


			$('#edit_payer_last_name_block, #edit_payer_second_name_block').css('display', 'none');
			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_kpp_block, #edit_payer_ogrn_block, #edit_payer_property_type_block, #edit_payer_off_addr_block').css('display', 'block');
		}
		else {
			if (RecordType == 1) {
				// Галочка "Совпадает с юридическим адресом"
				if ($('#edit_payer_equal_addr').is(':checked')) {
					$('#edit_payer_real_addr_block').hide(500);
				}
				else {
					$('#edit_payer_real_addr_block').show(500);
				}
			}
			else if (RecordType == 2) {
				$('#edit_payer_real_addr_block').show(500);
			}

			$('#edit_payer_last_name_block, #edit_payer_second_name_block').hide(500);
			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_kpp_block, #edit_payer_ogrn_block, #edit_payer_property_type_block, #edit_payer_off_addr_block').show(500);
		}

		$('#edit_payer_name_block label').html('Наименование:<span class="requiring_star">*</span>');
	}
	// ИП
	else if (NewPayerType == 3) {
		if (FastHideShow) {
			if (RecordType == 1) {
				// Галочка "Совпадает с юридическим адресом"
				if ($('#edit_payer_equal_addr').is(':checked')) {
					$('#edit_payer_real_addr_block').css('display', 'none');
				}
				else {
					$('#edit_payer_real_addr_block').css('display', 'block');				
				}
			}
			else if (RecordType == 2) {
				$('#edit_payer_real_addr_block').css('display', 'block');
			}

			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_off_addr_block, #edit_payer_ogrn_block, #edit_payer_last_name_block, #edit_payer_second_name_block').css('display', 'block');
			$('#edit_payer_kpp_block, #edit_payer_property_type_block').css('display', 'none');
		}
		else {
			if (RecordType == 1) {
				// Галочка "Совпадает с юридическим адресом"
				if ($('#edit_payer_equal_addr').is(':checked')) {
					$('#edit_payer_real_addr_block').hide(500);
				}
				else {
					$('#edit_payer_real_addr_block').show(500);
				}
			}
			else if (RecordType == 2) {
				$('#edit_payer_real_addr_block').show(500);
			}

			$('#edit_payer_equal_addr_block, #edit_payer_inn_block, #edit_payer_off_addr_block, #edit_payer_ogrn_block, #edit_payer_last_name_block, #edit_payer_second_name_block').show(500);
			$('#edit_payer_kpp_block, #edit_payer_property_type_block').hide(500);
		}

		$('#edit_payer_last_name_block label').html('Фамилия:<span class="requiring_star">*</span>');
		$('#edit_payer_name_block label').html('Имя:<span class="requiring_star">*</span>');
		$('#edit_payer_second_name_block label').html('Отчество:<span class="requiring_star">*</span>');
	}
	
	// если получатель
	if (RecordType == 2) {
		$('#edit_payer_off_addr_block').css('display', 'none');
		$('#edit_payer_equal_addr_block').css('display', 'none');

		$('#edit_payer_real_addr_block').css('display', 'block');
	}

	// Номер телефона
	if (RecordType == 1) {
		$('#edit_payer_phone_number_block').css('display', 'none');
	}
	else if ((RecordType == 2) || (RecordType == 3)) {
		$('#edit_payer_phone_number_block').css('display', 'block');
	}
}

// Заполнить форму данными плательщика
// PayerObj -- в формате json
function FillInPayerForm(PayerObj) {
	var RetObj = {};
	RetObj['record_type'] = PayerObj['record_type'];
	RetObj['id'] = PayerObj['id'];

	// id
	$('#edit_payer_id').val(PayerObj['id']);

	// Тип записи
	if (PayerObj['record_type'] == "1") {
		$('#edit_payer_choose_payer_type_p').html('Ваш тип плательщика:');
		$('#edit_payer_phone_number_block').css('display', 'none');
	}
	else if (PayerObj['record_type'] == "2") {
		$('#edit_payer_choose_payer_type_p').html('Ваш тип получателя:');
		$('#edit_payer_phone_number_block').css('display', 'block');
	}

	// Тип записи
	$('#edit_payer_record_type').val(PayerObj['record_type']);

	// Тип плательщика
	$('#edit_payer_type_value_'+PayerObj['payer_type']).prop('checked', true);
	OnEditPayerTypeChange(true);

	// Тип записи
	var RecordType = $('#edit_payer_record_type').val();
	if ((RecordType == 1)&&(PayerObj['payer_type'] != 1)) {
		$('#edit_payer_equal_addr').parent().css('display', 'block');
		$('#edit_payer_equal_addr').prop('checked', false);
	}
	else if (RecordType == 2) {
		$('#edit_payer_equal_addr').parent().css('display', 'none');
	}

	// Включаем блок с фактическим адресом
	$('#edit_payer_real_addr_block').css('display', 'block');

	// ИНН
	$('#edit_payer_inn').val(PayerObj['inn']);
	// КПП
	$('#edit_payer_kpp').val(PayerObj['kpp']);
	// ОГРН
	$('#edit_payer_ogrn').val(PayerObj['ogrn']);

	// Форма собственности
	$('#edit_payer_property_type_block input[type="radio"]').prop('checked', false);
	$('#edit_payer_property_type_value_'+PayerObj['property_type']).prop('checked', true);

	// Наименование/ФИО
	$('#edit_payer_last_name').val(PayerObj['last_name']);
	$('#edit_payer_name').val(PayerObj['name']);
	$('#edit_payer_second_name').val(PayerObj['second_name']);

	// Фактический адрес
	$('#edit_payer_off_addr_block input[type="text"], #edit_payer_real_addr_block input[type="text"]').each(function() {
		var InputName = $(this).attr('name');
		var KeyName = InputName.replace('edit_payer_', '');

		$('input[name="'+InputName+'"]').val(PayerObj[KeyName]);
	});

	// Телефон
	$('#edit_payer_phone_number').val(PayerObj['phone_number']);
	
	InitDialogAutocomplete('edit_payer_off_addr_');
	InitDialogAutocomplete('edit_payer_real_addr_');

	return RetObj;
}

// Выгрузить из формы данные плательщика
function ReadPayerForm() {
	var PayerObj = {};

	// id
	PayerObj['id'] = $('#edit_payer_id').val();
	// Тип записи
	PayerObj['record_type'] = $('#edit_payer_record_type').val();

	// Тип плательщика
	PayerObj['payer_type'] = $('input[name="edit_payer_type"]:checked').val();

	// ИНН
	PayerObj['inn'] = $('#edit_payer_inn').val();//.replace('"', '\\"');
	// КПП
	PayerObj['kpp'] = $('#edit_payer_kpp').val();
	// ОГРН
	PayerObj['ogrn'] = $('#edit_payer_ogrn').val();

	// Форма собственности
	PayerObj['property_type'] = $('input[name="edit_payer_property_type"]:checked').val();

	// Фамилия
	PayerObj['last_name'] = $('#edit_payer_last_name').val();//.val().replace('"', '\\"');
	// Наименование/ФИО
	PayerObj['name'] = $('#edit_payer_name').val();//.val().replace('"', '\\"');
	// Отчество
	PayerObj['second_name'] = $('#edit_payer_second_name').val();//.val().replace('"', '\\"');

	// Фактический адрес
	$('#edit_payer_off_addr_block input[type="text"], #edit_payer_real_addr_block input[type="text"]').each(function() {
		var InputName = $(this).attr('name');
		var KeyName = InputName.replace('edit_payer_', '');

		PayerObj[KeyName] = $('input[name="'+InputName+'"]').val();
	});

	// Телефон
	PayerObj['phone_number'] = $('#edit_payer_phone_number').val();

	return PayerObj;
}

// Загрузка данных плательщика в диалоговое окно
function LoadPayerToDialog(payer_id, func_ok, dialog_action) {
	if (!dialog_action) {
		dialog_action = 'edit';
	}
	
	var AjaxData = {};
	AjaxData['action_name'] = 'GetPayerInfo';
	AjaxData['payer_id'] = payer_id;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
			else if (data['operation_result'] == 'ok'){
				if (dialog_action == 'edit') {
					// alert(JSON.stringify(data['payer_obj']));
					// alert(data['payer_obj']['record_type']);
					// var EditFormOptions = FillInPayerForm(jQuery.parseJSON(data['payer_obj']));
					var EditFormOptions = FillInPayerForm(data['payer_obj']);
					var EditFormTitle = '';
					if (EditFormOptions['record_type'] == 1) {
						EditFormTitle = 'Изменить данные плательщика';
					}
					else if (EditFormOptions['record_type'] == 2) {
						EditFormTitle = 'Изменить данные получателя';
					}
					else if (EditFormOptions['record_type'] == 3) {
						EditFormTitle = 'Изменить данные плательщика-получателя';
					}
					EditPayerDialog(EditFormTitle, func_ok, null, null);
				}
				if (dialog_action == 'delete') {

					// alert('id = ' + data['payer_obj']['id']);
					// alert('last_name = ' + data['payer_obj']['last_name']);
					// alert('name = ' + data['payer_obj']['name']);
					// alert('second_name = ' + data['payer_obj']['second_name']);
					// alert('payer_type = ' + data['payer_obj']['payer_type']);

					var PayerObj = data['payer_obj'];

					var PayerName = undefined;
					if (PayerObj['payer_type'] == 1) {
						PayerName = PayerObj['last_name'] + ' ' + PayerObj['name'] + ' ' + PayerObj['second_name'];
					}
					else if (PayerObj['payer_type'] == 2) {
						var PropertyType = undefined;

						if (PayerObj['property_type'] == 1) {
							PropertyType = 'ОАО';
						}
						else if (PayerObj['property_type'] == 2) {
							PropertyType = 'ЗАО';
						}
						else if (PayerObj['property_type'] == 3) {
							PropertyType = 'ООО';
						}

						PayerName = PropertyType + ' "' + PayerObj['name'] + '"';
					}
					else if (PayerObj['payer_type'] == 3) {
						PayerName = 'ИП ' + PayerObj['last_name'] + ' ' + PayerObj['name'] + ' ' + PayerObj['second_name'];
					}

					var ConfirmMessage = undefined;
					if (PayerObj['record_type'] == 1) {
						ConfirmMessage = 'Вы действительно хотите удалить плательщика "';
					}
					else if (PayerObj['record_type'] == 2) {
						ConfirmMessage = 'Вы действительно хотите удалить получателя "';
					}
					else if (PayerObj['record_type'] == 3) {
						ConfirmMessage = 'Вы действительно хотите удалить плательщика-получателя "';
					}



					SITEconfirm(ConfirmMessage + PayerName + '"?',
						'Удалить',
						function() {
							var DeleteResult = DeletePayer(PayerObj['id']);
							if (DeleteResult == 'ok') {
								if (func_ok) {
									func_ok();
								}
								LoadExistingPayers($('div.existing_payers'), null, 1);
								LoadExistingPayers($('div.existing_receivers'), null, 2);
							}
							else if (DeleteResult == 'error') {
								SITEalert('Удаление не удалось.', 'Ошибка!');
							}
						}
					);
				}
			}

		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(101);
		},
		complete: function(xhr, textStatus) {
		}
	});
}

// Загрузить плательщиков в личный кабинет
function LoadPayersToUserProfile() {
	// alert(123);
	LoadExistingPayers($('#user_profile_payers_list'), null, 1, 'user_profile');
}
// Загрузить получателей в личный кабинет
function LoadReceiversToUserProfile() {
	LoadExistingPayers($('#user_profile_receivers_list'), null, 2, 'user_profile');
}
// Загрузить плательщиков и получателей в личный кабинет
function LoadPayersAndReceiversToUserProfile() {
	LoadPayersToUserProfile();
	LoadReceiversToUserProfile();
}

// Изменить плательщика/получателя
function EditPayer(payer_id, func_ok) {
	LoadPayerToDialog(payer_id, func_ok);
}

// Удалить плательщика/получателя
function DeletePayerDialog(payer_id, func_ok) {
	LoadPayerToDialog(payer_id, func_ok, 'delete');
}

// Выбираем плательщика/получателя по его id
function SelectPayerByID(payer_id, record_type) {
	if (!record_type) {
		record_type = 1;
	}

	if (record_type == 1) {
		PayerWord = 'payer';
	}
	else if (record_type == 2) {
		PayerWord = 'receiver';
	}

	$('#'+PayerWord+'_'+payer_id).prop('checked', true);
}

// Действия, выполняемые на странице корзины, если пользователь не залогинен
function ShowcartActionsOnAnonymousUser() {
	$('#showcart_tab_payer').hide();
	$('#showcart_tab_receiver').hide();
}



// Получить всех имеющихся плательщиков
function LoadExistingPayers(PayersListElement, select_id, record_type, DisplayMode) {
	if (!record_type) {
		record_type = 1;
	}
	
	var AjaxData = {};
	AjaxData['record_type'] = record_type;
	AjaxData['action_name'] = 'GetAllPayers';
	// alert(AjaxData['record_type']);

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
			else if (data['operation_result'] == 'login_error'){
				ShowcartActionsOnAnonymousUser();
			}
			else if (data['operation_result'] == 'ok'){
				PayersListElement.empty();

				var record_type_word = '';
				if (record_type == 1) {
					record_type_word = 'payer';
				}
				else if (record_type == 2) {
					record_type_word = 'receiver';
				}

				for (var i = 0; i < data['payers'].length; i++) {
					var id = data['payers'][i]['id'];
					var name = data['payers'][i]['name'];
					var addr = data['payers'][i]['real_address'];
					var discount_sum = data['payers'][i]['discount_sum'];

					if (DisplayMode == 'user_profile') {
						var FuncName = '';
						// if (record_type == 1) {
						// 	FuncName = 'LoadPayersToUserProfile';
						// }
						// else if (record_type == 2) {
						// 	FuncName = 'LoadReceiversToUserProfile';
						// }
						FuncName = 'LoadPayersAndReceiversToUserProfile';

						PayersListElement.append('\
						<li>\
							<a href="" onclick="EditPayer('+id+', '+FuncName+'); return false;">' + name + '</a>\
							<span>(' + addr + ')</span>\
							<a href="" onclick="DeletePayerDialog('+id+', LoadPayersAndReceiversToUserProfile); return false;">Удалить</a>\
						</li>');
					}
					else {
						PayersListElement.append('\
						<p>\
							<input id="'+record_type_word+'_' + id + '" type="radio" name="'+record_type_word+'_select" value="' + id + '">\
							<label for="'+record_type_word+'_' + id + '">' + name + ' (' + addr + ').</label>\
							<a href="#" onclick="EditPayer('+id+'); return false;">Изменить</a>\
							/ <a href="#" onclick="DeletePayerDialog('+id+'); return false;">Удалить</a>\
						</p>');
					}
				}

				// Выбор плательщика
				$('input[name="'+record_type_word+'_select"]:radio').change(function() {
					// Выбранный плательщик
					var CurrentPayer = $('input[name="'+record_type_word+'_select"]:checked').val();

					if (CurrentPayer == 0) {
						$('#new_'+record_type_word+'_pane').show(500);
					}
					else {
						$('#new_'+record_type_word+'_pane').hide(500);
					}
				});

				// Для группы "Офис" не выбираем
				if (data['user_group'] != '11') {
					if (select_id) {
						SelectPayerByID(select_id, record_type);
					}
					else {
						// Выбираем первый элемент
						if (data['payers'].length == 0) {
							$('#'+record_type_word+'_0').prop('checked', true);
							$('#new_'+record_type_word+'_pane').show(500);
						}
						else if (data['payers'].length > 0) {
							SelectPayerByID(data['payers'][0]['id'], record_type);
						}
					}
				}
				
				// Есть ли плательщик-физ.лицо
				if (data['has_payer_type1'] == 1) {
					$('#new_payer_type_value_1_p').css('display', 'none');
					$('#new_payer_type_value_1').prop('checked', false);
				}
				else {
					$('#new_payer_type_value_1_p').css('display', 'block');
				}
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(102);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);

				var payer_id = 0;
				var funded = 0;
				var delivery = 0;
				
				//переключение плательщика
				/*$( 'input:radio[name="payer_select"]' ).change(function () {
				
					if($('#funded').is(':checked')) funded = 1;

					payer_id = $('input:radio[name="payer_select"]:checked').val();

						$.get(site_root + "engine/ajax/get.discount.php", { payer_id: payer_id, funded:funded }, function(data){
	
								$("#discounttype").html(data.discounttype);
								$("#ordercounter").html(data.ordercounter);
								$("#discountsumm").html(data.discountsumm);	
								$("#realsumm").html(data.realsumm);
								$("#realsummtop").html(data.realsumm);									

						}, "json");
					
				});	*/					

				
				// if($('#funded').is(':checked')){

				// 	funded = 1;
				// 	payer_id = $('input:radio[name="payer_select"]:checked').val();
						
				// 	$.get(site_root + "engine/ajax/get.discount.php", { payer_id: payer_id, funded:funded }, function(data){
		
				// 				$("#discounttype").html(data.discounttype);
				// 				$("#ordercounter").html(data.ordercounter);
				// 				$("#discountsumm").html(data.discountsumm);		
				// 				$("#realsumm").html(data.realsumm);	
				// 				$("#realsummtop").html(data.realsumm);								

				// 	}, "json");					
				
				// }else{

				// 	funded = 0;
				// 	payer_id = 0;
					
				// 	$.get(site_root + "engine/ajax/get.discount.php", { payer_id: payer_id, funded:funded }, function(data){
		
				// 				$("#discounttype").html(data.discounttype);
				// 				$("#ordercounter").html(data.ordercounter);
				// 				$("#discountsumm").html(data.discountsumm);		
				// 				$("#realsumm").html(data.realsumm);	
				// 				$("#realsummtop").html(data.realsumm);								

				// 	}, "json");					
				
				// } 
				
		}
	});
	
	
}

// Сохранить редактируемого плательщика
function SaveEditedPayer() {
	// alert(JSON.stringify(ReadPayerForm()));

	var FuncRes = undefined;
	
	// Если поставлена галочка "Совпадает с юридическим адресом"
	if ($('#edit_payer_equal_addr').is(':checked')) {
		$('#edit_payer_off_addr_block input[type="text"]').each(function() {
			var InputName = $(this).attr('name');
			var InputName2 = InputName.replace('edit_payer_off_addr_', 'edit_payer_real_addr_');

			$('input[name="'+InputName2+'"]').val($('input[name="'+InputName+'"]').val());
		});
	}

	var AjaxData = ReadPayerForm();
	AjaxData['action_name'] = 'SavePayer';
	// alert(JSON.stringify(AjaxData));

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {

			if (data["operation_result"] == 'success') {
				var record_type_words = ['payer', 'receiver', 'all'];

				// Загрузить список повторно
				if (data['record_type'] == 3) {
					LoadExistingPayers($('div.existing_payers'), data['payer_id'], 1);
					LoadExistingPayers($('div.existing_receivers'), data['payer_id'], 2);
				}
				else {
					var PayersListElement = $('div.existing_'+record_type_words[data['record_type']-1]+'s');
					LoadExistingPayers(PayersListElement, data['payer_id'], data['record_type']);
				}

				FuncRes = 'ok';
			}
			else if (data["operation_result"] == 'error') {
				// alert('Errors: ' + data['errors']);

				// Заголовок диалогового окна с сообщением об ошибках
				var ErrorsCaption = '';
				if (data['record_type'] == 1) {
					ErrorsCaption = 'Ошибки при заполнении плательщика!'
				}
				else if (data['record_type'] == 2) {
					ErrorsCaption = 'Ошибки при заполнении получателя!'
				}

				SITEalert(data["errors"], ErrorsCaption);				

				FuncRes = 'error';
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(103);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
	
	return FuncRes;
}

// Сохранить редактируемого плательщика
function DeletePayer(payer_id, callback_ok) {
	var FuncRes = undefined;

	var AjaxData = {};
	AjaxData['action_name'] = 'DeletePayer';
	AjaxData['id'] = payer_id;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {

			if (data["operation_result"] == 'success') {
				if (callback_ok) {
					callback_ok();
				}

				FuncRes = 'ok';
			}
			else if (data["operation_result"] == 'error') {
				FuncRes = 'error';
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(104);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
	
	return FuncRes;
}

// Получить данные о новом плательщике, введённые пользователем
function GetAddPayerPaneData(record_type) {
	if (record_type == 1) {
		record_type_word = 'payer';
	}

	else if (record_type == 2) {
		record_type_word = 'receiver';
	}

	// Возвращаемый словарь
	var Ret = {};

	// Выбираем значения из radio input'ов
	$('#new_'+record_type_word+'_pane input[type="radio"]:checked').each(function(){
		Ret[$(this).attr('name')] = $(this).val();
	});
	// Выбираем значения из текстовых input'ов
	$('#new_'+record_type_word+'_pane input[type="text"]').each(function(){
		Ret[$(this).attr('name')] = $(this).val();
	});
	// Выбираем значения из checkbox'ов
	$('#new_'+record_type_word+'_pane input[type="checkbox"]').each(function(){
		Ret[$(this).attr('name')] = $(this).is(':checked');
	});

	return Ret;
}

// Очистка полей ввода данных плательщика или получателя
function CleanInputs(InputBlockElement) {
	// Сбрасываем radio input'ы
	$(InputBlockElement).find('input[type="radio"]').prop('checked', false);
	// Сбрасываем checkbox'ы
	$(InputBlockElement).find('input[type="checked"]').prop('checked', false);
	// Сбрасываем текстовые input'ы
	$(InputBlockElement).find('input[type="text"]').val('');
	// Показать скрытые div'ы
	$(InputBlockElement).children('div').css('display', 'block');
}

function SubmitAddPayer(record_type) {
	if (!record_type) {
		record_type = 1;
	}

	if (record_type == 1) {
		if ($('#new_payer_is_receiver').is(':checked')) {
			record_type = 3;
		}
	}

	if ((record_type == 1) || (record_type == 3)) {
		record_type_word = 'payer';

		if ($('#new_payer_equal_addr').is(':checked')) {
			$('#new_payer_off_addr_block input[type="text"]').each(function() {
				var InputName = $(this).attr('name');
				var InputName2 = InputName.replace('new_payer_off_addr_', 'new_payer_real_addr_');

				$('input[name="'+InputName2+'"]').val($('input[name="'+InputName+'"]').val());
			});
		}
	}
	else if (record_type == 2) {
		record_type_word = 'receiver';
	}
	
	var FuncRes = undefined;

	AjaxData = GetAddPayerPaneData(record_type);
	// alert(JSON.stringify(AjaxData));
	AjaxData['record_type'] = record_type;
	AjaxData['action_name'] = 'AddPayer';

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			// $('p#debug_sql').text(data["sql"]);

			if (data["operation_result"] == 'success') {

				// Загрузить список повторно
				if (record_type == 3) {
					LoadExistingPayers($('div.existing_payers'), data['payer_id'], 1);
					LoadExistingPayers($('div.existing_receivers'), data['payer_id'], 2);
				}
				else {
					var PayersListElement = $('div.existing_'+record_type_word+'s');
					LoadExistingPayers(PayersListElement, data['payer_id'], record_type);
				}
				
				// Спрятать форму создания плательщика
				$('#new_'+record_type_word+'_pane').hide(500);

				// FuncRes = {'payer_id': data['payer_id'], 'order_payer_id': data['order_payer_id']};
				FuncRes = {'payer_id': data['payer_id'], 'record_type': record_type};

				CleanInputs($('#new_'+record_type_word+'_pane'));
			}
			else if (data["operation_result"] == 'login_error') {
				FuncRes = 'login_error';
			}
			else if (data["operation_result"] == 'error') {
				// Заголовок диалогового окна с сообщением об ошибках
				var ErrorsCaption = '';
				if (record_type == 1) {
					ErrorsCaption = 'Ошибки при заполнении плательщика!'
				}
				else if (record_type == 2) {
					ErrorsCaption = 'Ошибки при заполнении получателя!'
				}

				var PreMessage = 'Уважаемый пользователь!<br/>'
					+ 'Заполните, пожалуйста, информацию на вкладках <b>Плательщик</b> и <b>Получатель</b>.';
					// + 'Информация на вкладках <b>Плательщик</b> и <b>Получатель</b> должна быть заполнена полностью.';
				SITEalert(PreMessage+data["errors"], ErrorsCaption);				

				FuncRes = -1;
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(105);

			FuncRes = 'error';
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
	
	return FuncRes;
}

function WritePayerReceiverToSession(payer_id, receiver_id) {
	var FuncRes = undefined;

	AjaxData = {};
	AjaxData['payer_id'] = payer_id;
	AjaxData['receiver_id'] = receiver_id;
	AjaxData['action_name'] = 'WritePayerReceiverToSession';

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'ok') {
				FuncRes = 'success';
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(106);

			FuncRes = 'error';
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});

	return FuncRes;
}

function IsUserLoggedIn() {
	var FuncRes = undefined;

	AjaxData = {};
	AjaxData['action_name'] = 'IsLoggedIn';

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'ok') {
				if (data['result'] == 1) {
					FuncRes = true;
				}
				else if (data['result'] == 0) {
					FuncRes = false;
				}
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(107);

			FuncRes = 'error';
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});

	return FuncRes;
}

// Валидация форм плательщика и получателя
function ValidatePayerReceiverForms_old(campaign) {
	var PayerOK = false;
	var ReceiverOK = false;
	
	if (!campaign) {
		campaign = 0;
	}
	
	// Выбранный плательщик
	var SelectedPayerID = $('input[name="payer_select"]:checked').val();
	// Выбранный получатель
	var SelectedReceiverID = $('input[name="receiver_select"]:checked').val();
	
	if (SelectedPayerID > 0) {
		PayerOK = true;
	}
	else {
		var a = SubmitAddPayer(1);
		if (a == 'error') {

		}
		else if (a == 'login_error') {
			var message = "Уважаемый пользователь!<br /> <br /><b>Оформление заказа возможно только после регистрации и авторизации на сайте.</b><br /><br /><b>Для входа в личный кабинет кликните на \"Вход для клиентов\" в правом верхнем углу сайта</b> или перейдите к <a href=\"/register/\"><b>Регистрации</b></a> на сайте.";
			SITEalert(message, 'Внимание!');
			return false;
		}
		else if (a.payer_id > 0) {
			SelectedPayerID = a.payer_id;
			if (a.record_type == 3) {
				SelectedReceiverID = SelectedPayerID;
			}
			PayerOK = true;
		}
	}

	if (SelectedReceiverID > 0) {
		ReceiverOK = true;
	}
	else {
		var a = SubmitAddPayer(2);
		if (a == 'error') {

		}
		else if (parseInt(a.payer_id) > 0) {
			SelectedReceiverID = a.payer_id;
			ReceiverOK = true;
		}
	}

 	if (PayerOK && ReceiverOK) {
		var SessionSaveResult = WritePayerReceiverToSession(SelectedPayerID, SelectedReceiverID);
		if (SessionSaveResult == 'success') {
			SubmitOrder(campaign);
			return true;
		}
	}
	else {
		// alert(":(");
		return false;
	}
}

function ValidatePayerReceiverForms() {
	
}

// Копирование значения текстового input'а
function CopyInputTextValue(src_name, dest_name) {
	$('input[name="'+dest_name+'"]').val($('input[name="'+src_name+'"]').val());
}

// Копирование значения RadioButton'а
function CopyRadioValueByName(src_name, dest_name) {
	var CurValue = $('input[name="'+src_name+'"]:checked').val();
	if (CurValue) {
		$('input[name="'+dest_name+'"][value="'+CurValue+'"]').prop('checked', true);
	}
}

// Копирование видимости
function CopyElementVisibility(src_selector, dest_selector) {
	$(dest_selector).css('display', $(src_selector).css('display'));
}

// Копирование данных из формы плательщика в форму получателя
function CopyPayerToReceiver() {
	// Копируем выбор типа плательщика
	CopyRadioValueByName('new_payer_type', 'new_receiver_type');
	// Копируем выбор формы собственности
	CopyRadioValueByName('new_payer_property_type', 'new_receiver_property_type');

	// Копируем значения текстовых input'ов
	$('#new_payer_pane input[type="text"]').each(function() {
		var SrcInputName = $(this).attr('name');
		if (SrcInputName.indexOf('new_payer_off_addr') != 0) {
			CopyInputTextValue(SrcInputName, SrcInputName.replace('_payer_', '_receiver_'));
		}
	});

	// Копируем видимость элементов
	CopyElementVisibility('#new_payer_inn_block', '#new_receiver_inn_block');
	CopyElementVisibility('#new_payer_kpp_block', '#new_receiver_kpp_block');
	CopyElementVisibility('#new_payer_ogrn_block', '#new_receiver_ogrn_block');
	CopyElementVisibility('#new_payer_property_type_block', '#new_receiver_property_type_block');
	CopyElementVisibility('#new_payer_name_block', '#new_receiver_name_block');
	CopyElementVisibility('#new_payer_real_addr_block', '#new_receiver_real_addr_block');

	// Выбираем пункт "Создать нового получателя" на вкладке "Получатель"
	SelectPayerByID(0, 2);
	$('#new_receiver_pane').show(500);

	// Переходим на вкладку "Получатель"
	// $('div.cart_tabs ul.tabs').data('tabs').click(3);
}

// Автозаполнение по почтовому индексу
function AutoFillByPostIndex(InputPrefix, PostIndex) {
	AjaxData = {};
	AjaxData['action_name'] = 'GetAddressByPostIndex';
	AjaxData['post_index'] = PostIndex;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			// alert(JSON.stringify(data));
			if (data['operation_result'] == 'ok') {
				// if (data['region']) {
					$('#'+InputPrefix+'region').val(data['region']);
				// }
				// if (data['district']) {
					$('#'+InputPrefix+'district').val(data['district']);
				// }
				// if (data['city']) {
					// if (!$('#'+InputPrefix+'city').val()) {
						$('#'+InputPrefix+'city').val(data['city']);
					// }
				// }
			}
			else if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			// SITEErrorMessage(112);
			WriteErrorLog(112);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
}

// Автозаполнение по почтовому индексу
function AutoFillByPostIndex2(InputPrefix, PostIndex) {
	AjaxData = {};
	AjaxData['action_name'] = 'GetAddressByPostIndex2';
	AjaxData['post_index'] = PostIndex;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: true,
		success: function(data, textStatus, xhr) {
			// alert(JSON.stringify(data));
			if (data['operation_result'] == 'ok') {
				// Автозаполнение, если вернулся результат
				if (data['has_result'] == 'yes') {
					$('#'+InputPrefix+'region').val(data['autofill']['region']);
					$('#'+InputPrefix+'district').val(data['autofill']['district']);
					$('#'+InputPrefix+'city').val(data['autofill']['town']);
					$('#'+InputPrefix+'street').val(data['autofill']['street']);
				}
				// autocomplete'ы
				AutoCompleteSources[InputPrefix+'region'] = data['regions'];
				$("#"+InputPrefix+'region').autocomplete( "option", "source", AutoCompleteSources[InputPrefix+'region']);
				AutoCompleteSources[InputPrefix+'district'] = data['districts'];
				$("#"+InputPrefix+'district').autocomplete( "option", "source", AutoCompleteSources[InputPrefix+'district']);
				AutoCompleteSources[InputPrefix+'city'] = data['towns'];
				$("#"+InputPrefix+'city').autocomplete( "option", "source", AutoCompleteSources[InputPrefix+'city']);
				AutoCompleteSources[InputPrefix+'street'] = data['streets'];
				$("#"+InputPrefix+'street').autocomplete( "option", "source", AutoCompleteSources[InputPrefix+'street']);
			}
			else if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			SITEErrorMessage(112);
			// WriteErrorLog(112);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
}

// Для autocomplete'ов
var AutoCompleteSources = {};
// ID's локаций для autocomplete'ов
var AutocompleteLocationsIDs = {};
// Режим autocomplete'а (по индексу или по частям адреса)
var AutocompleteMode = '';

// Получаем части адреса
function GetAddrParts(Prefix) {
	var AddrParts = {};
	AddrParts['zip'] = $('#'+Prefix+'zip').val();
	AddrParts['region'] = $('#'+Prefix+'region').val();
	AddrParts['district'] = $('#'+Prefix+'district').val();
	AddrParts['city'] = $('#'+Prefix+'city').val();
	AddrParts['street'] = $('#'+Prefix+'street').val();
	
	return AddrParts;
}

// Пытаемся найти id, соответствующий значению текущего поля
function DetectCurrentID(Prefix, AddrPartID) {
	if ((AddrPartID == 'region') || (AddrPartID == 'district') || (AddrPartID == 'city')
		|| (AddrPartID == 'street')) {
		AutocompleteLocationsIDs[Prefix]['current'][AddrPartID] = 0;
		for (var i = 0; i < AutocompleteLocationsIDs[Prefix][AddrPartID].length; i++) {
			if (AutoCompleteSources[Prefix+AddrPartID][i] == $('#'+Prefix+AddrPartID).val()) {
				AutocompleteLocationsIDs[Prefix]['current'][AddrPartID] =
					parseInt(AutocompleteLocationsIDs[Prefix][AddrPartID][i]);
			}
		}
	}
}

// Автозаполнение введённым элементам адреса
function GetAddressAutocompletions(CurrentLocationsIDs, Prefix, AddrParts, UpdateAddrFields, Mode) {
	AjaxData = {};
	AjaxData['action_name'] = 'GetAddressAutocompletions';
	AjaxData['address_parts'] = AddrParts;
	if (Mode == 'default') {
		AjaxData['locations_ids'] = CurrentLocationsIDs;
	}
	AjaxData['prefix'] = Prefix;
	AjaxData['mode'] = Mode;
	AjaxData['action'] = AutocompleteMode;
	
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'POST',
		data: AjaxData,
		dataType : 'json',
		async: true,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'ok') {
				// Заполнение адреса по почтовому индексу
				if (data['action'] == 'addr_by_zip_code') {
					// Автозаполнение, если вернулся результат
					if ((data['has_result'] == 'yes') && (UpdateAddrFields) && (Mode == 'default')) {
						$('#'+Prefix+'region').val(data['autofill']['region']['value']);
						$('#'+Prefix+'district').val(data['autofill']['district']['value']);
						$('#'+Prefix+'city').val(data['autofill']['town']['value']);
						$('#'+Prefix+'street').val(data['autofill']['street']['value']);
					}
					AutocompleteLocationsIDs[Prefix]['current']['region'] = data['autofill']['region']['id'];
					AutocompleteLocationsIDs[Prefix]['current']['district'] = data['autofill']['district']['id'];
					AutocompleteLocationsIDs[Prefix]['current']['city'] = data['autofill']['town']['id'];
					AutocompleteLocationsIDs[Prefix]['current']['street'] = data['autofill']['street']['id'];
				}
				
				// autocomplete'ы
				
				// регионы
				AutoCompleteSources[Prefix+'region'] = [];
				AutocompleteLocationsIDs[Prefix]['region'] = [];
				for (var i = 0; i < data['regions'].length; i++) {
					AutoCompleteSources[Prefix+'region'].push(data['regions'][i]['value']);
					AutocompleteLocationsIDs[Prefix]['region'].push(data['regions'][i]['id']);
				};
				$("#"+Prefix+'region').autocomplete("option", "source", AutoCompleteSources[Prefix+'region']);
				
				// районы
				AutoCompleteSources[Prefix+'district'] = [];
				AutocompleteLocationsIDs[Prefix]['district'] = [];
				for (var i = 0; i < data['districts'].length; i++) {
					AutoCompleteSources[Prefix+'district'].push(data['districts'][i]['value']);
					AutocompleteLocationsIDs[Prefix]['district'].push(data['districts'][i]['id']);
				};
				$("#"+Prefix+'district').autocomplete("option", "source", AutoCompleteSources[Prefix+'district']);
				
				// населённые пункты
				AutoCompleteSources[Prefix+'city'] = [];
				AutocompleteLocationsIDs[Prefix]['city'] = [];
				for (var i = 0; i < data['towns'].length; i++) {
					AutoCompleteSources[Prefix+'city'].push(data['towns'][i]['value']);
					AutocompleteLocationsIDs[Prefix]['city'].push(data['towns'][i]['id']);
				};
				$("#"+Prefix+'city').autocomplete("option", "source", AutoCompleteSources[Prefix+'city']);
				
				// улицы
				AutoCompleteSources[Prefix+'street'] = [];
				AutocompleteLocationsIDs[Prefix]['street'] = [];
				for (var i = 0; i < data['streets'].length; i++) {
					AutoCompleteSources[Prefix+'street'].push(data['streets'][i]['value']);
					AutocompleteLocationsIDs[Prefix]['street'].push(data['streets'][i]['id']);
				};
				$("#"+Prefix+'street').autocomplete("option", "source", AutoCompleteSources[Prefix+'street']);
				
				if ((data['action'] == 'simple_autocomplete') && (Mode != 'initial')) {
					$('#'+Prefix+'zip').val(data['autofill']['zip']['value']);
				}
				if (Mode == 'initial') {
					// Пытаемся найти id, соответствующий значению текущего поля
					DetectCurrentID(Prefix, 'region');
					DetectCurrentID(Prefix, 'district');
					DetectCurrentID(Prefix, 'city');
					DetectCurrentID(Prefix, 'street');
				}
			}
			else if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			// SITEErrorMessage(113);
			WriteErrorLog(113);
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location.pathname);
		}
	});
}

// Инициализация AutocompleteLocationsIDs
function InitAutocompleteLocationsIDs(Prefix) {
	AutocompleteLocationsIDs[Prefix] = {};
	AutocompleteLocationsIDs[Prefix]['region'] = [];
	AutocompleteLocationsIDs[Prefix]['district'] = [];
	AutocompleteLocationsIDs[Prefix]['city'] = [];
	AutocompleteLocationsIDs[Prefix]['street'] = [];
	AutocompleteLocationsIDs[Prefix]['current'] = {};
	AutocompleteLocationsIDs[Prefix]['current']['region'] = 0;
	AutocompleteLocationsIDs[Prefix]['current']['district'] = 0;
	AutocompleteLocationsIDs[Prefix]['current']['city'] = 0;
	AutocompleteLocationsIDs[Prefix]['current']['street'] = 0;
}

// Инициализация автозаполнения при инициализации диалога
function InitDialogAutocomplete(Prefix, Mode) {
	if (!Mode) {
		Mode = 'initial';
	}
	// Получаем части адреса
	var AddrParts = GetAddrParts(AddrParts);

	// Если в AutocompleteLocationsIDs нет ключа Prefix
	if (!AutocompleteLocationsIDs[Prefix]) {
		InitAutocompleteLocationsIDs(Prefix);
	}

	GetAddressAutocompletions(null, Prefix, AddrParts, false, Mode);
}

// Возвращает заполненные части адреса
function GetAddressParts(ElemID) {
	var IDParts = ElemID.split('_');
	var AddrPartID = IDParts[IDParts.length - 1];
	var Prefix = IDParts.slice(0, IDParts.length - 1).join('_') + '_';
	
	// Получаем части адреса
	var AddrParts = GetAddrParts(Prefix);
	
	// Если в AutocompleteLocationsIDs нет ключа Prefix
	if (!AutocompleteLocationsIDs[Prefix]) {
		InitAutocompleteLocationsIDs(Prefix);
	}
	// Если в AutocompleteLocationsIDs есть ключ Prefix
	else {
		// здесь определяем текущие ID's
		
		// Пытаемся найти id, соответствующий значению текущего поля
		DetectCurrentID(Prefix, AddrPartID);
	}
	
	// Обновлять ли элементы адреса
	var UpdateAddrFields = false;
	if (AddrPartID == 'zip') {
		UpdateAddrFields = true;
		InitAutocompleteLocationsIDs(Prefix);
		
		if (AddrParts['zip'] == '') {
			AutocompleteMode = 'simple_autocomplete';
		}
		else {
			AutocompleteMode = 'addr_by_zip_code';
		}
	}
	
	GetAddressAutocompletions(AutocompleteLocationsIDs[Prefix]['current'], Prefix, AddrParts,
		UpdateAddrFields, 'default');
}

// Диалог показа накладной в заказе
function ShowOrderFactureDialog(callback_ok, callback_cancel){
	// ID элемента
	var DialogElementID = 'order_facture';
	
	$('#'+DialogElementID).dialog({
		autoOpen: true,
		width: 850,
		height: 400,
		buttons: {
			"Закрыть": function() {
				if (callback_ok) {
					callback_ok();
				}
				$(this).dialog("close");
			}
		},
		close: function(event, ui) {
			if (callback_cancel) {
				callback_cancel();
			};
		}
	});

	$('#'+DialogElementID).dialog( "option", "position", ['0','0'] );
};

// Установка коллекции
function SetCollectionShowMode(CollectionID, link) {
	if (!link) {
		link = "/catalog/15/";
	}
	var AjaxData = {};
	AjaxData['action_name'] = 'SetCollectionShowMode';
	AjaxData['collection_id'] = CollectionID;

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'error') {
				SITEalert(data["errors"], 'Ошибка!');
			}
			else if (data['operation_result'] == 'ok'){
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location);
			window.location.replace(link);
		}
	});
}

// Функция, "кликающая" на таб
// TabID -- id блока с табами
function TabClick(TabID, TabNum) {
	$('#'+TabID).children().removeClass("active");
	$('#'+TabID).children().eq(TabNum).addClass("active");
	
	// не знаю, как это прокомментировать
	var TabInputs = $('#'+TabID).nextAll(".tabs-content").eq(0).children('input.hider[type="radio"]');
	
	TabInputs.prop('checked', false);
	TabInputs.eq(TabNum).prop('checked', true);
}

// Заказ в один клик
function OneClickOrderSubmit() {
	// alert("Achtung! One-click user detected!");
	var OneClickUserData = {
		phone: $('#one_click_phone').val(),
		email: $('#one_click_email').val(),
		user_name: $('#one_click_username').val()
	};
	// alert(JSON.stringify(OneClickUserData));
	SubmitOrder(0, "one_click", OneClickUserData);
}

// Закрытие формы заказа звонка
function CloseOrderPhoneCall() {
	$('.phonecall_order_popup_background').each(function() {
		$(this).siblings('.logo-inner').eq(0).find('.phonecall_order_wrapper').eq(0).slideUp(200);
		$(this).css('display', 'none');
	});
}
// Заказ звонка
function OrderPhoneCall() {
	var UserName = $('#phonecall_order_name').val().trim();
	var UserPhone = $('#phonecall_order_phone').val().trim();
	
	var Errors = 0;
	$('#phonecall_order_name_errors').removeClass('visible');
	$('#phonecall_order_phone_errors').removeClass('visible');
	if (UserName == '') {
		$('#phonecall_order_name_errors').addClass('visible');
		Errors += 1;
	}
	if (UserPhone == '') {
		$('#phonecall_order_phone_errors').addClass('visible');
		Errors += 1;
	}
	
	if (Errors == 0) {
		var AjaxData = {};
		AjaxData['action_name'] = 'PhoneCallOrder';
		AjaxData['user_name'] = UserName;
		AjaxData['user_phone'] = UserPhone;

		jQuery.ajax({
			url: '/engine/ajax/runpostaction.php',
			type: 'GET',
			data: AjaxData,
			dataType : 'json',
			async: false,
			success: function(data, textStatus, xhr) {
				if (data['operation_result'] == 'error') {
					SITEErrorMessage(125);
				}
				else if (data['operation_result'] == 'ok'){
					// Очищаем поля
					$('#phonecall_order_name').val('');
					$('#phonecall_order_phone').val('');
					// Показываем финальное сообщение
					$('#phonecall_order_ok_panel').addClass('visible');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				SITEErrorMessage(124);
			},
			complete: function(xhr, textStatus) {
			}
		});
	}
}
// Заказ звонка
function DeleteFromRecentlyViewed(product_id, remove_block) {
	if (remove_block === undefined) {
		remove_block = true;
	}
	
	var AjaxData = {};
	AjaxData['action_name'] = 'DeleteFromRecentlyViewed';
	AjaxData['product_id'] = product_id;
	
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (remove_block) {
				$('div.recently_viewed_'+product_id).eq(0).remove();
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
		}
	});
}

// Повторная отправка SMS
function ResendPassword() {
	var AjaxData = {};
	AjaxData['action_name'] = 'ResendPassword';
	
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data.operation_result == 'ok') {
				SITEalert("Ваши данные отправлены Вам повторно.", '');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
		}
	});
}

// Проверка бонусов по номеру телефона
function CheckBonusesByPhoneNumber() {
	var AjaxData = {};
	AjaxData['action_name'] = 'GetBonusesByPhoneNumber';
	AjaxData['phone_number'] = $('#bonuses_by_phone_number_check_form input.phone_number').val();
	
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data.operation_result == 'ok') {
				$('#bonuses_by_phone_number_check_form p.message').html(data.message);
				$('#bonuses_by_phone_number_check_form p.message').slideDown();
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			alert(textStatus);
		},
		complete: function(xhr, textStatus) {
		}
	});
}

// Проверка бонусов по номеру телефона
function GetSpData() {
	var AjaxData = {};
	AjaxData['action_name'] = 'SendSpData';
	AjaxData['phone_number'] = $('#sp_data_request input.phone_number').val();
	AjaxData['fio'] = $('#sp_data_request input.fio').val();
	AjaxData['email'] = $('#sp_data_request input.email').val();
	AjaxData['spadress'] = $('#sp_data_request input.spadress').val();
	

	
	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType : 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			//
			if (data.operation_result == 'ok') {
				$('#sp_data_request input.phone_number').val('');
				$('#sp_data_request input.fio').val('');	
				$('#sp_data_request input.spadress').val('');
				$('#sp_data_request input.email').val('');	
				$('#sppanel').hide("slow");	
				$('#sp_data_request p.message').html(data.message);
				$('#sp_data_request p.message').slideDown();
			}else{
				$('#sp_data_request p.message').html(data.message);
				$('#sp_data_request p.message').slideDown();				
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			alert(textStatus);
		},
		complete: function(xhr, textStatus) {
		}
	});
}


// Обработка нажатия на пагинацию отзывов
// elem -- кнопка, на которую нажали
function ProductCommentsPaginationClick(elem) {
	var AjaxData = {};
	AjaxData['action_name'] = 'GetProductCommentsHTML';
	AjaxData['page_num'] = elem.find('input.pagenum_value[type="hidden"]').val();
	AjaxData['product_id'] = $('#comments input[name="product_id"]').val();

	// alert("Achtung! Pagination click! " + AjaxData['page_num'] + ". Product ID: " + AjaxData['product_id']);

	jQuery.ajax({
		url: '/engine/ajax/runpostaction.php',
		type: 'GET',
		data: AjaxData,
		dataType: 'json',
		async: false,
		success: function(data, textStatus, xhr) {
			if (data['operation_result'] == 'ok'){
				// alert(JSON.stringify(data));
				$('#comments').replaceWith(data['comments_html']);
				$('#comments_paginator').replaceWith(data['comments_pagination_html']);
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		},
		complete: function(xhr, textStatus) {
			// window.location.replace(window.location);
		}
	});


	return false;
}

// Отправка объеката APRT_DATA
function SendActionpayObject(pageType, productID, purchasedProducts, orderInfo) {
	pageType = pageType || 0;
	var objectToSend = jQuery.extend(true, {}, window.APRT_DATA);
	
	// 8 - событие: товар добавлен в корзину - должен быть указан currentProduct
	// 9 - событие: товар удален из корзины - должен быть указан currentProduct
	// 10 - событие*: товар добавлен в “отложенное” - должен быть указан currentProduct
	// 11 - событие*: товар удален из “отложенного” - должен быть указан currentProduct   
	if ([8, 9, 10, 11].indexOf(pageType) != -1) {
		objectToSend.currentProduct = {
			id: window.productsInfo[productID].id,
			name: window.productsInfo[productID].name
		};
		objectToSend.pageType = pageType;
		
		window.APRT_SEND(objectToSend);
	}
	else if (pageType == 6) {
		objectToSend["purchasedProducts"] = purchasedProducts;
		objectToSend["orderInfo"] = orderInfo;
		
		window.APRT_SEND(objectToSend);
	}
	// alert(JSON.stringify(objectToSend));
}


//-------------------------------------------------------------------------
// < $(document).ready(function() { >
$(document).ready(function() {
	// Загрузка плательщиков
	var PayersListElement = $('div.existing_payers');
	if (PayersListElement.length > 0) {
		LoadExistingPayers(PayersListElement);
		// Устанавливаем текущего плательщика
		CurrentPayerID = parseInt($("#current_payer_id").text());
		if (CurrentPayerID > 0) {
			SelectPayerByID(CurrentPayerID, 1);
		}
	}

	// Загрузка получателей
	var ReceiversListElement = $('div.existing_receivers');
	if (ReceiversListElement.length > 0) {
		LoadExistingPayers(ReceiversListElement, 0, 2);
		// Устанавливаем текущего получателя
		CurrentReceiverID = parseInt($("#current_receiver_id").text());
		if (CurrentReceiverID > 0) {
			SelectPayerByID(CurrentReceiverID, 2);
		}
	}

	// Выбор типа плательщика в диалоге правки плательщика
	$('input[name="edit_payer_type"]:radio').change(function() {OnEditPayerTypeChange();});
	
	// Выбор типа плательщика
	$('input[name="new_payer_type"]:radio').change(function() {
		// Выбранный тип плательщика
		var NewPayerType = $('input[name="new_payer_type"]:checked').val();

		var rec_type = undefined;
		if ($('#new_payer_is_receiver').is(':checked')) {
			rec_type = 3;
		}
		else {
			rec_type = 1;
		}

		SetDialogBlocksVisibility('new', rec_type, {payer_type: NewPayerType, equal_addr: $('#new_payer_equal_addr').is(':checked')});
	});

	// Выбор типа получателя
	$('input[name="new_receiver_type"]:radio').change(function() {
		// Выбранный тип плательщика
		var NewPayerType = $('input[name="new_receiver_type"]:checked').val();

		SetDialogBlocksVisibility('new', 2, {payer_type: NewPayerType, equal_addr: $('#new_receiver_equal_addr').is(':checked')});
	});

	// Загрузка плательщиков в личный кабинет
	$('#user_profile_payers_list').each(function() {
		LoadExistingPayers($(this), null, 1, 'user_profile');
	});
	// Загрузка получателей в личный кабинет
	$('#user_profile_receivers_list').each(function() {
		LoadExistingPayers($(this), null, 2, 'user_profile');
	});

	// Нажатие на плательщика/получателя на странице состава заказа
	$('div.payer_info_block a.payer_info_toggle').click(function() {
		$(this).parents('div.payer_info_block').find('div.payer_info').toggle(500);
		return false;
	});

	// Нажатие на галочку "Совпадает с юридическим" при указании фактического адреса
	// во время создания плательщика в корзине
	$('#new_payer_equal_addr').change(function() {
		if ($(this).is(':checked')) {
			$('#new_payer_real_addr_block').hide(500);
		}
		else {
			$('#new_payer_real_addr_block').show(500);
		}
	});

	// Нажатие на галочку "Совпадает с юридическим" при редактировании фактического адреса
	// во всплывающем окне
	$('#edit_payer_equal_addr').change(function() {
		// if ($(this).is(':checked')) {
		if ($(this).is(':checked')) {
			$('#edit_payer_real_addr_block').hide(500);
		}
		else {
			$('#edit_payer_real_addr_block').show(500);
		}
	});

	// Нажатие на галочку "Данные плательщика и получателя совпадают"
	// во время создания плательщика в корзине
	$('#new_payer_is_receiver').change(function() {
		// Выбранный тип плательщика
		var NewPayerType = $('input[name="new_payer_type"]:checked').val();

		var rec_type = undefined;
		if ($('#new_payer_is_receiver').is(':checked')) {
			rec_type = 3;
		}
		else {
			rec_type = 1;
		}

		SetDialogBlocksVisibility('new', rec_type, {payer_type: NewPayerType, equal_addr: $('#new_payer_equal_addr').is(':checked')});
	});
	
	// Запускаем autocomplete'ы
	var AddDialogPrefixes = ['new_payer_off_addr_', 'new_payer_real_addr_', 'edit_payer_off_addr_',
		'edit_payer_real_addr_', 'new_receiver_real_addr_', 'off_addr_', 'real_addr_'];
	var FieldsList = ['zip', 'region', 'district', 'city', 'street'];
	for (var i = 0; i < AddDialogPrefixes.length; i++) {
		// alert(AddDialogPrefixes[i]);
		var Prefix = AddDialogPrefixes[i];
		AutoCompleteSources[Prefix+'region'] = [];
		$('#'+Prefix+'region').autocomplete({
			delay: 0,
			source: AutoCompleteSources[Prefix+'region']
		});
		AutoCompleteSources[Prefix+'district'] = [];
		$('#'+Prefix+'district').autocomplete({
			delay: 0,
			source: AutoCompleteSources[Prefix+'district']
		});
		AutoCompleteSources[Prefix+'city'] = [];
		$('#'+Prefix+'city').autocomplete({
			delay: 0,
			source: AutoCompleteSources[Prefix+'city']
		});
		AutoCompleteSources[Prefix+'street'] = [];
		$('#'+Prefix+'street').autocomplete({
			delay: 0,
			source: AutoCompleteSources[Prefix+'street']
		});
		
		// Автозаполнение
		for (var j = 0; j < FieldsList.length; j++) {
			var FieldName = FieldsList[j];
			$('#'+Prefix+FieldName).focusout(function() {
				var CurElemID = $(this).attr('id');
				GetAddressParts(CurElemID);
			});
		}
	}
	
	// debug
	$('form.edit_payer_form input[name="real_addr_zip"]').autocomplete({
		delay: 0,
		source: ['Vasya', 'Kolya', 'Petya']
	});
	
	if ($('#new_payer_off_addr_block').length > 0) {
		InitDialogAutocomplete('new_payer_off_addr_');
		InitDialogAutocomplete('new_payer_real_addr_');
	}
	if ($('#new_receiver_real_addr_block').length > 0) {
		InitDialogAutocomplete('new_receiver_real_addr_');
	}

	// Просмотр заказа в виде накладной
	$('#show_order_facture').click(function() {
		ShowOrderFactureDialog();
		return false;
	});

	//переключение типа доставки для выбора скидки
	$('select[name="delivery"]').change(function() {
		// delivery = $('input:radio[name="delivery"]:checked').val();
		// DeliveryType = GetDeliveryType();
		// alert(DeliveryType);
		DeliveryType = $(this).val();
		PayDeliveryType = GetPayDeliveryType();
		if ((DeliveryType == 6) ||
			(DeliveryType == 10) ||
			(DeliveryType == 3) ||
			(DeliveryType == 4) ||
			(DeliveryType == 7) ||
			(DeliveryType == 9) ||
			(DeliveryType == 11) ||
			(DeliveryType == 2)) {
			
			message = "Вы выбрали доставку в центр-выдачи товаров, цены будут пересчитаны с учетом цен в центре выдачи. Подробнее тут"
				+ " <a href=\"http://www.z-dama.ru/centr-vydachi-tovary/\">http://www.z-dama.ru/centr-vydachi-tovary/</a>."
				+ " Нажмите \"Пересчитать\", чтобы увидеть Вашу цену.";
			// OnDeliveryTypeChange(message, "", DeliveryType);
			ChangeDeliveryType(DeliveryType, PayDeliveryType);
		}
		else if (DeliveryType == 1) {
			WholesaleThreshold = parseInt($('#wholesale_threshold').text());
			
			CurrentCartSum = parseInt($("#sum_opt").text());
			if (CurrentCartSum < WholesaleThreshold) {
				NeedMoreGoldDialog("Для заказа товара по оптовым ценам, сумма заказа должна быть больше "+WholesaleThreshold+"&nbsp;рублей. "
					+"Вам необходимо дозаказать товара на сумму " + (WholesaleThreshold-CurrentCartSum) + " рублей.", "", DeliveryType);
			}
			else {
				ChangeDeliveryType(DeliveryType, PayDeliveryType);
			}
		}
		else {
			ChangeDeliveryType(DeliveryType, PayDeliveryType);
		}
	});
	
	// Переключение типа розничной скидки
	$('input:radio[name="retail_discount_type"]').change(function () {
		SelectedDiscountType = $('input:radio[name="retail_discount_type"]:checked').val();

		var AjaxData = {};
		AjaxData['action_name'] = 'SetRetailDiscountType';
		AjaxData['retail_discount_type'] = SelectedDiscountType;

		jQuery.ajax({
			url: '/engine/ajax/runpostaction.php',
			type: 'GET',
			data: AjaxData,
			dataType : 'json',
			async: false,
			success: function(data, textStatus, xhr) {
				if (data['operation_result'] == 'error') {
					SITEalert(data["errors"], 'Ошибка!');
				}
				else if (data['operation_result'] == 'ok'){
				}
			},
			error: function(xhr, textStatus, errorThrown) {
			},
			complete: function(xhr, textStatus) {
				window.location.reload();
			}
		});
	});
	
	// Переключение плательщика
	$('input:radio[name="payer_select"]').change(function () {
		SelectedPayer = $('input:radio[name="payer_select"]:checked').val();
		
		if (SelectedPayer != 0) {
			var AjaxData = {};
			AjaxData['action_name'] = 'SetCurrentPayerID';
			AjaxData['current_payer_id'] = SelectedPayer;

			jQuery.ajax({
				url: '/engine/ajax/runpostaction.php',
				type: 'GET',
				data: AjaxData,
				dataType : 'json',
				async: false,
				success: function(data, textStatus, xhr) {
					if (data['operation_result'] == 'error') {
						SITEalert(data["errors"], 'Ошибка!');
					}
					else if (data['operation_result'] == 'ok'){
					}
				},
				error: function(xhr, textStatus, errorThrown) {
				},
				complete: function(xhr, textStatus) {
					window.location.replace(window.location);
				}
			});
		}
	});
	
	// Переключение получателя
	$('input:radio[name="receiver_select"]').change(function () {
		SelectedReceiver = $('input:radio[name="receiver_select"]:checked').val();
		
		if (SelectedReceiver != 0) {
			var AjaxData = {};
			AjaxData['action_name'] = 'SetCurrentReceiverID';
			AjaxData['current_receiver_id'] = SelectedReceiver;

			jQuery.ajax({
				url: '/engine/ajax/runpostaction.php',
				type: 'GET',
				data: AjaxData,
				dataType : 'json',
				async: false,
				success: function(data, textStatus, xhr) {
					if (data['operation_result'] == 'error') {
						SITEalert(data["errors"], 'Ошибка!');
					}
					else if (data['operation_result'] == 'ok'){
					}
				},
				error: function(xhr, textStatus, errorThrown) {
				},
				complete: function(xhr, textStatus) {
					window.location.replace(window.location);
				}
			});
		}
	});
	
	// Раскрывашка в левом меню
	$('div.showhide a.toggler').click(function() {
		$(this).parents(".showhide").eq(0).find(".showhide_container").slideToggle(250);
		return false;
	});
	
	// Клик на "Заказать звонок"
	$('div.phone').click(function() {
		// Убираем финальную панельку
		$('#phonecall_order_ok_panel').removeClass('visible');
		// Очищаем поля
		// $('#phonecall_order_name').val('');
		// $('#phonecall_order_phone').val('');
		
		$(this).siblings('.phonecall_order_wrapper').eq(0).slideDown(200);
		$('.phonecall_order_popup_background').css('display', 'block');
		return false;
	});
	$('.phonecall_order_wrapper').click(function() {
		return false;
	});
	$('.phonecall_order_popup_background').click(function() {
		$(this).siblings('.logo-inner').eq(0).find('.phonecall_order_wrapper').eq(0).slideUp(200);
		$(this).css('display', 'none');
	});
	// Нажатие на кнопку
	$('#phonecall_order_submit_button').click(function() {
		OrderPhoneCall();
		return false;
	});
	// Нажатие на кнопку OK
	$('#phonecall_order_ok_button').click(function() {
		CloseOrderPhoneCall();
		return false;
	});

	jQuery(function($){
		$("input.phone_input").mask("(999) 999-99-99",{placeholder:" "});
	});
	
	// Смена категории в droplist-меню
	$('#left_droplist_menu').change(function() {
		window.location.replace('/catalog/'+$(this).val()+'/');
	});
	
	// Получение доступного для заказа количества в карточке товара
	$('.get_amount').focus(function() {
		var AjaxData = {};
		AjaxData['action_name'] = 'GetProductAmount';
		AjaxData['product_id'] = $('#product_id').val();
		AjaxData['color'] = $(this).prop('name');
		AjaxData['size'] = $(this).prop('id');
		
		var This = $(this);

		jQuery.ajax({
			url: '/engine/ajax/runpostaction.php',
			type: 'GET',
			data: AjaxData,
			dataType: 'json',
			async: false,
			success: function(data, textStatus, xhr) {
				if (data['operation_result'] == 'ok'){
					if (data['amount'] > 0) {
						// Показываем tooltip
						This.tooltip({
							content: 'Доступно ' + data['amount'] + ' шт.',
							items: "input.get_amount",
							position: {
								my: "center bottom-20",
								at: "center top",
								using: function( position, feedback ) {
									$( this ).css( position );
									$( "<div>" )
									.addClass( "arrow" )
									.addClass( feedback.vertical )
									.addClass( feedback.horizontal )
									.appendTo( this );
								}
							}
						});
						This.tooltip('open');
						
						// Для убирания tooltip'а
						This.focusout(function() {
							try {
								$(this).tooltip("destroy");
							}
							catch (e) {}
						});
					}
				}
			},
			error: function(xhr, textStatus, errorThrown) {
			}
		});
	});

	// Кнопка добавления комментария к товару
	$('#btn_send_product_comment').click(function() {
		var AjaxData = {};
		AjaxData['action_name'] = 'AddComment';
		AjaxData['product_id'] = $('#product_comment_form input[name="product_id"]').val();
		AjaxData['fio'] = $('#product_comment_form input[name="comment_FIO"]').val();
		AjaxData['email'] = $('#product_comment_form input[name="comment_email"]').val();
		AjaxData['message'] = $('#product_comment_form textarea[name="comment_message"]').val();
		
		jQuery.ajax({
			url: '/engine/ajax/runpostaction.php',
			type: 'POST',
			data: AjaxData,
			dataType: 'json',
			async: false,
			success: function(data, textStatus, xhr) {
				// alert(JSON.stringify(data));
				if (data['operation_result'] == 'ok') {
					// Скрываем форму
					$('#feedback-form').prop('checked', false);
					// Очищаем форму
					$('#product_comment_form input[name="comment_FIO"]').val('');
					$('#product_comment_form input[name="comment_email"]').val('');
					$('#product_comment_form textarea[name="comment_message"]').val('');
					
					// Пишем сообщение
					$('#comment_accepted_message_block').slideDown(300);
					$('#comment_accepted_message').html(
						'<p class="main">Спасибо за Ваш отзыв! Для нас очень важно Ваше мнение!</p>'
						+'<p class="little">Добавление всех отзывов происходит после проверки их модератором. Одобрение или неодобрение отзыва осуществляется исключительно на усмотрение модератора.</p>');
				}
				else if (data['operation_result'] == 'error') {
					// Пишем сообщение
					$('#comment_accepted_message_block').slideDown(300);
					$('#comment_accepted_message').html(data['errors_html']);
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				alert(JSON.stringify(textStatus));
			}
		});
		

		return false;
	});
});
$(function(){
    if($("#slide_new").length>0){
            $("#slide_new").owlCarousel({
		        items: 4,
		        autoPlay: false,
		        pagination: false,
		        navigation: true,
		        navigationText: false,
		        responsive: true,
                stopOnHover: true,
                autoPlay: 3000,
                lazyLoad: true,
		itemsCustom: [[0, 1], [200, 2], [300, 3], [800, 4]]
		    });
    }
    var counter=0;
    $("form.pop-login").on("submit", function(event){
        if($(this).attr("data-success")!="good"){
            event.preventDefault();
            ShowLoading("Пожалуйста подождите...");
            var adress="/engine/ajax/login.php";
            $.post(adress, $(this).serialize()+"&action=control", function(result){
                if(result=="login_error"){
                    console.log(result);
                }else{
                    result=$.parseJSON(result);
                    if(result['count']>0){
                        $("form.pop-login").attr("data-success", "good");
                        $("form.pop-login").submit();
                    }else{
                        counter++;
                        ShowLoading("Неправильный e-mail или пароль");
                        setTimeout(function(){
                            HideLoading();
                        },3000);
                    }
                }
            })
        }
    });
})
// </ $(document).ready(function() { >
//-------------------------------------------------------------------------
$(function(){
    $("form.objects_per_page select").on("change", function(){
        $("form.objects_per_page").submit();
    });
	$(".lb-phone__callback").on("click", function(e){
		e.preventDefault();
		$("a.b24-widget-button-callback span").trigger("click");
	});
	$("a.mess_button").on("click", function(e){
		e.preventDefault();
		var did=$(this).attr("data-id");
		var but=this;
		$.post("/engine/ajax/add.basket.php", {action: "get_message", id: did}, function () {
			$(but).remove();
			SITEalert("Товар добавлен в рассылку", "Рекламная рассылка");
		});
	});
});

// Форма добавления нового плательщика
////////////////////////////////////////////////////////////////////////////

jQuery(function($){
	$(document).keydown(function(event){
	    if (event.which == 13 && event.ctrlKey) {
			if (window.getSelection) {
				var selectedText = window.getSelection();
			}
			else if (document.getSelection) {
				var selectedText = document.getSelection();
			}
			else if (document.selection) {
				var selectedText = document.selection.createRange().text;
			}

			if (selectedText == "" ) { return; }

			if (selectedText.toString().length > 255 ) {if ($.browser.mozilla) { alert(site_big_text); } else {SITEalert(site_big_text, site_info);} return;}

			var b = {};
		
			b[site_act_lang[3]] = function() { 
				$(this).dialog('close');						
			};
		
			b[site_p_send] = function() { 
				if ( $('#site-promt-text').val().length < 1) {
					$('#site-promt-text').addClass('ui-state-error');
				} else {
					var response = $('#site-promt-text').val();
					var selectedText = $('#orfom').text();
					$(this).dialog('close');

					$('#sitepopup').remove();

					$.post(site_root + 'engine/ajax/complaint.php', { seltext: selectedText,  text: response, action: "orfo", url: window.location.href },
						function(data){
							if (data == 'ok') { if ($.browser.mozilla) { alert(site_p_send_ok); } else { SITEalert(site_p_send_ok, site_info);} } else { if ($.browser.mozilla) { alert(data); } else { SITEalert(data, site_info);} }
						});
		
				}				
			};
		
			$('#sitepopup').remove();
							
			$('body').append("<div id='sitepopup' title='"+site_orfo_title+"' style='display:none'><br /><textarea name='site-promt-text' id='site-promt-text' class='ui-widget-content ui-corner-all' style='width:97%;height:80px; padding: .4em;'></textarea><div id='orfom' style='display:none'>"+selectedText+"</div></div>");
							
			$('#sitepopup').dialog({
				autoOpen: true,
				width: 550,
				dialogClass: "modalfixed",
				buttons: b
			});
		
			// $('.modalfixed.ui-dialog').css({position:"fixed"});
			$('#sitepopup').dialog( "option", "position", ['0','0'] );
		};
	});
});