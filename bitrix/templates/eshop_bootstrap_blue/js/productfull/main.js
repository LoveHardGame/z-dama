$(function() {
	/*$('.popup').click(function() {
		$(this).prev('.popup-trigger')[0].checked = false;
	});*/
	var owl = $("#owl");
	if (owl.length) {
		$(window).resize(function() {
			owl.owlCarousel({
				items: 4,
				autoPlay: false,
				pagination: false,
				responsive: false,
				afterMove: function(owl) {
					var vis = owl.data('owlCarousel').visibleItems;
					owl.find('.owl-item').removeClass('last');
					owl.find('.owl-item').eq(vis[vis.length - 1]).addClass('last');
				},
				beforeMove: function() {
					owl.find('.popup-trigger').attr('checked', false);
				}

			});
			owl.find('.owl-item').eq(3).addClass('last');
			$(".next").unbind('click').click(function(){
				owl.trigger('owl.next');
			});
			$(".prev").unbind('click').click(function(){
				owl.trigger('owl.prev');
			});
			owl.find('.popup-trigger').change(function() {
				var t = $(this);
				owl.find('.popup-trigger').not(t).attr('checked', false);
				owl.find('.owl-item').removeClass('active');
				t.parents('.owl-item').addClass('active');
			});
		});
		$(window).resize();
	}

	var aside = $('#aside');
	if (aside.length) {
		aside.stickyMojo({footerID: '#footer', contentID: '#content'});
	}
	
	var tab = $(".tab");
	if (tab.length) {
		tab.click(function() {
			tab.removeClass('active');
			$(this).addClass('active');
		});
	}
	

	var compare = $(".compare .list");
	if (compare.length) {
		compare.jScrollPane({
			autoReinitialise: true,
			horizontalDragMaxWidth: 86,
			horizontalDragMinWidth: 86
		});

		$(window).resize(function() {
			if ($(window).width() >= 1008) {
				compare.prepend($('.delete'));
			}
			else {
				compare.find('.jspPane').prepend($('.delete'));
			}
		});
		$(window).resize();
	}

	var refer = $(".card .refer .list");
	if (refer.length) {
		$(window).resize(function() {
			refer.owlCarousel({
				items: 5,
				autoPlay: false,
				pagination: false,
				responsive: false,
				navigation: true
			});
		});
		$(window).resize();
	}

	$(".video").fancybox({
		maxWidth	: 640,
		maxHeight	: 480,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		arrows:false,
		modal:false				
	});
	
	var search = $(".search a");
	if (search.length) {
		var nav = search.parents('nav');
		search.click(function() {
			var t = $(this).parents('li:first');
			var o = t.find('.search-form');
			if (o.length) {
				var SearchInputs = o.find('input[name="searchword"]');
				var SearchWord = SearchInputs.val().trim();
				if (SearchWord) {
					o.find('form').submit();
				}
				else {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				}
			}
			else {
				t.prepend('<div class="search-form"><form action="/catalog/search/" method="post"><input type="text" name="searchword" id="searchform" autocomplete="off"/></form></div>');
				o = t.find('.search-form');
				$( "#searchform" ).focus();
				
				$( "#searchform" ).focusout(function() {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				});
			}
			var width = t.offset().left - nav.offset().left - 5;
			o.animate({
				left: -width,
				width: width
			}, 300);
			return false;
		});
	}
	
	//===========================================================================================
	
	// Pop-up
	$('input.popup-trigger').change(function() {
		$(this).parents("#site-content").eq(0).find(".item .popup").each(function() {
			$(this).stop();
			$(this).slideUp(500);
		});
		
		// alert($(this).css('left'));
		var RowElement = $(this).parents(".item").eq(0);
		// alert(El.position().left);
		var NewLeft = RowElement.position().left;
		if (NewLeft > 131) {
			NewLeft = 131;
		}
		
		$(this).siblings(".popup").each(function() {
			$(this).stop();
			// var NewLeft = Math.floor(Math.random() * 135);
			// NewLeft = 0;
			$(this).css('left', NewLeft+'px');
			$(this).css('top', '0px');
			$(this).slideDown(500);
		});
	});
	
		$(window).scroll(function(){
		if ($(this).scrollTop() > 600) {
		$('.scrollup').fadeIn();
		} else {
		$('.scrollup').fadeOut();
		}
		});
		 
		$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
		});	

});
	
// Нажатие на кнопку "В корзину"
function AddToBasketBtnClick(product_id) {
	if (product_id) {
		// alert("Color: " + $('input[name="color_' + product_id + '"]:checked').val());
		// alert("Size: " + $('input[name="size_' + product_id + '"]:checked').val());
		var ColorID = $('input[name="color_' + product_id + '"]:checked').val();
		var Size = $('input[name="size_' + product_id + '"]:checked').val();
		
		if (ColorID && Size) {
			// AddToCard(product_id,'1');
			AddToCardOneClick(product_id, ColorID, Size);
			// SITEalert("<p>Товар: " + product_id + ". Цвет: " + ColorID + ". Размер: " + Size + "</p><br/>", "");
		}
		else {
			SITEalert("<p>Для того, чтобы добавить товар в корзину, выберите цвет и размер.</p><br/>", "");
		}
	}
}

jQuery(function($){
	 $("#phone").mask("(999) 999-9999",{placeholder:" "});
});
