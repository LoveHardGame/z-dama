// kladrautocomplete.js
// Плагин для автозаполнения адресов.
// v0.1.0
// 2015-02-05

// Примеры подключения:
// $('form.edit_payer_form div.real_address').KladrAutocomplete({prefix: 'real_addr_'});
// $('form.edit_payer_form div.off_address').KladrAutocomplete({prefix: 'off_addr_'});

(function ($) {
	jQuery.fn.KladrAutocomplete = function(options) {
		var settings = jQuery.extend({
				prefix: '',
				// Атрибуты "name" input'ов с частями адреса
				input_names: {
					zip: 'zip',
					region: 'region',
					district: 'district',
					city: 'city',
					street: 'street'
				}
			}, options);
		
		return this.each(function() {
			// $(this).css('background', '#006633');
			
			// Input'ы
			var zipInput = $(this).find('input[name="'+settings.prefix+settings.input_names.zip+'"]').eq(0);
			var regionInput = $(this).find('input[name="'+settings.prefix+settings.input_names.region+'"]').eq(0);
			var districtInput = $(this).find('input[name="'+settings.prefix+settings.input_names.district+'"]').eq(0);
			var cityInput = $(this).find('input[name="'+settings.prefix+settings.input_names.city+'"]').eq(0);
			var streetInput = $(this).find('input[name="'+settings.prefix+settings.input_names.street+'"]').eq(0);
			
			function autofillPartHasValue(part, action) {
				if (part == undefined) {
					return false;
				}
				if (action == 'addr_by_zip_code') {
					return true;
				}
				
				if (part['id'] == 0) {
					return false;
				}
				
				if (part['value'] == '') {
					return false;
				}
				return true;
			}
			
			// Заполняет или не заполняет поле с частью адреса
			function AutofillAddresspart(part, action, elem) {
				if (autofillPartHasValue(part, action)) {
					// if (elem.val().replace(/^\s+|\s+$/g, '') == '') {
					// 	elem.val(part['value']);
					// }
					elem.val(part['value']);
				}
			}

			
			function plGetAddressAutocompletions(action) {
				var zipValue = zipInput.val();
				var regionValue = regionInput.val();
				var districtValue = districtInput.val();
				var cityValue = cityInput.val();
				var streetValue = streetInput.val();
				
				if (action == 'addr_by_zip_code') {
					if (zipValue.replace(/^\s+|\s+$/g, '') == '') {
						action = 'simple_autocomplete';
					}
				}
				
				var AddrParts = {
					'zip': zipValue,
					'region': regionValue,
					'district': districtValue,
					'city': cityValue,
					'street': streetValue
				};
				if (action == 'simple_autocomplete') {
					AddrParts['zip'] = '';
				}

				AjaxData = {};
				AjaxData['action_name'] = 'GetAddressAutocompletions';
				AjaxData['address_parts'] = AddrParts;
				AjaxData['action'] = AutocompleteMode;
				
				jQuery.ajax({
					url: '/engine/ajax/runpostaction.php',
					type: 'POST',
					data: AjaxData,
					dataType : 'json',
					async: true,
					success: function(data, textStatus, xhr) {
						// alert(JSON.stringify(data['districts']));
						if (data['operation_result'] == 'ok') {
							// Регионы
							var regionsList = [];
							for (var i = 0; i < data['regions'].length; i++) {
								regionsList.push(data['regions'][i]['value']);
							};							
							regionInput.autocomplete({
								delay: 0,
								source: regionsList
							});
							
							// Районы
							var districtsList = [];
							for (var i = 0; i < data['districts'].length; i++) {
								districtsList.push(data['districts'][i]['value']);
							};							
							districtInput.autocomplete({
								delay: 0,
								source: districtsList
							});
							
							// Населённые пункты
							var townsList = [];
							for (var i = 0; i < data['towns'].length; i++) {
								townsList.push(data['towns'][i]['value']);
							};							
							cityInput.autocomplete({
								delay: 0,
								source: townsList
							});
							
							// Улицы
							var streetsList = [];
							for (var i = 0; i < data['streets'].length; i++) {
								streetsList.push(data['streets'][i]['value']);
							};							
							streetInput.autocomplete({
								delay: 0,
								source: streetsList
							});
							
							// Автозаполнение
							AutofillAddresspart(data['autofill']['zip'], action, zipInput);
							if (action == 'addr_by_zip_code') {
								AutofillAddresspart(data['autofill']['region'], action, regionInput);
								AutofillAddresspart(data['autofill']['district'], action, districtInput);
								AutofillAddresspart(data['autofill']['town'], action, cityInput);
								AutofillAddresspart(data['autofill']['street'], action, streetInput);
							}
						}
						else if (data['operation_result'] == 'error') {
							// SITEalert(data["errors"], 'Ошибка!');
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						// SITEalert(textStatus, "ERROR");
					},
					complete: function(xhr, textStatus) {
					}
				});
			}
			
			plGetAddressAutocompletions('simple_autocomplete');
			
			$(zipInput).focusout(function() {
				plGetAddressAutocompletions('addr_by_zip_code');
			});
			$(regionInput).focusout(function() {
				plGetAddressAutocompletions('simple_autocomplete');
			});
			$(districtInput).focusout(function() {
				plGetAddressAutocompletions('simple_autocomplete');
			});
			$(cityInput).focusout(function() {
				plGetAddressAutocompletions('simple_autocomplete');
			});
			$(streetInput).focusout(function() {
				plGetAddressAutocompletions('simple_autocomplete');
			});
		});
	}
}) (jQuery);
