
$(function() {
	/*$('.popup').click(function() {
		$(this).prev('.popup-trigger')[0].checked = false;
	});*/
	// var owl = $("#owl");
	// if (owl.length) {
	// 	$(window).resize(function() {
	// 		owl.owlCarousel({
	// 			items: 4,
	// 			autoPlay: false,
	// 			pagination: false,
	// 			responsive: false,
	// 			afterMove: function(owl) {
	// 				var vis = owl.data('owlCarousel').visibleItems;
	// 				owl.find('.owl-item').removeClass('last');
	// 				owl.find('.owl-item').eq(vis[vis.length - 1]).addClass('last');
	// 			},
	// 			beforeMove: function() {
	// 				owl.find('.popup-trigger').attr('checked', false);
	// 			}

	// 		});
	// 		owl.find('.owl-item').eq(3).addClass('last');
	// 		$(".next").unbind('click').click(function(){
	// 			owl.trigger('owl.next');
	// 		});
	// 		$(".prev").unbind('click').click(function(){
	// 			owl.trigger('owl.prev');
	// 		});
	// 		owl.find('.popup-trigger').change(function() {
	// 			var t = $(this);
	// 			owl.find('.popup-trigger').not(t).attr('checked', false);
	// 			owl.find('.owl-item').removeClass('active');
	// 			t.parents('.owl-item').addClass('active');
	// 		});
	// 	});
	// 	$(window).resize();
	// }
	
	var owl = $("#owl");
	if (owl.length) {
        owl.owlCarousel({
            items: 4,
            autoPlay: false,
            pagination: false,
            responsive: false,
            afterMove: function(owl) {
            },
            beforeMove: function() {
                $('.close-popup').click();
            }

        });
        $(".next").click(function(){
            owl.trigger('owl.next');
            $('.close-popup').click();
        });
        $(".prev").click(function(){
            owl.trigger('owl.prev');
            $('.close-popup').click();
        });
	}
	
	
	var owlbanner = $("#owlbanner");
	if (owlbanner.length) {
		$(window).resize(function() {
			owlbanner.owlCarousel({
				items: 1,
				autoPlay: true,
				pagination: false,
				responsive: false,
				slideSpeed : 300,
				paginationSpeed : 400,
				autoplaySpeed:500

			});
			
			$(".owlbanner .next").unbind('click').click(function(){
				owlbanner.trigger('owl.next');
			});
			$(".owlbanner .prev").unbind('click').click(function(){
				owlbanner.trigger('owl.prev');
			});

		});
		$(window).resize();
	}

    var owl2 = $(".owl2");
    owl2.owlCarousel({
        items: 5,
        autoPlay: false,
        pagination: false,
        navigation: true,
        navigationText: false,
        responsive: false
    });

	var owlbest = $("#owlbest");
	if (owlbest.length) {
		$(window).resize(function() {
			owlbest.owlCarousel({
				items: 4,
				autoPlay: false,
				pagination: false,
				responsive: false,
				afterMove: function(owlbest) {
					var vis = owlbest.data('owlCarousel').visibleItems;
					owlbest.find('.owl-item').removeClass('last');
					owlbest.find('.owl-item').eq(vis[vis.length - 1]).addClass('last');
				},
				beforeMove: function() {
					owlbest.find('.popup-trigger').attr('checked', false);
				}

			});
			owlbest.find('.owl-item').eq(3).addClass('last');
			$(".owlbest .next").unbind('click').click(function(){
				owlbest.trigger('owl.next');
			});
			$(".owlbest .prev").unbind('click').click(function(){
				owlbest.trigger('owl.prev');
			});
			owlbest.find('.popup-trigger').change(function() {
				var t = $(this);
				owlbest.find('.popup-trigger').not(t).attr('checked', false);
				owlbest.find('.owl-item').removeClass('active');
				t.parents('.owl-item').addClass('active');
			});
		});
		$(window).resize();
	}

	var aside = $('#aside');
	if (aside.length) {
		aside.stickyMojo({footerID: '#footer', contentID: '#content'});
	}

	var compare = $(".compare .list");
	if (compare.length) {
		compare.jScrollPane({
			autoReinitialise: true,
			horizontalDragMaxWidth: 86,
			horizontalDragMinWidth: 86
		});

		$(window).resize(function() {
			if ($(window).width() >= 1008) {
				compare.prepend($('.delete'));
			}
			else {
				compare.find('.jspPane').prepend($('.delete'));
			}
		});
		$(window).resize();
	}

	var tab = $(".tab");
	if (tab.length) {
		tab.click(function() {
			tab.removeClass('active');
			$(this).addClass('active');
		});
	}

	var price = $(".filter .price");
	if (price.length) {
		price.each(function() {
			var t = $(this);
			t.append('<div class="slider"></div>');
			t.find('.slider').noUiSlider({
				start: [ 1000, 8000 ],
				range: {
					'min': [  1000 ],
					'max': [ 10000 ]
				}
			});
		});
	}

	var refer = $(".card .refer .list");
	if (refer.length) {
		$(window).resize(function() {
			refer.owlCarousel({
				items: 5,
				autoPlay: false,
				pagination: false,
				responsive: false,
				navigation: true
			});
		});
		$(window).resize();
	}

	// var fancy = $(".fancy");
	// if (fancy.length) {
	// 	fancy.fancybox({
	// 		padding: 30,
	// 		margin: [20, 50, 20, 50]
	// 	});
	// 	$('.link3d').click(function() {
	// 		fancy.click();
	// 		return false;
	// 	});
	// }
	var search = $(".search a");
	if (search.length) {
		var nav = search.parents('nav');
		search.click(function() {
			var t = $(this).parents('li:first');
			var o = t.find('.search-form');
			if (o.length) {
				var SearchInputs = o.find('input[name="searchword"]');
				var SearchWord = SearchInputs.val().trim();
				if (SearchWord) {
					o.find('form').submit();
				}
				else {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				}
			}
			else {
				t.prepend('<div class="search-form"><form action="/catalog/search/" method="post"><input type="text" name="searchword" id="searchform" autocomplete="off"/></form></div>');
				o = t.find('.search-form');
				$( "#searchform" ).focus();
				
				$( "#searchform" ).focusout(function() {
					o.stop();
					o.animate({
						left: -0,
						width: 0
					}, 300, 'swing', function() {
						o.remove();
					});
				});
			}
			var width = t.offset().left - nav.offset().left - 5;
			o.animate({
				left: -width,
				width: width
			}, 300);
			return false;
		});
	}

	
	//===========================================================================================
	
	// Pop-up
	$('input.popup-trigger').change(function() {
		// alert(12312);
		// return;
		
		$('#popup_off').css('display', 'block');
		
		$(this).siblings(".popup").each(function() {
			$(this).find('.col_right div.color input[type="radio"]').prop('checked', false);
			var FirstInput = $(this).find('.col_right div.color input[type="radio"]').eq(0);
			FirstInput.prop('checked', true);
			var ProductID = $(this).find('input.product_id[type="hidden"]').val();
			var func = new Function("ChangeColorOn"+ProductID+"($(\"#"+ FirstInput.attr('id') +"\"));");
			func();
		});
	});
	
	// Close pop-up
	$('#popup_off').click(function() {
		// Скрываем #popup_off
		$(this).css('display', 'none');
		
		// Закрываем все попапы
		$(".slider-main .slider-wrapper").find(".item .popup").each(function() {
			$(this).stop();
			$(".slider-main .slider-wrapper").find(".popup-trigger").prop('checked', false);
			$(this).slideUp(250, function() {
				$(this).css('display', 'none');
			});
		});
	});
	$('.popup_close_button').click(function() {
		// Скрываем #popup_off
		$("#popup_off").css('display', 'none');
		
		// Закрываем все попапы
		$(".slider-main .slider-wrapper").find(".item .popup").each(function() {
			$(this).stop();
			$(".slider-main .slider-wrapper").find(".popup-trigger").prop('checked', false);
			$(this).slideUp(250, function() {
				$(this).css('display', 'none');
			});
		});
	});

    //Открытие/закрытие окна Быстрого просмотра
    $('body').on('click', '.btn-preview', function(){
        $('.close-popup').click();
        var p = $(this).closest('.prod-item');
        var w = p.find('.popup');
        p.addClass('show-mw');
        w.appendTo('body').css({'top':p.offset().top, 'left':'50%', 'marginLeft': -w.outerWidth()/2}).addClass('active').fadeIn(200);
        if ($(this).closest('.catalog').length){
            w.css({'marginLeft':'-436px'})
        }

        w.find('.ctrl-group').each(function(){
            if ($(this).hasClass('checked-first')){
                $(this).find('input:eq(0)').prop('checked', true).change();
            }
            else{
                $(this).find('input').prop('checked', false);
                $(this).find('label').removeClass('selected');
            }
        });

        return false;
    });

    //Выбор цвета
    $('body').on('change', '.color input', function(){
    	// alert($(this).next('.color_image_path').data('val'));
        if ($(this).prop('checked')){
            $(this).closest('.popup').find('.large-photo').attr('src', $(this).next('.color_image_path').data('val'));
            // $(this).closest('.popup').find('.large-photo').css('border', '1px solid #f00');
        }
    });

    $('body').on('click', '.close-popup', function(){
        $('.popup.active').appendTo('.show-mw').css({'top':'', 'left':'', 'marginLeft': '', 'display': ''}).removeClass('active');
        $('.show-mw').removeClass('show-mw');

        return false;
    });

	
	// Нажатие на маленькую картинку в блоке товара
	$('img.small_thumb').click(function() {
		var ImagePath = $(this).attr('src');
		ImagePath = ImagePath.replace('/small/', '/thumbs/');
		$(this).parents('.small_thumbs').eq(0).siblings('.image').eq(0).find('img.big_picture').eq(0).attr('src', ImagePath);
	});

});

// Нажатие на кнопку "В корзину"
function AddToBasketBtnClick(product_id) {
	if (product_id) {
		var ColorID = $('input[name="color_' + product_id + '"]:checked').val();
		var Size = $('input[name="size_' + product_id + '"]:checked').val();
		
		if (ColorID && Size) {
			AddToCardOneClick(product_id, ColorID, Size);
		}
		else {
			SITEalert("<p>Для того, чтобы добавить товар в корзину, выберите цвет и размер.</p><br/>", "");
		}
	}
}

		$(window).scroll(function(){
		if ($(this).scrollTop() > 550) {
		$('.scrollup').fadeIn();
		} else {
		$('.scrollup').fadeOut();
		}
		});
		 
		$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
		});	

		$(document).ready(function(){
			var flag=1;
			lg=$("a.main_banner_sliding").length;
			ll=lg-1;
            if(lg>1){
				$("a.main_banner_sliding:not(:eq("+ll+"))").hide();
    			setInterval(function(){
    				if (flag<$("a.main_banner_sliding").length) {
						flag++;
    					$('.main_banner_sliding:visible').fadeOut(500, function () {
							$('.main_banner_sliding#id'+flag).fadeIn(500);
						});
    				} else {
						flag=1;
    					$('.main_banner_sliding:visible').fadeOut(500, function () {
							$('.main_banner_sliding#id'+flag).fadeIn(500);
						});
    				}
    			},5000);
            }
		});