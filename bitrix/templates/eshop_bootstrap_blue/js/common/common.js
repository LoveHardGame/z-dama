$(function() {

	// Pop-up
	$('input.popup-trigger').change(function() {
		$('#popup_off').css('display', 'block');
		
		$(this).parents("#site-content").eq(0).find(".item .popup").each(function() {
			$(this).stop();
			$(this).slideUp(250);
		});
		
		var RowElement = $(this).parents(".item").eq(0);
		var NewLeft = RowElement.position().left;
		if (NewLeft > 131) {
			NewLeft = 131;
		}
		NewLeft = -294;
		
		$(this).siblings(".popup").each(function() {
			$(this).stop();
			$(this).css('left', NewLeft+'px');
			$(this).css('top', '0px');
			$(this).slideDown(250);
			// $(this).show();
			
			$(this).find('.col_right div.color input[type="radio"]').prop('checked', false);
			// var FirstInput = $(this).find('.col_right div.color input[type="radio"]').eq(0);
			// FirstInput.prop('checked', true);
			// var ProductID = $(this).find('input.product_id[type="hidden"]').val();
			// var func = new Function("ChangeColorOn"+ProductID+"($(\"#"+ FirstInput.attr('id') +"\"));");
			// func();
		});
	});
	
	// Close pop-up
	$('#popup_off').click(function() {
		// Скрываем #popup_off
		$(this).css('display', 'none');
		$('.popup').hide();

	});
	// $('.popup_close_button').click(function() {
	$('.close-popup').click(function() {
		// Скрываем #popup_off
		$("#popup_off").css('display', 'none');
		
		// Закрываем все попапы
		$("#site-content").find(".item .popup").each(function() {
			$(this).stop();
			$(this).slideUp(250);
		});
	});

    //Открытие/закрытие окна Быстрого просмотра
    $('body').on('click', '.btn-preview', function(){

        $('.close-popup').click();
		$('#popup_off').css('display', 'block');
        var p = $(this).closest('.prod-item');
        var w = p.find('.popup');
        p.addClass('show-mw');
        w.appendTo('body').css({'top':p.offset().top, 'left':'50%', 'marginLeft': -w.outerWidth()/2}).addClass('active').fadeIn(200);
        if ($(this).closest('.catalog').length){
            w.css({'marginLeft':'-436px'})
        }

        // w.find('img.large-photo').eq(0).prop('src', w.find('span.initial_full_image').eq(0).text());
        
        w.find('.size-list').html('<p>выберите цвет</p>');

        w.find('.ctrl-group').each(function(){
            // $(this).find('input').prop('checked', false);
            // $(this).find('label').removeClass('selected');
            
            if ($(this).hasClass('checked-first')){
                $(this).find('input:eq(0)').prop('checked', true).change();
            }
            else{
                $(this).find('input').prop('checked', false);
                $(this).find('label').removeClass('selected');
            }
        });
        
        var ColorSelectionBlock = w.find('.color.ctrl-group').eq(0);
        // ColorSelectionBlock.find('input[type="radio"]').eq(0).prop('checked', true);
        // ColorSelectionBlock.find('input[type="radio"]').eq(0).addClass('selected');

        // Центруем pop-up:
        // Высота окна
        var WindowHeight = $(window).height();
        // Высота popup'а
        var PopUpHeight = w.height();
        // Позиция скролла
        var ScrollPosition = $(window).scrollTop();
        // Новый Y для popup'а
        var NewY = Math.round((WindowHeight - PopUpHeight - 59)/2 + ScrollPosition + 0.5*59);
        // alert(NewY);
        w.animate({top: NewY+'px'}, 500, 'easeOutQuad');
		
        return false;
    });

   //  //Выбор цвета
   //  $('body').on('change', '.color input', function(){
   //      if ($(this).prop('checked')){
   //          // $(this).closest('.popup').find('.large-photo').attr('src', $(this).next('.color_image_path').data('val'));
			
			// // // Смена большой картинки
   // //  		var PicPath = $(this).next('label').find('img').prop('src');
   // //  		$(this).parents('.popup').eq(0).find('img.large-photo').eq(0).attr('src', PicPath.replace("\/thumbs\/","\/original\/"));
   // //  		//cur
   // //  		// определить product_id (элемент с классом product_id)
   // //  		var ProductID = $(this).parents('.popup').eq(0).find('input.product_id').eq(0).val();
   //      }
   //  });

    $('body').on('click', '.close-popup', function(){
        $('.popup.active').appendTo('.show-mw').css({'top':'', 'left':'', 'marginLeft': '', 'display': ''}).removeClass('active');
        $('.show-mw').removeClass('show-mw');

        return false;
    });

	
    var smallThumbsCarousel = $(".small_thumbs_carousel");
    smallThumbsCarousel.each(function() {
    	// alert(!$(this).hasClass('popup'));
    	if (!$(this).hasClass('popup_thumbs')) {
		    $(this).owlCarousel({
		        items: 5,
		        autoPlay: false,
		        pagination: false,
		        navigation: true,
		        navigationText: false,
		        responsive: false
		    });
    	}
    	// if (!$(this).hasClass('popup')) {
    	// }
    });
	
    var smallThumbsPopUpCarousel = $(".small_thumbs_carousel.popup_thumbs");
    smallThumbsPopUpCarousel.owlCarousel({
        items: 5,
        autoPlay: false,
        pagination: false,
        navigation: true,
        navigationText: false,
        responsive: false
    });
	
	// Нажатие на маленькую картинку в блоке товара
	$('img.small_thumb').click(function() {
		var ImagePath = $(this).attr('src');
		ImagePath = ImagePath.replace('/small/', '/thumbs/');
		$(this).parents('.main-info').eq(0).find('img.big_picture').eq(0).attr('src', ImagePath);
	});
	
	// Выбирает нужный input[type="radio"] при нажатии на маленькую картинку во всплывалке в каталоге.
	// Само по себе не работает из-за плагина Jet Zoom.
	$('label.jetzoom-gallery').click(function() {
		var labelFor = $(this).prop('for');
		$('#'+labelFor).prop('checked', true);
		$('#'+labelFor).change();
	});
	
	//=================================================================
	// Корзина

	// Применяем классы
	function ApplyDependentClasses(mainElement, mainClass, appliedClass, timeMs) {
		if (timeMs == undefined) timeMs = 500;
		$(mainElement).find('.'+mainClass).slideUp(timeMs);
		$(mainElement).find('.'+mainClass+'.'+appliedClass).stop();
		$(mainElement).find('.'+mainClass+'.'+appliedClass).slideDown(timeMs);
	}
	
	// Выбор пользователем получателя
	// initial -- если true, то это начальный вызов функции.
	function OnChangeUserReceiver(elem, initial) {
		if (initial == undefined) {
			initial = false;
		}
		
		// Получаем информацию о получателе
		var ReceiverID = $(elem).val();
		var ReceiverInfoElementID = 'receiver_info_' + ReceiverID;
		var PayerType = $('#'+ReceiverInfoElementID).find('.property.payer_type').text();
		
		// Заполняем поля
		// Если новый получатель
		if (ReceiverID == 0) {
			// Если это не начальный вызов этой функции, то очищаем поля и ставим физ. лицо
			if (!initial) {
				$('.order_delivery_form input[name="delivery_address_zip"]').val('');
				$('.order_delivery_form input[name="delivery_address_region"]').val('');
				$('.order_delivery_form input[name="delivery_address_district"]').val('');
				$('.order_delivery_form input[name="delivery_address_city"]').val('');
				$('.order_delivery_form input[name="delivery_address_street"]').val('');
				$('.order_delivery_form input[name="delivery_address_house"]').val('');
				$('.order_delivery_form input[name="delivery_address_building"]').val('');
				$('.order_delivery_form input[name="delivery_address_flat"]').val('');
				
				$('.receiver_info_form input[name="payer_info_fio"]').val('');
				$('.receiver_info_form input[name="payer_info_name"]').val('');
				$('.receiver_info_form input[name="payer_info_email"]').val('');
				$('.receiver_info_form input[name="payer_info_phone"]').val('');

				$('#selUserReceiverType').val(1);
				$('#selPropertyType').val(3);
			}
			OnChangeUserReceiverType();
		}
		else {
			$('.order_delivery_form input[name="delivery_address_zip"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_zip').text());
			$('.order_delivery_form input[name="delivery_address_region"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_region').text());
			$('.order_delivery_form input[name="delivery_address_district"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_district').text());
			$('.order_delivery_form input[name="delivery_address_city"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_city').text());
			$('.order_delivery_form input[name="delivery_address_street"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_street').text());
			$('.order_delivery_form input[name="delivery_address_house"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_house').text());
			$('.order_delivery_form input[name="delivery_address_building"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_building').text());
			$('.order_delivery_form input[name="delivery_address_flat"]').val($('#'+ReceiverInfoElementID + ' .property.real_addr_flat').text());
			
			$('.receiver_info_form input[name="payer_info_fio"]').val($('#'+ReceiverInfoElementID + ' .property.name').text());
			$('.receiver_info_form input[name="payer_info_name"]').val($('#'+ReceiverInfoElementID + ' .property.name').text());
			$('.receiver_info_form input[name="payer_info_email"]').val($('#'+ReceiverInfoElementID + ' .property.email').text());
			$('.receiver_info_form input[name="payer_info_phone"]').val($('#'+ReceiverInfoElementID + ' .property.phone_number').text());
			
			var DeliveryTypeID = $('#'+ReceiverInfoElementID + ' .property.delivery_type').text();
			if (DeliveryTypeID > 0) {
				$('#selDeliveryType').val(DeliveryTypeID);
				
				if (!initial) {
					ChangeDeliveryType(DeliveryTypeID);
				}
			}
	
			$('#selUserReceiverType').val(PayerType);
			$('#selPropertyType').val($('#'+ReceiverInfoElementID + ' .property.property_type').text());
			OnChangeUserReceiverType();
		}

		ClearCartErrors();
	}
	
	// Выбор пользователем типа получателя
	function OnChangeUserReceiverType() {
		var PayerType = $('#selUserReceiverType').val();
		var PayerTypeClass = 'payer_type_' + PayerType;
		
		ApplyDependentClasses($('.receiver_info_form').eq(0), 'payer_type_dependent', PayerTypeClass);
		
		if (!$('#selPropertyType').val()) {
			$('#selPropertyType').val(3);
		}

		ClearCartErrors();
	}
	
	$('#selUserReceiver').change(function() {
		OnChangeUserReceiver($(this));
	});

	$('#selUserReceiverType').change(function() {
		OnChangeUserReceiverType();
	});
	
	// Переключение цвета в каталоге в мобильной версии
	function MobileColorThumbClick(elem) {
		elem.siblings('.mobile_color_thumbs .mobile_thumb_wrapper').removeClass('selected');
		elem.addClass('selected');
		var NewPath = elem.find('img.mobile_thumb').eq(0).prop('src').replace('/small/', '/thumbs/');
		elem.parents('.main-info').eq(0).find('img.big_picture').eq(0).prop('src', NewPath);
	}
	
	// Корзина
	//=================================================================
	
	$(document).ready(function() {
		// Для корзины
		$('#selUserReceiver').each(function () {
			OnChangeUserReceiver($(this), true);
		});
		
		// Переключение цвета в каталоге в мобильной версии
		$('.mobile_color_thumbs .mobile_thumb_wrapper').click(function() {
			MobileColorThumbClick($(this));
		});
		
		// Наведение мышки на маленькую иконку товара в блоке товара в каталоге
		// и увод мышки с этого блока.
		$('img.sizechange_hover').hover(function() {
			// Если картинка не конструкция
			if ($(this).attr("data-image-type") != 3) {
				// Помечаем картинку классом, чтобы знать, что мышка находится над ней
				$(this).addClass("mouse_is_here");
				
				var ImgElement = $(this);
				
				// Блок с размерами товара
				var SizeWrapperBlock = $(this).parents('.main-info').eq(0).find('.sizes_wrapper').eq(0);
				
				// Сохраняем оригинальный блок с размерами
				var OriginalSizesInfoBlock = $(this).parents('.main-info').eq(0).find('.original_sizes_info').eq(0);
				OriginalSizesInfoBlock.html(SizeWrapperBlock.html());
				
				// Получаем HTML, который будем вставлять на место оригинального блока с размерами
				var AjaxData = {};
				AjaxData['action_name'] = 'GetProductSizesHTML';
				AjaxData['product_id'] = $(this).attr("data-product-id");
				AjaxData['color_id'] = $(this).attr("data-color-id");
				jQuery.ajax({
					url: '/engine/ajax/runpostaction.php',
					type: 'GET',
					data: AjaxData,
					dataType: 'json',
					success: function(data, textStatus, xhr) {
						if (data['operation_result'] == 'ok'){
							// Вставляем, если мышка здесь
							if (ImgElement.hasClass("mouse_is_here")) {
								SizeWrapperBlock.html(data['html']);
							}
						}
					},
					error: function(xhr, textStatus, errorThrown) {
					},
					complete: function(xhr, textStatus) {
					}
				});
			}
		},
		function() {
			// Если картинка не конструкция
			if ($(this).attr("data-image-type") != 3) {
				// Блок с размерами товара
				var SizeWrapperBlock = $(this).parents('.main-info').eq(0).find('.sizes_wrapper').eq(0);
				// Получаем оригинальный блок с размерами
				var OriginalSizesInfoBlock = $(this).parents('.main-info').eq(0).find('.original_sizes_info').eq(0);
				// Восстанавливаем
				SizeWrapperBlock.html(OriginalSizesInfoBlock.html());
				// Убираем класс, обозначающий, что мышка находится здесь
				$(this).removeClass("mouse_is_here");
			}
		});
		
		$('.get_amount').spinner({
			icons: {down: "ui-icon-minus", up: "ui-icon-plus"},
			min: 1
		});
		$('.get_amount_form').spinner({
			icons: {down: "ui-icon-minus", up: "ui-icon-plus"},
			min: 1
		});
		
		// ==========================================================
		// Выбор роста в карточке товара
		
		// Выбор роста в карточке товара
		$('.growth_list_wrapper .growth_list.ctrl_group input[type="radio"]').change(function() {
			$(this).parent().siblings('label').removeClass('selected');
			var selectedLabel = $(this).parents('.growth_list.ctrl_group').find('input[type="radio"]:checked')
				.parents('label').eq(0);
			selectedLabel.addClass('selected');
		});
		// Выбирает нужный input[type="radio"] при нажатии на выбор роста в карточке товара.
		// Само по себе не работает из-за плагина Jet Zoom.
		$('.growth_list_wrapper label.jetzoom-gallery').click(function() {
			// Сбрасываем все input'ы
			$(this).siblings('label').find('input[type="radio"]').prop('checked', false);
			// Находим выбранный input
			var checkedInput = $(this).find('input[type="radio"]');
			checkedInput.prop('checked', true);
			checkedInput.change();
		});
		
		// Выбор роста в карточке товара
		// ==========================================================
	});
});
