<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
                                
<div id="footer">
    <footer>
        <div class="container footer-row">
            <div class="contacts">
                <p class="phone roistat-phone"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?></p>
                <p class="work-time"><i class="sprites-i-time"></i>время работы: пн-пт 9:00-18:00<br /><span style="padding:0 0 0 118px">сб-вс &nbsp;10:00-15:00</span></p>
                <p class="address"><i class="sprites-i-address"></i>Курск, улица 50 лет Октября 173Б</p>
                <p class="address"><i class="sprites-i-address"></i>Москва, Волжский Бульвар, д.8 (юридический)</p>
                <p class="social">Мы в соцсетях:
                <!--noindex-->
                    <a href="//vk.com/z.dama" target="_new" rel="nofollow" onclick="yaCounter1010447.reachGoal('GOVK'); return true;"><img src="<?=SITE_TEMPLATE_PATH?>/images/pic/vk.png" alt="" /></a>
                    <a href="//www.facebook.com/zdama.ru" target="_new" rel="nofollow" onclick="yaCounter1010447.reachGoal('GOFB'); return true;"><img src="<?=SITE_TEMPLATE_PATH?>/images/pic/fb.png" alt="" /></a>
                    <a href="https://twitter.com/Znatnay_Dama" target="_new" rel="nofollow" onclick="yaCounter1010447.reachGoal('GOTW'); return true;"><img src="<?=SITE_TEMPLATE_PATH?>/images/pic/tw.png" alt="" /></a>
                    <a href="//ok.ru/z.dama" target="_new" rel="nofollow" onclick="yaCounter1010447.reachGoal('GOODN'); return true;"><img src="<?=SITE_TEMPLATE_PATH?>/images/pic/od.png" alt="" /></a>
                    <a href="//www.youtube.com/channel/UC0QO4yygEz93NCZ3RQ13Ybg" target="_new" rel="nofollow" onclick="yaCounter1010447.reachGoal('GOTOUTUBE'); return true;"><img src="<?=SITE_TEMPLATE_PATH?>/images/pic/youtube.png" alt="" /></a>						
                <!--/noindex-->
                </p>
            </div>
            <div class="footer-nav a-big-block">

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu", 
                        "footer_menu", 
                        array(
                        "ROOT_MENU_TYPE" => "bottom",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "3",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "footer_menu"
                        ),
                        false
                    );?>
            </div>	
            <div class="payment a-big-block no_mobile">
                <span>Оплата без комиссии:</span>
                <div class="payment-icon">
                <noindex>
                <!--noindex-->
                    <a href="http://www.z-dama.ru/informaciya-dlya-pokupateley/retail/retail_order/" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/pay-1.png" alt="" width="88" height="55"></a>
                    <a href="http://www.z-dama.ru/informaciya-dlya-pokupateley/retail/retail_order/" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/pay-2.png" alt="" width="88" height="55"></a>
                    <a href="http://www.z-dama.ru/informaciya-dlya-pokupateley/retail/retail_order/" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/pay-4.png" alt="" width="88" style="border-radius:3px"></a>
                    <a href="http://www.z-dama.ru/informaciya-dlya-pokupateley/retail/retail_order/" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/pay-5.png" alt="" width="88" style="border-radius:3px"></a>
                </noindex>
                    <div class="last"></div>
                </div>
            </div>
            <div style="display: none;">
                    <!-- Rating@Mail.ru counter -->
                    <a href="//top.mail.ru/jump?from=2030090" rel="nofollow"><img src="//da.cf.be.a1.top.mail.ru/counter?id=2030090;t=212;l=1" style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru" /></a>
                    <!-- //Rating@Mail.ru counter -->
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="left-c"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?></div>
                <div class="right-c"><a href="/sitemap/">Карта сайта</a></div>
                <div class="center-c"><a href="/konfidecialnost/">Политика конфиденциальности</a></div>
            </div>
        </div>
    </footer>
</div>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.ui.slider.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/stickyMojo.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/site.js"></script>

<a href="#" class="scrollup" rel="nofollow" style="display: inline;">Наверх</a>

<script>
    BX.ready(function(){
        var upButton = document.querySelector('[data-role="eshopUpButton"]');
        BX.bind(upButton, "click", function(){
            var windowScroll = BX.GetWindowScrollPos();
            (new BX.easing({
                duration : 500,
                start : { scroll : windowScroll.scrollTop },
                finish : { scroll : 0 },
                transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
                step : function(state){
                    window.scrollTo(0, state.scroll);
                },
                complete: function() {
                }
            })).animate();
        })
    });
   
</script>
</body>
</html>