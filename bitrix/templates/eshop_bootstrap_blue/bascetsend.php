<?php

/*
 * добавлене в корзину
 * @link https://dev.1c-bitrix.ru/api_help/sale/classes/csalebasket/csalebasket__add.php
 *
 */

if (!CModule::IncludeModule("sale")) return;

$arFields = [
    "PRODUCT_ID" => $_POST['ID'],
    "PRICE" => $_POST['CATALOG_PRICE_1'],
    "CURRENCY" => "RUB",
    "QUANTITY" => 1,
    "LID" => LANG,
    "DELAY" => "N",
    "CAN_BUY" => "Y",
    "NAME" => $_POST['CODE'],
    "DETAIL_PAGE_URL" => $_POST['DETAIL_PAGE_URL']
];

$arProps = array();

$arProps[] = [
    "NAME" => "Цвет",
    "CODE" => "color",
    "VALUE" => "черный"
];

$arProps[] = [
    "NAME" => "Размер",
    "VALUE" => "1.5 x 2.5"
];

$arFields["PROPS"] = $arProps;

CSaleBasket::Add($arFields);
    
?>

