<?
$MESS["VR_CALLBACK_ZAKRYTQ"] = "Закрыть";
$MESS["VR_CALLBACK_SPASIBO"] = "Спасибо!";
$MESS["VR_CALLBACK_VASA_ZAAVKA_OTPRAVLE"] = "Ваша заявка отправлена.";
$MESS["VR_CALLBACK_V_BLIJAYSEE_VREMA_S"] = "В ближайшее время с вами свяжется менеджер";
$MESS["VR_CALLBACK_DLA_UTOCNENIA_DETALE"] = "для уточнения деталей.";
$MESS["VR_CALLBACK_ZAKRYTQ1"] = "Закрыть";
$MESS["VR_CALLBACK_ZAPOLNITE_FORMU_I_MY"] = "Заполните форму и мы вам";
$MESS["VR_CALLBACK_OBAZATELQNO_PEREZVON"] = "обязательно перезвоним";
$MESS["VR_CALLBACK_VVEDITE_IMA"] = "Введите имя";
$MESS["VR_CALLBACK_VVEDITE_KONTAKTNYY_T"] = "Введите контактный телефон *";
$MESS["VR_CALLBACK_ZAKAZATQ_ZVONOK"] = "Заказать звонок";
$MESS["VR_CALLBACK_ZAKAZATQ"] = "Заказать";
$MESS["VR_CALLBACK_TEXT"] = 'закажите звонок';
$MESS["VR_CALLBACK_OBRATNYY_ZVONOK"] = "обратный звонок";
$MESS["VR_CALLBACK_EMAIL_IBLOCK_ERROR"] = "Укажите Email и/или инфоблок для отправки заявок";
$MESS["VR_CALLBACK_EMAIL_ERROR"] = "Укажите корректный Email-адрес (вместо ";
$MESS["VR_BUILD_NE_UDALOSQ_OTPRAVITQ"] = "Не удалось отправить заявку. Повторите попытку позднее или позвоните по номеру, указанному на сайте";
$MESS["VR_BUILD_POLUCITE_PRIMEROV"] = "Получите 20 примеров";
$MESS["VR_BUILD_NASIH_RABOT_NA_POCTU"] = "наших работ на почту";
$MESS["VR_BUILD_BESPLATNYY_RASCET"] = "Бесплатный расчет";
$MESS["VR_BUILD_STOIMOSTI"] = "стоимости";
$MESS["VR_BUILD_KOLICESTVO_M"] = "Количество м2";
$MESS["VR_BUILD_VVEDITE"] = "Введите";
$MESS["VR_BUILD_OTPRAVITQ"] = "ОТПРАВИТЬ";
$MESS["VR_BUILD_POLUCITQ"] = "ПОЛУЧИТЬ";
$MESS["VR_BUILD_ZAKAZATQ"] = "Заказать ";
$MESS["VR_BUILD_OBRATNYY"] = "обратный";
$MESS["VR_BUILD_ZVONOK"] = "звонок
	";
?>