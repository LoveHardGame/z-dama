<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$dirName = dirname(__FILE__);
$arResult["TEMPLATE_DIR"] = substr($dirName, strpos($dirName,"/bitrix/"));
?>
<?if($arParams["INCLUDE_JQUERY"]!="N"):?><script type="text/javascript" src="<?=$arResult["TEMPLATE_DIR"];?>/js/jquery-1.9.js"></script>
<?endif;?>
<script type="text/javascript">
	function submitCallbackData(data)
	{
		//alert(data);
		data += "&IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>";
		$.ajax(
		{
			url: "<?=$arResult["CALLBACK_AJAX_PROCESSOR"]?>",
			//url: "\\bitrix\\components\\vr\\callback\\ajax\\process.php",
			type: "GET",	
			data: data,
			cache: false,
			error: function (er, ajaxOptions, thrownError) {
				alert('Sorry, unexpected error. Please try again later.');
				//alert(er.responseText);
				//alert(ajaxOptions);
				//alert(thrownError);
				return false;
			}, 
			success: function (html) 
			{		
				if (html!=1) 
				{
					alert(html);
					alert("<?=GetMessageJS("VR_BUILD_NE_UDALOSQ_OTPRAVITQ")?>");
					return false;
				}
				closeCallbackForm(null);
				$('.vr-callback-overlayBox').fadeOut();
				$('.vr-callback-popup-overlay').fadeIn('slow');
				setTimeout(' $(".vr-callback-popup-overlay").fadeOut("slow")', 5000);
			}		
		});
		return false;
	}
</script>
<style type="text/css">
.noneButton{display:none;}
.showButton{display:none;}
</style>
<div class="vr-callback-popup-overlay">
	<div class="popup-close">
		<a href="javascript:void(0)" class="popup__close"  onclick="closeSuccessForm(this);">
			x
		</a>
		<h2 style="font-family:arial; font-weight:normal;"><?=GetMessage("VR_CALLBACK_SPASIBO");?></h2>
		<p style=" font-size:20px;"><?=GetMessage("VR_CALLBACK_VASA_ZAAVKA_OTPRAVLE");?></p>
		<p style=" font-size:20px; margin-top:15px;"><?=GetMessage("VR_CALLBACK_V_BLIJAYSEE_VREMA_S");?><br /><?=GetMessage("VR_CALLBACK_DLA_UTOCNENIA_DETALE")?></p>
	</div>
</div>
<div class="vr-callback">
	<form action="" method="post" name="callbackForm" id="callbackForm" onsubmit="submitCallbackForm(this); return false;">
		<div class="popup_form" style="display: none;" onclick="closeCallbackForm(this);"></div>
		<div class="popup-w" style="display: none;">
			<a href="javascript:void(0)" class="popup__close" onclick="closeCallbackForm(this);">
				x
			</a>
			<div class="inner">
				<h2 id="lol" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_CALLBACK_ZAPOLNITE_FORMU_I_MY")?>
					<br/>
					<?=GetMessage("VR_CALLBACK_OBAZATELQNO_PEREZVON")?>
				</h2>
				<h2 id="lol2" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_BUILD_POLUCITE_PRIMEROV")?> 
					<br/>
					<?=GetMessage("VR_BUILD_NASIH_RABOT_NA_POCTU")?>
				</h2>
				<h2 id="lol3" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_BUILD_BESPLATNYY_RASCET")?>  
					<br/>
					<?=GetMessage("VR_BUILD_STOIMOSTI")?>
				</h2>
				
				<?
				$valid_data=(($arParams["EMAIL_TO"]=='')&&($arParams["IBLOCK_ID"]==''))?false:true;
				if(!($valid_data)):?>
					<div class="error-text" style="margin-bottom:40px;">
					<?=GetMessage("VR_CALLBACK_EMAIL_IBLOCK_ERROR");?>
					</div>
				<?endif;?>
				<?$valid_data = ($arParams["EMAIL_TO"] != "")?check_email($arParams["EMAIL_TO"]):true;
				if(!($valid_data)):?>
					<div class="error-text" style="margin-bottom:40px;">
					<?=GetMessage("VR_CALLBACK_EMAIL_ERROR");?>
					'<i><?=$arParams["EMAIL_TO"]?></i>')
					</div>
				<?endif;?>
				<div class="input_boxes">
				<input type="text" class="input-text" name="kvadrat" style="margin: 0 0 15px 0;" value="" id="kvadrat" title="" placeholder="<?=GetMessage("VR_BUILD_KOLICESTVO_M")?>"/>
				<input type="text" class="input-text" name="fio" style="margin: 0 0 15px 0;" value="" title="<?=GetMessage("VR_CALLBACK_VVEDITE_IMA")?>"  placeholder="<?=GetMessage("VR_CALLBACK_VVEDITE_IMA")?>"/>
					<br/>
					
					<input type="text" class="input-text" name="email" style="margin: 0 0 15px 0;" value="" id="noneDisp" title="" placeholder="<?=GetMessage("VR_BUILD_VVEDITE")?> email"/>
					<input type="text" class="input-text" name="phone" style="margin: 0 0 20px 0;" value="" title="<?=GetMessage("VR_CALLBACK_VVEDITE_KONTAKTNYY_T")?>"  placeholder="<?=GetMessage("VR_CALLBACK_VVEDITE_KONTAKTNYY_T")?>"/><br/>
					<input type="hidden" name="email_to" value="<?=$arParams["EMAIL_TO"];?>"/>
					<input type="hidden" name="PROPERTY_FIO" value="<?=$arParams["PROPERTY_FIO"];?>"/>
					<input type="hidden" name="PROPERTY_FORM_NAME" value="<?=$arParams["PROPERTY_FORM_NAME"];?>"/>
					<input type="hidden" name="page" value="<?=$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];?>"/>
					<input type="hidden" name="PROPERTY_PAGE" value="<?=$arParams["PROPERTY_PAGE"];?>"/>
					<input type="hidden" name="MAIL_TEMPLATE" value="<?=$arParams["MAIL_TEMPLATE"];?>"/>
					<input class="button hideButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit"  style="background:#ff6550; width:100%;" value=" <?=GetMessage("VR_CALLBACK_ZAKAZATQ_ZVONOK")?>" style="width: 100%;"/>
					<input class="button noneButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit"  style="background:#ff6550; width:100%;" value="<?=GetMessage("VR_BUILD_OTPRAVITQ")?>" style="width: 100%;"/>
					<input class="button showButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit"  style="background:#ff6550; width:100%;" value="<?=GetMessage("VR_BUILD_POLUCITQ")?>!" style="width: 100%;"/>
				</div>				
				<br/>
			</div>
		</div>
	</form>
</div>
<div class="vr-callback ">
	<a href="javascript:void(0);" onclick="switchCallbackForm(this);" class="callback2 " >
		<?=GetMessage("VR_CALLBACK_TEXT")?></a>
</div>