<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<hr class="style-hr1 hidden-desktop no_mobile">
<div class="container hdr a-big-block">
	<h2>Новинки</h2>
	<div id="slide_new">
		
			<?php 
			$i = 1;
			foreach ($arResult['ITEMS'] as $arItem) {
				
				$sizes = (!empty($arItem['PROPERTIES']['SIZE']['VALUE'])) ? implode(',',$arItem['PROPERTIES']['SIZE']['VALUE']) : '';
						
				if(!empty($arItem['PROPERTIES']['NEWPRODUCT']['VALUE'] )) { ?>
				
				<div class="owl-item prod-item item product_catalog_block" id="pid<? echo $i?>" onmouseover="ShowOnOverData('<? echo $i?>');" onmouseout="HideOnOverData('<? echo $i?>')">

					<div class="a-big-block label">
						<div class="main-info">
							<input class="product_link" value="<?= $arItem['DETAIL_PAGE_URL']?>" type="hidden">	
							<div class="image" href="<?= $arItem['DETAIL_PAGE_URL']?>">
								<a href="<?= $arItem['DETAIL_PAGE_URL']?>">
									<img class="big_picture" src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
								</a>
								<?if($arItem['PROPERTIES']['RECOMMENDED']['VALUE']):?>
								<div class="no_mobile" style="background-image:url(/bitrix/components/bitrix/scrollbar.horizontal/images/blue_new.jpg); height:17px; width:194px; bottom:7px; z-index:10; position:relative;"><span style="font-size:12px; color:#fff; padding-left:6px;"><?= $arItem['PROPERTIES']['RECOMMENDED']['NAME']?></span><span style="font-size:12px; color:#000; padding-left:30px;"><?= $arItem['PROPERTIES']['SEASON']['VALUE']?></span></div>
								
								<?elseif($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']):?>
								<div class="no_mobile" style="background-image:url(/bitrix/components/bitrix/scrollbar.horizontal/images/blue_new.jpg); height:17px; width:194px; bottom:7px; z-index:10; position:relative;"><span style="font-size:12px; color:#fff; padding-left:6px;"><?=$arItem['PROPERTIES']['NEWPRODUCT']['NAME']?></span><span style="font-size:12px; color:#000; padding-left:30px;"><?= $arItem['PROPERTIES']['SEASON']['VALUE']?></span></div>
								
								<?elseif($arItem['PROPERTIES']['DISCOUNT']['VALUE']):?>
								<div class="no_mobile" style="background-image:url(/bitrix/templates/eshop_bootstrap_blue/images/blue_discount.jpg); height:17px; width:194px; bottom:7px; z-index:10; position:relative;"><span style="font-size:12px; color:#fff; padding-left:6px;"><?= $arItem['PROPERTIES']['DISCOUNT']['NAME']?></span><span style="font-size:12px; color:#000; padding-left:30px;"><?= $arItem['PROPERTIES']['SEASON']['VALUE']?></span></div>

								<?elseif($arItem['PROPERTIES']['SALELEADER']['VALUE']):?>
								<div class="no_mobile" style="background-image:url(/bitrix/templates/eshop_bootstrap_blue/images/blue_hit.jpg); height:17px; width:194px; bottom:7px; z-index:10; position:relative;"><span style="font-size:12px; color:#fff; padding-left:6px;"><?= $arItem['PROPERTIES']['SALELEADER']['NAME']?></span><span style="font-size:12px; color:#000; padding-left:15px;"><?= $arItem['PROPERTIES']['SEASON']['VALUE']?></span></div>
								<?elseif($arItem['PROPERTIES']['SEASON']['VALUE']):?>
								<div class="no_mobile" style="background-image:url(/bitrix/templates/eshop_bootstrap_blue/images/alpha_blue.png); height:17px; width:194px; bottom:7px; z-index:10; position:relative;"><span style="font-size:12px; color:#000; padding-left:110px;"><?= $arItem['PROPERTIES']['SEASON']['VALUE']?></span></div>
								<?endif;?>
							</div>
							<a href="<?= $arItem['DETAIL_PAGE_URL']?>" class="title truncate"><span><?= $arItem['NAME']?></span></a>
							<div class="prices_block">
								<span class="price">
									<span class="opt"><? echo $arItem['PROPERTIES']['MINIMUM_PRICE']['VALUE'] ?> р <span>оптовая</span></span>
									<span class="rozn"><? echo $arItem['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] ?> р <span>розничная</span></span>
								</span>
							</div>	
							<div class="floated_window" id="showomoredata<? echo $i?>" style="display:none;  background: #f3f3f3; padding-bottom:2px;" >
								<div class="no_mobile">
									
									<div class="original_sizes_info" style="display: none;"></div>
									<div class="sizes_info" style="display: none;">
										<div class="size_info_block block_0">
											<div class="size">Доступные размеры:<br><span></span></div>
										</div>
									</div>
									<div class="small_thumbs_carousel owl-carousel owl-theme" style="opacity: 0; display: block;">
										<div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 132px; left: 0px; display: block;"><div class="owl-item" style="width: 33px;"><img title="Кликните для просмотра цвета" class="small_thumb sizechange_hover" src="/uploads/catalog/46/small/1490006885_6660-temno-siniy.jpg" alt="" data-block-number="0" data-color-id="37907" data-product-id="<? echo $i;?>" data-image-type="1"></div><div class="owl-item" style="width: 33px;"><div class="clear"></div></div></div></div>
					
											<div class="owl-controls clickable" style="display: none;"><div class="owl-buttons"><div class="owl-prev"></div><div class="owl-next"></div></div></div>

										</div><div class="fancy" style="display:none;"></div>
									</div>
									<div class="no_mobile">
										<div class="sizes_wrapper"><div class="size">Доступные размеры:<br> <span><? echo $sizes ?></span></div></div>
									</div>
							</div>							
							<div class="size hidden-desktop">Доступные размеры:<br> <span><? echo $sizes ?></span></div>
							<div class="mobile_color_thumbs hidden-desktop">
								<p>Цвета:</p>
								<div class="mobile_thumb_wrapper">
									<img title="Кликните для просмотра цвета" class="mobile_thumb" src="" alt="">
								</div>
							</div>
						</div>
					</div>
					<input class="popup-trigger" id="item<? echo $i;?>" type="checkbox">
				</div>
			<?php $i++; }}?>
	</div>
</div>


