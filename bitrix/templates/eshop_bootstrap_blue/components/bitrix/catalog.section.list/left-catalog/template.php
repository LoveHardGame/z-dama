<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"]) {
    
    $int = 0;
    $y = 0;
    foreach ($arResult['SECTIONS'] as &$arSection) {
    
        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
        $arSection['DEPTH_LEVEL'] == 1 ? $i = $arSection["SECTION_PAGE_URL"]: '';
        ?>
       

        <?if ($arSection['DEPTH_LEVEL'] == 1):?>
                <?if($y > 0):?>
                    </ul></li>
                    <?php $y=0;?>
                <?endif?>
                <?if($int > 0):?>
                    </ul></li>
                    <?php $int=0;?>
                <?endif?>
                <?php $parent_id = $this->GetEditAreaId($arSection['ID'])."_1";?>
                <li  id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" <?if(substr_count($APPLICATION->GetCurPage(),$arSection['SECTION_PAGE_URL'])!=0): ?>class="main_category parent current"<?else:?>class="main_category"<?endif?> >
                    <a href="<?=$arSection["SECTION_PAGE_URL"]?>" rel="nofollow" style="padding-left: 20px"><?=$arSection["NAME"]?>

                    <?php if ($arParams["COUNT_ELEMENTS"]) { ?><span><? echo $arSection["ELEMENT_CNT"]; ?></span>
                    <?}?>
                    </a>

                <?php $parent_url = $arSection['SECTION_PAGE_URL'];?>   

        <?elseif($arSection['DEPTH_LEVEL'] == 2):?>
            <?if($y > 0):?>
                </ul></li>
                <?php $y=0;?>
            <?endif?>
            <?if($int == 0):?>
                <ul id="<? echo $parent_id; ?>" style="margin-left: 0px !important;display:none;"> 
            <?endif?>
            <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" style="border-top: 1px solid #dbe4e0 !important;">
            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" rel="nofollow" style="padding-left: 35px">
                <?=$arSection["NAME"]?>
                <?php if ($arParams["COUNT_ELEMENTS"]) { ?><span><? echo $arSection["ELEMENT_CNT"]; ?></span>
                    <?}?>
                </a>
            <?php $int++;?> 
             <?php $parent_id_2 = $this->GetEditAreaId($arSection['ID'])."_1";?>
             <?php $parent_id_3 = $this->GetEditAreaId($arSection['ID']);?>
        <?elseif($arSection['DEPTH_LEVEL'] == 3):?>
            <?if($y == 0):?>
                <ul id="<? echo $parent_id_2; ?>" style="margin-left: 0px !important;display:none;">
            <?endif?>
            <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" style="border-top: 1px solid #dbe4e0 !important;">
            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" rel="nofollow" style="padding-left: 50px">
                <?=$arSection["NAME"]?>     
                <?php if ($arParams["COUNT_ELEMENTS"]) { ?><span><? echo $arSection["ELEMENT_CNT"]; ?></span>
                    <?}?>     
            </a></li>
            <?php $y++;?> 
        <?endif?>
  <script>
        $( document ).ready(function() {
            var url = location.pathname;
            var atr = $("#<?= $this->GetEditAreaId($arSection['ID']); ?>").attr("url");
            var href = $("#<?= $this->GetEditAreaId($arSection['ID']); ?> a").attr("href");

            if("<?= $parent_url ?>" != url)
                $("#<?= $parent_id ?>").find("#<?= $this->GetEditAreaId($arSection['ID']); ?>").addClass(" shadowed");
            $("#<?= $parent_id_2 ?>").find("#<?= $this->GetEditAreaId($arSection['ID']); ?>").removeClass(" shadowed");
            if (href == url) {
                $("#<?= $parent_id ?>").css("display","block");
                $("#<?= $parent_id_2 ?>").css("display","block");                
                $("#<?= $this->GetEditAreaId($arSection['ID']); ?>").show();

                $("#<?= $parent_id ?>").find("#<?= $this->GetEditAreaId($arSection['ID']); ?>").addClass(" parent current");
                if($("#<?= $parent_id_2 ?>").find("#<?= $this->GetEditAreaId($arSection['ID']); ?>").addClass(" parent current"))
                    $("#<?= $parent_id_3 ?>").removeClass(" shadowed");
                  
                $("#<?= $this->GetEditAreaId($arSection['ID']); ?>").removeClass(" shadowed");
            }
             
        });   
    </script> 
      
<?php
    }
    unset($arSection);
    echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}

?>



