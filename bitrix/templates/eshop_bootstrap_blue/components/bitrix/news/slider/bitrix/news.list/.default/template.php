<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<div class="no_mobile" style="height:110px; overflow:hidden; margin-bottom:10px;">
        <div class="owl-carousel owl-theme" id="owlbanner" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="">
                    <?php foreach($arResult["ITEMS"] as $arItem):?>
                        <div class="owl-item" style="width: 620px;">
                            <div class="owl-item" style="text-align: center; width: 620px;">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" rel="nofollow" style="text-decoration:none;" target="_blank" title="Перейти к просмотру"><img alt="" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" style="width: 100%; height: 113px;">
                                </a>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="owl-controls clickable">
                <div class="owl-pagination">
                    <?php $i=0; ?>
                    <?php foreach($arResult["ITEMS"] as $arItem):?>
                        <div class="owl-page <?= $i == 0 ?'active':''; ?>">
                            <span class=""></span>
                        </div>
                    <?php $i++; ?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>


