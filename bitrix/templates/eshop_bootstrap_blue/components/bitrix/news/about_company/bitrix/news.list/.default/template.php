<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<section class="container about no_mobile">
	<?php foreach ($arResult['ITEMS'] as $arItem){?>
	<h1><? echo $arItem['NAME'] ?></h1>
	<? echo $arItem['PREVIEW_TEXT'] ?>
	<?php }?>
</section>
