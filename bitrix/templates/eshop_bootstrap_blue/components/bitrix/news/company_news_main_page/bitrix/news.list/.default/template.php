<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

function rus_date() {
    $translate = array(
    "Monday" => "Понедельник",
    "Tuesday" => "Вторник",
    "Wednesday" => "Среда",
    "Thursday" => "Четверг",
    "Friday" => "Пятница",
    "Saturday" => "Суббота",   
    "Sunday" => "Воскресенье",  
    "January" => "Января", 
    "February" => "Февраля",   
    "March" => "Марта",   
    "April" => "Апреля",   
    "August" => "Августа",
    "September" => "Сентября",
    "October" => "Октября",
    "November" => "Ноября",
    "December" => "Декабря"    
    );
    
    if (func_num_args() > 1) {
        $timestamp = func_get_arg(1);
        return strtr(date(func_get_arg(0), $timestamp), $translate);
    } else {
        return strtr(date(func_get_arg(0)), $translate);
    }
}
?>
<section class="no_mobile">
	<div class="hdr a-big-block">
		<h2>Новости</h2></div>
	<div class="container">
	<h2 class="style-title1 hidden-desktop">Новости</h2>
	<div class="prev-news">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<article class="prev-new">
		<div class="overflow-hidden">
			<p class="date"><?php 
			if($arItem["ACTIVE_FROM"]){
				if(date('Y-m-d', time() - 86400) == date('Y-m-d',strtotime($arItem["DISPLAY_ACTIVE_FROM"]))) {
					echo 'Вчера,'.date('H:i',strtotime($arItem["ACTIVE_FROM"]));
				} elseif(date('Y-m-d') == date('Y-m-d',strtotime($arItem["DISPLAY_ACTIVE_FROM"]))){
					echo 'Сегодня,'.date('H:i',strtotime($arItem["ACTIVE_FROM"]));
				} else {
					echo rus_date('d F Y',strtotime($arItem["ACTIVE_FROM"]));
				}
			}?></p>
			<h3 class="title-p-n"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></h3>
			<p class="a-big-block"></p>
			<div id="news-id-<?=$arItem['ID']?>" style="display:inline;">
				<?echo $arItem["PREVIEW_TEXT"];?>
			</div>
			<p>
			</p>
		</div>
	</article>
	<? endforeach; ?></div>
 	<?=$arResult["NAV_STRING"]?>
</div>
</section>
