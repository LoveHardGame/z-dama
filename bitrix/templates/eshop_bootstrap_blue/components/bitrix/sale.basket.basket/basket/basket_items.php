<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;
$int = 1;
?>

<div class="container cart">
    <div class="text">
        <h2>Ваша корзина</h2>
    </div>
    <div id="site-content">
<!-- обработка заказа -->
<?php if (!empty($_POST['add'])): ?>

<?php
    global $USER;
    $user_id  = '';
    $error_text = array();
    if(!$USER->GetID()) {
        $user_name = explode(' ', $_POST["payer_info_name"]);
        $user_login = explode('@',$_POST['payer_info_email']);

        $res_email = $DB->Query("SELECT * 
         FROM b_user 
         WHERE EMAIL='".$_POST['payer_info_email']."'");

        if($res_email->SelectedRowsCount()){
          $error_text[] = "Email '".$_POST['payer_info_email']."' уже существует";
        }

        $res_phone = $DB->Query("SELECT * 
         FROM b_user 
         WHERE PERSONAL_PHONE='".$_POST['payer_info_phone']."'");

        if($res_phone->SelectedRowsCount()){
          $error_text[] = "Телефон '".$_POST['payer_info_phone']."' уже существует";
        }
        if(!$error_text) {
            $user = new CUser;
            $userFields = Array(
              "NAME"              => ($user_name[2]) ? $user_name[1] : '',
              "SECOND_NAME"       => ($user_name[2]) ? $user_name[2] : '',
              "LAST_NAME"         => $user_name[0],
              "EMAIL"             => $_POST['payer_info_email'],
              "LOGIN"             => $user_login[0],
              "LID"               => SITE_ID,
              "ACTIVE"            => "Y",
              "GROUP_ID"          => array(3,4,5),
              "PASSWORD"          => "123456",
              "CONFIRM_PASSWORD"  => "123456",
              "PERSONAL_PHONE"    => $_POST['payer_info_phone']
            );

            $ID = $user->Add($userFields);
            if (intval($ID) > 0) {
                $user_id = $ID;
                $USER->Login($user_login[0], "123456");
            } else {
                $error_text[] = $user->LAST_ERROR;
            }
        }        
    } else {
        $user_id = $USER->GetID();
    }

    foreach ($error_text as $value) {
        echo '<p><font style="color: red;">';
        echo $value;
        echo '<br></font></p>';
    }
    if($user_id) {
        $result  = explode(' ',$_POST['delivery']); 

        $arFields = array( 
            "LID" => SITE_ID, 
            "PERSON_TYPE_ID" => 1, 
            "PAYED" => "N", 
            "CANCELED" => "N", 
            "STATUS_ID" => "N", 
            "PRICE" => $_POST['inputCartSum'], 
            "CURRENCY" => CSaleLang::GetLangCurrency(SITE_ID), 
            "USER_ID" => IntVal($user_id), 
            "PAY_SYSTEM_ID" => $_POST['paydelivery'], 
            "PRICE_DELIVERY" => $result[1], 
            "DELIVERY_ID" => $result[0], 
            "TAX_VALUE" => 0.0, 
            "USER_DESCRIPTION" => "Регистрационный заказ" 
        ); 

        $ORDER_ID = CSaleOrder::Add($arFields); 
        CSaleBasket::OrderBasket($ORDER_ID, CSaleBasket::GetBasketUserID(), SITE_ID, false);

        
    }
    
?>
<?php if($user_id):?>
<script>
setTimeout( function() { 
    window.location='/personal/cart/compleat.php?o_id=' + '<?= $ORDER_ID?>' + '&sum=' + '<?=$_POST["inputCartSum"]?>' + '&name=' + '"'+'<?=$_POST["payer_info_name"]?>'+'"';
 }, 500 );
</script>
<?php endif; ?>

<?php endif; ?>



        <form action=""  method="POST" >
            <div class="ProductsWrapper">
                <div class="tabs" id="showcart_tabs">
                    <label class="tab retail_order active" for="tab1">Корзина</label>
                </div>
                <div class="tabs-content">
                    <input checked class="hider" id="tab1" name="tab" type="radio">
                    <div class="tab-content" id="retail_order_pane">

                        <!-- вывод товаров в цикле  -->
                    <?php foreach ($arResult["GRID"]["ROWS"] as $k => $arItem): ?>
                    
                        <div class="item">
                            <img alt="" class="image" style="height:109px !important"  src="<?=CFile::GetPath($arItem['PROPS'][2]['VALUE'])?>" width="72">
                            <a class="title" href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $arItem['NAME']; ?></a>
                            <div class="control">
                                <span>
                                <i class="sprites-del-icon"></i>
                                    <a href="/personal/cart/index.php?basketAction=delete&id=<?= $arItem['ID']; ?>">удалить</a>
                                </span>
                            </div>
                            <div class="number">
                                <div class="discount">
                                    <span>Кол-во</span>
                                    <div class="start_quantity"><?= $arItem['QUANTITY']; ?></div>
                                </div>
                                <div class="price">
                                    <span>Цена розн.</span>
                                    <?= $arItem['FULL_PRICE_FORMATED']; ?>
                                </div>                            
                                <div class="price">
                                    <span>Сумма розн</span>
                                    <div class="sum_roznica"><?= $arItem['PRICE'] * $arItem['QUANTITY']; ?></div>
                                </div>                                
                            </div>
                            
                            <div class="size">
                                <span class="t">Размер:</span>
                                <span class="l"><span class="el">
                                <span><?=$arItem['PROPS'][1]['VALUE']?></span> x
                                <input class="sizeselect retail" id="<?=$arItem['PROPS'][1]['VALUE']?>" name="" onchange="Recalculate(this);" title="<?= $int?>" type="text" value="<?= $arItem['QUANTITY']; ?>"></span>
                                </span>
                                <input type="hidden" name="price_roznica" value="<?= $arItem['FULL_PRICE']?>">
                               
                            </div>
                            <div class="color">
                                <span class="t">Цвет:</span> <span class="color_red"><?= $arItem['PROPS'][0]['VALUE']; ?></span>
                            </div>
                            <div class="color width_basket">
                                <span class="title" style="font-size:11px;"></span>
                            </div>
                            <div class="status_cart new_cart">
                                Новинка
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                    <?php endforeach; $int++;?>
                        <div class="bottom">
                            <div class="col_left">
                                <p style="color: #961e17; font-size: 14px;">
                                Минимальная сумма оптового заказа: 12 000 рублей.
                                </p>
                                <p style="font-size: 14px; margin-bottom: 20px;">
                                До оптового заказ не хватает товара на сумму 
                                <strong style="color: #961e17;">
                                    <?php 
                                        $strWithoutChars = preg_replace('/[^0-9]/', '', $arResult['allSum']);
                                        $res = $strWithoutChars-12000;
                                        echo number_format(preg_replace('/[^0-9]/', '', $res),0,',', ' ').' рублей';
                                    ?> 
                            
                                </strong> по оптовым ценам.
                                </p>
                            </div>
                            <div class="col_right total_frame">
                                <input id="inputCartSum" name="inputCartSum" type="hidden" value="<?= $arResult['allSum'] ?>">
                                <p class="total">Итого в розницу: 
                                <span id="total_price_rozn" style="font-weight: bold; font-size: 20px;"><?= $arResult['allSum'] ?></span>
                                
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="tabs-content" style="padding: 10px; border: 1px solid #9c1e17;">
                        <h3>Внимание! Акция для розничных покупателей!</h3>При <strong>100% предоплате</strong> Вашего заказа,Вы <strong>получаете скидку -10%</strong>
                        <p style="color: gray; font-size: 9px; font-style: italic;">Акция не распространяется на товар из раздела "распродажа", а также другие модели, уже имеющие скидку</p>
                    </div>
    <!-- //////////////////////////// ОФОРМЛЕНИЕ ЗАКАЗА ///////////////////////////////////////////////////////////-->
                    <div class="forms" style="display:block;">
                        <div class="no_desktop cart_error_messages_block form-info form-info-error" style="display: none;"></div>
                        <div class="col_left">
                            <div class="form form2 receiver_info_form">
                                <h2>Получатель</h2>

                                <div class="el">
                                    <select class="inp2" id="selUserReceiver" name="user_receiver">
                                        <option value="0">
                                            Новый получатель
                                        </option>
                                    </select>
                                </div>
                                <div class="el">
                                    <select class="inp2" id="selUserReceiverType" name="user_receiver_type">
                                        <option value="1">
                                            Физическое лицо
                                        </option>
                                        <option selected value="2">
                                            Юридическое лицо
                                        </option>
                                        <option value="3">
                                            ИП
                                        </option>
                                    </select>
                                </div>
                                <div id="selPropertyType_div" class="el payer_type_dependent payer_type_2" style="height: 34px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 15px;">
                                    <select class="inp2" id="selPropertyType" name="user_property_type">
                                        <option value="1">
                                            ОАО
                                        </option>
                                        <option value="2">
                                            ЗАО
                                        </option>
                                        <option selected value="3">
                                            ООО
                                        </option>
                                    </select>
                                </div>

                                <div class="el payer_type_dependent payer_type_2" style="height: 34px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 15px;">
                                    <label class="inp2">ФИО<span class="required_star">*</span></label>
                                    <div class="control">
                                        <input class="inp2" name="payer_info_name" placeholder="Иванова Анна Ивановна" type="text" value="" required="required">
                                    </div>
                                </div>
                                <div class="el">
                                    <label class="inp2">Электронная почта</label>
                                    <div class="control">
                                        <input class="inp2" name="payer_info_email" placeholder="ivanova@mail.ru" type="text" value="" required="required">
                                    </div>
                                </div>
                                <div class="el">
                                    <label class="inp2">Мобильный телефон <span class="required_star">*</span>+7</label>
                                    <div class="control">
                                        <input class="inp2" name="payer_info_phone" placeholder="(925) 336-6954" required="required" type="text" value="">
                                    </div>
                                </div><!-- payment_ways -->
                            </div>
                            <div class="no_mobile cart_error_messages_block form-info form-info-error" style="display: none;"></div>
                            <div class="delivery_info_wrapper"></div>
                        </div>

                        <div class="col_right">
                            <div class="form form2">
                                <h2>Оплата</h2>
                                <div class="el">
                                    <label>Тип оплаты <span class="required_star">*</span></label> 
                                    <select class="inp2" id="selPayDeliveryType" name="paydelivery">
                                        <?php // Выведем все активные платежные системы для текущего сайта, для типа плательщика с кодом 2, работающие с валютой RUR
                                            $db_ptype = CSalePaySystem::GetList(
                                                $arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), 
                                                Array("LID"=>SITE_ID, "CURRENCY"=>"RUB", "ACTIVE"=>"Y", "PERSON_TYPE_ID"=>2)
                                            );
                                        
                                            while ($ptype = $db_ptype->Fetch()): ?>
                                            <option value="<?echo $ptype["ID"] ?>">
                                                <?= $ptype["PSA_NAME"]; ?>
                                            </option>
                                            <?php endwhile; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col_right">
                            <div class="form form2 order_delivery_form">
                                <h2>Адрес доставки</h2>
                                <div class="el">
                                    <label>Тип доставки <span class="required_star">*</span></label> 
                                    <select class="inp2" name="delivery">
                                    <?php
                                        // Выберем отсортированные по индексу сортировки, а потом (при равных индексах) по имени
                                        // активные службы доставки, доступные для текущего сайта, заказа с весом $ORDER_WEIGHT и 
                                        // стоимостью $ORDER_PRICE (в базовой валюте текущего сайта), доставки в 
                                        // местоположение $DELIVERY_LOCATION
                                        $db_dtype = CSaleDelivery::GetList(
                                            [
                                                "SORT" => "ASC",
                                                "NAME" => "ASC"
                                            ],
                                            [
                                                "LID" => SITE_ID,
                                                "+<=WEIGHT_FROM" => $ORDER_WEIGHT,
                                                "+>=WEIGHT_TO" => $ORDER_WEIGHT,
                                                "+<=ORDER_PRICE_FROM" => $ORDER_PRICE,
                                                "+>=ORDER_PRICE_TO" => $ORDER_PRICE,
                                                "ACTIVE" => "Y",
                                                "LOCATION" => $DELIVERY_LOCATION
                                            ],
                                            false,
                                            false,
                                            array()
                                        );

                                        while ($ar_dtype = $db_dtype->Fetch()): ?>
                                        <option  value="<?php echo $ar_dtype["ID"].' '.$ar_dtype["PRICE"]; ?>">
                                        <?= $ar_dtype["NAME"]; ?>
                                        </option>
                                        <?php endwhile; ?>
                                    </select>
                                </div>
                                <div class="delivery_address_form delivery_type_dependent delivery_no_self_delivery" style="">
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Индекс <span class="required_star">*</span></label>
                                        <div class="control">
                                            <input class="inp2" name="delivery_address_zip" placeholder="307240" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Область (регион) <span class="required_star">*</span></label>
                                        <div class="control">
                                            <input autocomplete="off" class="inp2 ui-autocomplete-input" name="delivery_address_region" placeholder="Курская область" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Район</label>
                                        <div class="control">
                                            <input autocomplete="off" class="inp2 ui-autocomplete-input" name="delivery_address_district" placeholder="Курский район" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Город (нас. пункт) <span class="required_star">*</span></label>
                                        <div class="control">
                                            <input autocomplete="off" class="inp2 ui-autocomplete-input" name="delivery_address_city" placeholder="Курск" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Улица <span class="required_star">*</span></label>
                                        <div class="control">
                                            <input autocomplete="off" class="inp2 ui-autocomplete-input" name="delivery_address_street" placeholder="ул. 50 лет Октября" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Дом <span class="required_star">*</span></label>
                                        <div class="control">
                                            <input class="inp2" name="delivery_address_house" placeholder="173" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Корпус</label>
                                        <div class="control">
                                            <input class="inp2" name="delivery_address_building" placeholder="Б" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="el" style="display: none;">
                                        <label class="inp2">Квартира</label>
                                        <div class="control">
                                            <input class="inp2" name="delivery_address_flat" placeholder="24" type="text" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p><span class="required_star">*</span> — поля, обязательные для заполнения.</p>
                            </div>
                            <div class="submit">
                                <button class="inp2 inp3 gray recalc_btn" type="button">
                                    Пересчитать заказ
                                </button> 
                            
                                <button  value="add" name='add' class="inp2 inp3 green showcart_btn">
                                    оформить заказ
                                </button>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div data-retailrocket-markup-block="571a381f65bf1933680292fa"></div>
                    </div>
                </div>
        </form>
    </div>
</div>



<script type="text/javascript">
$(document).ready(function() {
    $('#selUserReceiverType').change(function() {
        if($('#selUserReceiverType').val() == 1 || $('#selUserReceiverType').val() == 3) {
            $('#selPropertyType_div').hide();
        } else {
            $('#selPropertyType_div').show();
        }
    });
});

function Recalculate(sel)
{
    
    var QUANTITY = $(sel).val();    
    var price = $(sel).closest('.item').find('input[name="price_roznica"]').val();
    var start_quantity = $(sel).closest('.item').find('.start_quantity').html();
    var total_quantity = document.getElementById("quantity").innerHTML;
    total_quantity = parseInt(total_quantity) - parseInt(start_quantity);
    total_quantity = total_quantity + parseInt(QUANTITY);
    
    var old_price = parseInt(start_quantity) * parseInt(price);
    var new_price = parseInt(QUANTITY) * parseInt(price);
    $(sel).closest('.item').find('.sum_roznica').html(new_price);
    $(sel).closest('.item').find('.start_quantity').html(QUANTITY);
    
    var old_total_price = parseInt($("#inputCartSum").val()) - parseInt(old_price);
    var new_total_price = parseInt(old_total_price) + parseInt(new_price);
   
    $("#inputCartSum").val(new_total_price);
    document.getElementById("quantity").innerHTML = total_quantity;
    document.getElementById("total_price").innerHTML = new_total_price;
    document.getElementById("total_price_rozn").innerHTML = new_total_price;
}
</script>