<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<pre>';
// print_r($arResult);
// echo '</pre>';





if (strlen($arResult["ERROR_MESSAGE"]) <= 0) {

    $normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
    $normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';
    $delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
    $delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';
    $subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
    $subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';
    $naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
    $naHidden = ($naCount == 0) ? 'style="display:none;"' : '';

    //вывод корзины  
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");


} else { ?>

<div class="container cart">
    <div class="text">
        <h2>Ваша корзина</h2>
    </div>
     <div id="site-content">
        <div class="textContent">
            <h1>Корзина пустая - нет добавленных позиций!</h1>
            <p>
                <br>Возможно Вы не добавили товары в корзину. 
                <br>
                <strong>Если у Вас возникли трудности в работе с сайтом, позвоните по бесплатному номеру 8-800-333-34-24 и мы Вам обязательно поможем.</strong>
            </p>
            <p>
                <br>
                <a href="/catalog/">Вернуться к покупкам</a>
            </p>
        </div>
    </div>
</div>

<?php } ?>

