<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="bx-system-auth-form">

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<?if($arResult["FORM_TYPE"] == "login"):?>
<li class="a-big-table-cell">
<div class="a-big-block menu-pop menu-pop-right">
	<div class="menu-pop-content">
		<form name="system_auth_form<?=$arResult["RND"]?>" target="_top" action="<?=$arResult["AUTH_URL"]?>" method="post" class="pop-login">
		<?foreach ($arResult["POST"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<input type="checkbox" style="display:none" checked id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />
		<div class="title">Вход для клиентов</div>
		<input class="inp1" id="login_name" value="<?=$arResult["USER_LOGIN"]?>" name="USER_LOGIN" placeholder="email" onblur="var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(regex.test(this.value)) { try {rrApi.setEmail(this.value);}catch(e){}}" autocomplete="off" type="text">
		<input class="inp1" id="login_password" name="USER_PASSWORD" placeholder="пароль" autocomplete="off" type="password">

		<div class="link-remind">
		<?php 
		function str_replace_once($search, $replace, $text) 
		{ 
		   $pos = strpos($text, $search); 
		   return $pos!==false ? substr_replace($text, $replace, $pos, strlen($search)) : $text; 
		} 
		$arResult["AUTH_FORGOT_PASSWORD_URL"] = str_replace_once('forgot','auth/',$arResult["AUTH_FORGOT_PASSWORD_URL"]);?>
			<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>">Забыли пароль</a>
		</div>
		<button class="inp1" onclick="form.submit()">Войти</button>
		<!-- <input type="submit" name="Login" class="inp1" value="Войти" /> -->
		<div class="link-register">
			<a href="<?=$arResult["AUTH_REGISTER_URL"]?>">Регистрация</a>
		</div>
		<p>или войти через соц.сети:</p>
		<?
		$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", 
			array(
				"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
				"SUFFIX"=>"form",
			), 
			$component, 
			array("HIDE_ICONS"=>"Y")
		);
		?>
		<div class="socialgroup">									
			<a href="http://oauth.vk.com/authorize?client_id=4578657&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dvk&amp;scope=offline%2Cwall%2Cemail&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Войти через вКонтакте"><img src="/templates/reviews/images/social/vkontakte.png" alt=""></a>
			<a href="http://www.odnoklassniki.ru/oauth/authorize?client_id=1104117248&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dod&amp;response_type=code" target="_blank" class="sociallink" title="Войти через Одноклассники"><img src="/templates/reviews/images/social/odnoklassniki.png" alt=""></a>
			<a href="https://connect.mail.ru/oauth/authorize?client_id=725485&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dmailru&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Mail.ru"><img src="/templates/reviews/images/social/mailru.png" alt=""></a>
			<a href="https://oauth.yandex.ru/authorize?client_id=2809056793af4e14b88431416c8c7579&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dyandex&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Yandex.ru"><img src="/templates/reviews/images/social/yandex.png" alt=""></a>		
		</div>												
		<input name="login" id="login" value="submit" type="hidden"> 
		</form>
	</div>
</div>
<a href="#"><i class="icon-6"></i>Войти</a>
</li>

<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>

<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?echo GetMessage("auth_form_comp_otp")?><br />
			<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
		</tr>
<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
		<tr>
			<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
		</tr>
<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
		</tr>
	</table>
</form>

<?
else:
?>
<li class="a-big-table-cell">
	<div class="a-big-block menu-pop menu-pop-right menu-auth">
		<div class="menu-pop-content">
		<ul>
			<li><a href="/personal">Ваши данные</a></li>
			<li><a href="/personal/order">Ваши заказы</a></li>
			<form id="logout-form" action="<?=$arResult["AUTH_URL"]?>">
				<?foreach ($arResult["GET"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
				<input type="hidden" name="logout" value="yes" />
				<li><a href="#" onclick="document.getElementById('logout-form').submit();">Выход</a></li>
			</form>
		</ul>
	</div>
</div>
 <a href="<?=$arResult["PROFILE_URL"]?>"><i class="icon-6"></i>Профиль</a>
</li>
<?endif?>
</div>
