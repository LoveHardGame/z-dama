<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	ShowError(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>


<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="form form3" id="registration" name="regform" enctype="multipart/form-data">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
	<div class="el el-required el-error">
		<label class="inp3" for="login">Логин на сайте</label>
		<div class="control">
			<div class="inp-70">
				<input required="required" class="inp3" id="login" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["LOGIN"]?>" placeholder="Укажите логин" type="text">
			</div>
		</div>
	</div>
	<div class="el el-required el-error">
		<label class="inp3" for="email">Электронная почта</label>
		<div class="control">
			<div class="inp-70">
				<input required="required" class="inp3" onblur="var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(regex.test(this.value)) { try {rrApi.setEmail(this.value);}catch(e){}}" id="email" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>" placeholder="Укажите Ваш email" type="text">
			</div>
		</div>
	</div>
	<div class="el el-required el-error">
		<label class="inp3" for="pass">Пароль</label>
		<div class="control">
			<div class="inp-70">
				<input required="required" class="inp3" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"]?>" placeholder="Укажите пароль для входа на сайт" id="pass" type="password">
			</div>
		</div>
	</div>
	<div class="el el-required el-error">
		<label class="inp3" for="rep_pass">Повтор пароля</label>
		<div class="control">
			<div class="inp-70">
				<input required="required" class="inp3" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" placeholder="Повторите пароль для входа на сайт" id="rep_pass" type="password">
			</div>
		</div>
	</div>
	<div class="el el-required el-error">
		<label class="inp3">ФИО</label>
		<div class="control">
			<div class="inp-30"><input required="required" class="inp3" name="REGISTER[LAST_NAME]" value="<?=$arResult["VALUES"]["LAST_NAME"]?>" id="fio" placeholder="Фамилия" type="text"></div>
			<div class="inp-30"><input required="required" class="inp3" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>" placeholder="Имя" type="text"></div>
			<div class="inp-30"><input required="required" class="inp3" name="REGISTER[SECOND_NAME]" value="<?=$arResult["VALUES"]["SECOND_NAME"]?>" placeholder="Отчество" type="text"></div>
		</div>
	</div>
	<div class="el el-required el-error">
		<label class="inp3">Мобильный телефон</label>
		<div class="control"><div class="inp-30"><input required="required" class="inp3" name="REGISTER[PERSONAL_PHONE]"value="<?=$arResult["VALUES"]["PERSONAL_PHONE"]?>" id="phone" placeholder="+7 (123) 456-78-90" type="text"></div></div>
	</div>		
	
	<div class="el">
		<label class="inp3">&nbsp;</label>
		<div class="control">
			<label><input name="agree" checked="" type="checkbox"> я подтверждаю свою согласие на обработку персональных данных в соответствии с <a href="http://www.z-dama.ru/obrabotka-personalnoy-informacii/" target="_new" title="Перейти к просмотру политики"><strong>Политикой обработки персональных данных</strong></a>.</label>
		</div>
	</div>
	<div class="el el-nomargin">
		<label class="inp3">&nbsp;</label>
		<div class="control">
			<input class="inp2 inp3 green low_height" type="submit" name="register_submit_button" value="Зарегистрироваться" />
		</div>
	</div>
</form>

<?endif; ?>