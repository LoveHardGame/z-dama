<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="menu-top">
	<nav class="container a-odd">
		<ul>

<?
$previousLevel = 0;
$submenu = false; 
?>

	
<?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "catalog_menu",
                array(
                    "ROOT_MENU_TYPE" => "catalog",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "COMPONENT_TEMPLATE" => "catalog_menu"
                ),
                false
            );?>

<?php foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<div class="a-big-block menu-pop menu-pop-left"><div class="menu-pop-content"><ul>
		<?elseif ($arItem["DEPTH_LEVEL"] == 2):?>	
			<?php $submenu = 0; ?>		
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<div class="a-big-block2 menu-pop-left2 submenu submenu15"><div class="menu-pop-content2"><ul class="column">	
		<?else:?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<div class="a-big-block menu-pop menu-pop-left"><div class="menu-pop-content"><ul>					
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<?if ($arItem["TEXT"] == "Распродажа"):?>
				<li><a href="<?=$arItem["LINK"]?>"><b><?=$arItem["TEXT"]?></b></a></li>
				<?elseif($arItem["LINK"] == "/opros/"):?>
				<li class="a-big-table-cell"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?else:?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?endif?>
			<?elseif ($submenu == 4):?>
				</ul><ul class="column">
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?php $submenu = 1; ?>
			<?else:?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?php  if($submenu !== false){$submenu = $submenu+1;} ?>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div></div>", ($previousLevel-1) );?>
<?endif?>

	

	<li class="a-big-table-cell">
	
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.basket.basket.line",
			"cart_link",
			Array(
				"COMPONENT_TEMPLATE" => ".default",
				"HIDE_ON_BASKET_PAGES" => "N",
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",
				"PATH_TO_PROFILE" => SITE_DIR."personal/",
				"PATH_TO_REGISTER" => SITE_DIR."login/",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "N",
				"SHOW_EMPTY_VALUES" => "Y",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_PRODUCTS" => "N",
				"SHOW_TOTAL_PRICE" => "Y"
			)
		);?>
		</li>
		<!-- netu tovarov -->
		<!-- <li class="a-big-table-cell">
			<a id="basket" href="/showcart/" rel="nofollow"><i class="icon-4"></i>Корзина: нет товаров</a>
		</li> -->
		<!-- est tovari -->
		<!-- <li class="a-big-table-cell">
			<div class="a-big-block menu-pop menu-pop-right menu-cart">
				<div class="menu-pop-content">
					<ul><li>
				<img class="thumb" src="/uploads/catalog/109/small/1490168394_img_6409.jpg">
				<div class="main">
					<span class="name">Брюки 625ВС15Т укороченные</span>
				</div>
				<div class="additional">
					<span class="block">Кол-во: <span class="value">1</span>.</span>
					<span class="block last">Сумма: <span class="value">1290 руб.</span></span>
				</div>
				<div class="clear"></div>
			</li>
					</ul>
					<div class="summary">
						<a class="href_to_cart" href="/showcart/">Перейти в корзину</a>
					</div>
				</div>
			</div>
			<a id="basket" href="/showcart/" rel="nofollow"><a id="basket" href="/showcart/" title="Перейти в корзину" rel="nofollow"><i class="icon-4"></i>Корзина: 1 шт. на 1290 P</a></a>
		</li> -->
		<!-- login -->
<!-- 		<li class="a-big-table-cell">
			<div class="a-big-block menu-pop menu-pop-right">
				<div class="menu-pop-content">
					<form action="" method="post" class="pop-login">
						<div class="title">Вход для клиентов</div>
						<input class="inp1" id="login_name" name="login_name" placeholder="email" onblur="var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(regex.test(this.value)) { try {rrApi.setEmail(this.value);}catch(e){}}" autocomplete="off" type="text">
						<input class="inp1" id="login_password" name="login_password" placeholder="пароль" autocomplete="off" type="password">
						<div class="link-remind"><a href="http://www.z-dama.ru/lostpassword/">Забыли пароль</a></div>
						<button class="inp1">Войти</button>
						<div class="link-register"><a href="http://www.z-dama.ru/register/">Регистрация</a></div>
						<p>или войти через соц.сети:</p>
						<div class="socialgroup">									
						<a href="http://oauth.vk.com/authorize?client_id=4578657&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dvk&amp;scope=offline%2Cwall%2Cemail&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Войти через вКонтакте"><img src="/templates/reviews/images/social/vkontakte.png" alt=""></a>
						<a href="http://www.odnoklassniki.ru/oauth/authorize?client_id=1104117248&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dod&amp;response_type=code" target="_blank" class="sociallink" title="Войти через Одноклассники"><img src="/templates/reviews/images/social/odnoklassniki.png" alt=""></a>
						<a href="https://connect.mail.ru/oauth/authorize?client_id=725485&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dmailru&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Mail.ru"><img src="/templates/reviews/images/social/mailru.png" alt=""></a>
						<a href="https://oauth.yandex.ru/authorize?client_id=2809056793af4e14b88431416c8c7579&amp;redirect_uri=http%3A%2F%2Fwww.z-dama.ru%2Findex.php%3Fdo%3Dauth-social%26provider%3Dyandex&amp;state=7e6661b906a7d91663f2eb66488151e0&amp;response_type=code" target="_blank" class="sociallink" title="Yandex.ru"><img src="/templates/reviews/images/social/yandex.png" alt=""></a>		
						</div>												
						<input name="login" id="login" value="submit" type="hidden"> 
					</form>
				</div>
			</div>
			<a href="#"><i class="icon-5"></i>Войти</a>												
		</li> -->
			<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "menu_login", Array(
				"FORGOT_PASSWORD_URL" => "/forgot",	// Страница забытого пароля
					"PROFILE_URL" => "/personal",	// Страница профиля
					"REGISTER_URL" => "/register",	// Страница регистрации
					"SHOW_ERRORS" => "N",	// Показывать ошибки
				),
				false
			);?>
			
		</ul>
	</nav>
</div>
<?endif?>