<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;

$previousLevel = 0;
$submenu = false; 
?>
<li><a href="#">Каталог</a>
				<div class="a-big-block menu-pop menu-pop-left"><div class="menu-pop-content"><ul>
<?php 
foreach($arResult as $arItem) {
	if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
		 echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
	}
	if ($arItem["IS_PARENT"] == 1) {
		$submenu = 0;
?>	
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<div class="a-big-block2 menu-pop-left2 submenu submenu15"><div class="menu-pop-content2"><ul class="column">	
<?php
	}
	else {
?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?php
	}
	$previousLevel = $arItem["DEPTH_LEVEL"];
}
?>
</ul></div></div>
<?if ($previousLevel > 2)://close last item tags?>
	<?=str_repeat("</ul></div></div>", ($previousLevel-2) );?>
<?endif?>
