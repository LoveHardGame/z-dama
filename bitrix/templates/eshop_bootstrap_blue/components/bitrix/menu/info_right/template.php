<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["ALL_ITEMS"]))
	return;

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/menu/index.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/main.js");
?>

<aside id="aside" class="padding_top">
	<div class="aside-menu">
		<ul class="a-odd">
<? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>
<? $class = ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]) ? 'main' : ''; ?>
	<li class="<?=$class?>">
	    <a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></a>
	</li>
	<?if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"] && is_array($arColumns) && count($arColumns) > 0):?>
		<?foreach($arColumns as $key=>$arRow):?>
			<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>
			<? $subclass = ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]) ? 'subitem_select' : 'subitem'; ?>
			<li class="<?=$subclass?>">
				<a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
			</li>
			<? endforeach; ?>
		<?endforeach ?>
	<? endif; ?>
<? endforeach; ?>
		</ul>
	</div>
</aside>




