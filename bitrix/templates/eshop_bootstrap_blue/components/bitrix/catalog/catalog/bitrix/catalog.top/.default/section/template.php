<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$this->setFrameMode(true);
// ЭТО ВКЛАДКА "Знатная распродажа и последний размер"
// ID свойства распродажа - 68
// ID свойства последний размер - 213
if((isset($_GET['SECTION_ID']) && $_GET['SECTION_ID'] == 88) || stristr($APPLICATION->GetCurPage(true), 'discount')) {
    $all_items = $arResult['ITEMS'];
    $arResult['ITEMS'] = array();
    foreach($all_items as $item) {
        if($item['PROPERTIES']['SALE']['VALUE'] == 'да') $arResult['ITEMS'][] = $item;
    }
} elseif(isset($_GET['SECTION_ID']) && $_GET['SECTION_ID'] == 87) {
    if($item['PROPERTIES']['POSLEDNIY_RAZMER']['VALUE'] == 'да') $arResult['ITEMS'][] = $item;;
}
//print_r($arResult['ITEMS']);
//die();
if (!empty($arResult['ITEMS'])) {

  

// *********************************НАЧАЛО КАТАЛОГА****************************************//


?>


<!-- слайдер  -->
<div class="col_right">
<?php  $APPLICATION->IncludeComponent(
        "bitrix:slids",
        "",
        [
            "CACHE_NOTES" => "",
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "IBLOCK_ID" => "9",
            "IBLOCK_TYPE" => ""
        ]
    );
?>
    <section>
        <div class="no_mobile"></div>
    <!--noindex-->
    <!-- ///////////////////////////////////////////ФИЛЬТР/////////////////////////////////////////////// -->
        <form enctype="application/x-www-form-urlencoded" id="bigfilter" method="get" name="bigfilter" onsubmit="yaCounter1010447.reachGoal('SetFilter'); return true;">
            <input name="filter" type="hidden" value="1">
            <div class="fw-wrap">
                <div class="fw-item fw1">
                    <div class="fw-select">
                        <span>Цена</span>
                    </div>
                    <div class="fw-popup">
                        <div class="slider noUi-target noUi-ltr noUi-horizontal noUi-background" data-end="6690" data-max="6690" data-min="390" data-start="390" id="slider">
                           
                        </div>
                        <div class="fw-inputs">
                            <label for="fw-range_from">от</label>
                            <input class="fw-input" id="input-from" name="rangeFrom" type="text" value="">
                            <label for="range_to">до</label>
                            <input class="fw-input fw-input--last" id="input-to" name="rangeTo" type="text" value=""> руб.
                        </div>
                        <div class="fw-line"></div>
                        <div class="fw-actions">
                            <div class="fw-clear">
                                <a href="#" id="btn_big_filter_reset" onclick="ClearFilterType('range'); return false;">Очистить</a>
                            </div>
                            <div class="fw-submit">
                                <button class="fw-btn" type="submit">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fw-item fw2">
                    <div class="fw-select">
                        <span>Размеры</span>
                    </div>
                    <div class="fw-popup">
                        <div class="fw-row">
                            <div class="fw-col50">
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="42"> 42</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="44"> 44</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="46"> 46</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="48"> 48</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="50"> 50</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="52"> 52</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="54"> 54</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="56"> 56</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="58"> 58</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="60"> 60</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="62"> 62</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="64"> 64</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="66"> 66</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="68"> 68</label>
                                <label class="label">
                                <input id="size" name="size[]" type="checkbox" value="70"> 70</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label"><input id="size" name="size[]" type="checkbox" value="72"> 72</label>
                            </div>
                        </div>
                        <div class="fw-line"></div>
                        <div class="fw-actions">
                            <div class="fw-clear">
                                <a href="#" id="btn_big_filter_reset" onclick="ClearFilterType('size'); return false;">Очистить</a>
                            </div>
                            <div class="fw-submit">
                                <button class="fw-btn" type="submit">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fw-item fw3">
                    <div class="fw-select">
                        <span>Сезоны</span>
                    </div>
                    <div class="fw-popup">
                        <div class="fw-row">
                            <div class="fw-col50">
                                <label class="label">
                                <input name="season[]" type="checkbox" value="3"> Всесезон</label>
                                <label class="label">
                                <input name="season[]" type="checkbox" value="2"> Осень-Зима</label>
                                <label class="label">
                                <input name="season[]" type="checkbox" value="1"> Весна-Лето</label>
                            </div>
                        </div>
                        <div class="fw-line"></div>
                        <div class="fw-actions">
                            <div class="fw-clear">
                                <a href="#" id="btn_big_filter_reset" onclick="ClearFilterType('season'); return false;">Очистить</a>
                            </div>
                            <div class="fw-submit">
                                <button class="fw-btn" type="submit">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fw-item fw1">
                    <div class="fw-select">
                        <span>Цвета</span>
                    </div>
                    <div class="fw-popup">
                        <div class="fw-row">
                            <div class="fw-col50">
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68401"> белый</label>
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68402"> бежевый</label>
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68403"> серый</label>
                                <label class="label"><input name="color[]" type="checkbox" value="68404"> желтый</label>
                                <label class="label"><input name="color[]" type="checkbox" value="68406"> розовый</label>
                                <label class="label"><input name="color[]" type="checkbox" value="68407"> синий</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68408"> красный</label>
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68409"> зеленый</label>
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68410"> коричневый</label>
                                <label class="label">
                                <input name="color[]" type="checkbox" value="68430"> черный</label>
                            </div>
                        </div>
                        <div class="fw-line"></div>
                        <div class="fw-actions">
                            <div class="fw-clear">
                                <a href="#" id="btn_big_filter_reset" onclick="ClearFilterType('color'); return false;">Очистить</a>
                            </div>
                            <div class="fw-submit">
                                <button class="fw-btn" type="submit">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fw-item fw1">
                    <div class="fw-select">
                        <span>Ткань</span>
                    </div>
                    <div class="fw-popup">
                        <div class="fw-row">
                            <div class="fw-col50">
                                <label class="label">
                                <input name="material[]" type="checkbox" value="6"> текстиль</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="5"> трикотаж</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="7"> шифон</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="9"> искусственная кожа</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="4"> масло</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="12"> джинс</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="21"> плащёвка</label>
                                <label class="label">
                                <input name="material[]" type="checkbox" value="2"> вискоза</label>
                            </div>
                            <div class="fw-col50">
                                <label class="label"><input name="material[]" type="checkbox" value="20"> пальтовая ткань</label>
                            </div>
                        </div>
                        <div class="fw-line"></div>
                        <div class="fw-actions">
                            <div class="fw-clear">
                                <a href="#" id="btn_big_filter_reset" onclick="ClearFilterType('material'); return false;">Очистить</a>
                            </div>
                            <div class="fw-submit">
                                <button class="fw-btn" type="submit">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <!-- ///////////////////////////////////////////СОРТИРОВКА/////////////////////////////////////////////// -->
        <div class="fw-nav">
            <div class="fw-sort">
                Сортировать по: <select name="sort_c">
                    <option selected="selected" value="sorting_price_no">
                        новизне
                    </option>
                    <option value="sorting_price_asc">
                        цене
                    </option>
                    <option value="sorting_position">
                        позиции
                    </option>
                </select>
            </div>
            <div class="fw-show">
<!--                Найдено --><?//=  CIBlock::GetElementCount($arResult['CATALOG']['PRODUCT_IBLOCK_ID']); ?><!-- товаров-->
                Найдено <?= count($arResult['ITEMS']); ?> товаров
            </div>
            <div class="fw-show no_mobile" style="float:right;">
                <form class="objects_per_page" method="GET">
                    Отобразить по: 
                    <select name="objects_per_page">
                        <option selected="selected" value="30">
                            30
                        </option>
                        <option value="60">
                            60
                        </option>
                        <option value="90">
                            90
                        </option>
                    </select>
                </form>
            </div>
        </div>
    <!--/noindex-->
    </section>
<!-- ///////////////////////////////////////////НАЧАЛО КАТАЛОГА/////////////////////////////////////////////// -->
    <h1 style="font-size:18px;">Женская одежда</h1>
        <div id="site-content">

            <?php
                // пагинация над каталогом
                echo $arResult["NAV_STRING"];

                $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
                $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
                $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
            ?>
            <div class="row">

<?php

$real_count = CIBlockSection::GetSectionElementsCount($_GET['SECTION_ID']);

foreach ($arResult['ITEMS'] as $key => $arItem) {
// echo "<pre>";
// print_r($arItem);
// echo "</pre>";

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
    $strMainID = $this->GetEditAreaId($arItem['ID']);

    $arItemIDs = [
        'ID' => $strMainID,
        'PICT' => $strMainID.'_pict',
        'SECOND_PICT' => $strMainID.'_secondpict',
        'STICKER_ID' => $strMainID.'_sticker',
        'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
        'QUANTITY' => $strMainID.'_quantity',
        'QUANTITY_DOWN' => $strMainID.'_quant_down',
        'QUANTITY_UP' => $strMainID.'_quant_up',
        'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
        'BUY_LINK' => $strMainID.'_buy_link',
        'BASKET_ACTIONS' => $strMainID.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
        'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
        'COMPARE_LINK' => $strMainID.'_compare_link',

        'PRICE' => $strMainID.'_price',
        'DSC_PERC' => $strMainID.'_dsc_perc',
        'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
        'PROP_DIV' => $strMainID.'_sku_tree',
        'PROP' => $strMainID.'_prop_',
        'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
        'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
    ];

    $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

    $productTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
        : $arItem['NAME']
    );
    $imgTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
        : $arItem['NAME']
    );

    $minPrice = false;

    if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
        $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

    ?>

<div class="owl-item prod-item item product_catalog_block" id="<?= $strMainID; ?>" >
    <div class="a-big-block label">
        <div class="main-info">
            <div class="image">
            <?php if ($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']): ?>
                <div class="percent perc"><div>- <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'].'%'; ?></div></div>
            <?php endif ?>
            
                <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                    <img alt="" class="big_picture" src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>">
                </a>

                <?php if($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']): ?>

                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Новинка</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>

                    </div>
                <?php elseif($arItem['PROPERTIES']['SALELEADER']['VALUE']): ?>
                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Лидер продаж</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>

                    </div>
                <?php elseif($arItem['PROPERTIES']['SPECIALOFFER']['VALUE']): ?>
                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Спецпредложение</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>

                    </div>
                <?php elseif($arItem['PROPERTIES']['SALE']['VALUE']): ?>

                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_discount.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">
                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Распродажа</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>
                    </div>
                <?php else: ?>

                <?php endif; ?>
            </div>
                <a class="title truncate" href="<?= $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>" style="float: left;">
                    <span><?= $productTitle; ?></span>
                </a>
            <div class="prices_block">
                
                <?php if ($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']): ?>
                    <span class="price" style="">
                        <span class="opt" style="text-decoration: line-through; font-size: 12px;"> <?= $arItem['CATALOG_PURCHASING_PRICE']; ?> р</span>
                        <span class="rozn" style="text-decoration: line-through; font-size: 12px;"><?= $arItem['CATALOG_PRICE_1']; ?> р</span>
                    </span>
                    <span class="price">
                        <span class="opt">
                           <?php 
                                $resProc = $arItem['CATALOG_PURCHASING_PRICE'] * $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']/100; 
                                echo $arItem['CATALOG_PURCHASING_PRICE'] -$resProc;

                           ?> руб. <span>оптовая</span>
                        </span>
                        <span class="rozn">
                            <?= $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_NOVAT']; ?>
                           <span>розничная</span>
                        </span>
                    </span>
               <?php else: ?>
                   <span class="price">
                       <?
                       if(empty($arResult['CATALOG_PRICE_1'])) {
                           $prices = [];
                           $res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>17, 'NAME' => $productTitle.'%'), false, false, array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL'));
                           for($i = 0; $i < $res->result->num_rows; $i++) {
                               $arElement = $res->GetNext();
                               $ar_res = CPrice::GetBasePrice($arElement['ID'], false, false);
                               $prices[] = $ar_res["PRICE"];
                               $VALUES = array();
                               $res1 = CIBlockElement::GetProperty(17, $arElement['ID'], "sort", "asc", array('CODE' => 'CML2_ATTRIBUTES'));
                               while ($ob = $res1->GetNext())
                               {
                                   $VALUES[] = $ob['VALUE'];
                               }
                               $sizes[] = $VALUES[0];
                           }
                           $arItem['CATALOG_PRICE_1'] = array_sum($prices)/count($prices);
                       }
                       ?>
<!--                            <span class="opt">-->
<!--                                --><?//= number_format($arItem['CATALOG_PURCHASING_PRICE'], 0, '', ' '); ?><!-- р <span>оптовая</span>-->
<!--                            </span>-->
                            <span class="rozn" style="color: black">
                                <?= number_format($arItem['CATALOG_PRICE_1'], 0, '', ' '); ?> р
                               <span>розничная</span>
                            </span>
                    </span>   
               <?php endif; ?> 
            </div>
            <div class="floated_window" id="showomoredata1354098" style="background: rgb(243, 243, 243); padding-bottom: 2px; display: none;">
                <div class="no_mobile">
                    <div class="original_sizes_info" style="display: none;"></div>
                    <div class="sizes_info" style="display: none;">
                        <div class="size_info_block block_0">
                            <div class="size">
                                Доступные размеры:<br>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="small_thumbs_carousel owl-carousel owl-theme" style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer">
                            <div class="owl-wrapper" style="width: 132px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);">
                                <div class="owl-item" style="width: 33px;"><img alt="" class="small_thumb sizechange_hover" data-block-number="0" data-color-id="37956" data-image-type="1" data-product-id="1354098" src="http://www.z-dama.ru/uploads/catalog/36/small/1492505542_img_9357.jpg" title="Кликните для просмотра цвета"></div>
                                <div class="owl-item" style="width: 33px;">
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-controls clickable" style="display: none;">
                            <div class="owl-buttons">
                                <div class="owl-prev"></div>
                                <div class="owl-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="fancy" style="display:none;"></div>
                </div>
                <div class="no_mobile">
                    <div class="sizes_wrapper">
                        <div class="size">
                            Доступные размеры:<br>
                            <span>50,52,54,56,58,60</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="size hidden-desktop">
                Доступные размеры:<br>
                <span>50,52,54,56,58,60</span>
            </div>
            <div class="mobile_color_thumbs hidden-desktop">
                <p>Цвета:</p>
                <div class="mobile_thumb_wrapper"><img alt="" class="mobile_thumb" src="http://www.z-dama.ru/uploads/catalog/36/small/1492505542_img_9357.jpg" title="Кликните для просмотра цвета"></div>
            </div>
        </div>
    </div>
    <input class="popup-trigger" id="item1354098" type="checkbox">
</div>

<?php if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
        if (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES'])) {

        }

        $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
        if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
?>
        <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">

<?php          if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {

                foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
?>
                    <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?php
                    if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
                        unset($arItem['PRODUCT_PROPERTIES'][$propID]);
                }
            }
            $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties) {
?>
                <table>
<?php               foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) { ?>

                    <tr>
                        <td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
                        <td>
<?
                            if('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 
                                'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']) {

                                foreach($propInfo['VALUES'] as $valueID => $value) { ?>
                                
                                    <label>
                                    <input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                                    </label>
                                    <br><?
                                }
                            } else { ?>

                                <select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">

                                <?php foreach($propInfo['VALUES'] as $valueID => $value) { ?>
                                    <option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>>
                                    <? echo $value; ?>
                                        
                                    </option>
                                <?php } ?>
                                
                                </select>
                           <?php } ?>
                        </td>
                    </tr>

                   <?php } ?>
                </table>
      <?php  } ?>
        </div>
<?php
        }
        $arJSParams = [
            'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
            'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
            'SHOW_ADD_BASKET_BTN' => false,
            'SHOW_BUY_BTN' => true,
            'SHOW_ABSENT' => true,
            'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'PRODUCT' => array(
                'ID' => $arItem['ID'],
                'NAME' => $productTitle,
                'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
                'CAN_BUY' => $arItem["CAN_BUY"],
                'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
                'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
                'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
                'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
                'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
                'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
                'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
            ),
            'BASKET' => array(
                'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
                'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                'EMPTY_PROPS' => $emptyProductProperties,
                'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
            ),
            'VISUAL' => array(
                'ID' => $arItemIDs['ID'],
                'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
                'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                'PRICE_ID' => $arItemIDs['PRICE'],
                'BUY_ID' => $arItemIDs['BUY_LINK'],
                'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
                'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
            ),
            'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
        ];

        if ($arParams['DISPLAY_COMPARE']) {
        
            $arJSParams['COMPARE'] = [
                'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                'COMPARE_PATH' => $arParams['COMPARE_PATH']
            ];
        }
        unset($emptyProductProperties);
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>

<?php
    } else {

        if ('Y' == $arParams['PRODUCT_DISPLAY_MODE']) {
        
            $canBuy = $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['CAN_BUY']; ?>
        <div class="bx_catalog_item_controls no_touch">
         
          <?php if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) { ?>

        <div class="bx_catalog_item_controls_blockone">
            <a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">-</a>
            <input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
            <a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">+</a>
            <span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"></span>
        </div>
            <?
            }

            if($showSubscribeBtn):
                $APPLICATION->includeComponent('bitrix:catalog.product.subscribe','',
                   [
                        'PRODUCT_ID' => $arItem['ID'],
                        'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                        'BUTTON_CLASS' => 'bx_bt_button bx_medium',
                        'DEFAULT_DISPLAY' => !$canBuy,
                    ],
                    $component, array('HIDE_ICONS' => 'Y')
                );
            endif;

            ?>
        <div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_catalog_item_controls_blockone" style="display: <? echo ($canBuy ? 'none' : ''); ?>;">
            <span class="bx_notavailable">
               <?php echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE')); ?>
           </span>
        </div>
        <div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
            <a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="bx_bt_button bx_medium" href="javascript:void(0)" rel="nofollow">

               <?php if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {

                    echo ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));

                } else {

                    echo ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
                }
                ?>
            </a>
        </div>
        <?
    if ($arParams['DISPLAY_COMPARE']) {
   
    ?>
<div class="bx_catalog_item_controls_blocktwo">
    <a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" class="bx_bt_button_type_2 bx_medium" href="javascript:void(0)"><? echo $compareBtnMessage; ?>
    </a>
</div>
<?php } ?>
        <div style="clear: both;"></div>
        </div>
<?php unset($canBuy);
        } else { ?>
            
        <div class="bx_catalog_item_controls no_touch">
            <a class="bx_bt_button_type_2 bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
            echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
            ?>
            </a>
        </div>

      <?php }
        ?>
        <div class="bx_catalog_item_controls touch">
            <a class="bx_bt_button_type_2 bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
            echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
            ?>
            </a>
        </div>
        <?
        $boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
        $boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
        if ($boolShowProductProps || $boolShowOfferProps) { ?>
            <div class="bx_catalog_item_articul">

 <?php      if ($boolShowProductProps) {

                foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp) { ?>
              
              <br><strong><? echo $arOneProp['NAME']; ?></strong> 
                   <? echo (
                        is_array($arOneProp['DISPLAY_VALUE'])
                        ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                        : $arOneProp['DISPLAY_VALUE']
                    );
                }
            }
            if ($boolShowOfferProps) {
            
?>
                <span id="<? echo $arItemIDs['DISPLAY_PROP_DIV']; ?>" style="display: none;"></span>
<?php
            }
?>
            </div>
<?php
        }
        if ('Y' == $arParams['PRODUCT_DISPLAY_MODE']) {
        
            if (!empty($arItem['OFFERS_PROP'])) {
            
                $arSkuProps = array();
?>
                <div class="bx_catalog_item_scu" id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
                foreach ($skuTemplate as $propId => $propTemplate) {
                
                    if (!isset($arItem['SKU_TREE_VALUES'][$propId]))
                        continue;
                    $valueCount = count($arItem['SKU_TREE_VALUES'][$propId]);
                    if ($valueCount > 5) {
                        $fullWidth = ($valueCount*20).'%';
                        $itemWidth = (100/$valueCount).'%';
                        $rowTemplate = $propTemplate['SCROLL'];
                    }
                    else {
                        $fullWidth = '100%';
                        $itemWidth = '20%';
                        $rowTemplate = $propTemplate['FULL'];
                    }
                    unset($valueCount);
                    echo '<div>', str_replace(array('#ITEM#_prop_', '#WIDTH#'), array($arItemIDs['PROP'], $fullWidth), $rowTemplate['START']);
                    foreach ($propTemplate['ITEMS'] as $value => $valueItem) {
                        if (!isset($arItem['SKU_TREE_VALUES'][$propId][$value]))
                            continue;
                        echo str_replace(array('#ITEM#_prop_', '#WIDTH#'), array($arItemIDs['PROP'], $itemWidth), $valueItem);
                    }
                    unset($value, $valueItem);
                    echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $rowTemplate['FINISH']), '</div>';
                }
                unset($propId, $propTemplate);
                foreach ($arResult['SKU_PROPS'] as $arOneProp) {
                    if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']]))
                        continue;
                    $arSkuProps[] = array(
                        'ID' => $arOneProp['ID'],
                        'SHOW_MODE' => $arOneProp['SHOW_MODE'],
                        'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
                    );
                }
                foreach ($arItem['JS_OFFERS'] as &$arOneJs) {
                    if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT']) {
                        $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
                        $arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
                    }
                }
                unset($arOneJs);
                ?>
                </div>
<?php
                if ($arItem['OFFERS_PROPS_DISPLAY']) {
                
                    foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer){
                    
                        $strProps = '';
                        if (!empty($arJSOffer['DISPLAY_PROPERTIES']))
                        {
                            foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp)
                            {
                                $strProps .= '<br>'.$arOneProp['NAME'].' <strong>'.(
                                    is_array($arOneProp['VALUE'])
                                    ? implode(' / ', $arOneProp['VALUE'])
                                    : $arOneProp['VALUE']
                                ).'</strong>';
                            }
                        }
                        $arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                    }
                }
                $arJSParams = array(
                    'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                    'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
                    'SHOW_ADD_BASKET_BTN' => false,
                    'SHOW_BUY_BTN' => true,
                    'SHOW_ABSENT' => true,
                    'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
                    'SECOND_PICT' => $arItem['SECOND_PICT'],
                    'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                    'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
                    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                    'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
                    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                    'DEFAULT_PICTURE' => array(
                        'PICTURE' => $arItem['PRODUCT_PREVIEW'],
                        'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
                    ),
                    'VISUAL' => array(
                        'ID' => $arItemIDs['ID'],
                        'PICT_ID' => $arItemIDs['PICT'],
                        'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
                        'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                        'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                        'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                        'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
                        'PRICE_ID' => $arItemIDs['PRICE'],
                        'TREE_ID' => $arItemIDs['PROP_DIV'],
                        'TREE_ITEM_ID' => $arItemIDs['PROP'],
                        'BUY_ID' => $arItemIDs['BUY_LINK'],
                        'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                        'DSC_PERC' => $arItemIDs['DSC_PERC'],
                        'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
                        'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                        'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                        'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                        'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                        'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                    ),
                    'BASKET' => array(
                        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                        'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
                        'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                        'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                    ),
                    'PRODUCT' => array(
                        'ID' => $arItem['ID'],
                        'NAME' => $productTitle
                    ),
                    'OFFERS' => $arItem['JS_OFFERS'],
                    'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
                    'TREE_PROPS' => $arSkuProps,
                    'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
                );
                if ($arParams['DISPLAY_COMPARE'])
                {
                    $arJSParams['COMPARE'] = array(
                        'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                        'COMPARE_PATH' => $arParams['COMPARE_PATH']
                    );
                }
                ?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>
                <?
            }
        }
        else
        {
            $arJSParams = array(
                'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                'SHOW_QUANTITY' => false,
                'SHOW_ADD_BASKET_BTN' => false,
                'SHOW_BUY_BTN' => false,
                'SHOW_ABSENT' => false,
                'SHOW_SKU_PROPS' => false,
                'SECOND_PICT' => $arItem['SECOND_PICT'],
                'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
                'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
                'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                'DEFAULT_PICTURE' => array(
                    'PICTURE' => $arItem['PRODUCT_PREVIEW'],
                    'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
                ),
                'VISUAL' => array(
                    'ID' => $arItemIDs['ID'],
                    'PICT_ID' => $arItemIDs['PICT'],
                    'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
                    'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                    'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                    'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                    'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
                    'PRICE_ID' => $arItemIDs['PRICE'],
                    'TREE_ID' => $arItemIDs['PROP_DIV'],
                    'TREE_ITEM_ID' => $arItemIDs['PROP'],
                    'BUY_ID' => $arItemIDs['BUY_LINK'],
                    'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                    'DSC_PERC' => $arItemIDs['DSC_PERC'],
                    'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
                    'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                    'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                    'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                    'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                    'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                ),
                'BASKET' => array(
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
                    'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                ),
                'PRODUCT' => array(
                    'ID' => $arItem['ID'],
                    'NAME' => $productTitle
                ),
                'OFFERS' => array(),
                'OFFER_SELECTED' => 0,
                'TREE_PROPS' => array(),
                'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
            );
            if ($arParams['DISPLAY_COMPARE'])
            {
                $arJSParams['COMPARE'] = array(
                    'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                    'COMPARE_PATH' => $arParams['COMPARE_PATH']
                );
            }
?>

<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>

<?php
        }
    }
}
?>

</div>
<script type="text/javascript">
BX.message({
    BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
    BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
    ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
    TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
    TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
    BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
    BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
    BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
    COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
    COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
    COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
    BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
    SITE_ID: '<? echo SITE_ID; ?>'
});
</script>

<?php if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>

    <?php echo $arResult["NAV_STRING"]; ?>

<?php
    }
} 
?>
        </div>
    </div>
</div>


<style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>



<script>
function preRender(data, renderFn) {
    data = data || [];
    for(var i = 0; i < data.length; i++) {

        data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
        if(data[i].OldPrice) {
            data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
        }    

    }
    renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
    if(window.jQuery){
        $.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
            $carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
                shrinkItems:  true,
                auto: false,        //Автозапуск
                pause: 4000,        //Длительность ожидания след слайда
                pager: false,       //Навигация
                infiniteLoop: true, //Бесконечная прокрутка
                speed: 500,         //Скорость прокрутки
                slideWidth: 194,    //Ширина
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideMargin: 19,
                adaptiveHeight: true,
                onSliderLoad: function(){   //Функция после загрузки слайдера
                    console.log('slider ready');
                }
            });
        });
    }
}

retailrocket.widget.render("rr-widget");
</script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/nouislider.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/filter.js"></script>	