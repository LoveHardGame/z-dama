<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// ЭТО ВКЛАДКА "КАТАЛОГ"

// *********************************НАЧАЛО КАТАЛОГА****************************************//

?>
<div class="fw-nav">
    <div class="fw-sort"><form class="sort_c" method="post">

    Сортировать по: <select name="sort_c"><option value="sorting_price_newproduct">новизне</option><option  value="sorting_price_asc">цене</option><option  value="sorting_position">позиции</option></select>
    </form>
    </div>
    <div class="fw-show">
        Найдено  <?= count($arResult['ITEMS']) ?> товаров
    </div>
    <div class="fw-show no_mobile" style="float:right;">
        <form class="objects_per_page" method="GET">
            Отобразить по: 
            <select name="objects_per_page">
                <option  value="30">30</option>
                <option value="60">60</option>
                <option value="90">90</option>
            </select>
        </form>
    </div>
</div><!--/noindex-->
<h1 style="font-size:18px;">Женская одежда</h1>
    <?
if (count($arResult['ITEMS']) < 1) {?>
   <div class="sort"><p><b>По данному критерию товары не найдены!</b></p><div data-retailrocket-markup-block="571a383665bf1933680292fe"></div><p></p>
    </div>

<? return;}
?>
<?php if (!empty($arResult['ITEMS'])) { ?>
        <div id="site-content">

<?php
if ($arParams["DISPLAY_TOP_PAGER"]) {
    // пагинация над каталогом
    echo $arResult["NAV_STRING"];
}

$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

?>
<div class="row">

<?php $increment = 1;
foreach ($arResult['ITEMS'] as $key => $arItem) {

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
    $strMainID = $this->GetEditAreaId($arItem['ID']);

    $arItemIDs = [
        'ID' => $strMainID,
        'PICT' => $strMainID.'_pict',
        'SECOND_PICT' => $strMainID.'_secondpict',
        'STICKER_ID' => $strMainID.'_sticker',
        'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
        'QUANTITY' => $strMainID.'_quantity',
        'QUANTITY_DOWN' => $strMainID.'_quant_down',
        'QUANTITY_UP' => $strMainID.'_quant_up',
        'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
        'BUY_LINK' => $strMainID.'_buy_link',
        'BASKET_ACTIONS' => $strMainID.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
        'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
        'COMPARE_LINK' => $strMainID.'_compare_link',

        'PRICE' => $strMainID.'_price',
        'DSC_PERC' => $strMainID.'_dsc_perc',
        'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
        'PROP_DIV' => $strMainID.'_sku_tree',
        'PROP' => $strMainID.'_prop_',
        'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
        'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
    ];

    $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

    $productTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
        : $arItem['NAME']
    );
    $imgTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
        : $arItem['NAME']
    );

    $minPrice = false;
    if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
        $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);?>

<div id="pid<? $arItem['ID']?>" class="owl-item prod-item item product_catalog_block" onmouseover="ShowOnOverData('<? echo $increment?>');" onmouseout="HideOnOverData('<? echo $increment?>');" test2="">
    <div class="a-big-block label">
        <div class="main-info">
            <div class="image">
            <?php if ($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']): ?>
                <div class="percent perc"><div>- <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'].'%'; ?></div></div>
            <?php endif ?>
                <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                    <img alt="" class="big_picture" src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>"></a>

                <?php if($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']): ?>
                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Новинка</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>
                    </div>
                <?php elseif($arItem['PROPERTIES']['SALELEADER']['VALUE']): ?>
                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Лидер продаж</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>
                    </div>
                <?php elseif($arItem['PROPERTIES']['SPECIALOFFER']['VALUE']): ?>
                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_new.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">

                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Спецпредложение</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>
                    </div>
                <?php elseif($arItem['PROPERTIES']['SALE']['VALUE']): ?>

                    <div class="no_mobile" style="background:url('/bitrix/templates/eshop_bootstrap_blue/images/blue_discount.jpg'); height:17px; width:194px; bottom:7px; z-index:10; position:relative;">
                        <span style="font-size:12px; color:#fff; padding-left:6px;font-weight: 100;float: left;">Распродажа</span>
                        <span style="font-size:12px; color:#000; padding-left:55px;font-weight: 100;"><?= $arItem['PROPERTIES']['TYPE']['VALUE']; ?></span>
                    </div>
                <?php endif; ?>
            </div>
                <a class="title truncate" href="<?= $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>" style="float: left;">
                    <span><?= $productTitle; ?></span>
                </a>
            <div class="prices_block"> 
                <?php if ($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']): ?>
                    <span class="price" style="">
                        <span class="opt" style="text-decoration: line-through; font-size: 12px;"> <?= $arItem['CATALOG_PURCHASING_PRICE']; ?> р</span>
                        <span class="rozn" style="text-decoration: line-through; font-size: 12px;"><?= $arItem['CATALOG_PRICE_1']; ?> р</span>
                    </span>
                    <span class="price">
                        <span class="opt">
                           <?php
                                $resProc = $arItem['CATALOG_PURCHASING_PRICE'] * $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']/100;
                                echo $arItem['CATALOG_PURCHASING_PRICE'] -$resProc;
                           ?> руб. <span>оптовая</span>
                        </span>
                        <span class="rozn">
                            <?= $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_NOVAT']; ?>
                           <span>розничная</span>
                        </span>
                    </span>
               <?php else: ?>
                   <span class="price">
                       <?                            
                            $prices = array();
                            $price = '';
                            $sizes = array();
                            $mxResult = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);

                            if (is_array($mxResult)) {                                
                               $rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $mxResult['IBLOCK_ID'], 'PROPERTY_'.$mxResult['SKU_PROPERTY_ID'] => $arItem['ID']));
                              
                                while ($arOffer = $rsOffers->GetNext()) { 
                                    //min price start  
                                   $ar_res = CPrice::GetBasePrice($arOffer['ID'], false, false);
                                   array_push($prices, (int)$ar_res["PRICE"]);   
                                   //min price end

                                   //size
                                    $res1 = CIBlockElement::GetProperty(33, $arOffer['ID'], "sort", "asc", array('CODE' => 'CML2_ATTRIBUTES'));
                                    $i = 0;

                                    while ($ob = $res1->GetNext()) {
                                        foreach ($ob as $key => $value) {
                                            $val[$i] = $value;
                                             if($key == 'DESCRIPTION' && $value == 'Размер' && !in_array($val[$i--], $sizes)) {
                                                array_push($sizes,$val[$i--]);
                                            } $i++;
                                        }                                        
                                    }    
                                }                                 
                            } 
                            $price = min($prices);
                            if(!$price) {
                                $cprice = CPrice::GetBasePrice($arItem['ID']);
                                $price = $cprice['PRICE'];
                            }
                            
                            if($sizes) {
                                $sizes = array_unique($sizes);
                                $sizes = implode(',', $sizes);    
                            } else {
                                $sizes = $arItem['RAZMER_1'];
                            }
                        ?>

<!--                            <span class="opt">-->
<!--                                --><?//= number_format($arItem['CATALOG_PURCHASING_PRICE'], 0, '', ' '); ?><!-- р <span>оптовая</span>-->
<!--                            </span>-->
                            <span class="rozn" style="color: black">
                                <?= number_format($price, 0, '', ' '); ?> р
                               <span>розничная</span>
                            </span>
                    </span>
               <?php endif; ?>
            </div>
            <div  style="background: rgb(243, 243, 243) none repeat scroll 0% 0%; padding-bottom: 2px; display: none;" class="floated_window" id="showomoredata<? echo $increment?>" >
                <div class="no_mobile">
                    <div class="original_sizes_info" style="display: none;"></div>
                    <div class="sizes_info" style="display: none;">
                        <div class="size_info_block block_0">
                            <div class="size"> Доступные размеры:<br><span></span></div>
                        </div>
                    </div>
                    <div class="small_thumbs_carousel owl-carousel owl-theme" style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer">
                            <div class="owl-wrapper" style="width: 132px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);">
                        <?php $sku_array = CCatalogSKU::getOffersList(array($arItem['ID']));
                            $image_array = array();
                            if($sku_array) {
                                foreach ($sku_array as $key => $value) {
                                    foreach ($value as $key => $val) {         
                                       $res = CIBlockElement::GetByID($val['ID']);
                                        if($obRes = $res->GetNextElement()) {
                                            $sku = $obRes->GetFields();  
                                            if(!in_array($sku['DETAIL_PICTURE'], $image_array) && $sku["DETAIL_PICTURE"]) {
                                                array_push($image_array, $sku["DETAIL_PICTURE"]);
                                        
                            ?>
                                    <div class="owl-item" style="width: 33px;">
                                    <img alt="" class="small_thumb sizechange_hover" data-block-number="0" data-color-id="37956" data-image-type="1" data-product-id="<?=$increment?>" src="<?=CFile::GetPath($sku["DETAIL_PICTURE"])?>" title="Кликните для просмотра цвета"></div>
                            <?php           }
                                        }
                                    }
                                }
                            } else {?>
                                    <div class="owl-item" style="width: 33px;">
                                    <img alt="" class="small_thumb sizechange_hover" data-block-number="0" data-color-id="37956" data-image-type="1" data-product-id="<?=$increment?>" src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="Кликните для просмотра цвета"></div>
                            <?php } ?>
                                <div class="owl-item" style="width: 33px;">
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-controls clickable" style="display: none;">
                            <div class="owl-buttons">
                                <div class="owl-prev"></div>
                                <div class="owl-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="fancy" style="display:none;"></div>
                </div>
                <div class="no_mobile">
                    <div class="sizes_wrapper">
                        <div class="size">
                            Доступные размеры:<br><span><?= $sizes?></span></div>
                    </div>
                </div>
            </div>
            <div class="size hidden-desktop">
                Доступные размеры:<br><span><?= $sizes?></span></div>
            <div class="mobile_color_thumbs hidden-desktop">
                <p>Цвета:</p>
                <div class="mobile_thumb_wrapper"><img alt="" class="mobile_thumb" src="" title="Кликните для просмотра цвета"></div>
            </div>
        </div>
    </div>
    <input class="popup-trigger" id="item<?=$increment?>" type="checkbox">
</div>
<?php $increment++;?>
<?php if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) { ?>

       <?
        if (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']))
        {
?>

<?
        }
        $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
        if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
        {
?>
        <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
            if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
                foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
?>
                    <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
                    if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
                        unset($arItem['PRODUCT_PROPERTIES'][$propID]);
                }
            }
            $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties) { ?>
                <table>
                    <? foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
?>
                        <tr><td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
                            <td>
<?
                                if('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE'] ) {
                                    foreach($propInfo['VALUES'] as $valueID => $value)
                                    {
                                        ?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
                                    }
                                }
                                else  {
                                    ?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                                    foreach($propInfo['VALUES'] as $valueID => $value)
                                    {
                                        ?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>><? echo $value; ?></option><?
                                    }
                                    ?></select><?
                                }
?>
                            </td></tr>
<?
                    }
?>
                </table>
<?
            }
?>
        </div>
<?
        }
        $arJSParams = array(
            'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
            'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
            'SHOW_ADD_BASKET_BTN' => false,
            'SHOW_BUY_BTN' => true,
            'SHOW_ABSENT' => true,
            'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'PRODUCT' => array(
                'ID' => $arItem['ID'],
                'NAME' => $productTitle,
                'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
                'CAN_BUY' => $arItem["CAN_BUY"],
                'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
                'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
                'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
                'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
                'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
                'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
                'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
            ),
            'BASKET' => array(
                'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
                'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                'EMPTY_PROPS' => $emptyProductProperties,
                'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
            ),
            'VISUAL' => array(
                'ID' => $arItemIDs['ID'],
                'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
                'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                'PRICE_ID' => $arItemIDs['PRICE'],
                'BUY_ID' => $arItemIDs['BUY_LINK'],
                'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
                'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
            ),
            'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
        );
        if ($arParams['DISPLAY_COMPARE'])
        {
            $arJSParams['COMPARE'] = array(
                'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                'COMPARE_PATH' => $arParams['COMPARE_PATH']
            );
        }
        unset($emptyProductProperties);
?><script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script><?
   }
    else
    {
        
        ?>
       
        <?
        $boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
        $boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
        if ($boolShowProductProps || $boolShowOfferProps)
        {
?>
            <div class="bx_catalog_item_articul">
<?
            if ($boolShowProductProps)
            {
                foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp)
                {
                ?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
                    echo (
                        is_array($arOneProp['DISPLAY_VALUE'])
                        ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                        : $arOneProp['DISPLAY_VALUE']
                    );
                }
            }
            if ($boolShowOfferProps)
            {
?>
                <span id="<? echo $arItemIDs['DISPLAY_PROP_DIV']; ?>" style="display: none;"></span>
<?
            }
?>
            </div>
<?
        }
        if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
        {
            if (!empty($arItem['OFFERS_PROP']))
            {
                $arSkuProps = array();
                ?><div class="bx_catalog_item_scu" id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
                foreach ($skuTemplate as $propId => $propTemplate)
                {
                    if (!isset($arItem['SKU_TREE_VALUES'][$propId]))
                        continue;
                    $valueCount = count($arItem['SKU_TREE_VALUES'][$propId]);
                    if ($valueCount > 5)
                    {
                        $fullWidth = ($valueCount*20).'%';
                        $itemWidth = (100/$valueCount).'%';
                        $rowTemplate = $propTemplate['SCROLL'];
                    }
                    else
                    {
                        $fullWidth = '100%';
                        $itemWidth = '20%';
                        $rowTemplate = $propTemplate['FULL'];
                    }
                    unset($valueCount);
                    echo '<div>', str_replace(array('#ITEM#_prop_', '#WIDTH#'), array($arItemIDs['PROP'], $fullWidth), $rowTemplate['START']);
                    foreach ($propTemplate['ITEMS'] as $value => $valueItem)
                    {
                        if (!isset($arItem['SKU_TREE_VALUES'][$propId][$value]))
                            continue;
                        echo str_replace(array('#ITEM#_prop_', '#WIDTH#'), array($arItemIDs['PROP'], $itemWidth), $valueItem);
                    }
                    unset($value, $valueItem);
                    echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $rowTemplate['FINISH']), '</div>';
                }
                unset($propId, $propTemplate);
                foreach ($arResult['SKU_PROPS'] as $arOneProp)
                {
                    if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']]))
                        continue;
                    $arSkuProps[] = array(
                        'ID' => $arOneProp['ID'],
                        'SHOW_MODE' => $arOneProp['SHOW_MODE'],
                        'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
                    );
                }
                foreach ($arItem['JS_OFFERS'] as &$arOneJs)
                {
                    if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'])
                    {
                        $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
                        $arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
                    }
                }
                unset($arOneJs);
                ?></div><?
                if ($arItem['OFFERS_PROPS_DISPLAY'])
                {
                    foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer)
                    {
                        $strProps = '';
                        if (!empty($arJSOffer['DISPLAY_PROPERTIES']))
                        {
                            foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp)
                            {
                                $strProps .= '<br>'.$arOneProp['NAME'].' <strong>'.(
                                    is_array($arOneProp['VALUE'])
                                    ? implode(' / ', $arOneProp['VALUE'])
                                    : $arOneProp['VALUE']
                                ).'</strong>';
                            }
                        }
                        $arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                    }
                }
                $arJSParams = array(
                    'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                    'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
                    'SHOW_ADD_BASKET_BTN' => false,
                    'SHOW_BUY_BTN' => true,
                    'SHOW_ABSENT' => true,
                    'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
                    'SECOND_PICT' => $arItem['SECOND_PICT'],
                    'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                    'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
                    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                    'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
                    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                    'DEFAULT_PICTURE' => array(
                        'PICTURE' => $arItem['PRODUCT_PREVIEW'],
                        'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
                    ),
                    'VISUAL' => array(
                        'ID' => $arItemIDs['ID'],
                        'PICT_ID' => $arItemIDs['PICT'],
                        'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
                        'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                        'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                        'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                        'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
                        'PRICE_ID' => $arItemIDs['PRICE'],
                        'TREE_ID' => $arItemIDs['PROP_DIV'],
                        'TREE_ITEM_ID' => $arItemIDs['PROP'],
                        'BUY_ID' => $arItemIDs['BUY_LINK'],
                        'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                        'DSC_PERC' => $arItemIDs['DSC_PERC'],
                        'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
                        'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                        'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                        'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                        'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                        'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                    ),
                    'BASKET' => array(
                        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                        'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
                        'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                        'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                    ),
                    'PRODUCT' => array(
                        'ID' => $arItem['ID'],
                        'NAME' => $productTitle
                    ),
                    'OFFERS' => $arItem['JS_OFFERS'],
                    'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
                    'TREE_PROPS' => $arSkuProps,
                    'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
                );
                if ($arParams['DISPLAY_COMPARE'])
                {
                    $arJSParams['COMPARE'] = array(
                        'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                        'COMPARE_PATH' => $arParams['COMPARE_PATH']
                    );
                }
                ?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>
                <?
            }
        }
        else
        {
            $arJSParams = array(
                'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                'SHOW_QUANTITY' => false,
                'SHOW_ADD_BASKET_BTN' => false,
                'SHOW_BUY_BTN' => false,
                'SHOW_ABSENT' => false,
                'SHOW_SKU_PROPS' => false,
                'SECOND_PICT' => $arItem['SECOND_PICT'],
                'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
                'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
                'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                'DEFAULT_PICTURE' => array(
                    'PICTURE' => $arItem['PRODUCT_PREVIEW'],
                    'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
                ),
                'VISUAL' => array(
                    'ID' => $arItemIDs['ID'],
                    'PICT_ID' => $arItemIDs['PICT'],
                    'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
                    'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                    'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                    'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                    'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
                    'PRICE_ID' => $arItemIDs['PRICE'],
                    'TREE_ID' => $arItemIDs['PROP_DIV'],
                    'TREE_ITEM_ID' => $arItemIDs['PROP'],
                    'BUY_ID' => $arItemIDs['BUY_LINK'],
                    'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                    'DSC_PERC' => $arItemIDs['DSC_PERC'],
                    'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
                    'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                    'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                    'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                    'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
                    'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                ),
                'BASKET' => array(
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
                    'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                ),
                'PRODUCT' => array(
                    'ID' => $arItem['ID'],
                    'NAME' => $productTitle
                ),
                'OFFERS' => array(),
                'OFFER_SELECTED' => 0,
                'TREE_PROPS' => array(),
                'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
            );
            if ($arParams['DISPLAY_COMPARE'])
            {
                $arJSParams['COMPARE'] = array(
                    'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                    'COMPARE_PATH' => $arParams['COMPARE_PATH']
                );
            }
?>

<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>

<?php
        }
    }  
}
?>

</div>
<script type="text/javascript">
BX.message({
    BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
    BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
    ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
    TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
    TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
    BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
    BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
    BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
    COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
    COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
    COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
    BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
    SITE_ID: '<? echo SITE_ID; ?>'
});
</script>

<?php if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>

        <?php echo $arResult["NAV_STRING"]; ?>

<?php
    }
}
?>
        </div>
    </div>
</div>


<style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {

}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}
.rr-widget .retailrocket-item-title {

}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
}
</style>



<script>
function preRender(data, renderFn) {
        data = data || [];
        for(var i = 0; i < data.length; i++) {

            data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
            if(data[i].OldPrice) {
                data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }

        } 

        renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
    if(window.jQuery){
        $.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
            $carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
                shrinkItems:  true,
                auto: false,        //Автозапуск
                pause: 4000,        //Длительность ожидания след слайда
                pager: false,       //Навигация
                infiniteLoop: true, //Бесконечная прокрутка
                speed: 500,         //Скорость прокрутки
                slideWidth: 194,    //Ширина
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideMargin: 19,
                adaptiveHeight: true,
                onSliderLoad: function(){   //Функция после загрузки слайдера
                    console.log('slider ready');
                }
            });
        });
    }
}

//retailrocket.widget.render("rr-widget");
</script>

