<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*
 * добавлене в корзину
 * @link https://dev.1c-bitrix.ru/api_help/sale/classes/csalebasket/csalebasket__add.php
 *
 */

if (!empty($_POST['ADD'])) {
    $items = json_decode($_POST['ITEMS'], true);
    foreach ($items as $item) {
        $arFields = [
            "PRODUCT_ID" => $_POST['ID'],
            "PRICE" => $_POST['CATALOG_PRICE_1'],
            "CURRENCY" => "RUB",
            "QUANTITY" => $item['quantity'],
            "LID" => LANG,
            "DELAY" => "N",
            "CAN_BUY" => "Y",
            "NAME" => $_POST['CODE'],
            "DETAIL_PAGE_URL" => $_POST['DETAIL_PAGE_URL']
        ];

        $arProps = array();

        $arProps[] = [
            "NAME" => "Цвет",
            "CODE" => "color",
            "VALUE" => $item['color']
        ];

        $arProps[] = [
            "NAME" => "Размер",
            "CODE" => "size",
            "VALUE" => $item['size']
        ];

        $arProps[] = [
            "NAME" => "Изображение",
            "CODE" => "image",
            "VALUE" => $item['img_id']
        ];

        $arProps[] = [
            "NAME" => "Количество",
            "CODE" => "quantity",
            "VALUE" => $item['quantity']
        ];

        $arFields["PROPS"] = $arProps;

        CSaleBasket::Add($arFields); 
    }
    die();
}


/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/engine.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/catalog/index.css");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/common.js");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/productfull/jetzoom.js");

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID.'_pict',
    'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
    'STICKER_ID' => $strMainID.'_sticker',
    'BIG_SLIDER_ID' => $strMainID.'_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
    'SLIDER_LIST' => $strMainID.'_slider_list',
    'SLIDER_LEFT' => $strMainID.'_slider_left',
    'SLIDER_RIGHT' => $strMainID.'_slider_right',
    'OLD_PRICE' => $strMainID.'_old_price',
    'PRICE' => $strMainID.'_price',
    'DISCOUNT_PRICE' => $strMainID.'_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
    'QUANTITY' => $strMainID.'_quantity',
    'QUANTITY_DOWN' => $strMainID.'_quant_down',
    'QUANTITY_UP' => $strMainID.'_quant_up',
    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
    'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
    'BASIS_PRICE' => $strMainID.'_basis_price',
    'BUY_LINK' => $strMainID.'_buy_link',
    'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
    'BASKET_ACTIONS' => $strMainID.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
    'COMPARE_LINK' => $strMainID.'_compare_link',
    'PROP' => $strMainID.'_prop_',
    'PROP_DIV' => $strMainID.'_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
    'OFFER_GROUP' => $strMainID.'_set_group_',
    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
    'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);

$arElement["COLOR_IMG"] = array();
if(isset($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']) && is_array($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
    foreach($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $FILE) {
        $FILE = CFile::GetFileArray($FILE);
        if(is_array($FILE))
            $arElement["COLOR_IMG"][]=$FILE;
    }
}
// echo "<pre>";
// print_r($arResult);
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);

?>
<script>
     JetZoom.quickStart();
</script>


<!--////////////////////////////////////////ДЕТАЛЬНЫЙ ПРОСМОТР///////////////////////////////////////////////////////-->
<div class="text"></div>
<div id="site-content">
    <div vocab="http://schema.org/" typeof="Product">
<!--/////////////////////////////////////////ДЕТАЛЬНЫЙ ПРОСМОТР КАРТОЧКИ ТОВАРА////////////////////////////////////////////////-->
        <div class="text">
            <h1><span property="name"><? echo $arResult['NAME'];?></span></h1>
        </div>
        <div class="card-nav a-odd">
            <?php if(!empty($arResult["TOLEFT"])) {?>
            <a class="prev" rel="nofollow" href="<? echo $arResult["TOLEFT"]['URL'];?>"><i class="sprites-n-prev"></i> предыдущая</a> <?php }?>
            <?php if(!empty($arResult["TORIGHT"])) {?>
            <a class="next" rel="nofollow" href="<? echo $arResult["TORIGHT"]['URL'];?>">следующая <i class="sprites-n-next"></i></a><?php }?>
            <a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>" rel="nofollow">вернуться в каталог <i class="sprites-n-top"></i></a>
        </div>
        <div class="image">
            <div class="image-big">
                <?php if($arResult['PROPERTIES']['NEWPRODUCT']){?>
                <div class="percent full">
                    <div class="new_prod">Новинка</div>
                </div> <?php }?>
                <img propertyu="image" class="jetzoom" alt="Топ 105ВС0 цвет бирюзовый" id="zoom1" src="<? echo $arResult['DETAIL_PICTURE']['SRC']; ?>" data-jetzoom="zoomImage:'<? echo $arResult['DETAIL_PICTURE']['SRC']; ?>', innerZoom:true,  tintColor:'#000', tintOpacity:0.25">
            </div>
            <div class="list">
                <?php foreach($arElement["COLOR_IMG"] as $FILE):?>
                
                <a class="img"><img class="jetzoom-gallery" src="<?= $FILE['SRC']?>" data-jetzoom="zoomImage: '<?= $FILE['SRC']?>', image: '<?= $FILE['SRC']?>', useZoom:'#zoom1'" title="Кликните на изображении для его увеличения" alt=""></a>
                <?endforeach?>
            </div>
        </div>
<!--/////////////////////////////////ЦЕНЫ СОЦ СЕТИ////////////////////////////////////////////-->
        <div class="info">
            <div class="control">
                <div class="rate">
                    <a href="#bottomtabs" onclick="TabClick('bottom_tabs', 0);" rel="nofollow" title="Просмотреть или оставить отзыв" style="color: #961e17;">Посмотреть или написать отзывы</a><br>
                    <span style="float:right;">Этот товар покупали: <span style="color:#1f8c52;"></span></span>
                </div>
            </div>
            <div class="share">
                <div class="t">Поделиться с друзьями</div>
                <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                <div class="yashare-auto-init b-share_theme_counter" data-yasharel10n="ru" data-yasharequickservices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-yasharetheme="counter" data-yasharelink="http://www.z-dama.ru/catalog/showitem/1353536/"><span class="b-share"><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="ВКонтакте" class="b-share__handle b-share__link b-share-btn__vkontakte" href="https://share.yandex.net/go.xml?service=vkontakte&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="vkontakte"><span class="b-share-icon b-share-icon_vkontakte"></span><span class="b-share-counter">0</span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Facebook" class="b-share__handle b-share__link b-share-btn__facebook" href="https://share.yandex.net/go.xml?service=facebook&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="facebook"><span class="b-share-icon b-share-icon_facebook"></span><span class="b-share-counter"></span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Twitter" class="b-share__handle b-share__link b-share-btn__twitter" href="https://share.yandex.net/go.xml?service=twitter&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="twitter"><span class="b-share-icon b-share-icon_twitter"></span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Одноклассники" class="b-share__handle b-share__link b-share-btn__odnoklassniki" href="https://share.yandex.net/go.xml?service=odnoklassniki&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="odnoklassniki"><span class="b-share-icon b-share-icon_odnoklassniki"></span><span class="b-share-counter">0</span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Мой Мир" class="b-share__handle b-share__link b-share-btn__moimir" href="https://share.yandex.net/go.xml?service=moimir&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="moimir"><span class="b-share-icon b-share-icon_moimir"></span><span class="b-share-counter">0</span></a></span><span class="b-share-btn__wrap"><a rel="nofollow" target="_blank" title="Google Plus" class="b-share__handle b-share__link b-share-btn__gplus" href="https://share.yandex.net/go.xml?service=gplus&amp;url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;title=%D0%91%D0%BB%D1%83%D0%B7%D0%B0%20220%D0%94%D0%9A13%D0%9A%D0%A3%2C%20%D1%86%D0%B2%D0%B5%D1%82%3A%20%D0%BA%D0%BE%D1%80%D0%B0%D0%BB%D0%BB%D0%BE%D0%B2%D1%8B%D0%B9%20%E2%80%93%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5%20%D0%97%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%94%D0%B0%D0%BC%D0%B0" data-service="gplus"><span class="b-share-icon b-share-icon_gplus"></span><span class="b-share-counter">0</span></a></span><iframe style="display: none" src="//yastatic.net/share/ya-share-cnt.html?url=http%3A%2F%2Fwww.z-dama.ru%2Fcatalog%2Fshowitem%2F1353536%2F&amp;services=vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"></iframe></span></div> 
            </div>
            <span property="offers" typeof="Offer">
                <div class="price">
                    <meta property="priceCurrency" content="RUB">
                    <?
                    $prices = array();
                    //$price = '';
                    $sizes = array();
                    $colors = array();
                    $image_array = array();
                    $mxResult = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);

                    if (is_array($mxResult)) {
                        
                       $rsOffers = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $mxResult['IBLOCK_ID'], 'PROPERTY_'.$mxResult['SKU_PROPERTY_ID'] => $arResult['ID']));
                      
                        while ($arOffer = $rsOffers->GetNext()) { 
                            //min price start  
                           $ar_res = CPrice::GetBasePrice($arOffer['ID'], false, false);

                           array_push($prices, (int)$ar_res["PRICE"]);   
                           //min price end

                            $sku_array = CCatalogSKU::getOffersList(array($arResult['ID']));
            
                            if($sku_array) {
                                foreach ($sku_array as $key => $value) {
                                    foreach ($value as $key => $val) {         
                                       $res = CIBlockElement::GetByID($val['ID']);
                                       
                                       $property = CIBlockElement::GetProperty(33, $val['ID'], "sort", "asc", array('CODE' => 'CML2_ATTRIBUTES'));
                                        while ($object = $property->GetNext()) {
                                            if($object['DESCRIPTION'] ==  'Цвет')  
                                                $colour =  $object['VALUE'];
                                        }
                                        if($obRes = $res->GetNextElement()) {
                                            $sku = $obRes->GetFields();  
                                            if(!in_array_r($sku['DETAIL_PICTURE'], $image_array) && $sku["DETAIL_PICTURE"]) {
                                                $image_array[][$colour] = $sku["DETAIL_PICTURE"];
                                            }
                                        }
                                    }
                                }
                            }

                            $res1 = CIBlockElement::GetProperty(33, $arOffer['ID'], "sort", "asc", array('CODE' => 'CML2_ATTRIBUTES'));
                            
                            while ($ob = $res1->GetNext()) {                  
                                if($ob['DESCRIPTION'] == 'Размер') { 
                                    $razmer =$ob['VALUE'];
                                }
                                if($ob['DESCRIPTION'] ==  'Цвет') {                    
                                    array_push($colors,$ob['VALUE']);
                                    $sizes[][$ob['VALUE']] = $razmer;
                                }                         
                            }    
                        }                                 
                    } 
                    
                    $arResult['CATALOG_PRICE_1'] = min($prices);

                    if(!$arResult['CATALOG_PRICE_1']) {
                        $cprice = CPrice::GetBasePrice($arResult['ID']);
                        $arResult['CATALOG_PRICE_1'] = $cprice['PRICE'];
                    }
                    
                    if(!$sizes) {
                        $sizes[] = $arResult['PROPERTIES']['RAZMER_1']['VALUE'];
                    }

                    if($colors) {
                        $colors = array_unique($colors);                               
                    } else {
                        $colors[] = $arResult['PROPERTIES']['TSVET']['VALUE'];
                    }

                    ?>
                    <span class="rozn" property="price"><span>розничная</span><? echo number_format($arResult['CATALOG_PRICE_1'], 0, '', ' ');?> P</span>
                   <!-- <span class="opt" property="price"><span>оптовая</span>/*<? echo number_format($arResult['CATALOG_PURCHASING_PRICE'], 0, '', ' ');?>*/ P</span>-->
                </div>
            </span>
<!--//////////////////////////////////////НАЧАЛО ОБРАБОТКИ ФОРМЫ//////////////////////////////////////////////////-->
            <div class="cart-box">
                <div class="minoptsumm"></div>
                <form  id="form" >
                    <input id="product_id" value="<? echo $arResult['ID'];?>" type="hidden">
                    <input id="offerID" value="<? echo $arResult['ID'];?>" type="hidden">
            <?php 

            foreach ($colors as $value) {?>
                <div class="item">
                    <?php 
                    $color_id = '';
                    foreach ($image_array as $image) {
                        foreach ($image as $image_key => $image_val) {
                            if($image_key == $value)
                                $color_id = $image_val;
                        }
                    }
                    if ($color_id) {?>
                    <a class="img" style="height:auto !important"><img class="jetzoom-gallery" src="<?=CFile::GetPath($color_id)?>" data-jetzoom="zoomImage: '<?=CFile::GetPath($color_id)?>', image: '<?=CFile::GetPath($color_id)?>', useZoom:'#zoom1'" title="Кликните на изображении для его увеличения" alt="" style="border: 1px solid #e2e2e2;"></a>

                    <?php } ?>
                    <div class="color"><span class="t">Цвет:</span> <?=$value?></div>    
                    <div class="color"><span class="t" style="width:132px;">Рост модели на фото:</span> <?=$arResult['PROPERTIES']['ROST_1']['VALUE']?> см.</div>

                    <?php
                    $i = 1;
                    $color_array = array();
                    
                    foreach($sizes as $variable) {
                        foreach ($variable as $key => $v) {
                            if($key == $value) { 
                                array_push($color_array, $v);
                            }
                        }
                    }
                    $max_size = max($color_array);
                   
                    foreach($sizes as $variable) {
                        foreach ($variable as $key => $v) {
                            if($key == $value) {                                
                               if($i == 1 || $i % 4 == 0) { ?>
                                    <div class="size"><span class="1"> 
                                <?php }?>
                                    <span class="el"><span><?= $v?></span> x <input class="get_amount_form ui-spinner-input" maxlength="2" name="size-<?= $v?>-color/<?= $value?>/<?=$color_id?>" id="<?= $v?>" autocomplete="off" aria-valuemin="1" role="spinbutton" type="text"></span>
                                
                                <?php if(($i+1)%4 == 0 || $max_size == $v) { ?> </span></div> <?php }
                                $i++;
                            }                            
                        }
                    }
                    ?>
                </div>
           <?php }
            
        ?>
                    
                    <div class="send">
                        <button style="background-image: linear-gradient(#60a372, #426f4e) !important;" class="but" >
                            Добавить в корзину
                        </button>
                       
                    </div>
                </form>
            </div>
<!--//////////////////////////////////ОПИСАНИЕ///////////////////////////////////////////////-->

            <ul class="char">
                <li><div>Состав:</div><span><?=$arResult['DETAIL_TEXT']?></span></li>
                <li><div>Производитель:</div><span><?=$arResult['PROPERTIES']['POSTAVSHCHIK']['VALUE']?></span></li>
                <li><div>Таблица размеров:</div><span><a class="page_detail_a" href="#bottomtabs" onclick="TabClick('bottom_tabs', 1);" rel="nofollow">просмотреть</a></span></li>
                <li><div>Сертификаты качества:</div><span><a class="page_detail_a" href="/informaciya-dlya-pokupateley/certificates/" rel="nofollow" target="_new">просмотреть</a></span></li>
                <li>Цвета на фото могут незначительно отличаться из-за сложности цветопередачи.</li>
                <li>Мы выбрали качественные ткани и лучшие цветовые решения, разработали идеальную посадку и предлагаем Вам элегантную и комфортную одежду по доступной цене.</li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="clear"></div><br>
<!--////////////////////////////////ТАБЫ////////////////////////////////////////////////-->
        <noindex>
            <a name="bottomtabs"></a>
            <div class="tabs" id="bottom_tabs">
                <label for="tab0" class="tab active">Отзывы</label>
                <label for="tab1" class="tab">Размерная сетка</label>
                <label for="tab2" class="tab">Оплата товара</label>
                <label for="tab3" class="tab">Доставка товара</label>
                <label for="tab4" class="tab">Возврат товара</label>
            </div>
            <div class="tabs-content">
                <input name="tab" class="hider" id="tab0" checked="" _flx1_13_1="1" type="radio">
                <div class="tab-content">
                    <div class="feedback">
                        <h2>Отзывы о товаре</h2>
                        <div class="mp-prod_id" style="display:none"><? echo $arResult['ID'];?></div>
                        <div class="mp-prod_name" style="display:none"><? echo $arResult['NAME'];?></div>
                        <script type="text/javascript" src="//dev.mneniya.pro/js/wwwz-damaru/mneniyafeedall.js"></script>
                        <div id="mneniyapro_feed"></div>
                    </div>
                </div>
                <input name="tab" class="hider" id="tab1" _flx1_13_1="1" type="radio">                  
                <div class="tab-content">
                    <?php $APPLICATION->IncludeComponent(
                        "bitrix:razmernaya.setka",
                        "",
                        Array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "IBLOCK_ID" => "20",
                            "IBLOCK_TYPE" => "product_details"
                        )
                    );?>
                            
                </div>
                <input name="tab" class="hider" id="tab2" _flx1_13_1="1" type="radio">
                <div class="tab-content">
                    <?php $APPLICATION->IncludeComponent(
                        "bitrix:oplata.tovara",
                        "",
                        Array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "IBLOCK_ID" => "21",
                            "IBLOCK_TYPE" => "product_details"
                        )
                    );?>                    
                </div>
                <input name="tab" class="hider" id="tab3" _flx1_13_1="1" type="radio">
                <div class="tab-content">
                    <?php $APPLICATION->IncludeComponent(
                        "bitrix:dostavka.tovara",
                        "",
                        Array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "IBLOCK_ID" => "22",
                            "IBLOCK_TYPE" => "product_details"
                        )
                    );?>      
                </div>
                <input name="tab" class="hider" id="tab4" _flx1_13_1="1" type="radio">
                <div class="tab-content">
                    <?php $APPLICATION->IncludeComponent(
                        "bitrix:vozvrat.tovara",
                        "",
                        Array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "IBLOCK_ID" => "23",
                            "IBLOCK_TYPE" => "product_details"
                        )
                    );?>                                        
                </div>
            </div>
        </noindex>
<!--////////////////////////////////КОНЕЦ////////////////////////////////////////////////-->
        <div id="loading-layer" style="left: 607px;top: 329px;position: fixed;z-index: 99;background-color: rgb(91, 155, 109);opacity: 0.98;display: none;">
            <div id="loading-layer-text">Товар добавлен в корзину!</div>
        </div>
        </div>
    </div>
</div>

<script>

$( document ).ready(function() {

     function funBeforeSend() {
        $('#loading-layer').show();
    }

    function funSuccess(price,quantity) {
        var quantity_start = parseInt(0);
        var price_start = 0;
        if($("#quantity").length > 0) {
            quantity_start = parseInt(document.getElementById('quantity').innerHTML);
            price_start = parseInt(document.getElementById('total_price').innerHTML);
            price = price_start + (parseInt(price)*quantity);
            quantity = quantity_start + parseInt(quantity);
            document.getElementById('quantity').innerHTML = quantity;
            document.getElementById('total_price').innerHTML = price + ' руб.'; 
        } else {
            var basket = document.getElementById("basket_ajax");
            basket.innerHTML = "<span id='quantity'>"+quantity+"</span>  шт. на <span id='total_price'>"+parseInt(price)*quantity+" руб.</span>";
        }           
        $('#loading-layer').hide();
     }


    $('.container').removeClass('catalog');
    $('.container').addClass('card');
   
    $('#form').submit(function() {
        var data = $('#form').serializeArray();
        basket_items = {};
        quantity = 0;
        data.forEach(function(item, i, arr) {
            if(item['value'] != '') {
                basket_items[i] = {};               
                basket_items[i]['quantity'] = item['value'];
                basket_items[i]['size'] = item['name'].split('-')[1];
                basket_items[i]['color'] = item['name'].split('/')[1];
                basket_items[i]['img_id'] = item['name'].split('/')[2];
                quantity = parseInt(quantity) + parseInt(item['value']);
            }
        });       
        
        if(basket_items) {
            // ajax  обработка
            $.ajax({
                url: "",
                type: "POST",
                data: ({
                    ID: "<?= $arResult['ID']; ?>",
                    CATALOG_PRICE_1: "<?= $arResult['CATALOG_PRICE_1']; ?>",
                    CODE: "<?= $arResult['CODE']; ?>",
                    DETAIL_PAGE_URL: "<?= $arResult['DETAIL_PAGE_URL']; ?>",
                    ITEMS: JSON.stringify(basket_items),
                    ADD: 'add',
                }),
                dataType: 'html',
                beforeSend: funBeforeSend,
                success: funSuccess("<?= $arResult['CATALOG_PRICE_1']; ?>",quantity)
            });
        } else {
            SiteAlert ( "Вы не указали кол-во товаров для добавления!", site_info );
        }

        return false;
        
    });
   
});
</script>




