<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


 $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/catalog/style.css");
?>

<div class="col_left" id="upperborder">
  <aside id="leftaside" style="position: static; margin-left: 0px; top: 0px;">
    <!-- begin -->
    <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "no_desktop_catalog",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "IBLOCK_ID" => "32",
                "IBLOCK_TYPE" => "catalog",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array("",""),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array("",""),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "3",
                "VIEW_MODE" => "LIST"
            )
        );?>
    <ul class="rubric a-big-block a-odd">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "left-catalog",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "IBLOCK_ID" => "15",
                "IBLOCK_TYPE" => "catalog",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array("", ""),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array("", ""),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "3",
                "VIEW_MODE" => "LIST"
            )
        );?>

    </ul> 

    <ul class="rubric a-big-block a-odd">

    <?php
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "left-catalog",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "IBLOCK_ID" => "32",
                "IBLOCK_TYPE" => "catalog",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array("",""),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array("",""),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "3",
                "VIEW_MODE" => "LIST"
            )
        ); 
    ?>

    </ul> 
    <div class="no_mobile">
        <div id="ok_group_widget">
        </div>
        <script>
            !function (d, id, did, st) {
              var js = d.createElement("script");
              js.src = "https://connect.ok.ru/connect.js";
              js.onload = js.onreadystatechange = function () {
              if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                if (!this.executed) {
                  this.executed = true;
                  setTimeout(function () {
                    OK.CONNECT.insertGroupWidget(id,did,st);
                  }, 0);
                }
              }}
              d.documentElement.appendChild(js);
            }(document,"ok_group_widget","51925631893692","{width:290,height:135}");
        </script> 
        <script src="//vk.com/js/api/openapi.js?121" type="text/javascript">
        </script> <!-- VK Widget -->
        <div id="vk_groups" style="margin-top: 12px; width: 290px; height: 161px; background: none;">
        </div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 1, width: "290", height: "135", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 49569254);
        </script>
    </div>
  </aside>
  </div>



