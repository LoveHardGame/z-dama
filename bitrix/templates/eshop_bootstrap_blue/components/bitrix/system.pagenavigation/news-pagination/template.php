<?php

if (!defined('B_PROLOG_INCLUDED') || (B_PROLOG_INCLUDED !== true)) {
    die();
}

if (!$arResult["NavShowAlways"]) {
    if (
       (0 == $arResult["NavRecordCount"])
       ||
       ((1 == $arResult["NavPageCount"]) && (false == $arResult["NavShowAll"]))
    ) {
        return;
    }
}

$navQueryString      = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$navQueryStringFull  = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

?>

<div class="page_top">
    <div class="clear"></div>
    <div class="pagination">
        <?php while ($arResult["nStartPage"] <= $arResult["nEndPage"]) { ?>

            <?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) { ?>
                
                <a href="<?php echo $arResult["sUrlPath"] ?><?php echo $navQueryStringFull ?>" class="active" ><?php echo $arResult["nStartPage"] ?></a>
            <?php } elseif ((1 == $arResult["nStartPage"]) && (false == $arResult["bSavePage"])) { ?>
                <a href="<?php echo $arResult["sUrlPath"] ?><?php echo $navQueryStringFull ?>"><?php echo $arResult["nStartPage"] ?></a>
            <?php } else { ?>
                <a href="<?php echo $arResult["sUrlPath"] ?>?<?php echo $navQueryString ?>PAGEN_<?php echo $arResult["NavNum"] ?>=<?php echo $arResult["nStartPage"] ?>"><?php echo $arResult["nStartPage"] ?></a>
            <?php } ?>
            <?php $arResult["nStartPage"]++ ?>

        <?php } ?>
<?php

if($this->NavPageNomer < $this->NavPageCount)
  echo ('<a class="nav right" href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.
  ($this->NavPageNomer+1).$strNavQueryString.'#nav_start'.$add_anchor.'"><i style="margin-top: 8px;" class="sprites-arr-right"></i></a>');

?>
    </div>
</div>