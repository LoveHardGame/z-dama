<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<? //print_r($arResult["arUser"]); die(); ?>

<div class="relative">
	<aside id="aside" style="position: static; margin-left: 0px; top: 0px;">
		<div class="form form3">
			<h2>Управление рассылкой</h2>
			<div class="el">
				<div class="control">
					<div class="spam_control_block">
						<p class="spam_control">Новости сайта</p>
						<label class="spam_control"><input type="checkbox" name="agreeNewsEmail" checked=""> получать по e-mail</label>
					</div>
					<div class="spam_control_block">
						<p class="spam_control">Обновления каталога</p>
						<label class="spam_control"><input type="checkbox" name="agreeCatalogEmail" checked=""> получать по e-mail</label>
					</div>
					<div class="spam_control_block">
						<p class="spam_control">Информация о заказах</p>
						<label class="spam_control"><input type="checkbox" name="agreeStatusSMS" checked=""> получать по SMS</label>
						<label class="spam_control"><input type="checkbox" name="agreeStatusEmail" checked=""> получать по e-mail</label>
					</div>
				</div>
			</div>
			<div class="el el-nomargin">
				<div class="control">
					<button class="inp2 inp3" type="submit">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</aside>
	<div id="content" style="margin-left: 0px;">
		
		<div class="form form3">
			<?=$arResult["BX_SESSION_CHECK"]?>
			<input type="hidden" name="lang" value="<?=LANG?>" />
			<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
			<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
			<h2>Личная информация</h2>
				<div class="el el-required el-error">
					<label class="inp3">Электронная почта</label>
					<div class="control"><div class="inp-70"><input class="inp3" type="text" id="email" value="<?=$arResult["arUser"]["EMAIL"]?>" name="EMAIL"></div></div>
				</div>
				<div class="el">
					<label class="inp3">Ваш пол</label>
					<div class="control chk3">
						<label><input type="radio" <?if($arResult["arUser"]["PERSONAL_GENDER"] == "W") echo 'checked'; ?> value="W" name="PERSONAL_GENDER"> женский</label>
						<label><input type="radio" <?if($arResult["arUser"]["PERSONAL_GENDER"] == "M") echo 'checked'; ?>  value="M" name="PERSONAL_GENDER"> мужской</label>
					</div>
				</div>
				<div class="el">
					<label class="inp3">ФИО</label>
					<div class="control">
						<div class="inp-30"><input class="inp3" type="text" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>" placeholder="Фамилия"></div>
						<div class="inp-30"><input class="inp3" type="text" name="NAME" value="<?=$arResult["arUser"]["NAME"]?>" placeholder="Имя"></div>
						<div class="inp-30"><input class="inp3" type="text" name="SECOND_NAME" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" placeholder="Отчество"></div>
					</div>
				</div>
				<div class="el el-required el-error">
					<label class="inp3">Мобильный телефон</label>
					<div class="control"><div class="inp-30"><input class="inp3" type="text" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" name="PERSONAL_PHONE" id="tel" placeholder="(123) 123-45-67"></div></div>
				</div>
				<div class="el">
					<label class="inp3">Дата рождения</label>
					<div class="control">
						<div class="inp-30"><input class="inp3" type="text" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" name="PERSONAL_BIRTHDAY" id="birthday_date"></div>
					</div>
				</div>
				<div class="el el-nomargin">
					<label class="inp3">&nbsp;</label>
					<div class="control">
						<input name="save" class="inp2 inp3" value="Сохранить изменения" type="submit">
					</div>
				</div>
		</div>
		<div class="form form3">
			<h2>Изменение пароля</h2>
				<div class="el el-required el-error">
					<label class="inp3">Новый пароль</label>
					<div class="control"><div class="inp-70"><input class="inp3" type="password" name="NEW_PASSWORD" id="pass"></div></div>
				</div>
				<div class="el el-required el-error">
					<label class="inp3">Повтор пароля</label>
					<div class="control"><div class="inp-70"><input class="inp3" type="password" name="NEW_PASSWORD_CONFIRM" id="rep_pass"></div></div>
				</div>
				<div class="el el-nomargin">
					<label class="inp3">&nbsp;</label>
					<div class="control">
						<input name="save" class="inp2 inp3" value="Сохранить изменения" type="submit">
					</div>
				</div>
		</div>
		
	</div>
</div>



