<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?><?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	"basket",
	Array(
		"ACTION_VARIABLE" => "basketAction",
		"AUTO_CALCULATION" => "Y",
		"COLUMNS_LIST" => array("NAME","DISCOUNT","WEIGHT","PROPS","DELETE","DELAY","TYPE","PRICE","QUANTITY","SUM","PROPERTY_TITLE","PROPERTY_KEYWORDS","PROPERTY_META_DESCRIPTION","PROPERTY_BRAND_REF","PROPERTY_NEWPRODUCT","PROPERTY_SALELEADER","PROPERTY_SPECIALOFFER","PROPERTY_SALE","PROPERTY_ARTNUMBER","PROPERTY_MANUFACTURER","PROPERTY_MATERIAL","PROPERTY_OUTPUT","PROPERTY_COLOR","PROPERTY_MORE_PHOTO","PROPERTY_RECOMMEND","PROPERTY_FORUM_TOPIC_ID","PROPERTY_FORUM_MESSAGE_CNT","PROPERTY_BLOG_POST_ID","PROPERTY_BLOG_COMMENTS_CNT","PROPERTY_BACKGROUND_IMAGE","PROPERTY_SIZE","PROPERTY_OPT","PROPERTY_TYPE","PROPERTY_RAZMERNAYA_SETKA","PROPERTY_OPLATA_TOVARA","PROPERTY_DOSTAVKA_TOVARA","PROPERTY_VOZVRAT_TOVARA","PROPERTY_MINIMUM_PRICE","PROPERTY_MAXIMUM_PRICE","PROPERTY_COLOR_REF","PROPERTY_SIZES_SHOES","PROPERTY_SIZES_CLOTHES"),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "Y",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "Y",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "N",
		"OFFERS_PROPS" => array("ARTNUMBER","COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
		"PATH_TO_ORDER" => "/personal/order.php",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N"
	)
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>