<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("compleat");

?><div class="container cart">
	<div class="text">
		<h2>Ваша корзина</h2>
	</div>
	<div id="site-content">
		<form id="cartlist" name="cartlist" method="post" action="">
 <span id="current_order_type" style="display: none;">1</span> <span id="current_payer_id" style="display: none;">0</span> <span id="current_receiver_id" style="display: none;">0</span> <span id="wholesale_threshold" style="display: none;">15000</span>
			<div class="ProductsWrapper">
				<h1>Оформление заказа</h1>
				<p>
					Уважаемая <?=$_GET['name']?>!<br>
					<br>
					<b>Ваш заказ № <?=$_GET['o_id']?> на сумму <?=$_GET['sum']?> рублей успешно оформлен и поступил на обработку. <br>
					Ваш менеджер Нагорных Елена свяжется с Вами в ближайшее время для уточнения Вашего заказа.<br>
					Оформить дозаказы Вы можете до <?=date('Y-m-d H:i:s', strtotime('+3 day', time()))?>. <br>
					 Посмотреть состояние заказа Вы можете в личном кабинете.</b>
				</p>
			</div>
		</form>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>