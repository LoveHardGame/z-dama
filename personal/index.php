<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кабинет пользователя");
?>



<div id="site-content">
    <form method="post" name="userinfo" id="userinfo" enctype="multipart/form-data" action="/personal/index.php">
        <div class="container cabinet">
            <div class="text">
                <h1>Кабинет пользователя</h1>
            </div>
            <div class="tab-button">
                <a href="/personal" class="active">Личная информация</a>
                <a href="/personal/order">Мои заказы</a>
                <?php

                global $USER;
                // проверяем пользователя на группу 'менеджер' 
                $arGroups = CUser::GetUserGroup($USER->GetID());

                if (in_array('1', $arGroups) or in_array('8', $arGroups)): 
                ?>
                <a href="/personal/manager">Линый кабинет менеджера</a>
                <? endif; ?>
                <!-- Кажется нам это не нужно и не знаю как реализовать -->
                <!-- <a href="http://www.z-dama.ru/payers/user1/">Получатели</a> -->
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:main.profile", "user_profile", Array(
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                ),
                false
            );?>			
        </div>
        <input type="hidden" name="doaction" value="adduserinfo">
        <input type="hidden" name="id" value="1">
        <input type="hidden" name="site_allow_hash" value="9f4ed1d98185022b861d43a84dedab42">
    </form>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>