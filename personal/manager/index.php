<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новый раздел");
?>
<!--подключаем https://www.datatables.net/ -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

<div class="container cabinet">
<br/>
<br/>

    <?php
    
        $filter = [ 
            "GROUPS_ID" => Array(5) // ID gruppi 
        ];

        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter); 
    ?>
    <table id="example" class="display table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Контактный номер</th>
                <th>Mail</th>
            </tr>
        </thead>
        <tbody>
        <?php  while($arItem = $rsUsers->GetNext()): ?>
            <tr>
                <td><?= $arItem['NAME']; ?></td>
                <td><?= $arItem['LAST_NAME']; ?></td>
                <td><?= $arItem['PERSONAL_PHONE']; ?></td>
                <td><?= $arItem['EMAIL']; ?></td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
</div>

<script>
// инициализация  dataTable
  $(function(){
    $("#example").dataTable();
  })
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>