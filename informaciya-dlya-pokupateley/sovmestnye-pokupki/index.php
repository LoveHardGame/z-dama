<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
  "bitrix:menu", 
  "info_right", 
  array(
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "info2level",
    "DELAY" => "N",
    "MAX_LEVEL" => "2",
    "MENU_CACHE_GET_VARS" => array(
    ),
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "left",
    "USE_EXT" => "N",
    "COMPONENT_TEMPLATE" => "info_right",
    "MENU_THEME" => "site"
  ),
  false
);?>
				
				 
				 	
				 				
				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h2><span style="line-height: 1.5;">Совместные покупки женской одежды больших размеров</span></h2>
<p><span style="line-height: 1.5;">Покупать совместно – это выгодно!</span></p>
<p><span style="line-height: 1.5;"><strong>Мы активно сотрудничаем с организаторами "совместных покупок" из РФ, Белоруссии и Казахстана!</strong></span></p>

<p><span style="line-height: 1.5;">Например, вы собираете заказ на 12&nbsp;000 рублей, ваша выгода может составить до -40% (4000 рублей). Зарабатывайте вместе с нами помогая друзьям и знакомым!</span></p>

<p><a href="/company/news/139-news.html" style="color: #951E17;">Внимание, акция! При заказе от 30&nbsp;000 рублей бесплатная доставка и дополнительные скидки</a></p>


<table border="0" cellspacing="10" cellpadding="10">
<tbody>
<tr>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/7.png" alt="" align="left"></td>
<td width="261">
<p><a href="#"><strong>12&nbsp;000 рублей</strong></a><br>Минимальная сумма заказа по оптовым ценам.</p>
</td>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/8.png" alt="" align="left"></td>
<td>
<p><strong><a href="/feedback/">85 центров выдачи товаров в РФ</a></strong><br>Отправим в любой регион России.</p>
</td>
</tr>
<tr>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/9.png" alt="" align="left"></td>
<td>
<p><a href="#"><strong>7 дней – резерв товара</strong></a><br>Мы фиксируем вашу цену!</p>
</td>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/1.png" alt="" align="left"></td>
<td>
<p><a href="/informaciya-dlya-pokupateley/roznichnym-pokupatelyam/sposoby-dostavki/"><strong>Бесплатная доставка</strong></a><br>При заказе от 30&nbsp;000 рублей, доставка за наш счет!</p>
</td>
</tr>
<tr>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/6.png" alt="" align="left"></td>
<td>
<p><a href="#"><strong>Собственное производство</strong></a><br>Уникальный дизайн, белорусское качество.</p>
</td>
<td><img class="icon_opt" src="<?=SITE_TEMPLATE_PATH?>/images/icons/2.png" alt="" align="left"></td>
<td>
<p><a href="#"><strong>Любые формы оплаты</strong></a><br>Картой на сайте, безналичный платеж,<!-- наложенный платеж,--> <a href="/informaciya-dlya-pokupateley/pokupka-v-kredit/" style="color: #951e17;">кредит</a></p>
</td>
</tr>
</tbody>
</table>

<p style="line-height: 1.5;">Пополните совместный бюджет, оказав услугу друзьям и знакомым! Для начала нужно всего лишь зарегистрироваться.</p>


<!--
<div style="text-align: center;"><a class="inp2 inp3 green low_height" href="/register/">Зарегистрироваться</a></div>
<div style="text-align: center;"><a href="http://www.z-dama.ru/catalog/discount/15/" target="_blank"  style="color: #951e17; font-weight: bold;">Распродажа</a></div>-->

<form id="sp_data_request" class="form form3">
<div id="sppanel">
	<div class="el">
		<label class="inp3" style="width:160px;">Ф.И.О</label>
		<div class="control">
			<div style="width: 100%;" class="inp-70">
				<input style="float: left; width: 260px; margin-right: 10px;" name="fio" class="inp3 fio" type="text">
			</div>
		</div>
	</div>	
	<div class="el">
		<label class="inp3" style="width:160px;">Номер телефона без 8</label>
		<div class="control">
			<div style="width: 100%;" class="inp-70">
				<input style="float: left; width: 260px; margin-right: 10px;" name="phone_number" class="phone_input phone_number inp3" placeholder="Введите номер мобильного телефона" type="text">
			</div>
		</div>
	</div>	
	<div class="el">		
		<label class="inp3" style="width:160px;">Email</label>
		<div class="control">
			<div style="width: 100%;" class="inp-70">
				<input style="float: left; width: 260px; margin-right: 10px;" onblur="var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(regex.test(this.value)) { try{rrApi.setEmail(this.value);}catch(e){}}" name="email" class="inp3 email" type="text">
			</div>
		</div>	
	</div>	
	<div class="el">		
		<label class="inp3" style="width:160px;">Адрес группы или форума</label>
		<div class="control">
			<div style="width: 100%;" class="inp-70">
				<input style="float: left; width: 260px; margin-right: 10px;" name="spadress" class="inp3 spadress" type="text">
			</div>
		</div>		
	</div>
	<div class="el">
	<button style="width: 120px; height: 35px; line-height: 35px;" class="inp2 inp3 green low_height" onclick="GetSpData(); return false;" type="submit">Отправить</button>
	</div>
	</div>
	<p class="message"></p>
</form>


<div></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Если у Вас остались вопросы мы с удовольствием на них ответим!</p>
<p><a id="callback" href="#callbackwidget" style="color: #951e17;"><strong>Закажите обратный звонок,</strong></a> и наш менеджер свяжется с Вами в течении 60 секунд!</p></div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>