<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>


    <style>
        #map {
            width: 800px;
            height: 500px;
            padding: 0;
            margin: 0 auto;
        }
    </style>
    <!-- @link  yandex map api  https://tech.yandex.ru/maps/jsbox/2.1/object_manager -->
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>


<? // список областей
$APPLICATION->IncludeComponent("bitrix:sale.ajax.locations", "region", Array(
    "CITY_OUT_LOCATION" => "Y",    // Возвращать ID местоположения (в противном случае - города)
    "ALLOW_EMPTY_CITY" => "N",    // Выбор города местоположения не обязателен
    "COUNTRY_INPUT_NAME" => "",    // Имя поля формы для страны
    "REGION_INPUT_NAME" => "REGION",    // Имя поля формы для региона
    "CITY_INPUT_NAME" => "",    // Имя поля формы для города (местоположения)
    "COUNTRY" => "",    // Стартовое значение страны
    "ONCITYCHANGE" => "",    // Обработчик смены значения города (местоположения)
    "NAME" => "",    // Имя поля ввода местоположения
),
    false
);
?>

    <!-- yandex map -->
    <div id="map"></div>
    <!-- end yandex map -->


    <script type="text/javascript">
        ymaps.ready(init);
        var map;

        function init() {
            var getResult = '<?= $_GET['region ']; ?>';
            if (getResult) {
                var myGeocoder = ymaps.geocode('<?= $_GET['region']; ?>');
                myGeocoder.then(
                    function (res) {

                        var geoStartPage = res.geoObjects.get(0).geometry.getCoordinates();
                        map = new ymaps.Map("map", {
                            center: [geoStartPage[0], geoStartPage[1]],
                            zoom: 3,
                            avoidFractionalZoom: false,
                            maxAnimationZoomDifference: 100
                        });
                    }
                )
            } else {

                map = new ymaps.Map("map", {
                    center: [65, 100.64],
                    zoom: 3,
                    avoidFractionalZoom: false,
                    maxAnimationZoomDifference: 100
                });

                map.controls.remove('searchControl');
                map.controls.remove('trafficControl');
                map.controls.remove('typeSelector');
            }


            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32
            });

            var geo;
            var myGeoObjects = [];

            <?php

            /* @link https://dev.1c-bitrix.ru/api_help/sale/classes/csaleorderpropsvalue/csaleorderpropsvalue__getlist.52da0d54.php */

            $db_vals = CSaleOrderPropsValue::GetList(
                array("SORT" => "ASC"),
                array('NAME' => 'Местоположение'),
                ['VALUE_ORIG'],
                false,
                array("*")
            );
            $i = 0;
            while($arVals = $db_vals->Fetch()):


            /* @link https://dev.1c-bitrix.ru/api_help/sale/classes/csalelocation/csalelocation__getlist.a60c2ce1.php */
            $db_vars = CSaleLocation::GetList(
                array(
                    "SORT" => "ASC",
                ),
                array("CODE" => $arVals['VALUE_ORIG']),
                false,
                false,
                array("CITY_NAME")
            );
            while ($vars = $db_vars->Fetch()): ?>

            var myGeocoder = ymaps.geocode("<?= $vars['CITY_NAME']; ?>");

            myGeocoder.then(
                function (res) {

                    if (geo === undefined) {
                        geo = "";
                    }

                    var geo = res.geoObjects.get(0).geometry.getCoordinates();

                    myGeoObjects.push(new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: [geo[0], geo[1]]
                        },
                        properties: {
                            clusterCaption: '<?= $arVals['CNT']; ?>',
                            balloonContentBody: '<b>г. <?= $vars['CITY_NAME ']; ?></b>.<br/>' + 'Клиентов: <?= $arVals[' CNT ']; ?>.<br/>'
                        }
                    }));

                    <?php
                    if (!empty($_GET['region'])) {
                        echo 'map.geoObjects.add(myGeoObjects[' . $i . '])';
                    } else {
                        echo '
												clusterer = new ymaps.Clusterer({clusterDisableClickZoom: false});
												clusterer.add(myGeoObjects);
												map.geoObjects.add(clusterer)';
                    }
                    ?>
                });

            <? $i++; ?>
            <?php endwhile;  ?>
            <?php endwhile; ?>

        }
    </script>

    <!-- end yandex map -->


    <!-- ************************************************************* -->
    <script>
        $(document).ready(function () {
            // Выбор региона
            $('#map_region_select').change(function () {
                var RegionCode = $("#map_region_select option:selected").attr("value");
                if (RegionCode != "") {
                    window.location.replace(window.location.pathname + '?region=' + RegionCode);
                } else {
                    window.location.replace(window.location.pathname);
                }

            });
        });
    </script>

    <!-- ************************************************************* -->

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>