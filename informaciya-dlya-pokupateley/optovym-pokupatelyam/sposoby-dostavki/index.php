<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Способы доставки");
?>

<div class="container page-frame">
			<div class="relative">
				
				 
				
				<?$APPLICATION->IncludeComponent(
				  "bitrix:menu", 
				  "info_right", 
				  array(
				    "ALLOW_MULTI_SELECT" => "N",
				    "CHILD_MENU_TYPE" => "info2level",
				    "DELAY" => "N",
				    "MAX_LEVEL" => "2",
				    "MENU_CACHE_GET_VARS" => array(
				    ),
				    "MENU_CACHE_TIME" => "3600",
				    "MENU_CACHE_TYPE" => "N",
				    "MENU_CACHE_USE_GROUPS" => "Y",
				    "ROOT_MENU_TYPE" => "left",
				    "USE_EXT" => "N",
				    "COMPONENT_TEMPLATE" => "info_right",
				    "MENU_THEME" => "site"
				  ),
				  false
				);?>			
				 	
				 				
				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h2>Способы доставки оптовых заказов</h2>
<p>Доставка осуществляется по территории РФ, а также в Белоруссию и Казахстан.</p>
<ul>
<li>бесплатная доставка в центры выдачи розничных магазинов "Примадонна" без предоплаты заказа</li>
<li>Доставка транспортной компанией Энергия по всей России до 300 рублей</li>
<li>EMS Почтой</li>
<li>самовывоз товара при покупке в офисе компании в г. Курске или в центрах выдачи интернет-магазина</li>
<li>самовывоз в региональных центрах выдачи заказов</li>
<li>служба экспресс-почты России EMS</li>
<li>курьерская служба доставки СДЭК</li>
</ul>
<p><br> <strong>Стоимость и сроки доставки</strong></p>
<p>Точную стоимость доставки можно уточнить у менеджера или рассчитать на сайте&nbsp;</p>
<noindex><a href="http://www.posthouse.ru/calculate/" rel="nofollow" target="_self">курьерской службы / Почты России</a></noindex>
<p>.</p>
<p>Сроки доставки в регионы РФ варьируются от 3 до 14 дней (в зависимости от удаленности адресата от г. Курска и способа доставки) с момента окончательного формирования заказа и подтверждения его менеджером.</p>
<p><strong>Внимание!&nbsp;</strong></p>
<ul>
<li>После поступления оплаты отгрузка осуществляется в течение 1-5 дней;</li>
<li>Номер трека или товарно-транспортной накладной посылки будет отправлен Вам в смс-оповещении и на e-mail в течение 2-х дней после отправки;&nbsp;</li>
<li>Ответственность за сохранность заказа во время пересылки несет транспортная компания, служба доставки "Экспресс-курьер" или служба Экспресс-почты России EMS. При получении заказа обязательно проверяйте целостность упаковки;</li>
<li>Согласно Закону "О защите прав потребителей" и "Правилам дистанционной продажи" при отказе покупателя от получения товара, он обязан оплатить продавцу стоимость доставки.</li>
</ul>
<p id="dtype"><strong>Мы сотрудничаем со следующими компаниями перевозчиками / службами доставки:</strong></p>
<ul>
<li><noindex><a href="http://www.jde.ru/" rel="nofollow" target="_blank">ЖелДорЭкспедиция</a></noindex></li>
<li><noindex><a href="http://www.pecom.ru/ru/" rel="nofollow" target="_blank">Первая экспедиционная компания</a></noindex></li>
<li><noindex><a href="http://www.baikalsr.ru/" rel="nofollow" target="_blank">Байкал-Сервис</a></noindex></li>
<li><noindex><a href="http://www.dellin.ru/" rel="nofollow" target="_blank">Деловые линии</a></noindex></li>
<li><noindex><a href="https://www.pochta.ru/" rel="nofollow" target="_blank">Почта России</a></noindex></li>
<li><noindex><a href="http://www.emspost.ru/" rel="nofollow" target="_blank">EMS Почта</a></noindex></li>
<li><noindex><a href="http://www.edostavka.ru/" rel="nofollow" target="_blank">СДЭК</a></noindex></li>
<li><noindex><a href="http://nrg-tk.ru/" rel="nofollow" target="_blank">Транспортная компания "Энергия"</a></noindex></li>
<li><noindex><a href="http://www.cse.ru/" rel="nofollow" target="_blank">КурьерСервисЭкспресс</a></noindex></li>
</ul>
</div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>