<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Размерная сетка");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
				  "bitrix:menu", 
				  "info_right", 
				  array(
				    "ALLOW_MULTI_SELECT" => "N",
				    "CHILD_MENU_TYPE" => "info2level",
				    "DELAY" => "N",
				    "MAX_LEVEL" => "2",
				    "MENU_CACHE_GET_VARS" => array(
				    ),
				    "MENU_CACHE_TIME" => "3600",
				    "MENU_CACHE_TYPE" => "N",
				    "MENU_CACHE_USE_GROUPS" => "Y",
				    "ROOT_MENU_TYPE" => "left",
				    "USE_EXT" => "N",
				    "COMPONENT_TEMPLATE" => "info_right",
				    "MENU_THEME" => "site"
				  ),
				  false
				);?>
				
				 
				 	
				 				
				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h2>Размерная сетка</h2>
<p><strong>Уважаемые клиенты!</strong></p>
<p>Мы предлагаем Вам ассортимент высококачественной женской одежды, выпускаемый на собственном швейном производстве.</p>
<p><strong>Размеры, указанные в каталоге, являются российскими (обхват груди/обхват бедер/рост, 96/106/174)</strong></p>
<p><strong style="line-height: 1.5;">Пример (размер 54, рост 174):</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/1426079439_razmer.jpg" alt="" align="left"></p>
<p>&nbsp;</p>
<p><strong>Как измерить свои размеры для выбора одежды?</strong></p>
<p>Если Вы сомневаетесь в своем размере или Ваша фигур изменилась, то рекомендуем Вам снова снять мерки.</p>
<p>Производите замеры в нижнем белье. Не напрягайтесь. Прижимайте измерительную ленту вплотную к телу. Попросите кого-нибудь помочь Вам, так Вы добьетесь большей точности. Все размеры указаны в сантиметрах.</p>
<p><strong>1.</strong> Обхват груди: измеряется горизонтально по наиболее выступающим точкам груди.</p>
<p><strong>2.</strong> Обхват бедер: измеряется горизонтально по наиболее выступающим точкам бедер и ягодиц, при этом поставьте ноги вместе, измеряйте бёдра в большей их части, примерно на 15-20 см ниже талии.</p>
<p><strong>Размерная сетка:</strong></p>
<table class="tabletab" style="width: 99%; border: 1px dashed #000000;" cellspacing="1" cellpadding="4"><colgroup><col width="38*"> <col width="18*"> <col width="18*"> <col width="20*"> <col width="22*"> <col width="21*"> <col width="19*"> <col width="19*"> <col width="18*"> <col width="16*"> <col width="17*"> <col width="16*"> <col width="13*"> </colgroup>
<tbody>
<tr valign="center">
<td style="border: 1px dashed #000000;" width="15%"><span style="font-size: small; padding-left: 5px;"><strong>Размер</strong></span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">42</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">44</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">46</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">48</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">50</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">52</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">54</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">56</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">58</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">60</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">62</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">64</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">66</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">68</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">70</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">72</span></td>
</tr>
<tr valign="center">
<td style="border: 1px dashed #000000;" width="15%"><span class="table_caption" style="font-size: small; padding-left: 5px;"><strong>Обхват груди</strong></span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">84</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">88</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">92</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">96</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">100</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">104</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">108</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">112</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">116</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">120</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">124</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">128</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">132</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">136</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">138</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">140</span></td>
</tr>
<tr valign="center">
<td style="border: 1px dashed #000000;" width="15%"><span class="table_caption" style="font-size: small; padding-left: 5px;"><strong>Обхват бедер</strong></span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">92</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">96</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">100</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">104</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">108</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">112</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">116</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">120</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">124</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">128</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">132</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">136</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">140</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">144</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">146</span></td>
<td style="text-align: center; border: 1px dashed #000000;" width="5%"><span style="font-size: small;">148</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>