<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
  "bitrix:menu", 
  "info_right", 
  array(
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "info2level",
    "DELAY" => "N",
    "MAX_LEVEL" => "2",
    "MENU_CACHE_GET_VARS" => array(
    ),
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "left",
    "USE_EXT" => "N",
    "COMPONENT_TEMPLATE" => "info_right",
    "MENU_THEME" => "site"
  ),
  false
);?>
				
				 
				 	
        <article class="page text" id="content" style="margin-left: 0px;">
          
          <div id="site-content"><h1>О компании</h1>
<p><strong>Наш лозунг:</strong> качественно, стильно, комфортно, а также по доступной цене!</p>
<p>В нашем интернет магазине представлен обширный ассортимент ЖЕНСКОЙ ОДЕЖДЫ на любой вкус, силуэт и возраст девушек и женщин с эффектными формами. Мы гарантируем лучшие цены оптом и в розницу, а также качественную, комфортную и доступную женскую одежду, которая является незаменимой частью гардероба любой модницы.</p>
<p>Покупая в нашем интернет-магазине «Знатная дама»: юбки, брюки, блузы, платья и жакеты разных цветов, можно оригинально скомбинировать их между собой и получить модные и стильные комплекты в виде костюмов. Мы прислушиваемся к Вашим пожеланиям, и именно поэтому при производстве нашей продукции мы улучшает лекала, посадку моделей, тщательно подбираем ткани и расцветки в соответствии с Вашими комментариями!</p>
<h3>Качество</h3>
<p>У нас двойной контроль качества на стадии выбора ткани и в готовой продукции. Покупая у нас на сайте женскую одежду Вы гарантируете себе спокойствие и уверенность в покупке!</p>
<h3>Стиль</h3>
<p>Мы спланировали цвета и оттенки ассортимента моделей таким образом, чтобы они все могли между собой сочетаться в виде комплектов!</p>
<h3>Комфорт</h3>
<p>Комфорт – это основополагающая проблема женской одежды больших размеров, но не у нас, так как мы постоянно усовершенствуем посадку моделей. Для Вашего комфорта мы производим юбки, брюки, платья, туники до 70 размера из трикотажных и текстильных стрейчевых тканей как прилегающего, так и свободного силуэта.</p>
<h3>Доступная цена</h3>
<p>Почему? Мы максимально сбалансировали бюджет и заложили минимальную наценку, не экономя на качестве продукции. Нам очень важна лояльность наших клиентов!</p>
<h3>Наши преимущества</h3>
<ul>
<li>Весь товар в наличии на наших складах.</li>
<li>Срок отгрузки товара всего 1-5 дней.</li>
<li><a href="/informaciya-dlya-pokupateley/pokupka-v-kredit/">Покупка без регистрации в рассрочку и в кредит</a>.</li>
<li><a href="/informaciya-dlya-pokupateley/roznichnym-pokupatelyam/oformlenie-zakaza/">Простая и мгновенная оплата товара!</a>.</li>
<li><a href="/informaciya-dlya-pokupateley/roznichnym-pokupatelyam/oformlenie-zakaza/">Возможность оплатить наложенным платежом</a>.</li>
<li>Бесплатный номер 8-800-333-34-24 для Вашего удобства.</li>
<li><a href="/informaciya-dlya-pokupateley/optovym-pokupatelyam/vozvrat-tovara/">Простой способ возврата товара</a>.</li>
<li>Персональный вежливый менеджер.</li>
<li><a href="/informaciya-dlya-pokupateley/roznichnym-pokupatelyam/razmernaya-setka/">Все размеры идеально соответствуют таблице размеров</a>.</li>
<li>Покупка в рассрочку и в кредит Мелкооптовый заказ от 10&nbsp;000 рублей!</li>
<li>Лучшие условия для крупного опта.</li>
<li>Индивидуальный подход к организаторам совместных покупок.</li>
<li>Бесплатная доставка до почты и транспортной компании, а также до пунктов выдачи в <a href="/feedback/">наших магазинах и офисах</a>.</li>
</ul>
<h3>Сертификаты</h3>
<ul class="img_block">
<li><a href="<?=SITE_TEMPLATE_PATH?>/images/info/sert/1.jpg" rel="lightbox:1"><img src="<?=SITE_TEMPLATE_PATH?>/images/info/sert/mini/1.jpg" alt=""></a><br><a href="<?=SITE_TEMPLATE_PATH?>/images/info/sert/1.jpg">Скачать</a></li>
<li><a href="<?=SITE_TEMPLATE_PATH?>/images/info/sert/2.jpg" rel="lightbox:1"><img src="<?=SITE_TEMPLATE_PATH?>/images/info/sert/mini/2.jpg" alt=""></a><br><a href="<?=SITE_TEMPLATE_PATH?>/images/info/sert/2.jpg">Скачать</a></li>
</ul>
<h3>Как оформить заказ?</h3>
<p>У нас это элементарно!</p>
<ol>
<li>1. Добавьте <a href="/catalog/">понравившийся товар</a> в <a href="/personal/cart/">корзину</a>.</li>
<li>2. <a href="/register/">Зарегистрируйтесь</a> или кликните «Купить без регистрации» и укажите <a href="/obrabotka-personalnoy-informacii/">Ваши контактные данные</a>, адрес доставки и оплатите заказ удобным для Вас способом.</li>
</ol>
<p>Наш интернет-магазин «Знатная дама» не разочарует Вас. Убедитесь в этом сами!</p>
</div>
        </article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>