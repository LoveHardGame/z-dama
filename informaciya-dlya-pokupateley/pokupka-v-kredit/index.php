<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
  "bitrix:menu", 
  "info_right", 
  array(
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "info2level",
    "DELAY" => "N",
    "MAX_LEVEL" => "2",
    "MENU_CACHE_GET_VARS" => array(
    ),
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "left",
    "USE_EXT" => "N",
    "COMPONENT_TEMPLATE" => "info_right",
    "MENU_THEME" => "site"
  ),
  false
);?>
				
				 
				 	
				 				
				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h2>Покупка в кредит</h2>
<table border="0" cellspacing="10" cellpadding="10">
<tbody>
<tr>
<td width="400">
<p>Оформите заказ в кредит.&nbsp;</p>
<p>Кредит оформляется онлайн в банке АО «Тинькофф Банк».</p>
</td>
<td><img src="<?=SITE_TEMPLATE_PATH?>/images/info/1458495351_kvk_logo.jpg" alt="" align="left"></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><strong>Как оформить заказ в кредит:</strong></p>
<ul>
<li>Добавьте товар в "Корзину";</li>
<li>Выберите способ оплаты "Покупка в кредит";</li>
<li>Подтвердите заказ;</li>
<li>Заполните анкету;</li>
<li>При положительном ответе от банка подпишите кредитный договор с представителем банка.&nbsp;</li>
</ul>
<p><span style="color: #951e17;"><strong>&nbsp;<a href="http://www.z-dama.ru/informaciya-dlya-pokupateley/credit/who-to-credit/"><span style="color: #951e17;">Подробное описание процесса оформления заказа в кредит.</span></a></strong></span></p>
<h2>Оформление кредита доступно:&nbsp;</h2>
<ul>
<li>Для заказов с доставкой в пределах Российской Федерации;</li>
<li>Для заказов на сумму от 3 000 до 200 000 рублей;</li>
</ul>
<p>Кредитное решение будет отправлено по SMS, на адрес электронной почты, а также показано на экране вашего монитора после заполнения заявки. После получения решения с вами свяжется сотрудник банка, чтобы уточнить ваши данные, подтвердить заказ, договориться о дате и времени подписания кредитного договора.</p>
<p>Кредитный договор будет доставлен представителем банка. В момент доставки внимательно ознакомьтесь и подпишите заявку - один экземпляр останется у вас, второй заберет представитель банка.</p>
<p><strong>Кредитный договор включает в себя:</strong></p>
<ul>
<li>Заявку;</li>
<li>Тарифный план;</li>
<li>Общие условия кредитования в АО «Тинькофф Банк».</li>
</ul>
<p>&nbsp;</p>
<h2>Условия обслуживания</h2>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Если я верну товар, кто будет платить по кредиту</span></p>
<p>В случае возврата товара мы вернем сумму, взятую в кредит, перечислив на ваш счет обслуживания кредита.</p>
<p>После возврата товара позвоните в Центр обслуживания клиентов АО «Тинькофф Банк» по телефону 8 800 555-08-08 (по России звонок бесплатный), чтобы урегулировать вопрос о погашении кредита.</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">На какой срок можно получить кредит</span></p>
<p>На срок от 3 до 36 месяцев. Более подробную информацию об условиях кредитования вы можете узнать в Центре обслуживания клиентов АО «Тинькофф Банк»: 8 800 555-08-08 (по России звонок бесплатный).</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Как узнать размер ежемесячного платежа по кредиту</span></p>
<p>Размер ежемесячного платежа указан в вашем экземпляре Договора. Также его можно посмотреть в письме, которое направлено вам на электронную почту. Ознакомиться с доступными способами погашения кредита можно&nbsp;</p>
<noindex><a style="margin: 0px; padding: 0px; color: #256aa3; text-decoration: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); box-sizing: border-box; background: 0px 0px;" href="https://www.kupivkredit.ru/payment" rel="nofollow" target="_blank">здесь</a></noindex>
<p>.</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Когда необходимо осуществлять платеж</span></p>
<p>Платежи по кредиту осуществляются ежемесячно равными частями согласно графику платежей по кредиту. Ознакомиться с доступными способами погашения кредита можно&nbsp;</p>
<noindex><a style="margin: 0px; padding: 0px; color: #256aa3; text-decoration: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); box-sizing: border-box; background: 0px 0px;" href="https://www.kupivkredit.ru/payment" rel="nofollow" target="_blank">здесь</a></noindex>
<p>.</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Как погашать кредит</span></p>
<p>Погашать кредит необходимо ежемесячно регулярными платежами. Размер регулярного платежа указан в вашем экземпляре договора. Ознакомиться с доступными способами погашения кредита можно&nbsp;</p>
<noindex><a style="margin: 0px; padding: 0px; color: #256aa3; text-decoration: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); box-sizing: border-box; background: 0px 0px;" href="https://www.kupivkredit.ru/payment" rel="nofollow" target="_blank">здесь</a></noindex>
<p>.</p>
<p>По вопросу погашения кредита вы также можете обратиться в Центр обслуживания клиентов АО «Тинькофф Банк»: 8 800 555-08-08 (по России звонок бесплатный).</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Можно ли заплатить несколько платежей вперед</span></p>
<p>Да, это возможно!</p>
<p>Внесите на свой счет сумму, достаточную для совершения желаемого количества платежей. В дату очередного платежа будет происходить списание денежных средств в счет погашения кредита.</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Как узнать, поступили ли деньги на счет</span></p>
<p>После поступления денежных средств на ваш счет, вы получите SMS-уведомление о пополнении баланса. Если не получили SMS - позвоните в Центр обслуживания клиентов АО «Тинькофф Банк» по телефону: 8 800 555-08-08 (по России звонок бесплатный).</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Как погасить кредит досрочно</span></p>
<p>Для досрочного погашения кредита достаточно разместить необходимую сумму на вашем счете обслуживания кредита и сообщить о своем желании досрочного погашения, позвонив в Центр обслуживания клиентов АО «Тинькофф Банк»: 8 800 555-08-08 (по России звонок бесплатный).</p>
<p>Сумму, необходимую для полного досрочного погашения кредита, вы также можете узнать в Центре обслуживания клиентов АО «Тинькофф Банк».</p>
<p><span style="margin: 0px; padding: 0px; font-weight: bold; box-sizing: border-box;">Как я могу узнать остаток задолженности</span></p>
<p>Остаток своей задолженности вы можете узнать, позвонив в Центр обслуживания клиентов АО «Тинькофф Банк» или в выписке, которую банк ежемесячно направляет на адрес электронной почты, указанный вами при оформлении заявки на кредит. Телефон Центра обслуживания клиентов АО «Тинькофф Банк»: 8 800 555-08-08 (по России звонок бесплатный).&nbsp;</p>
</div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>