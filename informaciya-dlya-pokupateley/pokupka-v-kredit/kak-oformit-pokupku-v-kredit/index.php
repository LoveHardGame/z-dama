<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как оформить покупку в кредит");
?>

<div class="container page-frame">
			<div class="relative">
				
				<?$APPLICATION->IncludeComponent(
				  "bitrix:menu", 
				  "info_right", 
				  array(
				    "ALLOW_MULTI_SELECT" => "N",
				    "CHILD_MENU_TYPE" => "info2level",
				    "DELAY" => "N",
				    "MAX_LEVEL" => "2",
				    "MENU_CACHE_GET_VARS" => array(
				    ),
				    "MENU_CACHE_TIME" => "3600",
				    "MENU_CACHE_TYPE" => "N",
				    "MENU_CACHE_USE_GROUPS" => "Y",
				    "ROOT_MENU_TYPE" => "left",
				    "USE_EXT" => "N",
				    "COMPONENT_TEMPLATE" => "info_right",
				    "MENU_THEME" => "site"
				  ),
				  false
				);?>	
				 				
				<article class="page text" id="content" style="margin-left: 0px;">
					
					<div id="site-content"><h2>Как оформить покупку в кредит на сайте интернет-магазина "Знатная Дама"?</h2>
<h3>Покупка в кредит – нет ничего проще!</h3>
<ul>
<li>Покупайте товары в кредит прямо на нашем сайте, не выходя из дома и не приезжая в банк.</li>
<li>Получение кредита займет не более 2-х минут.</li>
<li>Доступно для всех граждан России, кроме республики Крым.</li>
</ul>
<h3>Как это работает</h3>
<ul>
<li>Мы сотрудничаем с банком&nbsp;<noindex><a style="margin: 0px; padding: 0px; color: #0083ca; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: transparent;" href="https://www.tcsbank.ru/" rel="nofollow" target="_blank">АО «Тинькофф Банк»</a></noindex>, который разработал уникальный сервис онлайн-кредитования "<noindex><a style="margin: 0px; padding: 0px; color: #0083ca; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: transparent;" href="https://www.kupivkredit.ru/" rel="nofollow" target="_blank">КупиВкредит</a></noindex>".</li>
<li>С помощью этого сервиса вы получаете кредит в режиме реального времени в течение двух минут прямо на нашем сайте.</li>
<li>После оформления кредитной заявки вы видите решение банка прямо на экране, а также банк отправляет вам SMS и E-mail уведомление с решением.</li>
<li>Если кредит одобрен, представитель банка приедет к вам для заключения кредитного договора.</li>
<li>Доставка заказа или самовывоз осуществляется сразу же после заключения кредитного договора.</li>
</ul>
<h3>Условия кредитования</h3>
<ul>
<li>Кредит доступен гражданам России в возрасте от 18 до 70 лет.</li>
<li>Сумма кредитования – от 3 000 до 200 000 рублей.</li>
<li>Сроки кредитования - от 3 до 24 месяцев.</li>
<li>Первоначальный взнос - 0%.</li>
<li>Процентная ставка рассчитывается индивидуально.</li>
<li>Льготный период кредитования – до даты первого платежа по кредиту.</li>
<li>Бесплатное погашение кредита по всей России (в более чем 300 тысяч партнерских точек приема платежей).</li>
</ul>
<h3>Основные преимущества</h3>
<ul>
<li>Для оформления покупки и кредита вам не нужно выходить из дома.</li>
<li>Решение по онлайн-кредиту принимается за 2 минуты.</li>
<li>Комфортный график погашения – вы сами подбираете удобный ежемесячный платеж и срок кредита.</li>
<li>Погашение без комиссии&nbsp;<noindex><a style="margin: 0px; padding: 0px; color: #0083ca; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: transparent;" href="https://www.kupivkredit.ru/payment" rel="nofollow" target="_blank">здесь</a></noindex>.</li>
<li>Бесплатная доставка кредитного договора по вашему адресу.</li>
</ul>
<h3>Получение заказа</h3>
<ul>
<li>Банк АО «Тинькофф Банк» работает на всей территории России.</li>
<li>Отгрузка товара происходит сразу же после подписания кредитного договора.</li>
<li>Срок доставки заказа зависит от условий работы транспортной компании.</li>
</ul>
<h3>Процесс покупки в кредит</h3>
<ul>
<li>Добавьте один или несколько товаров в корзину.</li>
<li>Оформите заказ и выберете способ оплаты «Оформить кредит online»</li>
<li>После этого выберете дату доставки и подтвердите заказ.</li>
<li>После этого у вас появится онлайн-анкета банка.</li>
</ul>
<table style="width: 99%;" border="0">
<tbody>
<tr>
<td>
<p><strong>На первом этапе заполнения это выглядит примерно так:</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/credit/1458557785_1.jpg" alt="" align="left"></p>
</td>
</tr>
<tr>
<td>
<p><strong>После заполнения анкеты на экране появится таймер. Подожите около 2-х минут и Вы увидите решение банка.</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/credit/1458557711_2.jpg" alt="" align="left"></p>
</td>
</tr>
<tr>
<td>
<p><strong>В случае положительного решения Вы увидите примерно такую картину:</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/credit/1458557786_3.jpg" alt="" align="left"></p>
</td>
</tr>
<tr>
<td>
<p><strong>Если банк отказал в кредите, то это будет выглядеть примерно так:</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/credit/1458557803_4.jpg" alt="" align="left"></p>
</td>
</tr>
<tr>
<td>
<p><strong>Если в течение 2-х минут банк не принял решение, то Ваша заявка останется в процессе рассмотрения и на экране будет такое сообщение:</strong></p>
<p><img src="<?=SITE_TEMPLATE_PATH?>/images/info/credit/1458557731_5.jpg" alt="" align="left"></p>
</td>
</tr>
</tbody>
</table>
<p><a style="line-height: 1.5;" href="/informaciya-dlya-pokupateley/pokupka-v-kredit/">Подробная информация о покупке в кредит</a>.</p>
</div>
				</article>
				
				<article class="page">
				<div data-retailrocket-markup-block="571a37345a658825ccd6cc9d" initialized="true"><style type="text/css">
.rr-widget {
  position: relative;
  margin-bottom: 20px;
}   
.rr-widget .retailrocket-widgettitle {
  margin-bottom: 30px;
    padding-top: 7px;

}
.rr-widget .retailrocket-widgettitle div {
  color: #951E17;
  background: #fff;
  font-size: 24px;
  line-height: 24px;
  font-family: arial;
  font-weight: normal;
  text-align: center;



 
}
.rr-widget .retailrocket-items {
  overflow: hidden;
}    
.rr-widget .retailrocket-item {
  list-style-type: none;
  text-align: left;
}
.rr-widget .retailrocket-item:hover {
  
}
.rr-widget .retailrocket-item-image {
  position: relative;
  overflow: hidden;
  width: 194px;
  height: 293px;
  border: 1px solid #e2e2e2;
}   
.rr-widget .retailrocket-item-image img {
  margin-top: -3px;
}   
.rr-widget .retailrocket-item-title {
  
}
.rr-widget .retailrocket-item-old-price {
  text-decoration: line-through;
  font-size: 12px;
  color: #1f8c52;
}
.rr-widget .retailrocket-item-price {
  font-size: 19px;
  color: #000;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
  float: right;
}
.rr-widget .retailrocket-item-price-currency:after {
  content: ' р';
}
.rr-widget .retailrocket-item-opt-price {
  font-size: 19px;
  color: #1f8c52;
  overflow: hidden;
  line-height: 1;
  display: inline-block;
}
.rr-widget .retailrocket-item-opt-price-currency:after {
  content: ' р';
}
.rr-widget .rr-after-price {
  display: block;
  color: #a3abab;
  font-size: 11px;
  line-height: 1;
}
.rr-widget .retailrocket-actions {
  display: none;
}
.rr-widget .bx-wrapper {
  float: none;
  margin: 0 auto;
  position: relative;
}
.rr-widget .bx-viewport {
  overflow: hidden;
}
.rr-widget .bx-prev {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  left: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/prev.png');
  font-size: 0;
}  
.rr-widget .bx-next {
  display: block;
  position: absolute;
  top: 50%;
  z-index: 5;
  margin-top: -33px;
  width: 13px;
  height: 20px;
  right: -35px;
  cursor: pointer;
  transition: all ease .3s;
  background: url('http://rrstatic.retailrocket.net/z-dama/imgs/next.png');
  font-size: 0;
} 
</style>

<div class="rr rr-widget" data-algorithm="personal" data-algorithm-argument="0" data-template-param-header-text="Это может вам понравиться" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="297" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<div class="rr rr-widget" data-algorithm="popular" data-algorithm-argument="0" data-template-param-header-text="Хиты продаж" data-template-param-number-of-items="8" data-template-param-item-image-width="194" data-template-param-item-image-height="295" data-on-pre-render="preRender(data, renderFn)" data-on-post-render="postRenderFn(this)" data-template-container-id="widget-template" data-textoverflowhidden="true" data-retailrocket-morebutton="false" data-retailrocket-buybutton="false" data-widget-applied="true" data-number-of-rendered-items="0" data-rendered-items-ids=""></div>

<script id="widget-template" type="text/html">
<header class="retailrocket-widgettitle">
    <div><%=(headerText || "Вместе с этим товаром покупают")%></div>
</header>
<ul class="retailrocket-items">
    <% for (var i = 0 ; i < numberOfItems; ++i) with(items[i]) { %>
        <li class="retailrocket-item">
            <a class="retailrocket-item-info" href="<%=Url%>" onmousedown='retailrocket.widget.click(<%=ItemId%>,"<%=suggesterId%>","<%=algorithm%>") '>
                <div class="retailrocket-item-image"> <img src="//cdn.retailrocket.ru/api/1.0/partner/<%=partnerId%>/item/<%=ItemId%>/picture/?format=png&width=<%=itemImageWidth%>&height=<%=itemImageHeight%>&scale=both" style="width:<%=itemImageWidth%>px;height:<%=itemImageHeight%>px"></div>
                <div class="retailrocket-item-title">
                    <span><%=Name %></span>
                </div>
            </a>
            <% if (OldPrice) { %>
                <div class="retailrocket-item-old-price"> <span class="retailrocket-item-old-price-value"><%= OldPrice %></span> <span class="retailrocket-item-price-currency"></span> </div>
            <% } %>
            <div class="retailrocket-item-opt-price"> <span class="retailrocket-item-opt-price-value"><%= Params["Оптовая цена"] %></span> <span class="retailrocket-item-opt-price-currency"></span> <span class="rr-after-price">оптовая</span></div>
            <div class="retailrocket-item-price"> <span class="retailrocket-item-price-value"><%= Price %></span> <span class="retailrocket-item-price-currency"></span> <span class="rr-after-price">розничная</span></div>
            <nav class="retailrocket-actions">
                <a class="retailrocket-actions-more" href="<%=Url%>"></a>
                <a class="retailrocket-actions-buy" href="<%=Url%>" onclick='return retailrocket._widgetAddToBasket("<%=ItemId%>", "<%=onAddToBasket%>")'></a>
            </nav>
        </li>
    <% } %>
</ul>
</script>

<script>
function preRender(data, renderFn) {
		data = data || [];
		for(var i = 0; i < data.length; i++) {
          
        	data[i].Price = retailrocket.widget.formatNumber(data[i].Price, '.', ' ', 0);
          	if(data[i].OldPrice) {
            	data[i].OldPrice = retailrocket.widget.formatNumber(data[i].OldPrice, '.', ' ', 0);
            }    
          	
		}

		renderFn(data);
}

var $carousel = $carousel || [];
function postRenderFn(OBJ) {
	if(window.jQuery){
		$.getScript('http://cdn.retailrocket.ru/content/jquery.bxslider.4.1.2/jquery.bxslider.min.js',function(){
			$carousel[$(OBJ).parent().data('retailrocket-markup-block')] = $(OBJ).find('.retailrocket-items').bxSlider({
				shrinkItems:  true,
				auto: false,		//Автозапуск
				pause: 4000,		//Длительность ожидания след слайда
				pager: false,		//Навигация
				infiniteLoop: true,	//Бесконечная прокрутка
				speed: 500,			//Скорость прокрутки
				slideWidth: 194,	//Ширина
				minSlides: 2,
				maxSlides: 4,
				moveSlides: 1,
				slideMargin: 39,
				adaptiveHeight: true,
				onSliderLoad: function(){	//Функция после загрузки слайдера
					console.log('slider ready');
				}
			});
		});
	}
}

retailrocket.widget.render("rr-widget");
</script></div>
				</article>
			</div>
		</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>